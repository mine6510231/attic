# Default 3rd party include routine, override variables to redirect search path if you already included 
# some or all of them in the parent project. If you want to change libraries name please also search and update
# all cmake files in the project referring to them

# 3rd party lib used by image module

# ===================================================================
# OpenEXR with its own depencies

set(ZLIB_DIR ${CMAKE_CURRENT_LIST_DIR}/zlib/zlib-1.2.13)
set(ZLIB_LIBRARY ${ZLIB_DIR}/install/Release/zlibstatic.lib)
set(ZLIB_INCLUDE_DIR ${ZLIB_DIR})
add_library(ZLIB STATIC IMPORTED GLOBAL)
set_target_properties(ZLIB PROPERTIES IMPORTED_LOCATION "${ZLIB_LIBRARY}")

set(OpenEXR_INSTALL_DIR ${CMAKE_CURRENT_LIST_DIR}/openexr/openexr-3.1.9/install)

# Use debug build instead because release built dlls cannot 
# work in debug built source program.
if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    set(OpenEXR_INSTALL_DIR ${CMAKE_CURRENT_LIST_DIR}/openexr/openexr-3.1.9/install_debug)
endif()

set(Imath_DIR ${OpenEXR_INSTALL_DIR}/lib/cmake/Imath)
find_package(Imath CONFIG REQUIRED PATHS ${Imath_PATH} NO_DEFAULT_PATH)

set(OpenEXR_DIR ${OpenEXR_INSTALL_DIR}/lib/cmake/OpenEXR)
find_package(OpenEXR CONFIG REQUIRED PATHS ${OpenEXR_DIR} NO_DEFAULT_PATH)

# ===================================================================
# Stb library (Mainly for image utilities)

add_library(Stb INTERFACE)
set (Stb_DIR ${CMAKE_CURRENT_LIST_DIR}/stb)

target_include_directories(Stb INTERFACE ${Stb_DIR})

# App utils

# ===================================================================
# Spdlog for logging functionalities
add_library(SPDLOG STATIC IMPORTED GLOBAL)
set(SPDLOG_DIR ${CMAKE_CURRENT_LIST_DIR}/spdlog/spdlog-1.12.0)
set(SPDLOG_LIBRARY ${SPDLOG_DIR}/build/Release/spdlog.lib)
target_include_directories(SPDLOG INTERFACE ${SPDLOG_DIR}/include)
set_target_properties(SPDLOG PROPERTIES IMPORTED_LOCATION "${SPDLOG_LIBRARY}")

# ===================================================================
# Argparse for CLI option parsing
add_library(Argparse INTERFACE)
set(Argparse_DIR ${CMAKE_CURRENT_LIST_DIR}/argparse)
target_include_directories(Argparse INTERFACE ${Argparse_DIR}/include)
