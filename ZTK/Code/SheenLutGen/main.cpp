//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <SheenLut.h>

#include <ImageWriter.h>
#include <IPixelFormat.h>

#include <filesystem>
#include <sstream>
#include <iostream>

using namespace ZTK;
using namespace Image;

using namespace std;
using namespace filesystem;

namespace
{
	static constexpr uint32_t DefaultLUTSize = 256;
	static constexpr IPixelFormat DefaultLutFormat = IPixelFormat::R32F;
}

int main(int argc, char* argv[])
{
	SNDFType type = SNDFType::Charlie;
	uint32_t size = DefaultLUTSize;
	IPixelFormat format = DefaultLutFormat;

	IImagePtr lut = GenerateSheenBRDFLUT(type, size, format);

	path lutPath = path{ "SheenBRDF.dds" };

	std::string errors;
	bool result = WriteImageToFile(lut, lutPath, &errors);

	if (!result)
	{
		if (!errors.empty())
		{
			cout << errors << endl;
		}
		return -1;
	}
	
	cout << "LUT image wrote to file [" << lutPath << "]" << endl;

	return 0;
}
