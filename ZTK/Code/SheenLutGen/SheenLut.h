//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

#include <Image.h>

#include <cstdint>

namespace ZTK
{
	//! Estevez, A. C. and Kulla, C. 2017, "Production Friendly Microfacet Sheen BRDF"
	namespace CharlieNDF
	{
		Image::IImagePtr GenerateSheenBRDFLUT(uint32_t size, Image::IPixelFormat format);
	}

	

} // namespace ZTK
