set(FILES
    Charlie.cpp
    LTC.cpp
    SheenLut.cpp
    SheenLut.h
    main.cpp
)

# Check dependencies
if(NOT TARGET ZTKImage)
    message(SEND_ERROR "[ZTKSheenLutGen] ZTKImage module is required but not loaded")
    return()
endif()

if (ZTK_SheenLutGen)
    add_executable(ZTKSheenLutGen ${FILES})
    target_include_directories(ZTKSheenLutGen PRIVATE ${CMAKE_CURRENT_LIST_DIR})

    target_link_libraries(ZTKSheenLutGen
        PRIVATE ZTKImage
        PRIVATE ZTKAppUtils
    )
endif()