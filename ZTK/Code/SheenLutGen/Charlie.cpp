//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include "SheenLut.h"

#include <IPixelOp.h>

#include <cmath>
#include <random>
#include <iostream>
#include <thread>
#include <chrono>
#include <future>

#ifndef M_PI
#define M_PI     3.14159265358979323846 /* pi */
#endif

#ifndef M_EPSILON
#define M_EPSILON 1e-5f
#endif

namespace ZTK
{
	//! Implementation based on Kulla, C. and Estevez, A. C. "Production Friendly Microfacet Sheen BRDF" 2017.
	//! Further simplified for split sum IBL, the original model is designed for offline renderer.
	namespace CharlieNDF
	{
		using namespace Image;

		double DrawUniformSample() {
			static std::default_random_engine rng;
			std::uniform_real_distribution<double> dist(0.0, 1.0);
			return dist(rng);
		}

		inline float RadicalInverse2(uint32_t bits)
		{
			// Van der Corput radical inverse in base 2

			// Reverse bits
			bits = (bits << 16u) | (bits >> 16u);
			bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
			bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
			bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
			bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);

			return float(bits) * 2.3283064365386963e-10f;  // float(bits) * 2^-32
		}

		inline void HammersleySequence(uint32_t sampleIndex, uint32_t sampleCount, float* vXi)
		{
			vXi[0] = float(sampleIndex) / float(sampleCount);
			vXi[1] = RadicalInverse2(sampleIndex);
		}

		inline void UniformSampleHemisphere(double u0, double u1, double out[3])
		{
			out[2] = u0;
			double r = std::sqrt(1.0 - out[2] * out[2]);

			double phi = 2.0 * M_PI * u1;
			out[0] = r * std::cos(phi);
			out[1] = r * std::sin(phi);
		}

		inline double Dot(double a[3], double b[3])
		{
			return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
		};

		inline double Saturate(double i)
		{
			return std::clamp(i, 0.0, 1.0);
		};

		//! Charlie NDF (normal distribution function) as D term
		inline double CharlieD(double NdH, double a)
		{
			double ra = 1.0 / a;
			return ((2.0 + ra) * std::pow(std::sqrt(1.0 - NdH * NdH), ra)) / (2.0 * M_PI);
		}

		//! Lambda function of shadowing term G
		//! Input r = roughness = a, just use this symbol to align with original equation
		inline double Lambda(double NdV, double r)
		{
			//! Curve fitted coefficients for roughness = 0: [a, b, c, d, e]
			static const float CurveFittedA0Coeff[] = { 25.3245, 3.32435, 0.16801, -1.27393, -4.85967 };

			//! Coefficients for roughness = 1
			static const float CurveFittedA1Coeff[] = { 21.5473, 3.82987, 0.19823, -1.97760, -4.32054 };

			auto L = [r](double x)->double
				{
					double p = (1.0 - r) * (1.0 - r);

					//! a = (1 - r) ^ 2 * a0 + (1 - (1 - r) ^ 2) * a1 same for the rest
					double a = std::lerp(CurveFittedA1Coeff[0], CurveFittedA0Coeff[0], p);
					double b = std::lerp(CurveFittedA1Coeff[1], CurveFittedA0Coeff[1], p);
					double c = std::lerp(CurveFittedA1Coeff[2], CurveFittedA0Coeff[2], p);
					double d = std::lerp(CurveFittedA1Coeff[3], CurveFittedA0Coeff[3], p);
					double e = std::lerp(CurveFittedA1Coeff[4], CurveFittedA0Coeff[4], p);

					return a / (1.0 + b * std::pow(x, c)) + d * x + e;
				};

			if (NdV < 0.5)
			{
				return std::exp(L(NdV));
			}
			else
			{
				return std::exp(2.0 * L(0.5) - L(1.0 - NdV));
			}
		}

		inline double CharlieG(double NdV, double NdL, double a)
		{
			return 1.0 / (1.0 + Lambda(NdV, a) + Lambda(NdL, a));
		}

		double ConvolveBRDF(double roughness, double NdV)
		{
			double V[3]{ std::sqrt(1.0 - NdV * NdV), 0.0, NdV };
			double N[3]{ 0.0, 0.0, 1.0 };

			double a = roughness * roughness;

			static const uint32_t NumSamples = 1024;
			double sum = 0.0;
			for (uint32_t i = 0; i < NumSamples; ++i)
			{
				float u[2];
				HammersleySequence(i, NumSamples, u);

				double H[3];
				UniformSampleHemisphere(u[0], u[1], H);

				double VdH = Dot(V, H);
				double L[3]
				{
					2.0 * VdH * H[0] - V[0],
					2.0 * VdH * H[1] - V[1],
					2.0 * VdH * H[2] - V[2]
				};

				double NdL = Saturate(L[2]);
				double NdH = Saturate(H[2]);
				VdH = Saturate(VdH);

				//! Upper hemisphere
				if (NdL > 0.0)
				{
					double D = CharlieD(NdH, a);
					double G = CharlieG(NdV, NdL, a);

					//! Even if pdf is changed to uniform hemisphere, we are still sampling half vector, so a distribution transformation
					//! using Jacobian (1 / (4 * VdH)) is required
					//! fp = D * G * NdL (cosine term) / PDF 
					//!	   = D * G * NdL / (1 / (2 * PI * 4 * VdH)) 
					//!	   = (2 * PI) * (D * G * NdL * 4.0 * VdH)
					//! Purely EMPIRICAL model, 1.0 / (4.0 * NdV * NdL) removed to suppress energy gain cause by lack of Fresnel term,
					//! which is quite tricky for pre convolved IBL
					double fp = D * G * 4.0 * VdH;
					sum += fp;
				}
			}
			return ((2.0 * M_PI) * sum) / NumSamples;
		}
	}
} // namespace ZTK