//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <ImageWriter.h>

#ifndef STB_IMAGE_WRITE_IMPLEMENTATION
	#define STB_IMAGE_WRITE_IMPLEMENTATION
#endif

//! [3RD_PARTY] Stb library
#include <stb/stb_image_write.h>

#include <fstream>

namespace ZTK::Image
{
	namespace StbWriter
	{
		bool GetResult(int resultCode, std::string* err = nullptr)
		{
			if (resultCode == 0)
			{
				if (err) (*err) += "File writing failed\n";
				return false;
			}
			else
			{
				return true;
			}
		}

		bool WriteImageToPNG(const IImagePtr image, const std::string& path, std::string* err)
		{
			uint8_t* mem;
			uint32_t pitch;
			image->GetImagePointer(0, mem, pitch);

			int result = stbi_write_png(
				path.c_str(), image->GetWidth(0), image->GetHeight(0), GetPixelFormatInfo(image->GetPixelFormat()).m_channels, mem, pitch);

			return GetResult(result, err);
		}

		bool WriteImageToBMP(const IImagePtr image, const std::string& path, std::string* err)
		{
			uint8_t* mem;
			uint32_t pitch;
			image->GetImagePointer(0, mem, pitch);

			int result = stbi_write_bmp(
				path.c_str(), image->GetWidth(0), image->GetHeight(0), GetPixelFormatInfo(image->GetPixelFormat()).m_channels, mem);

			return GetResult(result, err);
		}

		bool WriteImageToTGA(const IImagePtr image, const std::string& path, std::string* err)
		{
			uint8_t* mem;
			uint32_t pitch;
			image->GetImagePointer(0, mem, pitch);

			int result = stbi_write_tga(
				path.c_str(), image->GetWidth(0), image->GetHeight(0), GetPixelFormatInfo(image->GetPixelFormat()).m_channels, mem);

			return GetResult(result, err);
		}

		bool WriteImageToJPG(const IImagePtr image, const std::string& path, std::string* err)
		{
			uint8_t* mem;
			uint32_t pitch;
			image->GetImagePointer(0, mem, pitch);

			int result = stbi_write_jpg(
				path.c_str(), image->GetWidth(0), image->GetHeight(0), GetPixelFormatInfo(image->GetPixelFormat()).m_channels, mem, 100);

			return GetResult(result, err);
		}

		bool WriteImageToHDR(const IImagePtr image, const std::string& path, std::string* err)
		{
			uint8_t* mem;
			uint32_t pitch;
			image->GetImagePointer(0, mem, pitch);

			IPixelFormat format = image->GetPixelFormat();
			int comp = -1;
			switch (format)
			{
			case ZTK::Image::R32G32B32A32F: comp = 4;  break;
			case ZTK::Image::R32G32F: comp = 2; break;
			case ZTK::Image::R32F: comp = 1; break;
			default:
				if (err) (*err) += "Stb HDR writer only supports single precision float format [RGBA32F | RG32F | R32F]\n";
				return false;
			}

			int result = stbi_write_hdr(
				path.c_str(), image->GetWidth(0), image->GetHeight(0), comp, (float*)mem);

			return GetResult(result, err);
		}
	}
} // namespace ZTK::Image