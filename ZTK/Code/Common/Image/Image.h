//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

#include <IPixelFormat.h>

#include <cstdint>
#include <algorithm>
#include <cassert>
#include <vector>
#include <array>
#include <string>
#include <memory>

namespace ZTK::Image
{
    namespace IFlag
    {
        //! 32bit bitmask 
        const static uint32_t Cubemap = 1u << 0;
        const static uint32_t Volumetexture = 1u << 1;
        const static uint32_t SRGB = 1u << 2; //! If gamma corrected rendering is on, this texture requires SRGB operation (it's not stored in linear)
    }

    class IImage;
    using IImagePtr = std::shared_ptr<IImage>;

    //! An intermediate abstraction of image data loaded by different image loaders
    class IImage
    {
    public:
        
        static IImagePtr Create(uint32_t width, uint32_t height, uint32_t mipCount, IPixelFormat pixelFormat);

        ~IImage();

        void Reset(uint32_t width, uint32_t height, uint32_t mipCount, IPixelFormat pixelFormat);

        IPixelFormat GetPixelFormat() const;
        uint32_t GetPixelCount(uint32_t mip) const;
        uint32_t GetWidth(uint32_t mip) const;
        uint32_t GetHeight(uint32_t mip) const;
        uint32_t GetMipCount() const;

        uint32_t GetImageFlags() const;
        void SetImageFlags(uint32_t flags);
        void AddImageFlags(uint32_t flags);
        void RemoveFlags(uint32_t flags);
        bool HasImageFlags(uint32_t flags) const;

        //! Pitch -> num of bytes per row (or scanline) of image
        void GetImagePointer(uint32_t mip, uint8_t*& pMem, uint32_t& pitch) const;
        uint32_t GetMipBufSize(uint32_t mip) const;

        uint32_t GetSizeInBytes() const;

        //! Set image name
        void SetName(const std::string& name);

        //! Set mip context with different parameter setup
        void SetMipData(uint32_t mip, uint8_t* mipBuf, uint32_t bufSize, uint32_t pitch);
        void SetMipData2(uint32_t mip, uint8_t* data, uint32_t width, uint32_t height, uint32_t bytesPerPixel);

        //! IImage data operations and calculations
        bool CompareImage(const IImagePtr& other) const;

    private:

        //! Use IImage::Create instead
        IImage(uint32_t width, uint32_t height, uint32_t mipCount, IPixelFormat pixelFormat);

        struct Mipmap final
        {
        public:

            uint32_t m_width;
            uint32_t m_height;
            uint32_t m_rowCount;  //! For compressed textures m_rowCount is usually less than m_height
            uint32_t m_pitch;     //! Bytes per row
            uint8_t* m_data;

        public:
            Mipmap()
                : m_width(0)
                , m_height(0)
                , m_rowCount(0)
                , m_pitch(0)
                , m_data(0)
            {
            }

            ~Mipmap()
            {
                delete[] m_data;
                m_data = 0;
            }

            void Allocate()
            {
                assert(m_data == 0 && "Mip data must be empty before Allocation!");
                m_data = new uint8_t[m_pitch * m_rowCount];
            }

            uint32_t GetSize() const
            {
                assert(m_pitch && "Pitch must be greater than zero!");
                return m_pitch * m_rowCount;
            }

            bool operator==(const Mipmap& other) const
            {
                if (m_width == other.m_width && m_height == other.m_height
                    && m_rowCount == other.m_rowCount && m_pitch == other.m_pitch)
                {
                    return (memcmp(m_data, other.m_data, m_pitch * m_rowCount) == 0);
                }
                return false;
            }
        };

        IPixelFormat            m_pixelFormat;
        std::vector<Mipmap*>    m_mips;                 //! Stores pointers to avoid reallocations when elements are erase()
        uint32_t                m_imageFlags;
        std::string             m_name;
    };
} // namespace ZTK::Image