//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <IPixelOp.h>

#include <algorithm>
#include <unordered_map>

namespace ZTK::Image
{
    namespace IPixelOp
    {
        //! Convertion: all data type supported by pixel channel <=> float
        float U8ToF32(uint8_t in)
        {
            return in / 255.f;
        }

        uint8_t F32ToU8(float in)
        {
            return static_cast<uint8_t>(round(std::clamp(in, 0.f, 1.f) * 255.f));
        }

        float U16ToF32(uint16_t in)
        {
            return in / 65535.f;
        }

        uint16_t F32ToU16(float in)
        {
            return static_cast<uint16_t>(round(std::clamp(in, 0.f, 1.f) * 65535.f));
        }

        //! Stucture for RGBE pixel format
        struct RGBE
        {
            static const int RGB9E5_EXPONENT_BITS = 5;
            static const int RGB9E5_MANTISSA_BITS = 9;
            static const int RGB9E5_EXP_BIAS = 15;
            static const int RGB9E5_MAX_VALID_BIASED_EXP = 31;
            static const int MAX_RGB9E5_EXP = (RGB9E5_MAX_VALID_BIASED_EXP - RGB9E5_EXP_BIAS);
            static const int RGB9E5_MANTISSA_VALUES = (1 << RGB9E5_MANTISSA_BITS);
            static const int MAX_RGB9E5_MANTISSA = (RGB9E5_MANTISSA_VALUES - 1);

            static float MAX_RGB9E5;

            unsigned int r : 9;
            unsigned int g : 9;
            unsigned int b : 9;
            unsigned int e : 5;

            static int log2(float x)
            {
                int bitfield = *((int*)(&x));
                bitfield &= ~0x80000000;

                return ((bitfield >> 23) - 127);
            }

            void GetRGBF(float& outR, float& outG, float& outB) const
            {
                int exponent = e - RGB9E5_EXP_BIAS - RGB9E5_MANTISSA_BITS;
                float scale = powf(2.0f, static_cast<float>(exponent));
                outR = r * scale;
                outG = g * scale;
                outB = b * scale;
            }

            void SetRGBF(const float& inR, const float& inG, const float& inB)
            {
                float rf = std::max(0.0f, std::min(inR, MAX_RGB9E5));
                float gf = std::max(0.0f, std::min(inG, MAX_RGB9E5));
                float bf = std::max(0.0f, std::min(inB, MAX_RGB9E5));
                float mf = std::max(rf, std::max(gf, bf));

                e = std::max(0, log2(mf) + (RGB9E5_EXP_BIAS + 1));

                int exponent = e - RGB9E5_EXP_BIAS - RGB9E5_MANTISSA_BITS;
                float scale = powf(2.0f, static_cast<float>(exponent));

                r = std::min(511, (int)floorf(rf / scale + 0.5f));
                g = std::min(511, (int)floorf(gf / scale + 0.5f));
                b = std::min(511, (int)floorf(bf / scale + 0.5f));
            }
        };

        float RGBE::MAX_RGB9E5 = (((float)MAX_RGB9E5_MANTISSA) / RGB9E5_MANTISSA_VALUES * (1 << MAX_RGB9E5_EXP));

        struct Half
        {
            explicit Half(float floatValue)
            {
                uint32_t Result;

                uint32_t intValue = ((uint32_t*)(&floatValue))[0];
                uint32_t Sign = (intValue & 0x80000000U) >> 16U;
                intValue = intValue & 0x7FFFFFFFU;

                if (intValue > 0x47FFEFFFU)
                {
                    //! The number is too large to be represented as a half.  Saturate to infinity.
                    Result = 0x7FFFU;
                }
                else
                {
                    if (intValue < 0x38800000U)
                    {
                        //! The number is too small to be represented as a normalized half.
                        //! Convert it to a denormalized value.
                        uint32_t Shift = 113U - (intValue >> 23U);
                        intValue = (0x800000U | (intValue & 0x7FFFFFU)) >> Shift;
                    }
                    else
                    {
                        //! Rebias the exponent to represent the value as a normalized half.
                        intValue += 0xC8000000U;
                    }

                    Result = ((intValue + 0x0FFFU + ((intValue >> 13U) & 1U)) >> 13U) & 0x7FFFU;
                }
                h = static_cast<uint16_t>(Result | Sign);
            }

            operator float() const
            {
                uint32_t Mantissa;
                uint32_t Exponent;
                uint32_t Result;

                Mantissa = h & 0x03FF;

                if ((h & 0x7C00) != 0)  //! The value is normalized
                {
                    Exponent = ((h >> 10) & 0x1F);
                }
                else if (Mantissa != 0) //! The value is denormalized
                {
                    //! Normalize the value in the resulting float
                    Exponent = 1;

                    do
                    {
                        Exponent--;
                        Mantissa <<= 1;
                    } while ((Mantissa & 0x0400) == 0);

                    Mantissa &= 0x03FF;
                }
                else                        //! The value is zero
                {
                    Exponent = static_cast<uint32_t>(-112);
                }

                Result = ((h & 0x8000) << 16) | //! Sign
                    ((Exponent + 112) << 23) |  //! Exponent
                    (Mantissa << 13);           //! Mantissa

                return *(float*)&Result;
            }

        private:
            uint16_t h;
        };

        float HalfToF32(Half in)
        {
            return in;
        }

        Half F32ToHalf(float in)
        {
            return Half(in);
        }

        void GetPixel(const uint8_t* buf, IPixelFormat format, float& r, float& g, float& b, float& a)
        {
            switch (format)
            {
            //! 8 bits per channel
            case R8G8B8A8:
            {
                r = U8ToF32(buf[0]); g = U8ToF32(buf[1]); b = U8ToF32(buf[2]); a = U8ToF32(buf[3]);
                break;
            }
            case R8G8B8X8:
            {
                r = U8ToF32(buf[0]); g = U8ToF32(buf[1]); b = U8ToF32(buf[2]); a = 1.0f;
                break;
            }
            case R8G8:
            {
                r = U8ToF32(buf[0]); g = U8ToF32(buf[1]); b = 0.f; a = 1.0f;
                break;
            }
            case R8:
            {
                r = U8ToF32(buf[0]); g = r; b = r; a = 1.0f;
                break;
            }
            case A8:
            {
                a = U8ToF32(buf[0]); r = a; g = a; b = a; //! Save alpha information to rgb too. useful for preview.
                break;
            }
            
            //! 16 bits per channel
            case R16G16B16A16:
            {
                const uint16_t* data = (uint16_t*)(buf);
                r = U16ToF32(data[0]); g = U16ToF32(data[1]); b = U16ToF32(data[2]); a = U16ToF32(data[3]);
                break;
            }
            case R16G16:
            {
                const uint16_t* data = (uint16_t*)(buf);
                r = U16ToF32(data[0]); g = U16ToF32(data[1]); b = 0.0f; a = 1.0f;
                break;
            }
            case R16:
            {
                const uint16_t* data = (uint16_t*)(buf);
                r = U16ToF32(data[0]); g = r; b = r; a = 1.f;
                break;
            }
            
            //! HDR
            case R9G9B9E5:
            {
                const RGBE* data = (RGBE*)(buf);
                data->GetRGBF(r, g, b);
                a = 1.0f;
                break;
            }

            //! 32 bits per channel, float
            case R32G32B32A32F:
            {
                const float* data = (float*)(buf);
                r = data[0]; g = data[1]; b = data[2]; a = data[3];
                break;
            }
            case R32G32F:
            {
                const float* data = (float*)(buf);
                r = data[0]; g = data[1]; b = 0.0f; a = 1.0f;
                break;
            }
            case R32F:
            {
                const float* data = (float*)(buf);
                r = data[0]; g = r; b = r; a = 1.0f;
                break;
            }

            //! 16 bits per channel, float
            case R16G16B16A16F:
            {
                const Half* data = (Half*)(buf);
                r = data[0]; g = data[1]; b = data[2]; a = data[3];
                break;
            }
            case R16G16F:
            {
                const Half* data = (Half*)(buf);
                r = data[0]; g = data[1]; b = 0.0f; a = 1.0f;
                break;
            }
            case R16F:
            {
                const Half* data = (Half*)(buf);
                r = data[0]; g = r; b = r; a = 1.0f;
                break;
            }

            //! Legacy formats
            case B8G8R8A8:
            {
                r = U8ToF32(buf[2]); g = U8ToF32(buf[1]); b = U8ToF32(buf[0]); a = U8ToF32(buf[3]);
                break;
            }
            case R8G8B8:
            {
                r = U8ToF32(buf[0]); g = U8ToF32(buf[1]); b = U8ToF32(buf[2]); a = 1.0f;
                break;
            }
            case B8G8R8:
            {
                r = U8ToF32(buf[2]); g = U8ToF32(buf[1]); b = U8ToF32(buf[0]); a = 1.0f;
                break;
            }

            //! Undesired
            case Unknown:
            default:
                assert("Unknown format or unimplemented");
                break;
            }
        }

        void SetPixel(uint8_t* buf, IPixelFormat format, const float& r, const float& g, const float& b, const float& a)
        {
            switch (format)
            {
            case R8G8B8A8:
            {
                buf[0] = F32ToU8(r); buf[1] = F32ToU8(g); buf[2] = F32ToU8(b); buf[3] = F32ToU8(a);
                break;
            }
            case R8G8B8X8:
            {
                buf[0] = F32ToU8(r); buf[1] = F32ToU8(g); buf[2] = F32ToU8(b); buf[3] = 0xff;
                break;
            }
            case R8G8:
            {
                buf[0] = F32ToU8(r); buf[1] = F32ToU8(g);
                break;
            }
            case R8:
            {
                buf[0] = F32ToU8(r);
                break;
            }
            case A8:
            {
                buf[0] = F32ToU8(a);
                break;
            }
            case R16G16B16A16:
            {
                uint16_t* data = (uint16_t*)(buf);
                data[0] = F32ToU16(r); data[1] = F32ToU16(g); data[2] = F32ToU16(b); data[3] = F32ToU16(a);
                break;
            }
            case R16G16:
            {
                uint16_t* data = (uint16_t*)(buf);
                data[0] = F32ToU16(r); data[1] = F32ToU16(g);
                break;
            }
            case R16:
            {
                uint16_t* data = (uint16_t*)(buf);
                data[0] = F32ToU16(r);
                break;
            }
            case R9G9B9E5:
            {
                RGBE* data = (RGBE*)(buf);
                data->SetRGBF(r, g, b);
                break;
            }
            case R32G32B32A32F:
            {
                float* data = (float*)(buf);
                data[0] = r; data[1] = g; data[2] = b; data[3] = a;
                break;
            }
            case R32G32F:
            {
                float* data = (float*)(buf);
                data[0] = r; data[1] = g;
                break;
            }
            case R32F:
            {
                float* data = (float*)(buf);
                data[0] = r;
                break;
            }
            case R16G16B16A16F:
            {
                Half* data = (Half*)(buf);
                data[0] = Half(r); data[1] = Half(g); data[2] = Half(b); data[3] = Half(a);
                break;
            }
            case R16G16F:
            {
                Half* data = (Half*)(buf);
                data[0] = Half(r); data[1] = Half(g);
                break;
            }
            case R16F:
            {
                Half* data = (Half*)(buf);
                data[0] = Half(r);
                break;
            }
            case B8G8R8A8:
            {
                buf[0] = F32ToU8(b); buf[1] = F32ToU8(g); buf[2] = F32ToU8(r); buf[3] = F32ToU8(a);
                break;
            }
            case R8G8B8:
            {
                buf[0] = F32ToU8(r); buf[1] = F32ToU8(g); buf[2] = F32ToU8(b);
                break;
            }
            case B8G8R8:
            {
                buf[0] = F32ToU8(b); buf[1] = F32ToU8(g); buf[2] = F32ToU8(r);
                break;
            }

            case Unknown:
            default:
                assert("Unknowna format or unimplemented");
                break;
            }
        }

    }
} // namespace ZTK::Image