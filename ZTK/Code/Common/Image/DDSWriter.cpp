//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <ImageWriter.h>
#include <DDSFile.h>

#include <IPixelFormat.h>

#include <fstream>
#include <vector>
#include <iostream>

namespace ZTK::Image
{
	namespace DDSWriter
	{
		namespace
		{
			//! Default cubemap faces 
			//! (please DO NOT change it unless you 100% know what you are doing)
			static constexpr uint32_t NumFaces = 6u;
		}

		uint32_t DDSFile::s_magic = FOURCC_DDS;

		DDSFile::DDSFile()
		{
			//! Default header setup
			
			//! By default, the pixel format struct is only there to point to the dx10 header by setting the fourCC to 'DX10',
			//! which contains actual format information
			m_header.ddspf.fourCC = FOURCC_DX10;
			m_header.ddspf.flags = DDS_FOURCC;

			//! Set compulsory component flags required by DDS spec
			m_header.flags = (DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT);
			m_header.caps = DDS_SURFACE_FLAGS_TEXTURE;
		}

		DDSFile::DDSFile(const IImage* const image)
			: DDSFile()
		{
			//! Get image information from top mip
			uint32_t mip = 0;

			uint32_t width = image->GetWidth(mip);
			uint32_t height = image->GetHeight(mip);
			bool cubemap = false;

			auto& info = GetPixelFormatInfo(image->GetPixelFormat());
			m_format = info.m_dxgiFormat;

			uint32_t faceCount = 1u;
			if (image->HasImageFlags(IFlag::Cubemap))
			{
				//! Note we only support 6 face full cubemap at current stage
				height /= NumFaces;
				cubemap = true;

				faceCount = NumFaces;
			}

			//! Set header
			SetSize(width, height, 1u);
			SetFormat(m_format);
			if (cubemap)
			{
				SetAsCubeMap();
			}
			SetMipLevels(image->GetMipCount());

			//! Set data
			uint32_t bufferSize = image->GetSizeInBytes();
			m_data.resize(bufferSize);

			//! DDS saves sub images in following order:
			//! face 1: mip 0, mip 1, mip2 ...
			//! face 2: mip 0, mip 1, mip2 ...
			//! ...
			//! face 6: mip 0, mip 1, mip2 ...
			//! 
			//! However our image object store cubemap in this order 
			//! (for compatibility with non cubemaps and other image types):
			//! mip 0: face 1 ... face 6
			//! mip 1: face 1 ... face 6
			//! ...
			//! mip n: face 1 ... face 6 
			//! So we need to reorder data before writing to file
			
			uint8_t* curPos = m_data.data();
			for (uint32_t face = 0; face < faceCount; ++face)
			{
				for (uint32_t mip = 0; mip < image->GetMipCount(); ++mip)
				{
					uint32_t mipBufSize = image->GetMipBufSize(mip);
					uint32_t subImageSize = mipBufSize / faceCount;

					uint8_t* mem;
					uint32_t pitch;
					image->GetImagePointer(mip, mem, pitch);

					uint32_t subImageStride = face * subImageSize;
					memcpy(curPos, mem + subImageStride, subImageSize);
					curPos += subImageSize;
				}
			}
		}

		void DDSFile::SetSize(uint32_t width, uint32_t height, uint32_t depth)
		{
			m_header.width = width;
			m_header.height = height;

			if (depth > 1)
			{
				m_header.flags |= DDS_HEADER_FLAGS_VOLUME;
				m_header.depth = depth;
			}
			else
			{
				m_header.flags &= ~DDS_HEADER_FLAGS_VOLUME;
				m_header.depth = 1u;
			}

			m_headerDx10.arraySize = 1u;

			if (width > 1u && height > 1u && depth > 1u)
			{
				m_headerDx10.resourceDimension = static_cast<uint32_t>(
					DDS_RESOURCE_DIMENSION::DDS_DIMENSION_TEXTURE3D);
			}
			else if (width > 1u && height > 1u)
			{
				m_headerDx10.resourceDimension = static_cast<uint32_t>(
					DDS_RESOURCE_DIMENSION::DDS_DIMENSION_TEXTURE2D);
			}
			else if (width > 1u)
			{
				m_headerDx10.resourceDimension = static_cast<uint32_t>(
					DDS_RESOURCE_DIMENSION::DDS_DIMENSION_TEXTURE1D);
			}
			else
			{
				m_headerDx10.resourceDimension = static_cast<uint32_t>(
					DDS_RESOURCE_DIMENSION::DDS_DIMENSION_UNKNOWN);
				assert(false && "Invalid dimension");
			}

			ResolvePitch();
		}

		void DDSFile::SetFormat(DXGI_FORMAT format)
		{
			m_format = format;
			m_headerDx10.dxgiFormat = static_cast<uint32_t>(format);

			ResolvePitch();
		}

		void DDSFile::SetAsCubeMap()
		{
			m_header.caps |= DDS_SURFACE_FLAGS_CUBEMAP;
			m_header.caps2 |= DDS_CUBEMAP_ALLFACES;
			m_headerDx10.miscFlag |= DDS_RESOURCE_MISC_FLAG::DDS_RESOURCE_MISC_TEXTURECUBE;
		}

		void DDSFile::SetMipLevels(uint32_t mipLevels)
		{
			m_header.mipMapCount = mipLevels;
			if (mipLevels > 1u)
			{
				m_header.caps |= DDS_SURFACE_FLAGS_MIPMAP;
				m_header.flags |= DDS_HEADER_FLAGS_MIPMAP;
			}
		}

		void DDSFile::ResolvePitch()
		{
			if (m_format != DXGI_FORMAT_UNKNOWN)
			{
				//! Clear out the flags first.
				m_header.flags &= ~(DDS_HEADER_FLAGS_PITCH | DDS_HEADER_FLAGS_LINEARSIZE);

				//! Get image layout
				DXGI::SubImageLayout layout = DXGI::EvaluateSubImageLayout(m_header.width, m_header.height, m_format);
				if (layout.m_compressed)
				{
					m_header.flags |= DDS_HEADER_FLAGS_LINEARSIZE;
				}
				else
				{
					m_header.flags |= DDS_HEADER_FLAGS_PITCH;
				}
				m_header.pitchOrLinearSize = layout.m_pitch;
			}
		}

		bool DDSFile::WriteToFile(std::ofstream& stream) const
		{
			//! Write header
			stream.write((char*)&s_magic, sizeof(s_magic));
			stream.write((char*)&m_header, sizeof(m_header));
			stream.write((char*)&m_headerDx10, sizeof(m_headerDx10));

			//! Write data
			stream.write((char*)m_data.data(), m_data.size());

			return true;
		}

		bool WriteImageToFile(const IImagePtr image, const std::string& path, std::string* err)
		{
			using namespace std;

			if (IsASTCFormat(image->GetPixelFormat()))
			{
				if(err) (*err) += "ASTC format is not supported by DDS file type\n";
				return false;
			}
			
			DDSFile ddsFile(image.get());

			ofstream file{ path, ios::out | ios::trunc | ios::binary };
			if (!file.is_open())
			{
				if (err)
				{
					std::stringstream s;
					s << "File path invalid [" << path << "]\n";
					(*err) += s.str();
				}
				return false;
			}

			bool success = ddsFile.WriteToFile(file);
			if (!success)
			{
				if(err) (*err) += "File writing failed\n";
			}

			return success;
		}

		bool WriteDDSDataToFile(const DDSFile* ddsFile, const std::string& path, std::string* err)
		{
			using namespace std;

			ofstream file{ path, ios::out | ios::trunc | ios::binary };
			if (!file.is_open())
			{
				if (err)
				{
					std::stringstream s;
					s << "File path invalid [" << path << "]\n";
					(*err) += s.str();
				}
				return false;
			}

			bool success = ddsFile->WriteToFile(file);
			if (!success)
			{
				if (err) (*err) += "File writing failed\n";
			}

			return success;
		}

	} // namespace DDSWriter
} // namespace ZTK::Image

