//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <IPixelFormat.h>

#include <DDSHeader.h>

#include <array>
#include <cassert>

namespace ZTK::Image
{
	const IPixelFormatInfo& GetPixelFormatInfo(IPixelFormat format)
	{
		static IPixelFormatInfo s_infos[IPixelFormat::Count] =
		{
			//! Elements of this array should be exactly 1 to 1 mapped to IPixelFormat enum in the same order

			//! Unsigned Formats
			//! Data in an unsigned format must be positive. Unsigned formats use combinations of
			//! (R)ed, (G)reen, (B)lue, (A)lpha, (L)uminance

			//! R8G8B8A8,      
			{ 32, 4,  true, "8", 1, 1, 1, 32, false, DXGI_FORMAT_R8G8B8A8_UNORM, IPixelType::Uint8, "R8G8B8A8", "32-bit RGBA pixel format with alpha, using 8 bits per channel", false, true, FOURCC_DX10 },
			//! R8G8B8X8
			{ 32, 4, false, "0", 1, 1, 1, 32, false, DXGI_FORMAT_R8G8B8A8_UNORM, IPixelType::Uint8, "R8G8B8X8", "32-bit RGB pixel format, where 8 bits are reserved for each color", false, true, FOURCC_DX10 },
			//! R8G8
			{ 16, 2, false, "0", 1, 1, 1, 16, false, DXGI_FORMAT_R8G8_UNORM, IPixelType::Uint8, "R8G8", "16-bit red/green, using 8 bits per channel", false, false, FOURCC_DX10 },
			//! R8
			{ 8, 1, false,  "0", 1, 1, 1,  8, false, DXGI_FORMAT_R8_UNORM, IPixelType::Uint8, "R8", "8-bit red only", false, false, FOURCC_DX10 },
			//! A8
			{ 8, 1,  true,  "8", 1, 1, 1,  8, false, DXGI_FORMAT_A8_UNORM, IPixelType::Uint8, "A8", "8-bit alpha only", false, true, FOURCC_DX10},
			//! R16G16B16A16
			{ 64, 4,  true, "16", 1, 1, 1, 64, false, DXGI_FORMAT_R16G16B16A16_UNORM, IPixelType::Uint16, "R16G16B16A16", "64-bit ARGB pixel format with alpha, using 16 bits per channel", false, false, FOURCC_DX10 },
			//! R16G16        
			{ 32, 2, false,  "0", 1, 1, 1, 32, false, DXGI_FORMAT_R16G16_UNORM, IPixelType::Uint16, "R16G16", "32-bit red/green, using 16 bits per channel", false, false, FOURCC_DX10 },
			//! R16           
			{ 16, 1, false,  "0", 1, 1, 1, 16, false, DXGI_FORMAT_R16_UNORM, IPixelType::Uint16, "R16", "16-bit red only", false, false, FOURCC_DX10 },

			//! Custom FourCC Formats
			//! Data in these FourCC formats is custom compressed data and only decodable by certain hardware.

			//! ASTC formats supported by ios devices with A8 processor. Also supported by Android Extension Pack. (Zircon currently supports neither of the two platforms and has no plan in the future, they are added just for completeness at present)
			//! ASTC_4x4
			{ 0, 4, true, "?", 16, 4, 4, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_4x4", "ASTC 4x4 compressed texture format", true, false, MAKEFOURCC('A', 'S', '4', '4') },
			//! ASTC_5x4 
			{ 0, 4, true, "?", 16, 5, 4, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_5x4", "ASTC 5x4 compressed texture format", true, false, MAKEFOURCC('A', 'S', '5', '4') },
			//! ASTC_5x5
			{ 0, 4, true, "?", 16, 5, 5, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_5x5", "ASTC 5x5 compressed texture format", true, false, MAKEFOURCC('A', 'S', '5', '5') },
			//! ASTC_6x5
			{ 0, 4, true, "?", 16, 6, 5, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_6x5", "ASTC 6x5 compressed texture format", true, false, MAKEFOURCC('A', 'S', '6', '5') },
			//! ASTC_6x6
			{ 0, 4, true, "?", 16, 6, 6, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_6x6", "ASTC 6x6 compressed texture format", true, false, MAKEFOURCC('A', 'S', '6', '6') },
			//! ASTC_8x5
			{ 0, 4, true, "?", 16, 8, 5, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_8x5", "ASTC 8x5 compressed texture format", true, false, MAKEFOURCC('A', 'S', '8', '5') },
			//! ASTC_8x6
			{ 0, 4, true, "?", 16, 8, 6, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_8x6", "ASTC 8x6 compressed texture format", true, false, MAKEFOURCC('A', 'S', '8', '6') },
			//! ASTC_8x8
			{ 0, 4, true, "?", 16, 8, 8, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_8x8", "ASTC 8x8 compressed texture format", true, false, MAKEFOURCC('A', 'S', '8', '8') },
			//! ASTC_10x5
			{ 0, 4, true, "?", 16, 10, 5, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_10x5", "ASTC 10x5 compressed texture format", true, false, MAKEFOURCC('A', 'S', 'A', '5') },
			//! ASTC_10x6
			{ 0, 4, true, "?", 16, 10, 6, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_10x6", "ASTC 10x6 compressed texture format", true, false, MAKEFOURCC('A', 'S', 'A', '6') },
			//! ASTC_10x8
			{ 0, 4, true, "?", 16, 10, 8, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_10x8", "ASTC 10x8 compressed texture format", true, false, MAKEFOURCC('A', 'S', 'A', '8') },
			//! ASTC_10x10
			{ 0, 4, true, "?", 16, 10, 10, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_10x10", "ASTC 10x10 compressed texture format", true, false, MAKEFOURCC('A', 'S', 'A', 'A') },
			//! ASTC_12x10
			{ 0, 4, true, "?", 16, 12, 10, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_12x10", "ASTC 12x10 compressed texture format", true, false, MAKEFOURCC('A', 'S', 'C', 'A') },
			//! ASTC_12x12
			{ 0, 4, true, "?", 16, 12, 12, 128, false, DXGI_FORMAT_UNKNOWN, IPixelType::Compressed, "ASTC_12x12", "ASTC 12x12 compressed texture format", true, false, MAKEFOURCC('A', 'S', 'C', 'C') },

			//! Standardized Compressed DXGI Formats (DX10+)
			//! Data in these compressed formats is hardware decodable on all DX10+ chips, and manageable with the DX10+ API.
			//! BC1
			{ 4, 3, false, "0", 4, 4, 4, 64, false, DXGI_FORMAT_BC1_UNORM, IPixelType::Compressed, "BC1", "BC1 compressed texture format", true, true, FOURCC_DX10 },
			//! BC1a
			{ 4, 4, true, "1", 4, 4, 4, 64, false, DXGI_FORMAT_BC1_UNORM, IPixelType::Compressed, "BC1a", "BC1a compressed texture format with transparency", true, true, FOURCC_DX10 },
			//! BC3
			{ 8, 4, true, "3of8", 4, 4, 4, 128, false, DXGI_FORMAT_BC3_UNORM, IPixelType::Compressed, "BC3", "BC3 compressed texture format", true, true, FOURCC_DX10 },
			//! BC3t
			{ 8, 4, true, "3of8", 4, 4, 4, 128, false, DXGI_FORMAT_BC3_UNORM, IPixelType::Compressed, "BC3t", "BC3t compressed texture format with transparency", true, true, FOURCC_DX10 },
			//! BC4
			{ 4, 1, false, "0", 4, 4, 4, 64, false, DXGI_FORMAT_BC4_UNORM, IPixelType::Compressed, "BC4", "BC4 compressed texture format for single channel maps. 3DCp", true, true, FOURCC_DX10 },
			//! BC4s
			{ 4, 1, false, "0", 4, 4, 4, 64, false, DXGI_FORMAT_BC4_SNORM, IPixelType::Compressed, "BC4s", "BC4 compressed texture format for signed single channel maps", true, true, FOURCC_DX10 },
			//! BC5
			{ 8, 2, false, "0", 4, 4, 4, 128, false, DXGI_FORMAT_BC5_UNORM, IPixelType::Compressed, "BC5", "BC5 compressed texture format for two channel maps or normalmaps. 3DC", true, true, FOURCC_DX10 },
			//! BC5s
			{ 8, 2, false, "0", 4, 4, 4, 128, false, DXGI_FORMAT_BC5_SNORM, IPixelType::Compressed, "BC5s", "BC5 compressed texture format for signed two channel maps or normalmaps", true, true, FOURCC_DX10 },
			//! BC6UH
			{ 8, 3, false, "0", 4, 4, 4, 128, false, DXGI_FORMAT_BC6H_UF16, IPixelType::Compressed, "BC6UH", "BC6 compressed texture format, unsigned half", true, true, FOURCC_DX10 },
			//! BC7
			{ 8, 4, true, "8", 4, 4, 4, 128, false, DXGI_FORMAT_BC7_UNORM, IPixelType::Compressed, "BC7", "BC7 compressed texture format", true, true, FOURCC_DX10 },
			//! BC7t
			{ 8, 4, true, "8", 4, 4, 4, 128, false, DXGI_FORMAT_BC7_UNORM, IPixelType::Compressed, "BC7t", "BC7t compressed texture format with transparency", true, true, FOURCC_DX10 },

			//! Float formats
			//! Data in a Float format is floating point data.
			//! R9G9B9E5
			{ 32, 3, false, "0", 1, 1, 1, 32, false, DXGI_FORMAT_R9G9B9E5_SHAREDEXP, IPixelType::Compressed, "R9G9B9E5", "32-bit RGB pixel format with shared exponent", false, true, FOURCC_DX10 },
			//! R32G32B32A32F
			{ 128, 4, true, "23", 1, 1, 1, 128, false, DXGI_FORMAT_R32G32B32A32_FLOAT, IPixelType::Float, "R32G32B32A32F", "four float channels", false, false, FOURCC_DX10 },
			//! R32G32F
			{ 64, 2, false, "0", 1, 1, 1, 64, false, DXGI_FORMAT_R32G32_FLOAT, IPixelType::Float, "R32G32F", "two float channels", false, false, FOURCC_DX10 },
			//! R32F
			{ 32, 1, false, "0", 1, 1, 1, 32, false, DXGI_FORMAT_R32_FLOAT, IPixelType::Float, "R32F", "one float channel", false, false, FOURCC_DX10 },
			//! R16G16B16A16F
			{ 64, 4, true, "10", 1, 1, 1, 64, false, DXGI_FORMAT_R16G16B16A16_FLOAT, IPixelType::Half, "R16G16B16A16F", "four half channels", false, false, FOURCC_DX10 },
			//! R16G16F
			{ 32, 2, false, "0", 1, 1, 1, 32, false, DXGI_FORMAT_R16G16_FLOAT, IPixelType::Half, "R16G16F", "two half channel", false, false, FOURCC_DX10 },
			//! R16F
			{ 16, 1, false, "0", 1, 1, 1, 16, false, DXGI_FORMAT_R16_FLOAT, IPixelType::Half, "R16F", "one half channel", false, false, FOURCC_DX10 },

			//! Legacy BGRA8
			//! B8G8R8A8
			{ 32, 4, true, "8", 1, 1, 1, 32, false, DXGI_FORMAT_B8G8R8A8_UNORM, IPixelType::Uint8, "B8G8R8A8", "32-bit BGRA pixel format with alpha, using 8 bits per channel", false, false, FOURCC_DX10 },

			//! Formats only used for loaded source image. They won't be used for output
			//! B8G8R8
			{ 24, 3, true, "0", 1, 1, 1, 24, false, DXGI_FORMAT_UNKNOWN, IPixelType::Uint8, "B8G8R8", "24-bit BGR pixel format, using 8 bits per channel", false, false, FOURCC_DX10 },
			//! R8G8B8
			{ 24, 3, true, "0", 1, 1, 1, 24, false, DXGI_FORMAT_UNKNOWN, IPixelType::Uint8, "R8G8B8", "24-bit RGB pixel format, using 8 bits per channel", false, false, FOURCC_DX10 },
			//! R32
			{ 32, 1, false, "0", 1, 1, 1, 32, false, DXGI_FORMAT_FORCE_UINT, IPixelType::Uint32, "R32", "32-bit red only", false, false, FOURCC_DX10 }
		};

		return s_infos[static_cast<size_t>(format)];
	}

	bool IsASTCFormat(IPixelFormat format)
	{
		if (format == IPixelFormat::ASTC_4x4 || format == IPixelFormat::ASTC_5x4 || format == IPixelFormat::ASTC_5x5 ||
			format == IPixelFormat::ASTC_6x5 || format == IPixelFormat::ASTC_6x6 || format == IPixelFormat::ASTC_8x5 ||
			format == IPixelFormat::ASTC_8x6 || format == IPixelFormat::ASTC_8x8 || format == IPixelFormat::ASTC_10x5 ||
			format == IPixelFormat::ASTC_10x6 || format == IPixelFormat::ASTC_10x8 || format == IPixelFormat::ASTC_10x10 ||
			format == IPixelFormat::ASTC_12x10 || format == IPixelFormat::ASTC_12x12)
		{
			return true;
		}
		return false;
	}

	bool IsPixelFormatWithoutAlpha(IPixelFormat format)
	{
		assert((format >= 0) && (format < IPixelFormat::Count) && "Invalid pixel format");
		return !GetPixelFormatInfo(format).m_hasAlpha;
	}

	bool IsPixelFormatUncompressed(IPixelFormat format)
	{
		assert((format >= 0) && (format < IPixelFormat::Count) && "Invalid pixel format");
		return !GetPixelFormatInfo(format).m_compressed;
	}

	bool IsPixelFormatSingleChannel(IPixelFormat format)
	{
		assert((format >= 0) && (format < IPixelFormat::Count) && "Invalid pixel format");
		return (GetPixelFormatInfo(format).m_channels == 1);
	}

	bool IsPixelFormatSigned(IPixelFormat format)
	{
		assert((format >= 0) && (format < IPixelFormat::Count) && "Invalid pixel format");

		//! All these formats contain signed data, the FP-formats contain scale & biased unsigned data
		return (format == IPixelFormat::BC4s || format == IPixelFormat::BC5s);
	}

	bool IsPixelFormatFloatingPoint(IPixelFormat format, bool fullPrecision)
	{
		//! All these formats contain floating point data
		if (!fullPrecision)
		{
			//! Half precision float (16 bits) formats
			return ((format == IPixelFormat::R16F || format == IPixelFormat::R16G16F ||
				format == IPixelFormat::R16G16B16A16F) || (format == IPixelFormat::BC6UH || format == IPixelFormat::R9G9B9E5));
		}
		else
		{
			//! Full precision formats
			return ((format == IPixelFormat::R32F || format == IPixelFormat::R32G32F || format == IPixelFormat::R32G32B32A32F));
		}
	}

	uint32_t EvaluateImageMaxMipCount(IPixelFormat format, uint32_t width, uint32_t height)
	{
		const auto& info = GetPixelFormatInfo(format);
		assert(info.m_name != nullptr && "Invalid pixel format info");

		uint32_t tmpWidth = width;
		uint32_t tmpHeight = height;

		bool bIgnoreBlockSize = IsASTCFormat(format);

		uint32_t mipCountW = 0;
		while ((tmpWidth >= info.m_alignment) && (bIgnoreBlockSize || (tmpWidth % info.m_blockWidth == 0)))
		{
			++mipCountW;
			tmpWidth >>= 1;
		}

		uint32_t mipCountH = 0;
		while ((tmpHeight >= info.m_alignment) && (bIgnoreBlockSize || (tmpHeight % info.m_blockHeight == 0)))
		{
			++mipCountH;
			tmpHeight >>= 1;
		}

		//! For compressed image, use minmum mip out of W and H because any size below won't be compressed properly
		//! for non-compressed image. use maximum mip count. for example the lowest two mips of 128x64 would be 2x1 and 1x1
		const uint32_t mipCount = (info.m_compressed)
			? std::min(mipCountW, mipCountH)
			: std::max(mipCountW, mipCountH);

		//! In some cases, user may call this function for image size which is qualified for this pixel format,
		//! the mipCount could be 0 for those cases. Round it to 1 if it happend.
		return std::max(1u, mipCount);
	}

	uint32_t EvaluateImageSizeInBytes(IPixelFormat format, uint32_t width, uint32_t height)
	{
		const auto& info = GetPixelFormatInfo(format);
		assert(info.m_name != nullptr && "Invalid pixel format info");

		if (!IsImageSizeValid(format, width, height))
		{
			return 0;
		}

		//! Get number of blocks (ceiling round up for block count) and multiply with bits per block. Divided by 8 to get
		//! final byte size
		return (((width + info.m_blockWidth - 1) / info.m_blockWidth) *
			((height + info.m_blockHeight - 1) / info.m_blockHeight) * info.m_bitsPerBlock) / 8 /*bit to byte*/;
	}

	bool IsImageSizeValid(IPixelFormat format, uint32_t width, uint32_t height)
	{
		const auto& info = GetPixelFormatInfo(format);
		assert(info.m_name != nullptr && "Invalid pixel format info");

		if (info.m_squarePow2 && ((width != height) || (width & (width - 1)) != 0))
		{
			//! The format requires image to be sqaure and power of 2 but input dimension doesn't satisfy
			return false;
		}

		if (width < info.m_alignment || height < info.m_alignment)
		{
			//! Smaller than minimum size allowed by the pixel format
			return false;
		}

		//! Check image size againest block size
		//! ASTC is a kind of block compression but it doesn't need the image size to be interger mutiples of block size.
		//! reference: https://www.khronos.org/registry/OpenGL/extensions/KHR/KHR_texture_compression_astc_hdr.txt
		//! "For images which are not an integer multiple of the block size, additional texels are added to the edges
		//! with maximum X and Y.These texels may be any color, as they will not be accessed."
		if (!IsASTCFormat(format))
		{
			if (width % info.m_blockWidth != 0 || height % info.m_blockHeight != 0)
			{
				return false;
			}
		}

		return true;
	}
} // namespace ZTK::Image