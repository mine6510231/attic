//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

#include <Image.h>

#include <filesystem>
#include <string>

namespace ZTK::Image
{
	//! It assumes extension string without leading dot, for example pass "bmp" instead of ".bmp"
	bool LoadExtensionValid(const std::string& ext);
	IImagePtr LoadImageFromFile(const std::filesystem::path& path, std::string* err = nullptr);

	namespace StbLoader
	{
		IImagePtr LoadImageFromFile(const std::string& path, std::string* err = nullptr);
	}

	namespace DDSLoader
	{
		IImagePtr LoadImageFromFile(const std::string& path, std::string* err = nullptr);
	}

	namespace EXRLoader
	{
		IImagePtr LoadImageFromFile(const std::string& path, std::string* err = nullptr);
	}

} // namespace ZTK::Image