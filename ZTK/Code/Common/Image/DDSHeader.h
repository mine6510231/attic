//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

//--------------------------------------------------------------------------------------
// DDS.h
//
// This header defines constants and structures that are useful when parsing
// DDS files.  DDS files were originally designed to use several structures
// and constants that are native to DirectDraw and are defined in ddraw.h,
// such as DDSURFACEDESC2 and DDSCAPS2.  This file defines similar
// (compatible) constants and structures so that one can use DDS files
// without needing to include ddraw.h.
//
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.
//
// http://go.microsoft.com/fwlink/?LinkId=248926
// http://go.microsoft.com/fwlink/?LinkId=248929
// http://go.microsoft.com/fwlink/?LinkID=615561
//--------------------------------------------------------------------------------------

#pragma once

#include <cstdint>
#include <algorithm>

//! DDS file structure definitions
//! Grabbed from the Microsoft's DirectXTex library

#ifndef MAKEFOURCC
#define MAKEFOURCC(ch0, ch1, ch2, ch3)                              \
                        ((uint32_t)(uint8_t)(ch0) | ((uint32_t)(uint8_t)(ch1) << 8) |       \
                        ((uint32_t)(uint8_t)(ch2) << 16) | ((uint32_t)(uint8_t)(ch3) << 24 ))
#endif /* defined(MAKEFOURCC) */

const static uint32_t FOURCC_DX10 = MAKEFOURCC('D', 'X', '1', '0');
const static uint32_t FOURCC_DDS = MAKEFOURCC('D', 'D', 'S', ' ');

constexpr uint32_t DDS_MAGIC = 0x20534444; // "DDS "

#define DDS_FOURCC      0x00000004  // DDPF_FOURCC
#define DDS_RGB         0x00000040  // DDPF_RGB
#define DDS_LUMINANCE   0x00020000  // DDPF_LUMINANCE
#define DDS_ALPHA       0x00000002  // DDPF_ALPHA
#define DDS_BUMPDUDV    0x00080000  // DDPF_BUMPDUDV
#define DDS_RGBA        0x00000041  // DDPF_RGB | DDPF_ALPHAPIXELS
#define DDS_LUMINANCEA  0x00020001  // DDS_LUMINANCE | DDPF_ALPHAPIXELS
#define DDS_A           0x00000001  // DDPF_ALPHAPIXELS
#define DDS_A_ONLY      0x00000002  // DDPF_ALPHA

#define DDS_FOURCC_A16B16G16R16   0x00000024  // FOURCC A16B16G16R16
#define DDS_FOURCC_V16U16         0x00000040  // FOURCC V16U16
#define DDS_FOURCC_Q16W16V16U16   0x0000006E  // FOURCC Q16W16V16U16
#define DDS_FOURCC_R16F           0x0000006F  // FOURCC R16F
#define DDS_FOURCC_G16R16F        0x00000070  // FOURCC G16R16F
#define DDS_FOURCC_A16B16G16R16F  0x00000071  // FOURCC A16B16G16R16F
#define DDS_FOURCC_R32F           0x00000072  // FOURCC R32F
#define DDS_FOURCC_G32R32F        0x00000073  // FOURCC G32R32F
#define DDS_FOURCC_A32B32G32R32F  0x00000074  // FOURCC A32B32G32R32F

#define DDSD_CAPS         0x00000001l   // default
#define DDSD_PIXELFORMAT  0x00001000l
#define DDSD_WIDTH        0x00000004l
#define DDSD_HEIGHT       0x00000002l
#define DDSD_LINEARSIZE   0x00080000l

#define DDS_HEADER_FLAGS_TEXTURE    0x00001007  // DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT
#define DDS_HEADER_FLAGS_MIPMAP     0x00020000  // DDSD_MIPMAPCOUNT
#define DDS_HEADER_FLAGS_VOLUME     0x00800000  // DDSD_DEPTH
#define DDS_HEADER_FLAGS_PITCH      0x00000008  // DDSD_PITCH
#define DDS_HEADER_FLAGS_LINEARSIZE 0x00080000  // DDSD_LINEARSIZE

#define DDS_SURFACE_FLAGS_TEXTURE 0x00001000 // DDSCAPS_TEXTURE
#define DDS_SURFACE_FLAGS_MIPMAP  0x00400008 // DDSCAPS_COMPLEX | DDSCAPS_MIPMAP
#define DDS_SURFACE_FLAGS_CUBEMAP 0x00000008 // DDSCAPS_COMPLEX

#define DDS_CUBEMAP_POSITIVEX 0x00000600 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEX
#define DDS_CUBEMAP_NEGATIVEX 0x00000a00 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEX
#define DDS_CUBEMAP_POSITIVEY 0x00001200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEY
#define DDS_CUBEMAP_NEGATIVEY 0x00002200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEY
#define DDS_CUBEMAP_POSITIVEZ 0x00004200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEZ
#define DDS_CUBEMAP_NEGATIVEZ 0x00008200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEZ

#define DDS_CUBEMAP_ALLFACES ( DDS_CUBEMAP_POSITIVEX | DDS_CUBEMAP_NEGATIVEX |\
                                       DDS_CUBEMAP_POSITIVEY | DDS_CUBEMAP_NEGATIVEY |\
                                       DDS_CUBEMAP_POSITIVEZ | DDS_CUBEMAP_NEGATIVEZ )

#define DDS_CUBEMAP 0x00000200 // DDSCAPS2_CUBEMAP

#define DDS_FLAGS_VOLUME 0x00200000 // DDSCAPS2_VOLUME

//! Subset here matches D3D10_RESOURCE_DIMENSION and D3D11_RESOURCE_DIMENSION
enum DDS_RESOURCE_DIMENSION : uint32_t
{
    DDS_DIMENSION_UNKNOWN   = 0,
    DDS_DIMENSION_BUFFER    = 1,
    DDS_DIMENSION_TEXTURE1D = 2,
    DDS_DIMENSION_TEXTURE2D = 3,
    DDS_DIMENSION_TEXTURE3D = 4
};

//! Subset here matches D3D10_RESOURCE_MISC_FLAG and D3D11_RESOURCE_MISC_FLAG
//! Only necessary included, add more if required
enum DDS_RESOURCE_MISC_FLAG : uint32_t
{
    DDS_RESOURCE_MISC_TEXTURECUBE = 0x4L,
};

enum DDS_MISC_FLAGS2 : uint32_t
{
    DDS_MISC_FLAGS2_ALPHA_MODE_MASK = 0x7L,
};

#ifndef DDS_ALPHA_MODE_DEFINED
#define DDS_ALPHA_MODE_DEFINED
enum DDS_ALPHA_MODE : uint32_t
{
    DDS_ALPHA_MODE_UNKNOWN = 0,
    DDS_ALPHA_MODE_STRAIGHT = 1,
    DDS_ALPHA_MODE_PREMULTIPLIED = 2,
    DDS_ALPHA_MODE_OPAQUE = 3,
    DDS_ALPHA_MODE_CUSTOM = 4,
};
#endif

struct DDS_PIXELFORMAT
{
    uint32_t    size                    = 32u; //! Must be set to 32, the size of the struct.
    uint32_t    flags                   = 0u;
    uint32_t    fourCC                  = 0u;
    uint32_t    RGBBitCount             = 0u;
    uint32_t    RBitMask                = 0u;
    uint32_t    GBitMask                = 0u;
    uint32_t    BBitMask                = 0u;
    uint32_t    ABitMask                = 0u;
};

struct DDS_HEADER_DXT10
{
    uint32_t        dxgiFormat          = 0u; //! Original type DXGI_FORMAT, replaced to remove platform dependency
    uint32_t        resourceDimension   = 0u;
    uint32_t        miscFlag            = 0u; //! See D3D11_RESOURCE_MISC_FLAG
    uint32_t        arraySize           = 0u;
    uint32_t        miscFlags2          = 0u;
};

struct DDS_HEADER
{
    uint32_t        size                = 124u; //! Must be set to 124, the size of the struct.
    uint32_t        flags               = 0u;
    uint32_t        height              = 0u;
    uint32_t        width               = 0u;
    uint32_t        pitchOrLinearSize   = 0u;
    uint32_t        depth               = 1u; //! Only if DDS_HEADER_FLAGS_VOLUME is set in flags
    uint32_t        mipMapCount         = 1u;
    uint32_t        reserved1[11]       = {0u};
    DDS_PIXELFORMAT ddspf;
    uint32_t        caps                = 0u;
    uint32_t        caps2               = 0u;
    uint32_t        caps3               = 0u;
    uint32_t        caps4               = 0u;
    uint32_t        reserved2           = 0u;

    inline const bool IsValid() const { return sizeof(*this) == size; }
    inline const bool IsDX10Ext() const { return ddspf.fourCC == FOURCC_DX10; }
    inline const uint32_t GetMipCount() const { return std::max(1u, (uint32_t)mipMapCount); }

    inline const size_t GetFullHeaderSize() const
    {
        if (IsDX10Ext())
        {
            return sizeof(DDS_HEADER) + sizeof(DDS_HEADER_DXT10);
        }

        return sizeof(DDS_HEADER);
    }
};

//! Standard description of file header
struct DDS_FILE_DESC
{
    uint32_t magic;
    DDS_HEADER header;

    inline const bool IsValid() const { return magic == FOURCC_DDS && header.IsValid(); }
    inline const size_t GetFullHeaderSize() const { return sizeof(magic) + header.GetFullHeaderSize(); }
};
