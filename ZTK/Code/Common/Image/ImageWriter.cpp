//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <ImageWriter.h>

namespace ZTK::Image
{
	enum class ImageWriterType : uint32_t
	{
		Unknown = 0,
		STB_PNG,
		STB_BMP,
		STB_TGA,
		STB_JPG,
		STB_HDR,
		DDS,
	};

	ImageWriterType GetImageWriterType(const std::string& ext)
	{
		if (ext.compare("png") == 0)
		{
			return ImageWriterType::STB_PNG;
		}
		else if (ext.compare("bmp") == 0)
		{
			return ImageWriterType::STB_BMP;
		}
		else if (ext.compare("tga") == 0)
		{
			return ImageWriterType::STB_TGA;
		}
		else if	(ext.compare("jpg") == 0 && ext.compare("jpeg"))
		{
			return ImageWriterType::STB_JPG;
		}
		else if (ext.compare("hdr") == 0)
		{
			return ImageWriterType::STB_HDR;
		}
		else if	(ext.compare("dds") == 0)
		{
			return ImageWriterType::DDS;
		}
		else
		{
			return ImageWriterType::Unknown;
		}
	}

	bool WriteExtensionValid(const std::string& ext)
	{
		return static_cast<bool>(GetImageWriterType(ext));
	}

	bool WriteImageToFile(const IImagePtr image, const std::filesystem::path& path, std::string* err)
	{
		std::string ext = path.extension().string();
		ext = ext.substr(1, ext.length() - 1);
		auto writerType = GetImageWriterType(ext);

		bool success = false;

		switch (writerType)
		{
		case ImageWriterType::STB_PNG:
			success = StbWriter::WriteImageToPNG(image, path.string(), err);
			break;
		case ImageWriterType::STB_BMP:
			success = StbWriter::WriteImageToBMP(image, path.string(), err);
			break;
		case ImageWriterType::STB_TGA:
			success = StbWriter::WriteImageToTGA(image, path.string(), err);
			break;
		case ImageWriterType::STB_JPG:
			success = StbWriter::WriteImageToJPG(image, path.string(), err);
			break;
		case ImageWriterType::STB_HDR:
			success = StbWriter::WriteImageToHDR(image, path.string(), err);
			break;
		case ImageWriterType::DDS:
			success = DDSWriter::WriteImageToFile(image, path.string(), err);
			break;
		case ImageWriterType::Unknown:
		default:
			if (err)
			{
				std::stringstream s;
				s << "File extension [" << ext << "] not supported\n";
				(*err) += s.str();
			}
		}

		return success;
	}
} // namespace ZTK::Image