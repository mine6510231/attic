//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <ImageLoader.h>

#include <DDSHeader.h>
#include <IPixelFormat.h>

#include <fstream>

namespace ZTK::Image
{
    namespace DDSLoader
    {
        //! Create an image object from standard dds header, the output will be 1x6 Image array in the order
        //! predefined by dds format : positive x, negative x, positive y, negative y, positive z, negative z
        //! [TODO] Add cubemap array support 
        //! [TODO] Add volume texture support
        IImagePtr CreateImageFromHeader(DDS_HEADER& header, DDS_HEADER_DXT10& exthead, std::string* err)
        {
            if ((header.caps & DDS_SURFACE_FLAGS_TEXTURE) != DDS_SURFACE_FLAGS_TEXTURE ||
                (header.flags & DDS_HEADER_FLAGS_TEXTURE) != DDS_HEADER_FLAGS_TEXTURE)
            {
                if (err) (*err) += "Invalid Header\n";
                return nullptr;
            }

            IPixelFormat format = IPixelFormat::Unknown;
            uint32_t imageFlags = 0;

            uint32_t width = header.width;
            uint32_t height = header.height;
            uint32_t mips = 1;
            if (header.flags & DDS_HEADER_FLAGS_MIPMAP)
            {
                mips = header.mipMapCount;
            }
            if ((header.caps & DDS_SURFACE_FLAGS_CUBEMAP) && (header.caps2 & DDS_CUBEMAP))
            {
                if ((header.caps2 & DDS_CUBEMAP_ALLFACES) != DDS_CUBEMAP_ALLFACES)
                {
                    if (err) (*err) += "Only cubemap with all faces supported\n";
                    return nullptr;
                }
                imageFlags |= IFlag::Cubemap;
                height *= 6;
            }

            //! Get pixel format
            if (header.ddspf.flags & DDS_FOURCC)
            {
                //! Dx10+ formats
                if (header.ddspf.fourCC == FOURCC_DX10)
                {
                    uint32_t dxgiFormat = exthead.dxgiFormat;

                    //! Remove the SRGB from dxgi format and add sRGB to image flag
                    if (dxgiFormat == DXGI_FORMAT_R8G8B8A8_UNORM_SRGB)
                    {
                        dxgiFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
                    }
                    else if (dxgiFormat == DXGI_FORMAT_BC1_UNORM_SRGB)
                    {
                        dxgiFormat = DXGI_FORMAT_BC1_UNORM;
                    }
                    else if (dxgiFormat == DXGI_FORMAT_BC2_UNORM_SRGB)
                    {
                        dxgiFormat = DXGI_FORMAT_BC2_UNORM;
                    }
                    else if (dxgiFormat == DXGI_FORMAT_BC3_UNORM_SRGB)
                    {
                        dxgiFormat = DXGI_FORMAT_BC3_UNORM;
                    }
                    else if (dxgiFormat == DXGI_FORMAT_BC7_UNORM_SRGB)
                    {
                        dxgiFormat = DXGI_FORMAT_BC7_UNORM;
                    }

                    //! Add rgb flag if the dxgiformat was changed (which means it was sRGB format) above
                    if (dxgiFormat != exthead.dxgiFormat)
                    {
                        imageFlags |= IFlag::SRGB;
                    }

                    //! Check all the pixel formats and find matching one
                    if (dxgiFormat != DXGI_FORMAT_UNKNOWN)
                    {
                        uint32_t i = 0;
                        for (; i < IPixelFormat::Count; i++)
                        {
                            const IPixelFormatInfo& info = GetPixelFormatInfo(static_cast<IPixelFormat>(i));
                            if (static_cast<uint32_t>(info.m_dxgiFormat) == dxgiFormat)
                            {
                                format = static_cast<IPixelFormat>(i);
                                break;
                            }
                        }
                        if (i == IPixelFormat::Count)
                        {
                            if (err) (*err) += "Unhandled d3d10 format\n";
                            return nullptr;
                        }
                    }
                }
                else if (header.ddspf.fourCC == MAKEFOURCC('D', 'X', 'T', '1'))
                {
                    format = IPixelFormat::BC1;
                }
                else if (header.ddspf.fourCC == MAKEFOURCC('D', 'X', 'T', '5'))
                {
                    format = IPixelFormat::BC3;
                }
                else if (header.ddspf.fourCC == MAKEFOURCC('A', 'T', 'I', '1'))
                {
                    format = IPixelFormat::BC4;
                }
                else if (header.ddspf.fourCC == MAKEFOURCC('A', 'T', 'I', '2'))
                {
                    format = IPixelFormat::BC5;
                }
                else if (header.ddspf.fourCC == DDS_FOURCC_R32F)
                {
                    format = IPixelFormat::R32F;
                }
                else if (header.ddspf.fourCC == DDS_FOURCC_G32R32F)
                {
                    format = IPixelFormat::R32G32F;
                }
                else if (header.ddspf.fourCC == DDS_FOURCC_A32B32G32R32F)
                {
                    format = IPixelFormat::R32G32B32A32F;
                }
                else if (header.ddspf.fourCC == DDS_FOURCC_R16F)
                {
                    format = IPixelFormat::R16F;
                }
                else if (header.ddspf.fourCC == DDS_FOURCC_G16R16F)
                {
                    format = IPixelFormat::R16G16F;
                }
                else if (header.ddspf.fourCC == DDS_FOURCC_A16B16G16R16F)
                {
                    format = IPixelFormat::R16G16B16A16F;
                }
                else if (header.ddspf.fourCC == DDS_FOURCC_A16B16G16R16)
                {
                    format = IPixelFormat::R16G16B16A16;
                }
            }
            else
            {
                if ((header.ddspf.flags == DDS_RGBA || header.ddspf.flags == DDS_RGB))
                {
                    if (header.ddspf.RBitMask == 0x00ff0000 || header.ddspf.GBitMask == 0x00ff0000 || header.ddspf.BBitMask == 0x00ff0000)
                    {
                        format = (header.ddspf.RGBBitCount == 32) ? IPixelFormat::B8G8R8A8 : IPixelFormat::B8G8R8;
                    }
                    else if (header.ddspf.BBitMask == 0x00ff0000 || header.ddspf.GBitMask == 0x00ff0000 || header.ddspf.RBitMask == 0x00ff0000)
                    {
                        format = (header.ddspf.RGBBitCount == 32) ? IPixelFormat::R8G8B8A8 : IPixelFormat::R8G8B8;
                    }
                }
                else if (header.ddspf.flags == DDS_LUMINANCEA && header.ddspf.RGBBitCount == 8)
                {
                    format = IPixelFormat::R8G8;
                }
                else if (header.ddspf.flags == DDS_LUMINANCE && header.ddspf.RGBBitCount == 8)
                {
                    format = IPixelFormat::A8;
                }
                else if ((header.ddspf.flags == DDS_A || header.ddspf.flags == DDS_A_ONLY || header.ddspf.flags == (DDS_A | DDS_A_ONLY)) && header.ddspf.RGBBitCount == 8)
                {
                    format = IPixelFormat::A8;
                }
            }

            if (format == IPixelFormat::Unknown)
            {
                if (err) (*err) += "Unknown dds pixel format\n";
                return nullptr;
            }

            //! Resize to block size for compressed format. This could happened to those 1x1 bc1 dds files
            const IPixelFormatInfo& info = GetPixelFormatInfo(format);
            if (width < info.m_blockWidth && height < info.m_blockHeight)
            {
                width = info.m_blockWidth;
                height = info.m_blockHeight;
            }

            IImagePtr image = IImage::Create(width, height, mips, format);

            //! Set property flag
            image->SetImageFlags(imageFlags);

            return image;
        }

        IImagePtr LoadImageFromFile(const std::string& path, std::string* err)
        {
            std::ifstream file{ path.c_str(), std::ifstream::in | std::ifstream::binary };

            if (!file)
            {
                if (err)
                {
                    std::stringstream s;
                    s << "Image loading failed [" << path.c_str() << "]\n";
                    (*err) += s.str();
                }
                return nullptr;
            }

            DDS_FILE_DESC desc;
            DDS_HEADER_DXT10 exthead;

            //! Read fourcc and standard dds header from binary stream
            file.read((char*)&desc, sizeof(desc));

            //! Validate magic
            if (desc.magic != FOURCC_DDS || !desc.IsValid())
            {
                if(err) (*err) += "Invalid DDS file, fourcc mismatch\n";
                return nullptr;
            }

            //! Try to retrieve dx10 extented header
            if (desc.header.IsDX10Ext())
            {
                file.read((char*)&exthead, sizeof(exthead));
            }

            IImagePtr image = CreateImageFromHeader(desc.header, exthead, err);
            if (image == nullptr)
            {
                if(err) (*err) += "Image creation failed\n";
                return nullptr;
            }

            uint32_t faces = 1;
            if (image->HasImageFlags(IFlag::Cubemap))
            {
                //! Set number of faces for cubemap, note we only support cubemap with all faces (6) set
                faces = 6;
            }

            for (uint32_t face = 0; face < faces; face++)
            {
                for (uint32_t mip = 0; mip < image->GetMipCount(); ++mip)
                {
                    uint32_t pitch;
                    uint8_t* buffer;
                    image->GetImagePointer(mip, buffer, pitch);
                    uint32_t faceBufferSize = image->GetMipBufSize(mip) / faces;

                    file.read((char*)(buffer + faceBufferSize * face), faceBufferSize);
                }
            }

            return image;
        }
    }
} // namespace ZTK::Image