//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <Image.h>

#include <algorithm>

namespace ZTK::Image
{
	IImagePtr IImage::Create(uint32_t width, uint32_t height, uint32_t mipCount, IPixelFormat pixelFormat)
	{
		IImage* image = new IImage(width, height, mipCount, pixelFormat);
		return std::shared_ptr<IImage>(image);
	}

	IImage::IImage(uint32_t width, uint32_t height, uint32_t mipCount, IPixelFormat pixelFormat)
		: m_pixelFormat(pixelFormat)
		, m_imageFlags(0)
	{
		Reset(width, height, mipCount, pixelFormat);
	}

	IImage::~IImage()
	{
		m_mips.clear();
		m_imageFlags = 0;
		m_pixelFormat = IPixelFormat::Unknown;
		m_name = "";
	}

	void IImage::Reset(uint32_t width, uint32_t height, uint32_t mipCount, IPixelFormat pixelFormat)
	{
		//! Clean up mipmaps
		for (auto* mip : m_mips)
		{
			if (mip != nullptr)
			{
				delete mip;
			}
		}
		m_mips.clear();

		m_pixelFormat = pixelFormat;
		m_imageFlags = 0;
		
		const IPixelFormatInfo& info = GetPixelFormatInfo(m_pixelFormat);
		mipCount = std::min(mipCount, EvaluateImageMaxMipCount(m_pixelFormat, width, height));

		m_mips.reserve(mipCount);

		for (uint32_t mip = 0; mip < mipCount; ++mip)
		{
			Mipmap* mipmap = new Mipmap;

			uint32_t localWidth = width >> mip;
			uint32_t localHeight = height >> mip;
			if (localWidth < 1)
			{
				localWidth = 1;
			}
			if (localHeight < 1)
			{
				localHeight = 1;
			}

			mipmap->m_width = localWidth;
			mipmap->m_height = localHeight;

			if (info.m_compressed)
			{
				const uint32_t blocksInRow = (mipmap->m_width + (info.m_blockWidth - 1)) / info.m_blockWidth;
				mipmap->m_pitch = (blocksInRow * info.m_bitsPerBlock) / 8;
				mipmap->m_rowCount = (localHeight + (info.m_blockHeight - 1)) / info.m_blockHeight;
			}
			else
			{
				mipmap->m_pitch = (mipmap->m_width * info.m_bitsPerBlock) / 8;
				mipmap->m_rowCount = localHeight;
			}

			mipmap->Allocate();

			m_mips.push_back(mipmap);
		}
	}

	IPixelFormat IImage::GetPixelFormat() const
	{
		return m_pixelFormat;
	}

	uint32_t IImage::GetPixelCount(uint32_t mip) const
	{
		assert(mip < m_mips.size() && m_mips[mip] && "Mip out of range");
		return m_mips[mip]->m_width * m_mips[mip]->m_height;
	}

	uint32_t IImage::GetWidth(uint32_t mip) const
	{
		assert(mip < m_mips.size() && m_mips[mip] && "Mip out of range");
		return m_mips[mip]->m_width;
	}

	uint32_t IImage::GetHeight(uint32_t mip) const
	{
		assert(mip < m_mips.size() && m_mips[mip] && "Mip out of range");
		return m_mips[mip]->m_height;
	}

	uint32_t IImage::GetMipCount() const
	{
		return m_mips.size();
	}

	uint32_t IImage::GetImageFlags() const
	{
		return m_imageFlags;
	}

	void IImage::SetImageFlags(uint32_t imageFlags)
	{
		m_imageFlags = imageFlags;
	}

	void IImage::AddImageFlags(uint32_t imageFlags)
	{
		m_imageFlags |= imageFlags;
	}

	void IImage::RemoveFlags(uint32_t imageFlags)
	{
		m_imageFlags &= ~imageFlags;
	}

	bool IImage::HasImageFlags(const uint32_t flags) const
	{
		return (m_imageFlags & flags) != 0;
	}

	void IImage::GetImagePointer(const uint32_t mip, uint8_t*& pMem, uint32_t& pitch) const
	{
		assert(mip < m_mips.size() && m_mips[mip] && "Mip out of range");
		pMem = m_mips[mip]->m_data;
		pitch = m_mips[mip]->m_pitch;
	}

	uint32_t IImage::GetMipBufSize(uint32_t mip) const
	{
		assert(mip < m_mips.size() && m_mips[mip] && "Mip out of range");
		return m_mips[mip]->m_rowCount * m_mips[mip]->m_pitch;
	}

	void IImage::SetMipData(uint32_t mip, uint8_t* mipBuf, uint32_t bufSize, uint32_t pitch)
	{
		assert(mip < m_mips.size() && "Mip out of range");
		m_mips[mip]->m_data = mipBuf;
		m_mips[mip]->m_pitch = pitch;
		m_mips[mip]->m_rowCount = bufSize / pitch;
		assert((bufSize == m_mips[mip]->m_rowCount * pitch) && "Bad pitch size");
	}

	void IImage::SetMipData2(uint32_t mip, uint8_t* data, uint32_t width, uint32_t height, uint32_t bytesPerPixel)
	{
		assert(mip < m_mips.size() && "Mip out of range");
		m_mips[mip]->m_data = data;
		m_mips[mip]->m_pitch = width * bytesPerPixel;
		m_mips[mip]->m_rowCount = height;
	}

	bool IImage::CompareImage(const IImagePtr& other) const
	{
		if (m_pixelFormat == other->m_pixelFormat
			&& m_mips.size() == other->m_mips.size()
			&& m_imageFlags == other->m_imageFlags)
		{
			for (int mip = 0; mip < m_mips.size(); mip++)
			{
				if (!(*m_mips[mip] == *other->m_mips[mip]))
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}

	uint32_t IImage::GetSizeInBytes() const
	{
		int totalSize = 0;
		for (int mip = 0; mip < m_mips.size(); mip++)
		{
			totalSize += EvaluateImageSizeInBytes(m_pixelFormat,
				m_mips[mip]->m_width, m_mips[mip]->m_height);
		}

		return totalSize;
	}

	void IImage::SetName(const std::string& name)
	{
		m_name = name;
	}

} // namespace ZTK::Image