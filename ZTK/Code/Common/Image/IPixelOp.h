//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

#include <IPixelFormat.h>

#include <memory>

namespace ZTK::Image
{
    namespace IPixelOp
    {
        //! Decoder and encoder for getting formatted pixel data from binary stream
        void GetPixel(const uint8_t* buf, IPixelFormat format, float& r, float& g, float& b, float& a);
        void SetPixel(uint8_t* buf, IPixelFormat format, const float& r, const float& g, const float& b, const float& a);
    }
} // namespace ZTK::Image