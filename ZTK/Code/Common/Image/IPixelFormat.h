//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

#include <IDXGIFormat.h>

#include <cstdint>
#include <cassert>

namespace ZTK::Image
{
    enum IPixelFormat : uint32_t
    {
        //! Unsigned formats
        R8G8B8A8 = 0,
        R8G8B8X8,
        R8G8,
        R8,
        A8,
        R16G16B16A16,
        R16G16,
        R16,

        //! Custom FourCC Formats
        //! Data in these FourCC formats is custom compressed data and only decodable by certain hardware.
        //! ASTC formats supported by ios devices with A8 processor. Also supported by Android Extension Pack. 
        //! (Neither platform supported by Zircon, preserved for completeness)
        ASTC_4x4,
        ASTC_5x4,
        ASTC_5x5,
        ASTC_6x5,
        ASTC_6x6,
        ASTC_8x5,
        ASTC_8x6,
        ASTC_8x8,
        ASTC_10x5,
        ASTC_10x6,
        ASTC_10x8,
        ASTC_10x10,
        ASTC_12x10,
        ASTC_12x12,

        //! Standardized Compressed DXGI Formats (DX10+)
        //! Data in these compressed formats is hardware decodable on all DX10 chips, and manageable with the DX10-API.
        BC1,       // RGB without alpha, 0.5 byte/px
        BC1a,      // RGB with 1 bit of alpha, 0.5 byte/px.
        BC3,       // RGBA 1 byte/px, color maps with full alpha.
        BC3t,      // BC3 with alpha weighted color
        BC4,       // One color channel, 0.5 byte/px, unsigned
        BC4s,      // BC4, signed
        BC5,       // Two color channels, 1 byte/px, unsigned. Usually use for tangent-space normal maps
        BC5s,      // BC5, signed
        BC6UH,     // RGB, floating-point. Used for HDR images. Decompress to RGB in half floating point
        BC7,       // RGB or RGBA. 1 byte/px. Three color channels (4 to 7 bits per channel) with 0 to 8 bits of alpha
        BC7t,      // BC& with alpha weighted color

        //! Float formats
        //! Data in a Float format is floating point data.
        R9G9B9E5,
        R32G32B32A32F,
        R32G32F,
        R32F,
        R16G16B16A16F,
        R16G16F,
        R16F,

        //! Legacy format. Only used to load old converted dds files.
        B8G8R8A8,   //32bits rgba format

        //! Formats only used for loaded source image. They won't be used for output
        R8G8B8,
        B8G8R8,

        R32,

        //! Remember to add corresponding PixelFormatInfo if you add new pixel format 

        Count,
        Unknown = Count
    };

    enum class IPixelType : uint32_t
    {
        Unknown,
        Uint8,
        Uint16,
        Uint32,
        Half,
        Float,
        Compressed,
    };

    struct IPixelFormatInfo
    {
        uint32_t    m_channels;       //! Channel count per pixel
        bool        m_hasAlpha;       //! Has alpha channel or not
        const char* m_alphaBits;      //! Bits of alpha channel used to show brief of the pixel format
        uint32_t    m_alignment;      //! Pixel alignment in bytes required for image using this pixel format, 
                                      //!   compressed and packed formats usually require certain alignments in dimension to work,
                                      //!   which also constrains minimum size for subresources, for example, the lowest level mip with
                                      //!   block compressed format needs to be at least 1 full block large instead of 1x1
        uint32_t    m_blockWidth;     //! Width of the block for block based compressing
        uint32_t    m_blockHeight;    //! Height of the block for block based compressing
        uint32_t    m_bitsPerBlock;   //! Bits per pixel before uncompressed
        bool        m_squarePow2;     //! Whether the pixel format requires image size be square and power of 2.
        DXGI_FORMAT m_dxgiFormat;     //! The mapping d3d10 pixel format
        IPixelType  m_pixelType;      //! The data type used to present pixel
        const char* m_name;           //! Name
        const char* m_desc;           //! A short description for the format
        bool        m_compressed;     //! If it's a compressed format
        uint32_t    m_fourCC;         //! FourCC to identify a none d3d10 format

        //! Default constructor
        IPixelFormatInfo()
            : m_channels(0)
            , m_hasAlpha(false)
            , m_alphaBits(0)
            , m_alignment(0)
            , m_blockWidth(0)
            , m_blockHeight(0)
            , m_bitsPerBlock(0)
            , m_squarePow2(false)
            , m_dxgiFormat(DXGI_FORMAT_UNKNOWN)
            , m_pixelType(IPixelType::Unknown)
            , m_name(0)
            , m_desc(0)
            , m_compressed(false)
            , m_fourCC(0)
        {
        }

        IPixelFormatInfo(
            uint32_t    bitsPerPixel,
            uint32_t    channels,
            bool        hasAlpha,
            const char* alphaBits,
            uint32_t    alignment,
            uint32_t    blockWidth,
            uint32_t    blockHeight,
            uint32_t    bitsPerBlock,
            bool        squarePow2,
            DXGI_FORMAT dxgiFormat,
            IPixelType  pixelType,
            const char* name,
            const char* desc,
            bool        compressed,
            bool        selectable,
            uint32_t    fourCC)
            : m_channels(channels)
            , m_hasAlpha(hasAlpha)
            , m_alignment(alignment)
            , m_blockWidth(blockWidth)
            , m_blockHeight(blockHeight)
            , m_bitsPerBlock(bitsPerBlock)
            , m_squarePow2(squarePow2)
            , m_dxgiFormat(dxgiFormat)
            , m_alphaBits(alphaBits)
            , m_fourCC(fourCC)
            , m_pixelType(pixelType)
            , m_name(name)
            , m_desc(desc)
            , m_compressed(compressed)
        {
            //! Validate pixel format
            //! bitsPerPixel could be 0 if it's ACTC format since the actual bits per-pixel could be 6.4, 5.12 etc.
            if (bitsPerPixel)
            {
                assert((bitsPerPixel * m_blockWidth * m_blockHeight == m_bitsPerBlock) && "Wrong block setting");
            }
            assert(m_name && "Name can't be nullptr");
            assert(m_desc && "Descriptor can't be nullptr");
            if (!compressed)
            {
                assert(blockWidth == 1 && blockHeight == 1 && "Uncompressed format should set block size > 1");
            }
        }
    };

    const IPixelFormatInfo& GetPixelFormatInfo(IPixelFormat format);

    bool IsASTCFormat(IPixelFormat format);
    bool IsPixelFormatWithoutAlpha(IPixelFormat format);
    bool IsPixelFormatUncompressed(IPixelFormat format);

    //! Functions seems only used for BC compressions.
    bool IsPixelFormatSingleChannel(IPixelFormat format);
    bool IsPixelFormatSigned(IPixelFormat format);
    bool IsPixelFormatFloatingPoint(IPixelFormat format, bool fullPrecision);

    //! Returns maximum lod levels for image which has certain pixel format, width and height.
    uint32_t EvaluateImageMaxMipCount(IPixelFormat format, uint32_t width, uint32_t height);

    //! Return image data size, doesn't take mips into account (i.e. always assume mip 0)
    uint32_t EvaluateImageSizeInBytes(IPixelFormat format, uint32_t width, uint32_t height);

    //! Check if the input image size work with the pixel format. Some compression formats have requirements with the input image size.
    bool IsImageSizeValid(IPixelFormat format, uint32_t width, uint32_t height);

} // namespace ZTK::Image
