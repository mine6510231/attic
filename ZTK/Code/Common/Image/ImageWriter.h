//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

#include <Image.h>

#include <string>
#include <filesystem>

namespace ZTK::Image
{
	//! It assumes extension string without leading dot, for example pass "bmp" instead of ".bmp"
	bool WriteExtensionValid(const std::string& ext);
	bool WriteImageToFile(const IImagePtr image, const std::filesystem::path& path, std::string* err = nullptr);

	//! Note: if the following functions individually than the input path should be validated beforehead, 
	//! which is done by the parent WriteImageToFile function by default.
	namespace StbWriter
	{
		bool WriteImageToPNG(const IImagePtr image, const std::string& path, std::string* err = nullptr);
		bool WriteImageToBMP(const IImagePtr image, const std::string& path, std::string* err = nullptr);
		bool WriteImageToTGA(const IImagePtr image, const std::string& path, std::string* err = nullptr);

		//! Stb supports custom image quality for jpg but not exposed by our writer, check it in the source.
		//! We output in best quality (100) by default and stb uses 90 if quality is not set (0) by the caller.
		bool WriteImageToJPG(const IImagePtr image, const std::string& path, std::string* err = nullptr);
		bool WriteImageToHDR(const IImagePtr image, const std::string& path, std::string* err = nullptr);
	}

	namespace DDSWriter
	{
		bool WriteImageToFile(const IImagePtr image, const std::string& path, std::string* err = nullptr);

		//! A special routine that allows to directly write to dds file bypassing Image data stucture.
		//! It could be useful for texture types supported by .dds but not covered by ZTK::Image::Image class
		//! (because most of them are purely game oriented and cannot be trivally authored or retrieved), like volume textures.
		class DDSFile;
		bool WriteDDSDataToFile(const DDSFile* ddsFile, const std::string& path, std::string* err = nullptr);
	}

} // namespace ZTK::Image