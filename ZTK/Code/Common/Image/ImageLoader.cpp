//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <ImageLoader.h>

#include <sstream>

namespace ZTK::Image
{
	enum class ImageLoaderType : uint32_t
	{
		Unknown = 0,
		STB,
		DDS,
		EXR
	};

	ImageLoaderType GetImageLoaderType(const std::string& ext)
	{
		if (ext.compare("jpg") == 0  ||
			ext.compare("jpeg") == 0 ||
			ext.compare("png") == 0)
		{
			return ImageLoaderType::STB;
		}
		else if (ext.compare("dds") == 0)
		{
			return ImageLoaderType::DDS;
		}
		else if (ext.compare("exr") == 0)
		{
			return ImageLoaderType::EXR;
		}
		else
		{
			return ImageLoaderType::Unknown;
		}
	}

	bool LoadExtensionValid(const std::string& ext)
	{
		return static_cast<bool>(GetImageLoaderType(ext));
	}

	IImagePtr LoadImageFromFile(const std::filesystem::path& path, std::string* err)
	{
		if (!std::filesystem::exists(path))
		{
			if (err)
			{
				std::stringstream s;
				s << "File [" << path.string() << "] doesn't exist\n";
				(*err) += s.str();
			}
			return nullptr;
		}

		std::string ext = path.extension().string();
		ext = ext.substr(1, ext.length() - 1);
		auto loaderType = GetImageLoaderType(ext);

		IImagePtr image = nullptr;

		switch (loaderType)
		{
		case ImageLoaderType::STB:
			image = StbLoader::LoadImageFromFile(path.string(), err);
			break;
		case ImageLoaderType::DDS:
			image = DDSLoader::LoadImageFromFile(path.string(), err);
			break;
		case ImageLoaderType::EXR:
			image = EXRLoader::LoadImageFromFile(path.string(), err);
			break;
		case ImageLoaderType::Unknown:
		default:
			if (err)
			{
				std::stringstream s;
				s << "File extension [" << ext << "] not supported\n";
				(*err) += s.str();
			}
		}

		return image;
	}
} // namespace ZTK::Image