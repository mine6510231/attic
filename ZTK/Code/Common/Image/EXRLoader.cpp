//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <ImageLoader.h>

#include <IPixelFormat.h>

//! [3RD_PARTY] OpenEXR
#include <OpenEXR/ImfArray.h>
#include <OpenEXR/ImfChannelList.h>
#include <OpenEXR/ImfHeader.h>
#include <OpenEXR/ImfInputFile.h>
#include <OpenEXR/ImfFrameBuffer.h>
#include <OpenEXR/ImfRgba.h>
#include <OpenEXR/ImfTestFile.h>
#include <OpenEXR/ImfStandardAttributes.h>

//! Define imf namespace with current library version
#define Imf OPENEXR_IMF_INTERNAL_NAMESPACE
#define ImfMath IMATH_INTERNAL_NAMESPACE

namespace ZTK::Image
{
	namespace EXRLoader
	{
		namespace
		{
			static constexpr uint32_t NumChannels = 4u;
		}

		IImagePtr LoadImageFromScanlineFile(const std::string& path, std::string* err)
		{
			try
			{
				Imf::InputFile exrFile(path.c_str());

				if (!exrFile.isComplete())
				{
					if (err) (*err) += "Image loading failed\n";
					return nullptr;
				}

				const Imf::Header& header = exrFile.header();

				//! Get Channel information for RGBA
				const Imf::Channel* channels[NumChannels];
				channels[0] = header.channels().findChannel("R");
				channels[1] = header.channels().findChannel("G");
				channels[2] = header.channels().findChannel("B");
				channels[3] = header.channels().findChannel("A");

				//! Init pixel format to invalid state
				Imf::PixelType pixelType = Imf::NUM_PIXELTYPES;
				bool hasChannels = false;
				for (uint32_t idx = 0; idx < NumChannels; ++idx)
				{
					if (channels[idx])
					{
						if (hasChannels)
						{
							//! Validate all color channels have the same type
							if (pixelType != channels[idx]->type)
							{
								if (err) (*err) += "Image channels have different data types\n";
								return nullptr;
							}
						}
						else
						{
							pixelType = channels[idx]->type;
							hasChannels = true;
						}
					}
				}

				if (!hasChannels)
				{
					if (err) (*err) += "Image doesn't contain any RGBA channel\n";
					return nullptr;
				}

				//! Find pixel format 
				IPixelFormat format = IPixelFormat::Unknown;

				//! Size in bytes
				uint32_t pixelSize = 0u;
				if (pixelType == Imf::FLOAT)
				{
					format = IPixelFormat::R32G32B32A32F;
					pixelSize = 16u; //! 4 channels, 4 bytes each 
				}
				else if (pixelType == Imf::HALF)
				{
					format = IPixelFormat::R16G16B16A16F;
					pixelSize = 8u; //! 4 channels, 2 bytes each
				}
				else
				{
					if (err) (*err) += "Pixel format not supported\n";
					return nullptr;
				}

				//! Get the image size
				int width, height;
				ImfMath::Box2i dw = header.dataWindow();
				width = dw.max.x - dw.min.x + 1;
				height = dw.max.y - dw.min.y + 1;

				//! Create image 
				//! EXR image doesn't have multiple mips so we just hardcode 1
				IImagePtr image = IImage::Create(uint32_t(width), uint32_t(height), 1, format);

				//! Set up Imf FrameBuffer for loading data
				char* data = new char[width * height * pixelSize];
				Imf::FrameBuffer fb;
				size_t xStride = pixelSize;
				size_t yStride = pixelSize * width;
				int32_t channelPixelSize = pixelSize / NumChannels;
				char* base = data;
				fb.insert("R", Imf::Slice(pixelType, base, xStride, yStride));
				fb.insert("G", Imf::Slice(pixelType, base + channelPixelSize, xStride, yStride));
				fb.insert("B", Imf::Slice(pixelType, base + channelPixelSize * 2, xStride, yStride));

				//! Insert alpha channel with default value 1
				fb.insert("A", Imf::Slice(pixelType, base + channelPixelSize * 3, xStride, yStride, 1, 1, 1.0));
				exrFile.setFrameBuffer(fb);

				//! Read the whole image (from scanline 0 to height - 1)
				exrFile.readPixels(0, height - 1);

				//! Save image data to our internal structure
				uint32_t pitch;
				uint8_t* mem;
				image->GetImagePointer(0, mem, pitch);
				memcpy(mem, base, image->GetMipBufSize(0));

				//! Clean up original buffer
				delete[] data;

				return image;
			}
			catch (...)
			{
				if (err) (*err) += "Unhandled OpenEXR exception\n";
				return nullptr;
			}
		}

		IImagePtr LoadImageFromFile(const std::string& path, std::string* err)
		{
			//! In the current implementation it supports load one flat image with one or few of rgba channels.
			//! It wont handle multi-part, deep image or some arbitrary channels. It also won't handle layers.
			//! It's often the environment map wasn't saved with "envmap" header, so we are not trying get the information.

			//! Get .exr file feature information
			bool isTiled, isDeep, isMultiPart;
			bool isExr = Imf::isOpenExrFile(path.c_str(), isTiled, isDeep, isMultiPart);

			if (!isExr)
			{
				if (err)
				{
					std::stringstream s;
					s << "Invalid OpenEXR file [" << path << "]\n";
					(*err) += s.str();
				}
				return nullptr;
			}
			
			if(isDeep || isMultiPart || isTiled)
			{
				if (isTiled)
				{
					if (err) (*err) += "Tiled exr file is detected but not supported\n";
				}
				else
				{
					if (err) (*err) += "Deep and MultiPart information is detected but not supported\n";
				}
				return nullptr;
			}

			return LoadImageFromScanlineFile(path, err);
		}

	} // namespace EXRLoader
} // namespace ZTK::Image