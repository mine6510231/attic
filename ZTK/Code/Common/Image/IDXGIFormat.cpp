//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <IDXGIFormat.h>

#include <cassert>
#include <algorithm>

namespace ZTK::Image
{
    namespace DXGI
    {
        uint32_t GetFormatSize(DXGI_FORMAT format)
        {
            switch (format)
            {
            case DXGI_FORMAT_UNKNOWN:
                return 0;

            case DXGI_FORMAT_R32G32B32A32_FLOAT:
            case DXGI_FORMAT_R32G32B32A32_UINT:
            case DXGI_FORMAT_R32G32B32A32_SINT:
                return 16;

            case DXGI_FORMAT_R32G32B32_FLOAT:
            case DXGI_FORMAT_R32G32B32_UINT:
            case DXGI_FORMAT_R32G32B32_SINT:
                return 12;

            case DXGI_FORMAT_R16G16B16A16_FLOAT:
            case DXGI_FORMAT_R16G16B16A16_UNORM:
            case DXGI_FORMAT_R16G16B16A16_UINT:
            case DXGI_FORMAT_R16G16B16A16_SNORM:
            case DXGI_FORMAT_R16G16B16A16_SINT:
            case DXGI_FORMAT_R32G32_FLOAT:
            case DXGI_FORMAT_R32G32_UINT:
            case DXGI_FORMAT_R32G32_SINT:
            case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
            case DXGI_FORMAT_Y416:
            case DXGI_FORMAT_Y210:
            case DXGI_FORMAT_Y216:
                return 8;

            case DXGI_FORMAT_R10G10B10A2_UNORM:
            case DXGI_FORMAT_R10G10B10A2_UINT:
            case DXGI_FORMAT_R11G11B10_FLOAT:
            case DXGI_FORMAT_R8G8B8A8_UNORM:
            case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
            case DXGI_FORMAT_R8G8B8A8_UINT:
            case DXGI_FORMAT_R8G8B8A8_SNORM:
            case DXGI_FORMAT_R8G8B8A8_SINT:
            case DXGI_FORMAT_R16G16_FLOAT:
            case DXGI_FORMAT_R16G16_UNORM:
            case DXGI_FORMAT_R16G16_UINT:
            case DXGI_FORMAT_R16G16_SNORM:
            case DXGI_FORMAT_R16G16_SINT:
            case DXGI_FORMAT_D32_FLOAT:
            case DXGI_FORMAT_R32_FLOAT:
            case DXGI_FORMAT_R32_UINT:
            case DXGI_FORMAT_R32_SINT:
            case DXGI_FORMAT_D24_UNORM_S8_UINT:
            case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
            case DXGI_FORMAT_R8G8_B8G8_UNORM:
            case DXGI_FORMAT_G8R8_G8B8_UNORM:
            case DXGI_FORMAT_B8G8R8A8_UNORM:
            case DXGI_FORMAT_B8G8R8X8_UNORM:
            case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
            case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
            case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
            case DXGI_FORMAT_AYUV:
            case DXGI_FORMAT_Y410:
            case DXGI_FORMAT_YUY2:
                return 4;

            case DXGI_FORMAT_P010:
            case DXGI_FORMAT_P016:
                return 24;

            case DXGI_FORMAT_R8G8_UNORM:
            case DXGI_FORMAT_R8G8_UINT:
            case DXGI_FORMAT_R8G8_SNORM:
            case DXGI_FORMAT_R8G8_SINT:
            case DXGI_FORMAT_R16_FLOAT:
            case DXGI_FORMAT_D16_UNORM:
            case DXGI_FORMAT_R16_UNORM:
            case DXGI_FORMAT_R16_UINT:
            case DXGI_FORMAT_R16_SNORM:
            case DXGI_FORMAT_R16_SINT:
            case DXGI_FORMAT_B5G6R5_UNORM:
            case DXGI_FORMAT_B5G5R5A1_UNORM:
            case DXGI_FORMAT_A8P8:
            case DXGI_FORMAT_B4G4R4A4_UNORM:
                return 2;

            case DXGI_FORMAT_NV12:
            case DXGI_FORMAT_NV11:
                return 12;

            case DXGI_FORMAT_R8_UNORM:
            case DXGI_FORMAT_R8_UINT:
            case DXGI_FORMAT_R8_SNORM:
            case DXGI_FORMAT_R8_SINT:
            case DXGI_FORMAT_A8_UNORM:
            case DXGI_FORMAT_AI44:
            case DXGI_FORMAT_IA44:
            case DXGI_FORMAT_P8:
            case DXGI_FORMAT_R1_UNORM:
                return 1;

            case DXGI_FORMAT_BC1_UNORM:
            case DXGI_FORMAT_BC1_UNORM_SRGB:
            case DXGI_FORMAT_BC4_UNORM:
            case DXGI_FORMAT_BC4_SNORM:
                return 8;

            case DXGI_FORMAT_BC2_UNORM:
            case DXGI_FORMAT_BC2_UNORM_SRGB:
            case DXGI_FORMAT_BC3_UNORM:
            case DXGI_FORMAT_BC3_UNORM_SRGB:
            case DXGI_FORMAT_BC5_UNORM:
            case DXGI_FORMAT_BC5_SNORM:
            case DXGI_FORMAT_BC6H_UF16:
            case DXGI_FORMAT_BC6H_SF16:
            case DXGI_FORMAT_BC7_UNORM:
            case DXGI_FORMAT_BC7_UNORM_SRGB:
                return 16;

            default:
                assert(false && "Unimplemented");
                return 0;
            }
        }

        const char* ToString(DXGI_FORMAT format)
        {
            switch (format)
            {
            case DXGI_FORMAT_UNKNOWN:                         return "DXGI_FORMAT_UNKNOWN";
            case DXGI_FORMAT_R32G32B32A32_FLOAT:              return "DXGI_FORMAT_R32G32B32A32_FLOAT";
            case DXGI_FORMAT_R32G32B32A32_UINT:               return "DXGI_FORMAT_R32G32B32A32_UINT";
            case DXGI_FORMAT_R32G32B32A32_SINT:               return "DXGI_FORMAT_R32G32B32A32_SINT";
            case DXGI_FORMAT_R32G32B32_FLOAT:                 return "DXGI_FORMAT_R32G32B32_FLOAT";
            case DXGI_FORMAT_R32G32B32_UINT:                  return "DXGI_FORMAT_R32G32B32_UINT";
            case DXGI_FORMAT_R32G32B32_SINT:                  return "DXGI_FORMAT_R32G32B32_SINT";
            case DXGI_FORMAT_R16G16B16A16_FLOAT:              return "DXGI_FORMAT_R16G16B16A16_FLOAT";
            case DXGI_FORMAT_R16G16B16A16_UNORM:              return "DXGI_FORMAT_R16G16B16A16_UNORM";
            case DXGI_FORMAT_R16G16B16A16_UINT:               return "DXGI_FORMAT_R16G16B16A16_UINT";
            case DXGI_FORMAT_R16G16B16A16_SNORM:              return "DXGI_FORMAT_R16G16B16A16_SNORM";
            case DXGI_FORMAT_R16G16B16A16_SINT:               return "DXGI_FORMAT_R16G16B16A16_SINT";
            case DXGI_FORMAT_R32G32_FLOAT:                    return "DXGI_FORMAT_R32G32_FLOAT";
            case DXGI_FORMAT_R32G32_UINT:                     return "DXGI_FORMAT_R32G32_UINT";
            case DXGI_FORMAT_R32G32_SINT:                     return "DXGI_FORMAT_R32G32_SINT";
            case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:            return "DXGI_FORMAT_D32_FLOAT_S8X24_UINT";
            case DXGI_FORMAT_Y416:                            return "DXGI_FORMAT_Y416";
            case DXGI_FORMAT_Y210:                            return "DXGI_FORMAT_Y210";
            case DXGI_FORMAT_Y216:                            return "DXGI_FORMAT_Y216";
            case DXGI_FORMAT_R10G10B10A2_UNORM:               return "DXGI_FORMAT_R10G10B10A2_UNORM";
            case DXGI_FORMAT_R10G10B10A2_UINT:                return "DXGI_FORMAT_R10G10B10A2_UINT";
            case DXGI_FORMAT_R11G11B10_FLOAT:                 return "DXGI_FORMAT_R11G11B10_FLOAT";
            case DXGI_FORMAT_R8G8B8A8_UNORM:                  return "DXGI_FORMAT_R8G8B8A8_UNORM";
            case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:             return "DXGI_FORMAT_R8G8B8A8_UNORM_SRGB";
            case DXGI_FORMAT_R8G8B8A8_UINT:                   return "DXGI_FORMAT_R8G8B8A8_UINT";
            case DXGI_FORMAT_R8G8B8A8_SNORM:                  return "DXGI_FORMAT_R8G8B8A8_SNORM";
            case DXGI_FORMAT_R8G8B8A8_SINT:                   return "DXGI_FORMAT_R8G8B8A8_SINT";
            case DXGI_FORMAT_R16G16_FLOAT:                    return "DXGI_FORMAT_R16G16_FLOAT";
            case DXGI_FORMAT_R16G16_UNORM:                    return "DXGI_FORMAT_R16G16_UNORM";
            case DXGI_FORMAT_R16G16_UINT:                     return "DXGI_FORMAT_R16G16_UINT";
            case DXGI_FORMAT_R16G16_SNORM:                    return "DXGI_FORMAT_R16G16_SNORM";
            case DXGI_FORMAT_R16G16_SINT:                     return "DXGI_FORMAT_R16G16_SINT";
            case DXGI_FORMAT_D32_FLOAT:                       return "DXGI_FORMAT_D32_FLOAT";
            case DXGI_FORMAT_R32_FLOAT:                       return "DXGI_FORMAT_R32_FLOAT";
            case DXGI_FORMAT_R32_UINT:                        return "DXGI_FORMAT_R32_UINT";
            case DXGI_FORMAT_R32_SINT:                        return "DXGI_FORMAT_R32_SINT";
            case DXGI_FORMAT_D24_UNORM_S8_UINT:               return "DXGI_FORMAT_D24_UNORM_S8_UINT";
            case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:              return "DXGI_FORMAT_R9G9B9E5_SHAREDEXP";
            case DXGI_FORMAT_R8G8_B8G8_UNORM:                 return "DXGI_FORMAT_R8G8_B8G8_UNORM";
            case DXGI_FORMAT_G8R8_G8B8_UNORM:                 return "DXGI_FORMAT_G8R8_G8B8_UNORM";
            case DXGI_FORMAT_B8G8R8A8_UNORM:                  return "DXGI_FORMAT_B8G8R8A8_UNORM";
            case DXGI_FORMAT_B8G8R8X8_UNORM:                  return "DXGI_FORMAT_B8G8R8X8_UNORM";
            case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:      return "DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM";
            case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:             return "DXGI_FORMAT_B8G8R8A8_UNORM_SRGB";
            case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:             return "DXGI_FORMAT_B8G8R8X8_UNORM_SRGB";
            case DXGI_FORMAT_AYUV:                            return "DXGI_FORMAT_AYUV";
            case DXGI_FORMAT_Y410:                            return "DXGI_FORMAT_Y410";
            case DXGI_FORMAT_YUY2:                            return "DXGI_FORMAT_YUY2";
            case DXGI_FORMAT_P010:                            return "DXGI_FORMAT_P010";
            case DXGI_FORMAT_P016:                            return "DXGI_FORMAT_P016";
            case DXGI_FORMAT_R8G8_UNORM:                      return "DXGI_FORMAT_R8G8_UNORM";
            case DXGI_FORMAT_R8G8_UINT:                       return "DXGI_FORMAT_R8G8_UINT";
            case DXGI_FORMAT_R8G8_SNORM:                      return "DXGI_FORMAT_R8G8_SNORM";
            case DXGI_FORMAT_R8G8_SINT:                       return "DXGI_FORMAT_R8G8_SINT";
            case DXGI_FORMAT_R16_FLOAT:                       return "DXGI_FORMAT_R16_FLOAT";
            case DXGI_FORMAT_D16_UNORM:                       return "DXGI_FORMAT_D16_UNORM";
            case DXGI_FORMAT_R16_UNORM:                       return "DXGI_FORMAT_R16_UNORM";
            case DXGI_FORMAT_R16_UINT:                        return "DXGI_FORMAT_R16_UINT";
            case DXGI_FORMAT_R16_SNORM:                       return "DXGI_FORMAT_R16_SNORM";
            case DXGI_FORMAT_R16_SINT:                        return "DXGI_FORMAT_R16_SINT";
            case DXGI_FORMAT_B5G6R5_UNORM:                    return "DXGI_FORMAT_B5G6R5_UNORM";
            case DXGI_FORMAT_B5G5R5A1_UNORM:                  return "DXGI_FORMAT_B5G5R5A1_UNORM";
            case DXGI_FORMAT_A8P8:                            return "DXGI_FORMAT_A8P8";
            case DXGI_FORMAT_B4G4R4A4_UNORM:                  return "DXGI_FORMAT_B4G4R4A4_UNORM";
            case DXGI_FORMAT_NV12:                            return "DXGI_FORMAT_NV12";
            case DXGI_FORMAT_NV11:                            return "DXGI_FORMAT_NV11";
            case DXGI_FORMAT_R8_UNORM:                        return "DXGI_FORMAT_R8_UNORM";
            case DXGI_FORMAT_R8_UINT:                         return "DXGI_FORMAT_R8_UINT";
            case DXGI_FORMAT_R8_SNORM:                        return "DXGI_FORMAT_R8_SNORM";
            case DXGI_FORMAT_R8_SINT:                         return "DXGI_FORMAT_R8_SINT";
            case DXGI_FORMAT_A8_UNORM:                        return "DXGI_FORMAT_A8_UNORM";
            case DXGI_FORMAT_AI44:                            return "DXGI_FORMAT_AI44";
            case DXGI_FORMAT_IA44:                            return "DXGI_FORMAT_IA44";
            case DXGI_FORMAT_P8:                              return "DXGI_FORMAT_P8";
            case DXGI_FORMAT_R1_UNORM:                        return "DXGI_FORMAT_R1_UNORM";
            case DXGI_FORMAT_BC1_UNORM:                       return "DXGI_FORMAT_BC1_UNORM";
            case DXGI_FORMAT_BC1_UNORM_SRGB:                  return "DXGI_FORMAT_BC1_UNORM_SRGB";
            case DXGI_FORMAT_BC4_UNORM:                       return "DXGI_FORMAT_BC4_UNORM";
            case DXGI_FORMAT_BC4_SNORM:                       return "DXGI_FORMAT_BC4_SNORM";
            case DXGI_FORMAT_BC2_UNORM:                       return "DXGI_FORMAT_BC2_UNORM";
            case DXGI_FORMAT_BC2_UNORM_SRGB:                  return "DXGI_FORMAT_BC2_UNORM_SRGB";
            case DXGI_FORMAT_BC3_UNORM:                       return "DXGI_FORMAT_BC3_UNORM";
            case DXGI_FORMAT_BC3_UNORM_SRGB:                  return "DXGI_FORMAT_BC3_UNORM_SRGB";
            case DXGI_FORMAT_BC5_UNORM:                       return "DXGI_FORMAT_BC5_UNORM";
            case DXGI_FORMAT_BC5_SNORM:                       return "DXGI_FORMAT_BC5_SNORM";
            case DXGI_FORMAT_BC6H_UF16:                       return "DXGI_FORMAT_BC6H_UF16";
            case DXGI_FORMAT_BC6H_SF16:                       return "DXGI_FORMAT_BC6H_SF16";
            case DXGI_FORMAT_BC7_UNORM:                       return "DXGI_FORMAT_BC7_UNORM";
            case DXGI_FORMAT_BC7_UNORM_SRGB:                  return "DXGI_FORMAT_BC7_UNORM_SRGB";
            }

            return "Unimplemented";
        }

        uint32_t GetAlignment(DXGI_FORMAT format)
        {
            switch (format)
            {
            case DXGI_FORMAT_BC1_UNORM:
            case DXGI_FORMAT_BC1_UNORM_SRGB:
            case DXGI_FORMAT_BC4_UNORM:
            case DXGI_FORMAT_BC4_SNORM:
            case DXGI_FORMAT_BC2_UNORM:
            case DXGI_FORMAT_BC2_UNORM_SRGB:
            case DXGI_FORMAT_BC3_UNORM:
            case DXGI_FORMAT_BC3_UNORM_SRGB:
            case DXGI_FORMAT_BC5_UNORM:
            case DXGI_FORMAT_BC5_SNORM:
            case DXGI_FORMAT_BC6H_UF16:
            case DXGI_FORMAT_BC6H_SF16:
            case DXGI_FORMAT_BC7_UNORM:
            case DXGI_FORMAT_BC7_UNORM_SRGB:
                return 4;

            case DXGI_FORMAT_R8G8_B8G8_UNORM:
            case DXGI_FORMAT_G8R8_G8B8_UNORM:
            case DXGI_FORMAT_YUY2:
            case DXGI_FORMAT_Y210:
            case DXGI_FORMAT_Y216:
            case DXGI_FORMAT_NV12:
            case DXGI_FORMAT_P010:
            case DXGI_FORMAT_P016:
                return 2;

            default:
                return 1;
            }
        }

        DXGI_FORMAT MakeSRGB(DXGI_FORMAT format)
        {
            switch (format)
            {
            case DXGI_FORMAT_R8G8B8A8_UNORM:
                return DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;

            case DXGI_FORMAT_BC1_UNORM:
                return DXGI_FORMAT_BC1_UNORM_SRGB;

            case DXGI_FORMAT_BC2_UNORM:
                return DXGI_FORMAT_BC2_UNORM_SRGB;

            case DXGI_FORMAT_BC3_UNORM:
                return DXGI_FORMAT_BC3_UNORM_SRGB;

            case DXGI_FORMAT_B8G8R8A8_UNORM:
                return DXGI_FORMAT_B8G8R8A8_UNORM_SRGB;

            case DXGI_FORMAT_B8G8R8X8_UNORM:
                return DXGI_FORMAT_B8G8R8X8_UNORM_SRGB;

            case DXGI_FORMAT_BC7_UNORM:
                return DXGI_FORMAT_BC7_UNORM_SRGB;

            default:
                return format;
            }
        }

        DXGI_FORMAT MakeLinear(DXGI_FORMAT format)
        {
            switch (format)
            {
            case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
                return DXGI_FORMAT_R8G8B8A8_UNORM;

            case DXGI_FORMAT_BC1_UNORM_SRGB:
                return DXGI_FORMAT_BC1_UNORM;

            case DXGI_FORMAT_BC2_UNORM_SRGB:
                return DXGI_FORMAT_BC2_UNORM;

            case DXGI_FORMAT_BC3_UNORM_SRGB:
                return DXGI_FORMAT_BC3_UNORM;

            case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
                return DXGI_FORMAT_B8G8R8A8_UNORM;

            case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
                return DXGI_FORMAT_B8G8R8X8_UNORM;

            case DXGI_FORMAT_BC7_UNORM_SRGB:
                return DXGI_FORMAT_BC7_UNORM;

            default:
                return format;
            }
        }

        SubImageLayout EvaluateSubImageLayout(uint32_t width, uint32_t height, DXGI_FORMAT format)
        {
            SubImageLayout out;

            bool isBlockCompressed = false;
            bool isPacked = false;
            bool isPlanar = false;
            uint32_t bytesPerElement = 0; //! For block compressions this is bytes per block
            uint32_t numBlocks = 0;

            switch (format)
            {
            case DXGI_FORMAT_R32G32B32A32_FLOAT:
            case DXGI_FORMAT_R32G32B32A32_UINT:
            case DXGI_FORMAT_R32G32B32A32_SINT:
            case DXGI_FORMAT_R32G32B32_FLOAT:
            case DXGI_FORMAT_R32G32B32_UINT:
            case DXGI_FORMAT_R32G32B32_SINT:
            case DXGI_FORMAT_R16G16B16A16_FLOAT:
            case DXGI_FORMAT_R16G16B16A16_UNORM:
            case DXGI_FORMAT_R16G16B16A16_UINT:
            case DXGI_FORMAT_R16G16B16A16_SNORM:
            case DXGI_FORMAT_R16G16B16A16_SINT:
            case DXGI_FORMAT_R32G32_FLOAT:
            case DXGI_FORMAT_R32G32_UINT:
            case DXGI_FORMAT_R32G32_SINT:
            case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
            case DXGI_FORMAT_R10G10B10A2_UNORM:
            case DXGI_FORMAT_R10G10B10A2_UINT:
            case DXGI_FORMAT_R11G11B10_FLOAT:
            case DXGI_FORMAT_R8G8B8A8_UNORM:
            case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
            case DXGI_FORMAT_R8G8B8A8_UINT:
            case DXGI_FORMAT_R8G8B8A8_SNORM:
            case DXGI_FORMAT_R8G8B8A8_SINT:
            case DXGI_FORMAT_R16G16_FLOAT:
            case DXGI_FORMAT_R16G16_UNORM:
            case DXGI_FORMAT_R16G16_UINT:
            case DXGI_FORMAT_R16G16_SNORM:
            case DXGI_FORMAT_R16G16_SINT:
            case DXGI_FORMAT_D32_FLOAT:
            case DXGI_FORMAT_R32_FLOAT:
            case DXGI_FORMAT_R32_UINT:
            case DXGI_FORMAT_R32_SINT:
            case DXGI_FORMAT_D24_UNORM_S8_UINT:
            case DXGI_FORMAT_B8G8R8A8_UNORM:
            case DXGI_FORMAT_B8G8R8X8_UNORM:
            case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
            case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
            case DXGI_FORMAT_R8G8_UNORM:
            case DXGI_FORMAT_R8G8_UINT:
            case DXGI_FORMAT_R8G8_SNORM:
            case DXGI_FORMAT_R8G8_SINT:
            case DXGI_FORMAT_R16_FLOAT:
            case DXGI_FORMAT_D16_UNORM:
            case DXGI_FORMAT_R16_UNORM:
            case DXGI_FORMAT_R16_UINT:
            case DXGI_FORMAT_R16_SNORM:
            case DXGI_FORMAT_R16_SINT:
            case DXGI_FORMAT_R8_UNORM:
            case DXGI_FORMAT_R8_UINT:
            case DXGI_FORMAT_R8_SNORM:
            case DXGI_FORMAT_R8_SINT:
            case DXGI_FORMAT_A8_UNORM:
            case DXGI_FORMAT_R1_UNORM:
            case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
                break;

            case DXGI_FORMAT_BC1_UNORM:
            case DXGI_FORMAT_BC1_UNORM_SRGB:
            case DXGI_FORMAT_BC4_UNORM:
            case DXGI_FORMAT_BC4_SNORM:
                isBlockCompressed = true;
                bytesPerElement = 8;
                numBlocks = 4;
                break;

            case DXGI_FORMAT_BC2_UNORM:
            case DXGI_FORMAT_BC2_UNORM_SRGB:
            case DXGI_FORMAT_BC3_UNORM:
            case DXGI_FORMAT_BC3_UNORM_SRGB:
            case DXGI_FORMAT_BC5_UNORM:
            case DXGI_FORMAT_BC5_SNORM:
            case DXGI_FORMAT_BC6H_UF16:
            case DXGI_FORMAT_BC6H_SF16:
            case DXGI_FORMAT_BC7_UNORM:
            case DXGI_FORMAT_BC7_UNORM_SRGB:
                isBlockCompressed = true;
                bytesPerElement = 16;
                numBlocks = 4;
                break;

            case DXGI_FORMAT_R8G8_B8G8_UNORM:
            case DXGI_FORMAT_G8R8_G8B8_UNORM:
            case DXGI_FORMAT_YUY2:
                isPacked = true;
                bytesPerElement = 4;
                break;

            case DXGI_FORMAT_Y210:
            case DXGI_FORMAT_Y216:
                isPacked = true;
                bytesPerElement = 8;
                break;

            case DXGI_FORMAT_NV12:
                break;

            case DXGI_FORMAT_P010:
            case DXGI_FORMAT_P016:
                isPlanar = true;
                bytesPerElement = 4;
                break;

            default:
                assert(false && "Unimplemented");
            }

            if (isBlockCompressed)
            {
                uint32_t numBlocksWidth = 0;
                if (width > 0)
                {
                    numBlocksWidth = std::max(1u, (width + (numBlocks - 1u)) / numBlocks);
                }
                uint32_t numBlocksHeight = 0;
                if (height > 0)
                {
                    numBlocksHeight = std::max(1u, (height + (numBlocks - 1u)) / numBlocks);
                }
                out.m_pitch = numBlocksWidth * bytesPerElement;
                out.m_rowCount = numBlocksHeight;
                out.m_width = width;
                out.m_height = height;
                out.m_blockWidth = numBlocks;
                out.m_blockHeight = numBlocks;
            }
            else if (isPacked)
            {
                //! Ceil to nearest width divided by 2
                out.m_pitch = ((width + 1u) >> 1u) * bytesPerElement;
                out.m_rowCount = height;
                out.m_width = width;
                out.m_height = height;
            }
            else if (format == DXGI_FORMAT_NV11)
            {
                out.m_pitch = ((width + 3u) >> 2u) * 4u;
                out.m_rowCount = height * 2u;
                out.m_width = (width + 1u) & ~1u; //! Align up to nearest value which is multiply of 2
                out.m_height = (height + 1u) & ~1u;
            }
            else if (isPlanar)
            {
                out.m_pitch = ((width + 1u) >> 1u) * bytesPerElement;
                out.m_rowCount = height + ((height + 1u) >> 1u);
                out.m_width = (width + 1u) & ~1u;
                out.m_height = (height + 1u) & ~1u;
            }
            else
            {
                const uint32_t bitsPerPixel = GetFormatSize(format) * 8u;
                out.m_pitch = (width * bitsPerPixel + 7u) / 8u; //! Round up to nearest byte
                out.m_rowCount = height;
                out.m_width = width;
                out.m_height = height;
            }

            out.m_compressed = isBlockCompressed;
            out.m_totalSize = out.m_pitch * out.m_rowCount;

            return out;
        }

    } // namespace DXGI
} // namespace ZTK::Image