// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/

#pragma once

#include <DDSHeader.h>

namespace ZTK::Image
{
	namespace DDSWriter
	{
		class DDSFile final
		{
		public:

			DDSFile();
			DDSFile(const IImage* const image);

			~DDSFile() = default;

			void SetSize(uint32_t width, uint32_t height, uint32_t depth);
			void SetFormat(DXGI_FORMAT format);
			void SetAsCubeMap();
			void SetMipLevels(uint32_t mipLevels);

			bool WriteToFile(std::ofstream& stream) const;

		private:

			void ResolvePitch();
			
			static uint32_t s_magic;
			DDS_HEADER m_header;
			DDS_HEADER_DXT10 m_headerDx10; //! Header extension for DX10+ APIs

			DXGI_FORMAT m_format = DXGI_FORMAT_UNKNOWN;

			std::vector<uint8_t> m_data;
		};
	} // namespace DDSWriter
} // namespace ZTK::Image