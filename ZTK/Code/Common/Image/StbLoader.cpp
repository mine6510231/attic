//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <ImageLoader.h>

#ifndef STB_IMAGE_IMPLEMENTATION
	#define STB_IMAGE_IMPLEMENTATION
#endif

//! [3RD_PARTY] Stb library
#include <stb/stb_image.h>

namespace ZTK::Image
{
	namespace StbLoader
	{
		IImagePtr LoadImageFromFile(const std::string& path, std::string* err)
		{
			int32_t width = 0u;
			int32_t height = 0u;
			int32_t channel = 0u;

			//! According to stb_image's document, "The pixel data consists of *y scanlines of *x pixels,
			//! with each pixel consisting of N interleaved 8-bit components", the output of stbi_load
			//! is guaranteed to be 8 bit depth per pixel so its format can be determined solely based on number of 
			//! available channel
			unsigned char* buffer = stbi_load(path.c_str(), &width, &height, &channel, 0);
			if (!buffer)
			{
				if (err)
				{
					std::stringstream s;
					s << "Image loading failed [" << path << "]\n";
					(*err) += s.str();
				}
				return nullptr;
			}

			IPixelFormat format = IPixelFormat::Unknown;
			switch (channel)
			{
			case 1: format = IPixelFormat::R8;  break;
			case 2: format = IPixelFormat::R8G8; break;
			case 3: format = IPixelFormat::R8G8B8; break;
			case 4: format = IPixelFormat::R8G8B8A8; break;
			default: break;
			}

			//! All types supported by stb image are plain images that doesn't contain mipmaps 
			//! so the input parameter is hardcoded to single top mip
			IImagePtr image = IImage::Create(width, height, 1, format);
			if (!image)
			{
				if (err) (*err) += "Image creation failed\n";
				return nullptr;
			}

			//! 8 bits input format, 1 channels = 1 bytes per pixel
			image->SetMipData2(0, buffer, width, height, channel);
			
			return image;
		}
	}
} // namespace ZTK::Image






