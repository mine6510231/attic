set(FILES
    AppUtils/Logger.cpp
    AppUtils/Logger.h
    AppUtils/ScanlineProcessor.cpp
    AppUtils/ScanlineProcessor.h
)

add_library(ZTKAppUtils ${FILES})
target_include_directories(ZTKAppUtils PUBLIC ${CMAKE_CURRENT_LIST_DIR}/AppUtils)
target_link_libraries(ZTKAppUtils PUBLIC SPDLOG)