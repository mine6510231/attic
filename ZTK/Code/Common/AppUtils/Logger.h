//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

#include <spdlog/spdlog.h>

#include <memory>

namespace ZTK
{
    //! A simple spdlog based logger plus a simple std out progress bar, which is only for console output. 
    //! Note: Not thread safe
	class Logger final
	{
    public:

        using Logger_Type = spdlog::logger;

        static constexpr const char LogFilePath[] = "EnvMapGenLogs.txt";

	public:

        Logger() = default;
        Logger(const std::shared_ptr<Logger_Type>& logger);

        ~Logger();

        void SetLogger(const std::shared_ptr<Logger_Type>& logger) { m_logger = logger; };

        static void Register(Logger* logger)
        {
            s_logger = logger;
        }

        static void Unregister()
        {
            s_logger = nullptr;
        }

        //! Get underlying logger
        static Logger_Type* Get()
        {
            assert(s_logger && "No logger registered");
            assert(s_logger->m_logger && "Registered logger is not properly initialized");
            return s_logger->m_logger.get();
        }

        //! A mechanical stopwatch like timer, click once to start recording (return 0.0) and click again to stop and get the elapsed time
        //! in second since last click. The underlying type is spdlog::stopwatch. 
        static double StopWatchClick();

        static Logger* CreateDefault();

    private:

        std::shared_ptr<Logger_Type> m_logger = nullptr;

    private:

        static Logger* s_logger;
	};

} // namespace ZTK