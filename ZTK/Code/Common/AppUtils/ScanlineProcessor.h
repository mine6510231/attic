//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

#include <cstdint>
#include <functional>
#include <thread>
#include <chrono>
#include <future>

namespace ZTK
{
	using SPProcessLineCallback = std::function<void(uint32_t, uint32_t)>;

	//! A simple processor that process a vertical chunk of a given image in parallel with provided callback
	class ScanlineProcessor final
	{
	public:

		void SetWorkerCount(uint32_t numThreads);
		void SetLinesPerWorker(uint32_t lines);
		void SetProcessLineCallback(SPProcessLineCallback callback);

		void Launch();
		void Wait();

	private:

		struct ThreadContext
		{
			std::thread m_thread;

			//! The template parameter is return value of our task, which is void constrained by SPProcessLineCallback.
			std::future<void> m_future;
			std::future_status m_status = std::future_status::ready;
		};

		std::vector<ThreadContext> m_threads;
		uint32_t m_numThreads = 1;
		uint32_t m_linesPerTask = 1;
		SPProcessLineCallback m_callback;
	};
} // namespace ZTK