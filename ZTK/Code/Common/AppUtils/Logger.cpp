//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <Logger.h>

#include <spdlog/async.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/stopwatch.h>

namespace ZTK
{
    Logger* Logger::s_logger = nullptr;

    Logger::Logger(const std::shared_ptr<Logger::Logger_Type>& logger)
        : m_logger(logger)
    {
    }

    Logger::~Logger()
    {
        if (m_logger)
        {
            m_logger->flush();
            m_logger = nullptr;
        }
    }

    Logger* Logger::CreateDefault()
    {
        //! Create a logger with 2 targets, with different formats.
        
        //! Time stamp will not be shown in console log
        auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        console_sink->set_level(spdlog::level::trace);
        console_sink->set_pattern("[%^%l%$] %v");

        auto file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(Logger::LogFilePath, true);
        file_sink->set_level(spdlog::level::trace);

        std::vector<spdlog::sink_ptr> sinks{ console_sink, file_sink };

        //! Create logger
        auto logger = std::shared_ptr<spdlog::logger>(new spdlog::logger("Default", { console_sink, file_sink }));

        return new Logger(logger);
    }

    double Logger::StopWatchClick()
    {
        static spdlog::stopwatch s_stopWatch;
        static bool s_running = false;

        if (s_running)
        {
            s_running = false;
            return s_stopWatch.elapsed().count();
        }
        else
        {
            s_running = true;
            s_stopWatch.reset();
            return 0.0;
        }
    }

} // namespace ZTK