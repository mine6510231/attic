//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <MikkTangentGen.h>
#include <TangentGen.h>

#include <External/mikktspace.h>
#include <External/weldmesh.h>

namespace ZTK::Common::MikkT
{
	//! Helper functions, signature defined by SMikkTSpaceInterface so please don't change it

	//! Returns the number of faces (triangles/quads) on the mesh to be processed.
	int GetNumFaces(const SMikkTSpaceContext* pContext)
	{
		MikkTData* data = static_cast<MikkTData*>(pContext->m_pUserData);
		return data->m_numFaces;
	}

	//! Returns the number of vertices on face number iFace
	//! iFace is a number in the range {0, 1, ..., getNumFaces()-1}
	int GetNumVerticesOfFace(const SMikkTSpaceContext* pContext, const int iFace)
	{
		MikkTData* data = static_cast<MikkTData*>(pContext->m_pUserData);
		return data->m_numVerticesOfFace;
	}

	void GetPosition(const SMikkTSpaceContext* pContext, float fvPosOut[], const int iFace, const int iVert)
	{
		MikkTData* data = static_cast<MikkTData*>(pContext->m_pUserData);
		size_t offset = data->GetVertexIndex(iFace, iVert) * MikkTData::PositionStride;
		fvPosOut[0] = data->m_positions[offset];
		fvPosOut[1] = data->m_positions[offset + 1];
		fvPosOut[2] = data->m_positions[offset + 2];
	}

	void GetNormal(const SMikkTSpaceContext* pContext, float fvNormOut[], const int iFace, const int iVert)
	{
		MikkTData* data = static_cast<MikkTData*>(pContext->m_pUserData);
		size_t offset = data->GetVertexIndex(iFace, iVert) * MikkTData::NormalStride;
		fvNormOut[0] = data->m_normals[offset];
		fvNormOut[1] = data->m_normals[offset + 1];
		fvNormOut[2] = data->m_normals[offset + 2];
	}

	void GetTexCoord(const SMikkTSpaceContext* pContext, float fvTexcOut[], const int iFace, const int iVert)
	{
		MikkTData* data = static_cast<MikkTData*>(pContext->m_pUserData);
		size_t offset = data->GetVertexIndex(iFace, iVert) * MikkTData::TexcoordStride;
		fvTexcOut[0] = data->m_texcoords[offset];
		fvTexcOut[1] = data->m_texcoords[offset + 1];
	}

	//! This function is used to return the tangent and fSign to the application.
	//! fvTangent is a unit length vector.
	//! For normal maps it is sufficient to use the following simplified version of the bitangent which is generated at pixel/vertex level.
	//! bitangent = fSign * cross(vN, tangent);
	//! Note that the results are returned unindexed. It is possible to generate a new index list
	//! But averaging/overwriting tangent spaces by using an already existing index list WILL produce INCRORRECT results.
	//! DO NOT! use an already existing index list.
	void SetTSpaceBasic(const SMikkTSpaceContext* pContext, const float fvTangent[], const float fSign, const int iFace, const int iVert)
	{
		MikkTData* data = static_cast<MikkTData*>(pContext->m_pUserData);
		size_t offset = data->GetVertexIndex(iFace, iVert) * MikkTData::NormalStride;
		data->m_tangents[offset] = fvTangent[0];
		data->m_tangents[offset + 1] = fvTangent[1];
		data->m_tangents[offset + 2] = fvTangent[2];

		float fvNormal[3]{
			data->m_normals[offset],
			data->m_normals[offset + 1],
			data->m_normals[offset + 2]
		};

		//! Bitangent = fSign * cross(normal, tangent);
		data->m_bitangents[offset] = (fvNormal[1] * fvTangent[2] - fvNormal[2] * fvTangent[1]) * fSign;
		data->m_bitangents[offset + 1] = (fvNormal[2] * fvTangent[0] - fvNormal[0] * fvTangent[2]) * fSign;
		data->m_bitangents[offset + 2] = (fvNormal[0] * fvTangent[1] - fvNormal[1] * fvTangent[0]) * fSign;
	}

	//! This function is used to return tangent space results to the application.
	//! fvTangent and fvBiTangent are unit length vectors and fMagS and fMagT are their
	//! true magnitudes which can be used for relief mapping effects.
	//! fvBiTangent is the "real" bitangent and thus may not be perpendicular to fvTangent.
	//! However, both are perpendicular to the vertex normal.
	//! For normal maps it is sufficient to use the following simplified version of the bitangent which is generated at pixel/vertex level.
	//! fSign = bIsOrientationPreserving ? 1.0f : (-1.0f);
	//! bitangent = fSign * cross(vN, tangent);
	//! Note that the results are returned unindexed. It is possible to generate a new index list
	//! But averaging/overwriting tangent spaces by using an already existing index list WILL produce INCRORRECT results.
	//! DO NOT! use an already existing index list.
	void SetTSpace(const SMikkTSpaceContext* pContext, const float fvTangent[], const float fvBiTangent[], const float fMagS, const float fMagT,
		const tbool bIsOrientationPreserving, const int iFace, const int iVert)
	{
		MikkTData* data = static_cast<MikkTData*>(pContext->m_pUserData);
		size_t offset = data->GetVertexIndex(iFace, iVert) * MikkTData::NormalStride;
		data->m_tangents[offset] = fvTangent[0] * fMagS;
		data->m_tangents[offset + 1] = fvTangent[1] * fMagS;
		data->m_tangents[offset + 2] = fvTangent[2] * fMagS;

		data->m_bitangents[offset] = fvBiTangent[0] * fMagT;
		data->m_bitangents[offset + 1] = fvBiTangent[1] * fMagT;
		data->m_bitangents[offset + 2] = fvBiTangent[2] * fMagT;
	}

	bool GenerateTangent(MikkTData& data, bool fast)
	{
		//! Create MikkT interface
		SMikkTSpaceInterface mikkTInterface;
		mikkTInterface.m_getNumFaces = GetNumFaces;
		mikkTInterface.m_getNumVerticesOfFace = GetNumVerticesOfFace;
		mikkTInterface.m_getPosition = GetPosition;
		mikkTInterface.m_getNormal = GetNormal;
		mikkTInterface.m_getTexCoord = GetTexCoord;

		//! Allocate memeory for output
		data.m_tangents.resize(data.m_normals.size());
		data.m_bitangents.resize(data.m_normals.size());

		if (fast)
		{
			mikkTInterface.m_setTSpace = nullptr;
			mikkTInterface.m_setTSpaceBasic = SetTSpaceBasic;
		}
		else
		{
			mikkTInterface.m_setTSpace = SetTSpace;
			mikkTInterface.m_setTSpaceBasic = nullptr;

			//! Additionally store flip sign of tangent to get compute orthogonal bitangent at run time
			data.m_flipSign.resize(data.m_normals.size());
		}

		SMikkTSpaceContext mikkTContext;
		mikkTContext.m_pInterface = &mikkTInterface;
		mikkTContext.m_pUserData = &data;

		if (genTangSpaceDefault(&mikkTContext) == 0)
		{
			return false;
		}

		return true;
	}

	int GenerateIndexList(int* piRemapTable, float* pfVertexDataOut,
		const float pfVertexDataIn[], const int iNrVerticesIn, const int iFloatsPerVert)
	{
		//! piRemapTable must be initialized and point to an area in memory
		//! with the byte size: iNrVerticesIn * sizeof(int).
		//! pfVertexDataOut must be initialized and point to an area in memory
		//! with the byte size: iNrVerticesIn * iFloatsPerVert * sizeof(float).
		//! At the end of the WeldMesh() call the array pfVertexDataOut will contain
		//! unique vertices only. Each entry in piRemapTable contains the index to
		//! the new location of the vertex in pfVertexDataOut in units of iFloatsPerVert.
		//! Note that this code is suitable for welding both unindexed meshes but also
		//! indexed meshes which need to have duplicates removed. In the latter case
		//! one simply uses the remap table to convert the old index list to the new vertex array.
		//! Finally, the return value is the number of unique vertices found.
		return WeldMesh(piRemapTable, pfVertexDataOut,
			pfVertexDataIn, iNrVerticesIn, iFloatsPerVert);
	}


} // namespace ZTK::Common::Mikkt