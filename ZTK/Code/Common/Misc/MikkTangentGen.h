//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

#include <cstdint>
#include <span>
#include <vector>

namespace ZTK::Common::MikkT
{
	struct MikkTData
	{
		//! Number of components for each attribute type
		static constexpr uint32_t PositionStride = 3;
		static constexpr uint32_t NormalStride = 3;
		static constexpr uint32_t TexcoordStride = 2;

		uint32_t GetVertexIndex(const int iFace, const int iVert) const
		{
			uint32_t vertexId = iFace * m_numVerticesOfFace + iVert;

			if (m_indexed)
			{
				return m_indices[vertexId];
			}
			else
			{
				return vertexId;
			}
		}

		//! Inputs

		//! Vertex index list
		std::span<uint32_t> m_indices;

		//! Flattened vertex attribute lists
		std::span<float> m_positions;
		std::span<float> m_normals;
		std::span<float> m_texcoords;

		uint32_t m_numFaces = 0;
		uint32_t m_numVerticesOfFace = 3;

		bool m_indexed = false;

		//! Outputs
		std::vector<float> m_tangents;
		std::vector<float> m_bitangents;
		std::vector<float> m_flipSign;
	};
} // ZTK::Common::MikkT