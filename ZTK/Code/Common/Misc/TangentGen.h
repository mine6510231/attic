//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

#include <MikkTangentGen.h>

namespace ZTK::Common
{
	namespace MikkT
	{
		//! A simple implementation of MikkTSpace to generate tangent and bitangent for full triangle / quad meshes 
		//! (Could be relaxed by changing MikkTData's fields with extra memory cost)
		//! Param: 
		//!	fast - use a fast routine that only generates tangent and compute bitangent via cross production, 
		//!		   which should be sufficient for most of cases.
		bool GenerateTangent(MikkTData& data, bool fast = true);

		//! An utility to create new index list after tangent generation
		int GenerateIndexList(int* piRemapTable, float* pfVertexDataOut,
			const float pfVertexDataIn[], const int iNrVerticesIn, const int iFloatsPerVert);
	}
} // namespace ZTK::Common