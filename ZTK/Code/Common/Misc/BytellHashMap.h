//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//
#pragma once

//! Bytell hashing based unordered map, STL compatible and faster than defualt implementation
#include <External/unordered_map.hpp>


