set(FILES
    Image/DDSFile.h
    Image/DDSHeader.h
    Image/DDSLoader.cpp
    Image/DDSWriter.cpp
    Image/EXRLoader.cpp
    Image/IDXGIFormat.cpp
    Image/IDXGIFormat.h
    Image/Image.cpp
    Image/Image.h
    Image/ImageLoader.cpp
    Image/ImageLoader.h
    Image/ImageWriter.cpp
    Image/ImageWriter.h
    Image/IPixelFormat.cpp
    Image/IPixelFormat.h
    Image/IPixelOp.cpp
    Image/IPixelOp.h
    Image/StbLoader.cpp
    Image/StbWriter.cpp
)

add_library(ZTKImage ${FILES})
target_include_directories(ZTKImage PUBLIC ${CMAKE_CURRENT_LIST_DIR}/Image)
target_link_libraries(ZTKImage PUBLIC Stb
                                PUBLIC OpenEXR::OpenEXR
                                PUBLIC OpenEXR::OpenEXRCore
                                PUBLIC OpenEXR::OpenEXRUtil)