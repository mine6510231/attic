//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <Cubemap.h>
#include <Logger.h>

#include <ImageLoader.h>
#include <ImageWriter.h>

#include <argparse/argparse.hpp>

#include <filesystem>

using namespace ZTK;
using namespace ZTK::Image;

//! Because irradiance calculation requires convolution on entire image per pixel, setting 
//! this value too large will result in hours long baking time so please be cautious.
//! [TODO] Tile based multithreading to CCubeMapProcessor to speed up
namespace
{
    static constexpr uint32_t DefaultIrradianceMapFaceSize = 256u;
    static constexpr uint32_t DefaultRadianceMapSamples = 512u;
    static constexpr uint32_t DefaultBRDFLutSize = 256u;
    static constexpr char DefaultBRDFLutFormat[] = "R32G32F";
}

void ParseError(const std::string& err)
{
    std::stringstream sstream(err);
    std::string line;
    while (std::getline(sstream, line, '\n')) {
        Logger::Get()->error(line);
    }
}

int ProcessEnvMap(const std::filesystem::path& input, const std::filesystem::path& outPath, uint32_t outSize, uint32_t numSample)
{
    using namespace std::filesystem;

    std::string fileName = input.stem().string();

    //! [TODO] Add more output type support, for now only .dds cubemap map is supported
    path diffuseOutPath = outPath / path{ fileName + "_diffuse.dds"};
    path specularOutPath = outPath / path{ fileName + "_specular.dds"};
    
    std::string errors;
    IImagePtr image = LoadImageFromFile(input, &errors);
    if (!image)
    {
        ParseError(errors);
        Logger::Get()->info("Image loading failed");
        return -1;
    }
    else
    {
        Logger::Get()->info("Image loaded");
    }

    IImagePtr cubemap = ZTK::ConvertImageToCubemap(image, ZTK::InputLayoutType_LatLong, ZTK::CubemapLayoutType_Vertical);
    if (!cubemap)
    {
        Logger::Get()->error("Image conversion failed");
        return -1;
    }
    else
    {
        Logger::Get()->info("Image converted");

        //! Release source image memory
        image = nullptr;
    }

    {
        ZTK::ECubemapGenDesc desc;
        desc.m_cmgParams.m_filter = ZTK::ECubemapFilter::Cosine; //! Cosine weighted
        desc.m_cmgParams.m_baseFilterAngle = 180.0f; //! Full hemisphere convolution
        desc.m_faceSizeOverride = outSize;
        IImagePtr diffuse = ZTK::GenerateCubemap(cubemap, desc);
        if (!diffuse)
        {
            Logger::Get()->error("Irradiance map generation failed");
            return -1;
        }
        else
        {
            Logger::Get()->info("Irradiance map generated");
        }

        errors.clear();
        bool result = WriteImageToFile(diffuse, diffuseOutPath, &errors);
        if (!result)
        {
            ParseError(errors);
            Logger::Get()->error("Irradiance map writing failed");
            return -1;
        }
        else
        {
            Logger::Get()->info("Iradiance map wrote to file [{}]", diffuseOutPath.filename().string());
            diffuse = nullptr;
        }
    }
    
    {
        ZTK::ECubemapGenDesc desc;
        desc.m_cmgParams.m_filter = ZTK::ECubemapFilter::GGX;
        desc.m_cmgParams.m_samples = numSample;
        IImagePtr specular = ZTK::GenerateCubemap(cubemap, desc);
        if (!specular)
        {
            Logger::Get()->error("Radiance map generation failed");
            return -1;
        }
        else
        {
            Logger::Get()->info("Radiance map generated");
        }

        errors.clear();
        bool result = WriteImageToFile(specular, specularOutPath, &errors);
        if (!result)
        {
            ParseError(errors);
            Logger::Get()->error("Radiance map writing failed");
            return -1;
        }
        else
        {
            Logger::Get()->info("Radiance map wrote to file [{}]", specularOutPath.filename().string());
        }
    }

    return 0;
}

int ProcessBRDFLut(const std::filesystem::path& output, uint32_t size, std::string sFormat)
{
    using namespace std::filesystem;

    IPixelFormat format = R16G16F;
    if (!sFormat.empty())
    {
        if (sFormat.compare("R32G32F") == 0)
        {
            format = IPixelFormat::R32G32F;
        }
        else if (sFormat.compare("R16G16F") == 0)
        {
            format = IPixelFormat::R16G16F;
        }
        else if (sFormat.compare("R8G8") == 0)
        {
            format = IPixelFormat::R8G8;
        }
        else
        {
            Logger::Get()->error("Invalid LUT format");
            return -1;
        }
    }

    path lutPath = output / path{ "BrdfLut_CosThetaX_RoughnessY_TopDownOrigin.dds" };

    IImagePtr lut = GenerateBRDFLUT(size, format);
    if (!lut)
    {
        Logger::Get()->error("BRDF LUT generation failed");
        return -1;
    }
    else
    {
        Logger::Get()->info("BRDF LUT generated");
    }

    std::string errors;
    bool result = WriteImageToFile(lut, lutPath, &errors);
    if (!result)
    {
        ParseError(errors);
        Logger::Get()->error("BRDF LUT writing failed");
        return -1;
    }
    else
    {
        Logger::Get()->info("BRDF LUT wrote to file [{}]", lutPath.filename().string());
    }

    return 0;
}

int ProcessPath(std::filesystem::path& srcPath, std::filesystem::path& dstPath, bool& runEnvMap, bool& runLut)
{
    using namespace std::filesystem;

    if (srcPath.empty())
    {
        if (runLut)
        {
            //! Special routine, if no source path specified and lut generation enabled, create brdf lut to dstPath and ignore 
            //! environment map related commands
            return 0;
        }
        else
        {
            Logger::Get()->error("Please specify a source image");
        }
    }

    std::string srcName = srcPath.stem().string();
    std::string srcExt = srcPath.extension().string();
    if (srcName.empty() || srcExt.empty())
    {
        Logger::Get()->error("Source must contains a valid file name and extension");
        return -1;
    }

    //! [TODO] Add more input type support, for now only .exr latlong map is supported
    if (srcExt.compare(".exr") != 0)
    {
        Logger::Get()->info("At present only .exr latlong map is supported");
        return -1;
    }

    if (!dstPath.empty())
    {
        if (!exists(dstPath))
        {
            Logger::Get()->error("Destination doesn't exists");
            return -1;
        }

        if (!is_directory(dstPath))
        {
            Logger::Get()->error("Destination is not a valid directory");
            return -1;
        }
    }

    runEnvMap = true;
    return 0;
}

int main(int argc, char* argv[]) {

    argparse::ArgumentParser program("EnvMapGen");
    {
        program.add_argument("-s").help("source image path").default_value("");
        program.add_argument("-d").help("image output destination").default_value("");
        program.add_argument("-r").help("face size for output image, derived from source if not set").default_value(int(DefaultIrradianceMapFaceSize)).scan<'i', int>();
        program.add_argument("-n").help("number of samples for importance sampling").default_value(int(DefaultRadianceMapSamples)).scan<'i', int>();
        program.add_argument("-bl").help("additionally create a Unreal split sum brdf lut in the destination").default_value(false).implicit_value(true);
        program.add_argument("-bs").help("specify generated brdf lut's size, recommanded size [ 512 | 256 | 128 | 64 ]").default_value(int(DefaultBRDFLutSize)).scan<'i', int>();
        program.add_argument("-bf").help("specify generated brdf lut's format, available format [ R32G32F | R16G16F | R8G8 ]").default_value(DefaultBRDFLutFormat);
        
        //! [TODO] Add custom output type and layout in the future, for now it's only .dds and pre convolved cubemap
    }

    Logger* logger = Logger::CreateDefault();
    Logger::Register(logger);

    try 
    {
        program.parse_args(argc, argv);
    }
    catch (const std::runtime_error& err)
    {
        Logger::Get()->error(err.what());
        return -1;
    }

    int result = 0;

    bool runLut = program.get<bool>("-bl");
    bool runEnvMap = false;
   
    std::filesystem::path srcPath = program.get<std::string>("-s");
    std::filesystem::path dstPath = program.get<std::string>("-d");

    result += ProcessPath(srcPath, dstPath, runEnvMap, runLut);

    if (runEnvMap)
    {
        uint32_t size = static_cast<uint32_t>(program.get<int>("-r"));
        uint32_t sample = static_cast<uint32_t>(program.get<int>("-n"));
        result += ProcessEnvMap(srcPath, dstPath, size, sample);
    }
    
    if (runLut)
    {
        uint32_t size = static_cast<uint32_t>(program.get<int>("-bs"));
        std::string format = program.get<std::string>("-bf");
        result += ProcessBRDFLut(dstPath, size, format);
    }

    Logger::Unregister();
    delete logger;

    return 0;
}
