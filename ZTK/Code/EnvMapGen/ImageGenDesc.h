//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

namespace ZTK
{
	//! Parent class for all image generation descriptor used by EnvMapGenerator
	struct EImageGenDesc
	{
	};
} // namespace ZTK