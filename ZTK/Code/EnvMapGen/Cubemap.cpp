//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#include <Cubemap.h>

#include <Logger.h>
#include <Backend/CCubeMapProcessor.h>

#include <IPixelOp.h>

#include <algorithm>
#include <cmath>
#include <thread>
#include <chrono>
#include <future>

namespace ZTK
{
    using namespace Image;

    ECubemapLayout ECubemap::s_layoutList[CubemapLayoutType_Count];

    template <class TInteger>
    inline bool IsPowerOfTwo(TInteger x)
    {
        return (x & (x - 1)) == 0;
    }

    ECubemapLayout::ECubemapLayout()
        : m_type(CubemapLayoutType_Count)
        , m_rows(0)
        , m_columns(0)
    {
    }

    void ECubemapLayout::SetFaceInfo(ECubemapFace face, uint8_t row, uint8_t column, ECubemapFaceRotation rotation)
    {
        m_faceInfos[size_t(face)].row = row;
        m_faceInfos[size_t(face)].column = column;
        m_faceInfos[size_t(face)].rotation = rotation;
    }

    void ECubemap::InitCubemapLayouts()
    {
        //! Assumes right handed z-up, -y forword coodinate system

        using R = ECubemapFaceRotation;

        //! Horizontal:
        //!     left , right, front, back, top, bottom;
        //! 
        //! Left: rotated left (clockwise) 90 degree. Right: rotated right (counter clock wise) 90 degree
        //! The direction matters when using it as input for Cubemap generation filter.
        ECubemapLayout* layout = &s_layoutList[CubemapLayoutType_Horizontal];
        layout->m_rows = 1;
        layout->m_columns = 6;
        layout->m_type = CubemapLayoutType_Horizontal;
        layout->SetFaceInfo(CubemapFace_Left,     0, 0, R::RotateLeftCW90);
        layout->SetFaceInfo(CubemapFace_Right,    0, 1, R::RotateRightCCW90);
        layout->SetFaceInfo(CubemapFace_Front,    0, 2, R::Rotate180);
        layout->SetFaceInfo(CubemapFace_Back,     0, 3, R::None);
        layout->SetFaceInfo(CubemapFace_Top,      0, 4, R::Rotate180);
        layout->SetFaceInfo(CubemapFace_Bottom,   0, 5, R::None);

        //!  HorizontalCross
        //!       top
        //!  left front  right back
        //!       bottom
        layout = &s_layoutList[CubemapLayoutType_HorizontalCross];
        layout->m_rows = 3;
        layout->m_columns = 4;
        layout->m_type = CubemapLayoutType_HorizontalCross;
        layout->SetFaceInfo(CubemapFace_Left,     1, 0, R::None);
        layout->SetFaceInfo(CubemapFace_Right,    1, 2, R::None);
        layout->SetFaceInfo(CubemapFace_Front,    1, 1, R::None);
        layout->SetFaceInfo(CubemapFace_Back,     1, 3, R::None);
        layout->SetFaceInfo(CubemapFace_Top,      0, 1, R::None);
        layout->SetFaceInfo(CubemapFace_Bottom,   2, 1, R::None);

        //! CubemapLayoutVerticalCross
        //!        top
        //!   left front  right
        //!        bottom
        //!        back
        layout = &s_layoutList[CubemapLayoutType_VerticalCross];
        layout->m_rows = 4;
        layout->m_columns = 3;
        layout->m_type = CubemapLayoutType_VerticalCross;
        layout->SetFaceInfo(CubemapFace_Left,     1, 0, R::None);
        layout->SetFaceInfo(CubemapFace_Right,    1, 2, R::None);
        layout->SetFaceInfo(CubemapFace_Front,    1, 1, R::None);
        layout->SetFaceInfo(CubemapFace_Back,     3, 1, R::Rotate180);
        layout->SetFaceInfo(CubemapFace_Top,      0, 1, R::None);
        layout->SetFaceInfo(CubemapFace_Bottom,   2, 1, R::None);

        //! CubemapLayoutVertical
        //!        left
        //!        right
        //!        front
        //!        back
        //!        top
        //!        bottom
        layout = &s_layoutList[CubemapLayoutType_Vertical];
        layout->m_rows = 6;
        layout->m_columns = 1;
        layout->m_type = CubemapLayoutType_Vertical;
        layout->SetFaceInfo(CubemapFace_Left,     0, 0, R::RotateLeftCW90);
        layout->SetFaceInfo(CubemapFace_Right,    1, 0, R::RotateRightCCW90);
        layout->SetFaceInfo(CubemapFace_Front,    2, 0, R::Rotate180);
        layout->SetFaceInfo(CubemapFace_Back,     3, 0, R::None);
        layout->SetFaceInfo(CubemapFace_Top,      4, 0, R::Rotate180);
        layout->SetFaceInfo(CubemapFace_Bottom,   5, 0, R::None);
    }

    /////////////////////
    /// CubemapLayout ///
    /////////////////////

    ECubemap::ECubemap()
        : m_layout(nullptr)
        , m_image(nullptr)
        , m_faceSize(256)
    {
    }

    ECubemap* ECubemap::Create(const IImagePtr& image)
    {
        //! Only support uncompressed format.
        if (!IsPixelFormatUncompressed(image->GetPixelFormat()))
        {
            assert(false && "CubemapLayout only support uncompressed image");
            return nullptr;
        }

        ECubemap* cubemap = nullptr;
        ECubemapLayout* layout = GetCubemapLayout(image);
        if (layout)
        {
            cubemap = new ECubemap();
            cubemap->m_layout = layout;
            cubemap->m_image = image;
            cubemap->m_faceSize = image->GetWidth(0) / cubemap->m_layout->m_columns;
        }
        return cubemap;
    }

    ECubemapLayout* ECubemap::GetCubemapLayout(ECubemapLayoutType type)
    {
        //! If it's never initialized
        if (s_layoutList[0].m_type == CubemapLayoutType_Count)
        {
            InitCubemapLayouts();
        }

        return &s_layoutList[type];
    }

    ECubemapLayout* ECubemap::GetCubemapLayout(const IImagePtr& image)
    {
        //! If it's never initialized
        if (s_layoutList[0].m_type == CubemapLayoutType_Count)
        {
            InitCubemapLayouts();
        }

        if (image == nullptr)
        {
            return nullptr;
        }

        uint32_t width, height;
        width = image->GetWidth(0);
        height = image->GetHeight(0);
        ECubemapLayout* layout = nullptr;

        for (int i = 0; i < CubemapLayoutType_Count; i++)
        {
            if (width * s_layoutList[i].m_rows == height * s_layoutList[i].m_columns)
            {
                layout = &s_layoutList[i];

                //! We require the face size need to be power of two
                if (IsPowerOfTwo(width / layout->m_columns))
                {
                    return layout;
                }
                else
                {
                    assert(false && "Target face size not power of two, invalid input dimension");
                    return nullptr;
                }
            }
        }
        return nullptr;
    }

    //! Public functions to get faces information for associated image
    uint32_t ECubemap::GetFaceSize()
    {
        return m_faceSize;
    }

    ECubemapLayout* ECubemap::GetLayout()
    {
        return m_layout;
    }

    ECubemapFaceRotation ECubemap::GetFaceRotation(ECubemapFace face)
    {
        return m_layout->m_faceInfos[face].rotation;
    }

    void ECubemap::GetFaceData(ECubemapFace face, void* outBuffer, uint32_t& outSize)
    {
        //! Only valid for uncompressed
        uint32_t sizePerPixel = GetPixelFormatInfo(m_image->GetPixelFormat()).m_bitsPerBlock / 8;

        uint8_t* mem;
        uint32_t pitch;
        m_image->GetImagePointer(0, mem, pitch);

        uint8_t* dstBuf = (uint8_t*)outBuffer;

        uint32_t startX = m_layout->m_faceInfos[face].column * m_faceSize;
        uint32_t startY = m_layout->m_faceInfos[face].row * m_faceSize;

        //! Face size is same as rows for uncompressed format
        for (uint32_t y = 0; y < m_faceSize; y++)
        {
            uint32_t scanlineSize = m_faceSize * sizePerPixel;
            uint8_t* srcBuf = &mem[(startY + y) * pitch + startX * sizePerPixel];
            memcpy(dstBuf, srcBuf, scanlineSize);
            dstBuf += scanlineSize;
        }

        outSize = m_faceSize * m_faceSize * sizePerPixel;
    }

    void ECubemap::SetFaceData(ECubemapFace face, void* dataBuffer, uint32_t dataSize)
    {
        //! Only valid for uncompressed
        uint32_t sizePerPixel = GetPixelFormatInfo(m_image->GetPixelFormat()).m_bitsPerBlock / 8;

        uint8_t* mem;
        uint32_t pitch;
        m_image->GetImagePointer(0, mem, pitch);
        uint8_t* srcBuf = (uint8_t*)dataBuffer;

        uint32_t startX = m_layout->m_faceInfos[face].column * m_faceSize;
        uint32_t startY = m_layout->m_faceInfos[face].row * m_faceSize;

        //! Face size is same as rows for uncompressed format
        for (uint32_t y = 0; y < m_faceSize; y++)
        {
            uint32_t scanlineSize = m_faceSize * sizePerPixel;
            uint8_t* dstBuf = &mem[(startY + y) * pitch + startX * sizePerPixel];
            memcpy(dstBuf, srcBuf, scanlineSize);
            srcBuf += scanlineSize;
        }
    }

    void* ECubemap::GetFaceMemBuffer(uint32_t mip, ECubemapFace face, uint32_t& outPitch)
    {
        if (CubemapLayoutType_Vertical != m_layout->m_type)
        {
            assert(false && "this should only be used for CubemapLayoutVertical which has continuous memory for each face");
            return nullptr;
        }

        uint32_t faceSize = m_faceSize >> mip;
        uint8_t* mem;
        m_image->GetImagePointer(mip, mem, outPitch);
        uint32_t startY = m_layout->m_faceInfos[face].row * faceSize;

        //! Use startY is same as rows from m_image since the pixel format is uncompressed
        return &mem[startY * outPitch];
    }

    void ECubemap::SetToFaceMemBuffer(uint32_t mip, ECubemapFace face, void* dataBuffer)
    {
        if (CubemapLayoutType_Vertical != m_layout->m_type)
        {
            assert(false && "this should only be used for CubemapLayoutVertical which has continuous memory for each face");
            return;
        }

        uint32_t faceSize = m_faceSize >> mip;
        uint32_t pitch;
        uint8_t* mem;
        m_image->GetImagePointer(mip, mem, pitch);
        uint32_t startY = m_layout->m_faceInfos[face].row * faceSize;

        //! Use startY is same as rows from m_image since the pixel format is uncompressed
        memcpy(&mem[startY * pitch], dataBuffer, faceSize * pitch);
    }

    bool IsValidLatLongMap(const IImagePtr& image)
    {
        uint32_t width = image->GetWidth(0);
        uint32_t height = image->GetHeight(0);
        return (width == height * 2 && width % 4 == 0);
    }

    //! Get unit direction a cubemap pixel points to from surface uv and cubemap surface index
    void GetDirForVerticalLayoutUV(ECubemapFace face, float u, float v, float outDir[3])
    {
        switch (face)
        {
        case CubemapFace_Left:
            outDir[0] = -1;
            outDir[1] = -u;
            outDir[2] =  v;
            break;
        case CubemapFace_Right:
            outDir[0] =  1;
            outDir[1] =  u;
            outDir[2] =  v;
            break;
        case CubemapFace_Front:
            outDir[0] = -u;
            outDir[1] = -v;
            outDir[2] =  1;
            break;
        case CubemapFace_Back:
            outDir[0] = -u;
            outDir[1] = v;
            outDir[2] = -1;
            break;
        case CubemapFace_Top:
            outDir[0] = -u;
            outDir[1] = 1;
            outDir[2] = v;
            break;
        case CubemapFace_Bottom:
            outDir[0] = u;
            outDir[1] = -1;
            outDir[2] =  v;
            break;
        }

        //! Normalize
        const float length = std::sqrt(outDir[0] * outDir[0] + outDir[1] * outDir[1] + outDir[2] * outDir[2]);
        outDir[0] /= length;
        outDir[1] /= length;
        outDir[2] /= length;
    }

    //! Convert a world space unit direction into (u,v) coordinates of latitude-longitude map
    void DirToLatLongUV(const float inDir[3], float& outU, float& outV)
    {
        //! The unit direction we calculate from cubemap is left handed z up. +y forward
        float r = std::sqrt(inDir[0] * inDir[0] + inDir[1] * inDir[1]);

        float latitude = std::asin(inDir[2]);
        float longitude = std::atan2(inDir[0], inDir[1]);

        //! Longitude [-Pi, Pi] -> [0, 1]
        outU = 1.0f - (longitude * 0.159154943f + 0.5f); 

        //! Latitude [Pi/2, -Pi/2] -> [0, 1]
        outV = 0.5f - latitude * 0.318309886f; 
    }

    IImagePtr ConvertLatLongMapToCubemap(const IImagePtr& image)
    {
        const IPixelFormat srcFormat = image->GetPixelFormat();

        //! The map need to be uncompressed format
        if (!IsPixelFormatUncompressed(srcFormat))
        {
            Logger::Get()->error("The input image must have uncompressed pixel format.");
            return nullptr;
        }

        if (image->GetMipCount() > 1)
        {
            Logger::Get()->warn("Source image have [{}] mips, only mip 0 will be converted", image->GetMipCount());
        }
        
        uint32_t srcWidth = image->GetWidth(0);
        uint32_t srcHeight = image->GetHeight(0);
        uint8_t* srcBuf;
        uint32_t outPitch;
        image->GetImagePointer(0, srcBuf, outPitch);

        if (!IsValidLatLongMap(image))
        {
            Logger::Get()->error(
                "Invalid latlong resulution [{}:{}], the aspect ratio shoud be 2:1 and the width should be multiple of 4", srcWidth, srcHeight);
            return nullptr;
        }

        //! This is how latlong map defined, the longtitude of latlong map is essentially 4 reprojected continuous cubemap faces.
        //! If you want custom output face size please be aware you need proper upsampling/downsampling scheme for generation
        uint32_t srcfaceSize = srcWidth / 4;

        //! Convert face size to power of 2 since CreateCubemapLayout doesn't support non-power of 2 face size.
        //! Find the highest power of 2 less than or equal to source face size
        srcfaceSize >>= 1;
        uint32_t faceSize = 1;
        while (srcfaceSize > 0)
        {
            faceSize <<= 1;
            srcfaceSize >>= 1;
        }
        assert(faceSize <= srcWidth / 4 && "Wrong conversion");

        //! Create output image
        IImagePtr outImage = IImage::Create(faceSize, faceSize * CubemapFace_Count, 1, srcFormat);
        outImage->SetImageFlags(image->GetImageFlags());
        outImage->AddImageFlags(IFlag::Cubemap);

        ECubemap* dstCubemap = ECubemap::Create(outImage);

        //! Only valid for uncompressed
        uint32_t sizePerPixel = GetPixelFormatInfo(srcFormat).m_bitsPerBlock / 8u;
        uint32_t facePixelCount = faceSize * faceSize;
        float radius = faceSize / 2.0f;

        Logger::Get()->info("Converting source image to cubemap:");

        for (uint32_t faceIdx = 0; faceIdx < CubemapFace_Count; faceIdx++)
        {
            ECubemapFace face = (ECubemapFace)faceIdx;
            uint8_t* buf = (uint8_t*)dstCubemap->GetFaceMemBuffer(0, face, outPitch);

            //! Get value from the original map and assign it to each pixel on this face
            for (uint32_t pixelIdx = 0; pixelIdx < facePixelCount; pixelIdx++)
            {
                //! Cubemap destination uv, origin is image center, v axis flipped to up
                float dstU = ((pixelIdx % faceSize) - radius) / radius;
                float dstV = -((pixelIdx / faceSize) - radius) / radius;

                float dir[3]{ 0.0f, 0.0f, 0.0f };
                GetDirForVerticalLayoutUV(face, dstU, dstV, dir);

                float srcU, srcV;
                DirToLatLongUV(dir, srcU, srcV);

                //! Get 4 corner pixels' color and use them to interpolate the final color of destination pixel
                float px = srcU * (float)(srcWidth - 1);
                float py = srcV * (float)(srcHeight - 1);
                uint32_t px1 = (uint32_t)px;
                uint32_t px2 = ((uint32_t)px + 1) % srcWidth;
                uint32_t py1 = (uint32_t)py;
                uint32_t py2 = ((uint32_t)py + 1) % srcHeight;
                float t1 = px - px1;
                float t2 = py - py1;

                float p1[4], p2[4], p3[4], p4[4];
                IPixelOp::GetPixel(&srcBuf[(px1 + py1 * srcWidth) * sizePerPixel], srcFormat, p1[0], p1[1], p1[2], p1[3]);
                IPixelOp::GetPixel(&srcBuf[(px1 + py2 * srcWidth) * sizePerPixel], srcFormat, p2[0], p2[1], p2[2], p2[3]);
                IPixelOp::GetPixel(&srcBuf[(px2 + py1 * srcWidth) * sizePerPixel], srcFormat, p3[0], p3[1], p3[2], p3[3]);
                IPixelOp::GetPixel(&srcBuf[(px2 + py2 * srcWidth) * sizePerPixel], srcFormat, p4[0], p4[1], p4[2], p4[3]);

                //! Bilateral filter
                float dstP[4];
                dstP[0] = (1 - t2) * ((1 - t1) * p1[0] + t1 * p3[0]) + t2 * ((1 - t1) * p2[0] + t1 * p4[0]);
                dstP[1] = (1 - t2) * ((1 - t1) * p1[1] + t1 * p3[1]) + t2 * ((1 - t1) * p2[1] + t1 * p4[1]);
                dstP[2] = (1 - t2) * ((1 - t1) * p1[2] + t1 * p3[2]) + t2 * ((1 - t1) * p2[2] + t1 * p4[2]);
                dstP[3] = (1 - t2) * ((1 - t1) * p1[3] + t1 * p3[3]) + t2 * ((1 - t1) * p2[3] + t1 * p4[3]);

                IPixelOp::SetPixel(&buf[pixelIdx * sizePerPixel], srcFormat, dstP[0], dstP[1], dstP[2], dstP[3]);
            }
        }

        //! This will not release underlying memory owned by outImage
        delete dstCubemap;
        return outImage;
    }

    //! Return a 4 component array [m00, m01, m10, m11] which forms a 2x2 rotation matrix
    //! | m00, m01 |
    //! | m10, m11 |
    void GetTransform(ECubemapFaceRotation rotation, bool isInvert, float out[4])
    {
        switch (rotation)
        {
        case ECubemapFaceRotation::None:

            //! Identity
            out[0] = 1; out[1] = 0;
            out[2] = 0; out[3] = 1;
            break;
        case ECubemapFaceRotation::RotateLeftCW90:

            //! Theta = 90 degree
            //! | cosTheta, -sinTheta |
            //! | sinTheta,  cosTheta |
            if (isInvert)
            {
                GetTransform(ECubemapFaceRotation::RotateRightCCW90, false, out);
            }
            else
            {
                out[0] = 0; out[1] = -1;
                out[2] = 1; out[3] = 0;
            }
            break;
        case ECubemapFaceRotation::RotateRightCCW90:
            //! Theta = -90 degree
            if (isInvert)
            {
                GetTransform(ECubemapFaceRotation::RotateLeftCW90, false, out);
            }
            else
            {
                out[0] =  0; out[1] = 1;
                out[2] = -1; out[3] = 0;
            }
            break;
        case ECubemapFaceRotation::Rotate180:
            //! Theta = 180 degree
            out[0] = -1; out[1] =  0;
            out[2] =  0; out[3] = -1;
            break;
        case ECubemapFaceRotation::MirrorHorizontal:
            out[0] = 1; out[1] =  0;
            out[2] = 0; out[3] = -1;
            break;
        default:
            assert(false && "Unimplemented");
            break;
        }
    }

    void TransformImage(
        ECubemapFaceRotation srcDir, ECubemapFaceRotation dstDir,
        const uint8_t* srcImageBuf, uint8_t* dstImageBuf,
        uint32_t bytesPerPixel, uint32_t faceSize)
    {
        //! Get final matrix to transform dst back to src
        float m1[4] =
        {
            1, 0,
            0, 1
        };

        float m2[4] =
        {
            1, 0, 
            0, 1
        };

        GetTransform(dstDir, true, m1);
        GetTransform(srcDir, false, m2);

        float mtx[4];
        mtx[0] = m1[0] * m2[0] + m1[1] * m2[2];
        mtx[1] = m1[0] * m2[1] + m1[1] * m2[3];
        mtx[2] = m1[2] * m2[0] + m1[3] * m2[2];
        mtx[3] = m1[2] * m2[1] + m1[3] * m2[3];

        float noOp[4];
        GetTransform(ECubemapFaceRotation::None, false, noOp);

        if (memcmp(noOp, mtx, 4 * sizeof(float)) == 0)
        {
            memcpy(dstImageBuf, srcImageBuf, faceSize * faceSize * bytesPerPixel);
            return;
        }

        //! For each pixel in dst image, find it's location in src and copy the data from there
        float halfSize = static_cast<float>(faceSize / 2);
        for (uint32_t row = 0; row < faceSize; row++)
        {
            for (uint32_t col = 0; col < faceSize; col++)
            {
                //! Coordinate in image center as origin and right as positive X, up as positive Y
                float dstX = col + 0.5f - halfSize;
                float dstY = halfSize - row - 0.5f;
                float srcX = dstX * mtx[0] + dstY * mtx[1];
                float srcY = dstX * mtx[2] + dstY * mtx[3];
                uint32_t srcCol = static_cast<uint32_t>(srcX + halfSize);
                uint32_t srcRow = static_cast<uint32_t>(halfSize - srcY);

                memcpy(&dstImageBuf[(row * faceSize + col) * bytesPerPixel],
                    &srcImageBuf[(srcRow * faceSize + srcCol) * bytesPerPixel], bytesPerPixel);
            }
        }
    }

    Image::IImagePtr ConvertImageToCubemap(const Image::IImagePtr& image, EInputLayoutType srcLayoutType, ECubemapLayoutType dstLayoutType)
    {
        const IPixelFormat srcPixelFormat = image->GetPixelFormat();

        //! It need to be uncompressed format
        if (!IsPixelFormatUncompressed(srcPixelFormat))
        {
            Logger::Get()->error("Compressed format not supported");
            return nullptr;
        }

        //! Convert input image to cubemap first
        Image::IImagePtr src = nullptr;
        {
            switch (srcLayoutType)
            {
            case InputLayoutType_LatLong:
                src = ConvertLatLongMapToCubemap(image);
                break;
            case InputLayoutType_Cubemap:
                break;
            default:
                Logger::Get()->error("Conversion to cubemap unimplemented ");
                return nullptr;
            }
        }
        
        //! Check if it's valid cubemap size
        ECubemapLayout* srclayout = ECubemap::GetCubemapLayout(src);
        if (srclayout == nullptr)
        {
            Logger::Get()->error("Source image size (layout) incompatible with cubemap");
            return nullptr;
        }

        //! If the source is same as output layout, return directly
        if (srclayout->m_type == dstLayoutType)
        {
            return src;
        }

        ECubemapLayout* dstLayout = ECubemap::GetCubemapLayout(dstLayoutType);

        //! Create cubemap wrapper for converted source image for later processing.
        ECubemap* srcCubemap = ECubemap::Create(src);
        uint32_t faceSize = srcCubemap->GetFaceSize();

        //! Create new image with same pixel format and flags from source image
        Image::IImagePtr dst = IImage::Create(faceSize * dstLayout->m_columns, faceSize * dstLayout->m_rows, 1, srcPixelFormat);
        ECubemap* dstCubemap = ECubemap::Create(dst);
        dst->SetImageFlags(src->GetImageFlags());

        //! Copy data from src cube to dst cube for each face
        //! temp buf for copy over data
        uint32_t sizePerPixel = GetPixelFormatInfo(srcPixelFormat).m_bitsPerBlock / 8u; //! Only valid for uncompressed
        uint8_t* buf = new uint8_t[faceSize * faceSize * sizePerPixel];
        uint8_t* tempBuf = new uint8_t[faceSize * faceSize * sizePerPixel];

        for (uint32_t faceIdx = 0; faceIdx < CubemapFace_Count; faceIdx++)
        {
            uint32_t outSize = 0;
            ECubemapFace face = static_cast<ECubemapFace>(faceIdx);
            srcCubemap->GetFaceData(face, buf, outSize);
            ECubemapFaceRotation srcDir = srcCubemap->GetFaceRotation(face);
            ECubemapFaceRotation dstDir = dstCubemap->GetFaceRotation(face);
            if (srcDir == dstDir)
            {
                dstCubemap->SetFaceData(face, buf, outSize);
            }
            else
            {
                //! Transform image to a face
                TransformImage(srcDir, dstDir, buf, tempBuf, sizePerPixel, faceSize);
                dstCubemap->SetFaceData(face, tempBuf, outSize);
            }
        }

        //! Clean up
        delete[] buf;
        delete[] tempBuf;
        delete srcCubemap;
        delete dstCubemap;

        src->AddImageFlags(IFlag::Cubemap);
        return src;
    }

    IImagePtr GenerateCubemap(const IImagePtr& srcImage, const ECubemapGenDesc& desc)
    {
        const IPixelFormat srcFormat = srcImage->GetPixelFormat();

        //! This function only works with pixel format RGBA32f (because our backend requires, safe to remove if other backend used)
        if (srcFormat != IPixelFormat::R32G32B32A32F)
        {
            Logger::Get()->error("Pixel format must be RGBA32f");
            return nullptr;
        }

        //! Only if the src image has one mip
        if (srcImage->GetMipCount() != 1)
        {
            Logger::Get()->error("Mip count must be 1");
            return nullptr;
        }

        ECubemap* srcCubemap = ECubemap::Create(srcImage);
        if (!srcCubemap)
        {
            Logger::Get()->error(
                "Input srcImage is not a valid cubemap, call ConvertImageToCubemap to convert your input image first if desired");
            return nullptr;
        }

        uint32_t srcFaceSize = srcCubemap->GetFaceSize();
        uint32_t dstFaceSize = srcFaceSize;
        if (desc.m_faceSizeOverride)
        {
            if (IsPowerOfTwo(desc.m_faceSizeOverride))
            {
                dstFaceSize = desc.m_faceSizeOverride;
            }
            else
            {
                Logger::Get()->error("Target cubemap face size must be power of 2");
                return nullptr;
            }
        }

        //! Get final cubemap image size
        uint32_t outWidth = dstFaceSize * srcCubemap->GetLayout()->m_columns;
        uint32_t outHeight = dstFaceSize * srcCubemap->GetLayout()->m_rows;

        //! Get max mipmap count
        uint32_t maxMipCount = 1u;
        if (desc.m_mipmap)
        {
            //! Calculate based on face size, and use final export format which may save some low level mip calculation
            maxMipCount = EvaluateImageMaxMipCount(srcFormat, dstFaceSize, dstFaceSize);

            //! The FilterImage function won't do well with rect size 1. avoiding cubemap with face size 1
            if ((srcFaceSize >> (maxMipCount - 1)) == 1 && maxMipCount > 1)
            {
                maxMipCount -= 1;
            }
        }

        //! Generate box filtered source image mip chain
        IImagePtr mippedSrcImage = IImage::Create(outWidth, outHeight, maxMipCount, srcFormat);
        mippedSrcImage->SetImageFlags(srcImage->GetImageFlags());

        Logger::Get()->info("Computing mips");
        for (int faceIdx = 0; faceIdx < CubemapFace_Count; ++faceIdx)
        {
            for (int mipIdx = 0; mipIdx < (int)maxMipCount; ++mipIdx)
            {
                float srcSize = static_cast<float>(srcFaceSize);

                //! Texture space scope, assume top left origin
                float srcRect[4] = {
                    0,                  srcSize, /*x axis start and end*/
                    faceIdx * srcSize,  (faceIdx + 1) * srcSize /*y axis start and end*/
                };

                uint32_t mipFaceSize = dstFaceSize >> mipIdx;
                float dstSize = static_cast<float>(mipFaceSize);

                float dstRect[4] = {
                    0,                  dstSize,
                    faceIdx * dstSize,  (faceIdx + 1) * dstSize
                };

                int mipGenType = (mipIdx == 0 ? eWindowFunction_Point : eWindowFunction_Box);
                FilterImage(mipGenType, eWindowEvaluation_Sum, 0, 0, srcImage, 0, mippedSrcImage, mipIdx, srcRect, dstRect);
            }
        }

        //! Replace the source cubemap with the mipped version
        delete srcCubemap;
        srcCubemap = ECubemap::Create(mippedSrcImage);

        //! Create new new output image with proper face
        IImagePtr outImage = IImage::Create(outWidth, outHeight, maxMipCount, srcFormat);
        outImage->SetImageFlags(srcImage->GetImageFlags());

        ECubemap* dstCubemap = ECubemap::Create(outImage);
        uint32_t dstMipCount = outImage->GetMipCount();

        Logger::Get()->info("Adjusting size and orientation");

        //! Filter mip 0 from source to destination
        for (int faceIdx = 0; faceIdx < CubemapFace_Count; ++faceIdx)
        {
            float srcSize = static_cast<float>(srcFaceSize);

            float srcRect[4] = {
                0,                  srcSize,
                faceIdx * srcSize,  (faceIdx + 1) * srcSize
            };

            float dstSize = static_cast<float>(dstFaceSize);

            float dstRect[4] = {
                0,                  dstSize,
                faceIdx * dstSize,  (faceIdx + 1) * dstSize
            };

            FilterImage(desc.m_firParams.m_mipFilerFunc, desc.m_firParams.m_mipFilterOp, 0, 0, srcImage, 0,
                outImage, 0, srcRect, dstRect);
        }

        CCubeMapProcessor  atiCubemapGen;
        //! ATI's cubemap generator to filter the image edges to avoid seam problem
        //! https://gpuopen.com/archive/gamescgi/cubemapgen/

        //! The thread support was done with windows thread function so it's removed for multi-dev platform support
        atiCubemapGen.m_NumFilterThreads = 0;

        //! Input and output cubemap set to have save dimensions
        atiCubemapGen.Init(dstFaceSize, dstFaceSize, dstMipCount, 4);

        //! Load the 6 faces of the input cubemap for each mip level into the cubemap processor
        void* pMem;
        uint32_t nPitch;
        uint32_t numChannel = GetPixelFormatInfo(srcFormat).m_channels;

        //! Init input data
        for (int faceIdx = 0; faceIdx < 6; ++faceIdx)
        {
            for (int mipIdx = 0; mipIdx < (int)maxMipCount; ++mipIdx)
            {
                pMem = srcCubemap->GetFaceMemBuffer(mipIdx, static_cast<ECubemapFace>(faceIdx), nPitch);

                atiCubemapGen.SetInputFaceData(
                    faceIdx,                    //! FaceIdx,
                    mipIdx,                     //! MipIdx
                    CP_VAL_FLOAT32,             //! SrcType,
                    numChannel,                 //! SrcNumChannels,
                    nPitch,                     //! SrcPitch,
                    pMem,                       //! SrcDataPtr,
                    1000000.0f,                 //! MaxClamp,
                    desc.m_cmgParams.m_degama,  //! Degamma,
                    desc.m_cmgParams.m_intensityScale); //! Scale
            }
        }

        {
            Logger::Get()->info("Filter process initiated");
            Logger::StopWatchClick();

            //! Convolve cubemap
            atiCubemapGen.InitiateFiltering(
                desc.m_cmgParams.m_baseFilterAngle,             //! BaseFilterAngle,
                desc.m_cmgParams.m_initialMipAngle,             //! InitialMipAngle,
                desc.m_cmgParams.m_mipAnglePerLevelScale,       //! MipAnglePerLevelScale,
                (int)desc.m_cmgParams.m_filter,                 //! FilterType, CP_FILTER_TYPE_COSINE for diffuse cube
                desc.m_cmgParams.m_edgeFixupWidth > 0 ? CP_FIXUP_PULL_LINEAR : CP_FIXUP_NONE, //! FixupType, CP_FIXUP_PULL_LINEAR if FixupWidth> 0
                static_cast<int32_t>(desc.m_cmgParams.m_edgeFixupWidth), //! FixupWidth,
                true,                                           //! bUseSolidAngle,
                16.0f,                                          //! GlossScale, only used by CP_FILTER_TYPE_COSINE_POWER
                0.0f,                                           //! GlossBias, only used by CP_FILTER_TYPE_COSINE_POWER
                desc.m_cmgParams.m_samples);

            Logger::Get()->info("Filter process completed, time elapsed [{:.3} seconds]", Logger::StopWatchClick());
        }
        
        //! Copy the convolved cubemap data for each face and mip into the output image
        for (int faceIdx = 0; faceIdx < 6; ++faceIdx)
        {
            for (unsigned int dstMip = 0; dstMip < dstMipCount; ++dstMip)
            {
                pMem = dstCubemap->GetFaceMemBuffer(dstMip, (ECubemapFace)faceIdx, nPitch);
                atiCubemapGen.GetOutputFaceData(faceIdx, dstMip, CP_VAL_FLOAT32, 4, nPitch, pMem, 1.0f, 1.0f);
            }
        }

        delete srcCubemap;
        delete dstCubemap;

        Logger::Get()->info("Generated cubemap info [Width {}, Height {}, Face Size {}, MipCount {}]", 
            outImage->GetWidth(0), outImage->GetHeight(0), dstFaceSize, maxMipCount);

        return outImage;
    }

    //! Input : { Roughness, N dot V }
    //! Output : { Scale, Bias } for rearranged Schlick's approximation: F(v, h) = F0 * Scale + Bias
    void ConvolveBRDF(float roughness, float NdV, float out[2])
    {
        auto dot = [](float a[3], float b[3])->float { return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]; };
        auto saturate = [](float i)->float { return std::clamp(i, 0.0f, 1.0f); };

//! Height Correlated G for GGX Titan fall 2017, also 2013 Frostbite
#if 1
        auto HeightCorrelatedSmithG = [&](float a, float NdV, float NdL)->float
        {
            float a2 = a * a;
            float lambdaL = 0.5f * (std::sqrt(a2 + (1.0f - a2) * (NdL * NdL)) / NdL - 1.0f);
            float lambdaV = 0.5f * (std::sqrt(a2 + (1.0f - a2) * (NdV * NdV)) / NdV - 1.0f);
            
            return 1.0 / (1.0 + lambdaL + lambdaV);
        };

#endif

//! Unreal's Smith G, use it to get same result as Unreal 2013's model
#if 0
        //! One factor Smith shadowing factor approximation for GGX 
        //! Note: you have to use other equation if NDF is not GGX
        auto SmithG1 = [](float a, float NdV)->float
        {
            //! Unreal's k = (a + 1) ^ 2 / 8 term (from Disney) are only used for analytical light rather than IBL
            float k = 0.5f * a;
            float g = NdV / (NdV * (1.0f - k) + k);
            return g;
        };

        auto SmithG = [&](float a, float NdV, float NdL)->float
        {
            //! Self shadowing factor view from light source
            float GL = SmithG1(a, NdL);

            //! Self shadowing factor view from observer
            float GV = SmithG1(a, NdV);

            return GL * GV;
        };
#endif 

        //! Get tangent space half vector from sample
        auto ImportanceSampleGGX = [](float u0, float u1, float a, float out[3])
        {
            float phi = 2.0f * M_PI * u0;
            float cosTheta = std::sqrtf((1.0f - u1) / (1.0f + (a * a - 1.0f) * u1));
            float sinTheta = std::sqrtf(1.0f - cosTheta * cosTheta);

            //! +z up, -y forward left handed
            out[0] = sinTheta * std::cosf(phi);
            out[1] = sinTheta * std::sinf(phi);
            out[2] = cosTheta;
        };

        //! Assumes z-up
        float V[3]
        {
            std::sqrt(1.0f - NdV * NdV), //! sinTheta, theta is angle between view dir and normal
            0.0f,
            NdV                          //! cosTheta
        };

        float N[3] { 0.0, 0.0, 1.0 };

        //! Disney convention, convert perceptual roughness to alpha roughness
        float a = roughness * roughness;

        float scale = 0.0f;
        float bias = 0.0f;

        static const uint32_t NumSamples = 1024u;
        for (uint32_t i = 0; i < NumSamples; ++i)
        {
            float u[2];
            float H[3];

            HammersleySequence(i, NumSamples, u);
            ImportanceSampleGGX(u[0], u[1], a, H);

            float VdH = dot(V, H);
            float L[3]
            {
                2 * VdH * H[0] - V[0],
                2 * VdH * H[1] - V[1],
                2 * VdH * H[2] - V[2]
            };

            float NdL = saturate(L[2]);
            float NdH = saturate(H[2]);
                  VdH = saturate(VdH);

            if (NdL > 0.0f)
            {
                float G = HeightCorrelatedSmithG(a, NdV, NdL);

                //! BRDF without F = D(h) * G(v, h) / (4 * NdL * NdV)
                //! Since we draw sample from D(h), but BRDF's PDF is associated with either L and V, so we need to tranform pdf(h) 
                //! to pdf(v) or pdf(l), which is the same because of reciprocity:
                //! PDF w.r.t H = D(h) * NdH;
                //! Jacobian for transforming distribution of H to V = 1 / (4 * VdH) or 1 / (4 * LdH) since VdH = LdH;
                //! PDF w.r.t V = pdf(h) * jacobian = D(h) * NdH / (4 * VdH) 
                //! (BRDF no F * costine term (NdL)) / PDF =  G * VdH (or LdH) / (NdH * NdV)
                float FonP = G * VdH / (NdH * NdV);
                float Fc = std::powf((1.0 - VdH), 5.0f);

                scale += (1.0 - Fc) * FonP;
                bias += (Fc * FonP);
            }
        }

        out[0] = scale / NumSamples;
        out[1] = bias / NumSamples;
    }

    Image::IImagePtr GenerateBRDFLUT(uint32_t size, Image::IPixelFormat outFormat)
    {
        using namespace Image;

        //! Default format for writing to png
        IPixelFormat format = outFormat;
        auto& info = GetPixelFormatInfo(format);

        //! Check format
        if (info.m_channels < 2)
        {
            Logger::Get()->error("The output format needs at least 2 channels");
            return nullptr;
        }

        if (info.m_compressed)
        {
            Logger::Get()->error("Compressed format currently not supported");
            return nullptr;
        }
        
        if (!IsPowerOfTwo(size))
        {
            Logger::Get()->warn("It's recommended to set size to power of 2 for LUT, current size is [{}]", size);
        }

        //! Create data
        IImagePtr image = IImage::Create(size, size, 1, format);

        uint8_t* mem = nullptr;
        uint32_t pitch;
        image->GetImagePointer(0, mem, pitch);

        Logger::Get()->info("Generating LUT");

        //! Only work with uncompressed format
        uint32_t sizePerPixel = info.m_bitsPerBlock / 8u;
        auto ProcessLines = [&](uint32_t yStart, uint32_t yEnd)
        {
            for (uint32_t y = yStart; y < yEnd; ++y)
            {
                //! Reverse y axis for ease of use, texture uv usually uses bottom left as origin.
                //! Add a small epsilon as bias to avoid divided by 0
                float roughness = (1.0f - y / float(size)) + 5e-4f;
                for (uint32_t x = 0; x < size; ++x)
                {
                    //! Cosine of angle between view direction and normal
                    //! This adds approx 0.03 degree bias to the result
                    float NdV = (x % size / float(size)) + 5e-4f;

                    float scaleBias[2];
                    ConvolveBRDF(roughness, NdV, scaleBias);

                    IPixelOp::SetPixel(mem + (y * size + x) * sizePerPixel, format, scaleBias[0], scaleBias[1], 0, 0);
                }
            }
        };

        {
            //! [TODO] Replace with AppUtils::ScanlineProcessor
            using namespace std::chrono_literals;

            struct threadContext
            {
                std::thread m_thread;
                std::future<void> m_future; //! Return value of our task, which is ProcessLines here.
                std::future_status m_status = std::future_status::ready;
            };

            //! No rationale, just randomly set, feel free to change it 
            static const uint32_t NumThreads = 8u;

            //! How many scan lines a single thread task will process
            static const uint32_t LinesPerTask = 2u;

            //! Thread pool and status table (1 to 1)
            std::vector<threadContext> threads(NumThreads);

            //! Consume task until all scanlines processed
            uint32_t taskCount = std::max(1u, size / LinesPerTask);
            uint32_t taskLeft = taskCount;
            uint32_t nextLine = 0u;

            std::mutex counterMutex;
            while (taskLeft != 0)
            {
                for (auto& thread : threads)
                {
                    if (taskLeft == 0)
                    {
                        break;
                    }
                    
                    //! Allocate new task for idle threads
                    if (thread.m_status == std::future_status::ready)
                    {
                        if (thread.m_future.valid())
                        {
                            //! Drop old task
                            thread.m_thread.detach();
                        }
                        
                        //! Get new
                        std::packaged_task<void(uint32_t, uint32_t)> task(ProcessLines);
                        thread.m_future = task.get_future();
                        thread.m_thread = std::thread(std::move(task), nextLine, nextLine + LinesPerTask);

                        //! Update counters
                        {
                            std::lock_guard<std::mutex> lock(counterMutex);
                            nextLine += LinesPerTask;
                            --taskLeft;
                        }
                    }

                    thread.m_status = thread.m_future.wait_for(0ms);
                }
            }

            //! Wait for all threads return
            for (auto& thread : threads)
            {
                if (thread.m_status != std::future_status::ready)
                {
                    thread.m_thread.join();
                }
            }
        }

        return image;
    }

} // namespace ZTK