//
// Copyright 2023-2024 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

#pragma once

#include <ImageGenDesc.h>
#include <Backend/FIR-Windows.h>

#include <Image.h>

#include <cstdint>

namespace ZTK
{
    //! Input layout type for cube map conversion
    enum EInputLayoutType : uint32_t
    {
        InputLayoutType_LatLong = 0, //! Latitude longitude map
        InputLayoutType_Cubemap
    };

    //! Layouts type
    //! Used primarily as array index so use plain enum instead of class
    enum ECubemapLayoutType : uint32_t
    {
        CubemapLayoutType_Horizontal = 0,    //! 6x1 strip. with rotations.
        CubemapLayoutType_HorizontalCross,   //! 4x3.
        CubemapLayoutType_VerticalCross,     //! 3x4
        CubemapLayoutType_Vertical,          //! 1x6 strip. new output format. it's better because the memory is continuous for each face
        CubemapLayoutType_Count
    };

    //! This enum put in order to map ATI's CubemapGen's left handed Y up coordinate system to 
    //! right handed Z up (which used by myself because it align with convention in math). 
    enum ECubemapFace : uint32_t
    {
        CubemapFace_Left = 0,
        CubemapFace_Right,
        CubemapFace_Front,
        CubemapFace_Back,
        CubemapFace_Top,
        CubemapFace_Bottom,
        CubemapFace_Count
    };

    //! Treating the orientation of faces in 4x3 layout as the original direction.
    enum class ECubemapFaceRotation : uint32_t
    {
        None = 0,   //! No rotarion in texture space
        RotateLeftCW90,   //! Rotate left (clock wise) in texture space by 90 degree
        RotateRightCCW90, //! Rotate right (counter clock wise) in texture space by 90 degree
        Rotate180,
        MirrorHorizontal
    };

    //! Please don't alter this enum because it maps to filter type enum of ATI CubeMapGen which used as 
    //! our filtering backend. See CCubeMapProcessor.h for more info
    //! Sampling distribution for pre filtering.
    enum class ECubemapFilter : uint32_t
    {
        Disc = 0,
        Cone = 1,
        Cosine = 2,         //! Cosine distribution for lambertian diffuse surface, irradiance map
        AngularGaussian = 3,
        CosinePower = 4,
        GGX = 5             //! GGX normal distribution for GGX based physically based specular surface, radiance map
    };

    enum class EMipGenType : uint32_t
    {
        point,          //Also called nearest neighbor
        box,            //Also called 'average'. When shrinking images it will average, and merge the pixels together.
        triangle,       //Also called linear or Bartlett window
        quadratic,      //Also called bilinear or Welch window
        gaussian,       //It remove high frequency noise in a highly controllable way.
    };

    //! This class contains information to descript a cubemap layout
    class ECubemapLayout
    {
    public:
        struct FaceInfo
        {
            uint32_t row;
            uint32_t column;
            ECubemapFaceRotation rotation;
        };

        //! Rows and columns of how cubemap's faces laid
        uint32_t m_rows;
        uint32_t m_columns;

        ECubemapLayoutType m_type;

        //! The index of row and column where all the faces located
        FaceInfo m_faceInfos[CubemapFace_Count];

        ECubemapLayout();
        void SetFaceInfo(ECubemapFace face, uint8_t row, uint8_t column, ECubemapFaceRotation rotation);
    };

    //! Wrapper class to help doing operations with faces for an image as cubemap
    class ECubemap
    {
    public:
        //! Create a cubemapLayout object for the image. Return null if input image is not eligible for cubemap conversion
        static ECubemap* Create(const Image::IImagePtr& image);

        //! Get layout for input layout type
        static ECubemapLayout* GetCubemapLayout(ECubemapLayoutType type);

        //! Get layout for input image based on its size
        static ECubemapLayout* GetCubemapLayout(const Image::IImagePtr& image);

        //! Public functions to get faces information for associated image
        uint32_t GetFaceSize();

        ECubemapLayout* GetLayout();

        //! Set/get pixels' data from/to specific face. only works for mip 0
        void GetFaceData(ECubemapFace face, void* outBuffer, uint32_t& outSize);
        void SetFaceData(ECubemapFace face, void* dataBuffer, uint32_t dataSize);

        //! Get the face's direction
        ECubemapFaceRotation GetFaceRotation(ECubemapFace face);

        //! Get memory for a face from Image data. only works for CubemapLayoutVertical since its memory for each face is continuous
        void* GetFaceMemBuffer(uint32_t mip, ECubemapFace face, uint32_t& outPitch);
        void SetToFaceMemBuffer(uint32_t mip, ECubemapFace face, void* dataBuffer);

    private:

        //! Information for all supported cubemap layouts
        static ECubemapLayout s_layoutList[CubemapLayoutType_Count];

        //! The image associated for this CubemapLayout
        Image::IImagePtr m_image;

        //! The layout information of m_image
        ECubemapLayout* m_layout;

        //! The size of the cubemap's face (which is square and power of 2).
        uint32_t m_faceSize;

        //! Private constructor. User should always use CreateCubemapLayout create a layout for an image object
        ECubemap();

        //! Initialize information of all available cubemap layouts
        static void InitCubemapLayouts();
    };

    //! Image filter function
    void FilterImage(int filterIndex, int filterOp, float blurH, float blurV, const Image::IImagePtr& srcImg, int srcMip,
        Image::IImagePtr dstImg, int dstMip, float srcRect[4], float dstRect[4]);

    bool IsValidLatLongMap(const Image::IImagePtr& image);
    Image::IImagePtr ConvertLatLongMapToCubemap(const Image::IImagePtr& image);
    Image::IImagePtr ConvertImageToCubemap(const Image::IImagePtr& image, EInputLayoutType srcLayoutType, ECubemapLayoutType dstLayoutType);

    struct FIRParam
    {
        //! Filter function to generate mipmaps
        EWindowFunction m_mipFilerFunc = eWindowFunction_BlackmanHarris;

        //! Filter operation to downsample 2x2 block, sum - average sum, min - minima, max - maxima
        EWindowEvaluation m_mipFilterOp = eWindowEvaluation_Sum;
    };

    struct CubeMapGenParam
    {
        //! Original gamma level of input image for ATI's CubeMapGen backend to undo by degamma
        //! 1.0 - srgb to linear conversion required
        float m_degama = 1.0;

        //! Scale to apply to pixel values after degamma (in linear space)
        //! 1.0 - no scale is applied
        float m_intensityScale = 1.0;

        //! Base filter angle for cubemap filtering in degree
        //! 0.0 - disabled
        float m_baseFilterAngle = 0.0f;

        //! Initial mip filter angle bias in degree
        //! 0.0 - disabled
        float m_initialMipAngle = 0.0f;

        //! Mip filter angle scale per mip level in degree
        //! 1.0 - default
        float m_mipAnglePerLevelScale = 1.0f;

        ECubemapFilter m_filter = ECubemapFilter::Cosine;
        uint32_t m_samples = 1;

        //! Cubemap edge fix up width.
        //! 0 - disabled
        uint32_t m_edgeFixupWidth = 0;
    };

    struct ECubemapGenDesc final
        : public EImageGenDesc
    {
        //! Parameters for FIR backend
        FIRParam m_firParams;

        //! Parameters for CubeMapGen backend
        CubeMapGenParam m_cmgParams;
        
        //! Output cubemap size, if set to 0 program will derivate suitable size from source image if possible
        //! upsampling is not supported.
        uint32_t m_faceSizeOverride = 0;

        //! Generate mip map or not
        bool m_mipmap = true;
    };

    Image::IImagePtr GenerateCubemap(const Image::IImagePtr& srcImage, const ECubemapGenDesc& desc);

    //! Generate brdf look up table for Unreal's split sum approximation based on Schlick's Fresnel approximation
    //! Return a float point type raw image and a R8G8 preview image
    //! Note: This assumes following PBR set up: 
    //!         - Fresnel is Schlick's approximation <- Critical, that's how it's splitted from
    //!         - GGX used as normal distribution function (NDF, or D), for other D you need to rewrite both sampling and 
    //!           BRDF convolution part [ConvolveBRDF and ConvolveBRDF::ImportanceSampleGGX], because a number of functions have their formula
    //!           approximated from original integral based on the assumption of GGX D
    //!         - SmithG used as geometry function (self shadowing or G), for other setup you need to rewrite BRDF part [ConvolveBRDF]
    Image::IImagePtr GenerateBRDFLUT(uint32_t size, Image::IPixelFormat outFormat = Image::IPixelFormat::R32G32F);

}// namspace ZTK
