//=============================================================================
//CBBoxInt32
// 3D bounding box with int32 coordinates
//
//=============================================================================
// (C) 2005 ATI Research, Inc., All rights reserved.
//=============================================================================

#pragma once

#include <cstdint>

namespace ZTK
{
    //bounding box class with coords specified as int32
    class CBBoxInt32
    {
    public:
        int32_t m_minCoord[3];  //upper left back corner
        int32_t m_maxCoord[3];  //lower right front corner

        CBBoxInt32();
        bool Empty(void);
        void  Clear(void);
        void  Augment(int32_t a_X, int32_t a_Y, int32_t a_Z);
        void  AugmentX(int32_t a_X);
        void  AugmentY(int32_t a_Y);
        void  AugmentZ(int32_t a_Z);
        void  ClampMin(int32_t a_X, int32_t a_Y, int32_t a_Z);
        void  ClampMax(int32_t a_X, int32_t a_Y, int32_t a_Z);
    };
} //namespace ZTK
