//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

struct PSInput
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD;
};

Texture2D g_texture : register(t0);
SamplerState g_sampler : register(s0);

PSInput VSMain(float4 position : POSITION, float4 uv : TEXCOORD)
{
    PSInput result;

    result.position = position;
    result.uv = uv;

    return result;
}

float4 PSMain(PSInput input) : SV_TARGET
{
    return g_texture.SampleLevel(g_sampler, input.uv, 0);
}

float4 GetVertexPositionAndTexCoords(uint vertexID)
{
    float u = float((vertexID << 1) & 2);
    float v = float(vertexID & 2);

    float x = u * 2.0 - 1.0;
    float y = v * -2.0 + 1.0;

    return float4(x, y, u, v);
}

PSInput FullScreenVS(uint vertexId : SV_VertexID)
{
    PSInput result;

    float4 posTex = GetVertexPositionAndTexCoords(vertexId);
    
    result.position = float4(posTex.x, posTex.y, 0.0, 1.0);
    result.uv = float2(posTex.z, posTex.w);
    
    return result;
}

//! Since my little test app doesn't support window resizing at all, I can comfortably 
//! define the screen resolution as inline constants instead of shader uniforms to save 
//! some effort.
# define SCREEN_RESOLUTION_X 1280.0
# define SCREEN_RESOLUTION_Y 720.0

float4 FullScreenPS(PSInput input) : SV_Target
{
    //! 64x64 fixed size grid
    uint2 cell = uint2(input.uv * float2(SCREEN_RESOLUTION_X, SCREEN_RESOLUTION_Y)) >> 6;
    
    if ((cell.x + cell.y) & 0x1)
    {
        return float4(0.18, 0.212, 0.247, 1.0);
    }
    else
    {
        return float4(0.141, 0.165, 0.192, 1.0);
    }
}
