#include "stdafx.h"
#include "ImageViewer.h"

#include "imgui.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx12.h"
#include "imgui_internal.h"

#include "zinc/image/ImageReader.h"
#include "zinc/image/ImageWriter.h"
#include "zinc/image/Utils.h"

#include <print>
#include <format>

namespace
{
    //! Window size in pixel, created as float solely for the convenience of computation, 
    //! generally these values should be unsigned integers.
    constexpr float WindowWidth = 1280.0f;
    constexpr float WindowHeight = 720.0f;

    //! 64 x 5
    constexpr float SidePanelWidth = 320.0f;
    constexpr float SidePanelHeight = 720.0f;

    constexpr float StatusBarHeight = 22.0f;
}

ImageViewer::ImageViewer(UINT width, UINT height, std::wstring name) :
    Base(width, height, name),
    m_frameIndex(0),
    m_viewport(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height)),
    m_scissorRect(0, 0, static_cast<LONG>(width), static_cast<LONG>(height)),
    m_fenceValues{0},
    m_rtvDescriptorSize(0)
{
}

void ImageViewer::OnInit()
{
    CreateContext();
    CreatePipeline();
    CreateResources();
    CreateImGui();
}

void ImageViewer::CreateContext()
{
    UINT dxgiFactoryFlags = 0;

#if defined(_DEBUG)
    // Enable the debug layer (requires the Graphics Tools "optional feature").
    // NOTE: Enabling the debug layer after device creation will invalidate the active device.
    {
        ComPtr<ID3D12Debug> debugController;
        if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
        {
            debugController->EnableDebugLayer();

            // Enable additional debug layers.
            dxgiFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
        }
    }
#endif

    ComPtr<IDXGIFactory4> factory;
    ThrowIfFailed(CreateDXGIFactory2(dxgiFactoryFlags, IID_PPV_ARGS(&factory)));

    if (m_useWarpDevice)
    {
        ComPtr<IDXGIAdapter> warpAdapter;
        ThrowIfFailed(factory->EnumWarpAdapter(IID_PPV_ARGS(&warpAdapter)));

        ThrowIfFailed(D3D12CreateDevice(
            warpAdapter.Get(),
            D3D_FEATURE_LEVEL_12_2,
            IID_PPV_ARGS(&m_device)
        ));
    }
    else
    {
        ComPtr<IDXGIAdapter1> hardwareAdapter;
        GetHardwareAdapter(factory.Get(), &hardwareAdapter);

        ThrowIfFailed(D3D12CreateDevice(
            hardwareAdapter.Get(),
            D3D_FEATURE_LEVEL_12_2,
            IID_PPV_ARGS(&m_device)
        ));
    }

    // Describe and create the command queue.
    D3D12_COMMAND_QUEUE_DESC queueDesc = {};
    queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
    queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

    ThrowIfFailed(m_device->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_commandQueue)));

    // Describe and create the swap chain.
    DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
    swapChainDesc.BufferCount = FrameCount;
    swapChainDesc.Width = m_width;
    swapChainDesc.Height = m_height;
    swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
    swapChainDesc.SampleDesc.Count = 1;

    ComPtr<IDXGISwapChain1> swapChain;
    ThrowIfFailed(factory->CreateSwapChainForHwnd(
        m_commandQueue.Get(),        // Swap chain needs the queue so that it can force a flush on it.
        Win32Application::GetHwnd(),
        &swapChainDesc,
        nullptr,
        nullptr,
        &swapChain
    ));

    // This sample does not support fullscreen transitions.
    ThrowIfFailed(factory->MakeWindowAssociation(Win32Application::GetHwnd(), DXGI_MWA_NO_ALT_ENTER));

    ThrowIfFailed(swapChain.As(&m_swapChain));
    m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();

    // Create descriptor heaps.
    {
        // Describe and create a render target view (RTV) descriptor heap.
        D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
        rtvHeapDesc.NumDescriptors = FrameCount;
        rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
        rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        ThrowIfFailed(m_device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&m_rtvHeap)));

        // Describe and create a shader resource view (SRV) heap for the texture.
        D3D12_DESCRIPTOR_HEAP_DESC csuHeapDesc = {};

        //! 1 srv for imgui font atlas + 1 srv for canvas + 1 srv for staging buffer + 1 uav for canvas
        csuHeapDesc.NumDescriptors = 4;
        csuHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
        csuHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
        ThrowIfFailed(m_device->CreateDescriptorHeap(&csuHeapDesc, IID_PPV_ARGS(&m_csuHeap)));

        m_rtvDescriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
        m_csuDescriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

        m_csuCpuDescHandle = m_csuHeap->GetCPUDescriptorHandleForHeapStart();
        m_csuGpuDescHandle = m_csuHeap->GetGPUDescriptorHandleForHeapStart();
    }

    // Create frame resources.
    {
        CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(m_rtvHeap->GetCPUDescriptorHandleForHeapStart());

        // Create a RTV for each frame.
        for (UINT n = 0; n < FrameCount; ++n)
        {
            ThrowIfFailed(m_swapChain->GetBuffer(n, IID_PPV_ARGS(&m_renderTargets[n])));
            m_device->CreateRenderTargetView(m_renderTargets[n].Get(), nullptr, rtvHandle);
            rtvHandle.Offset(1, m_rtvDescriptorSize);

            //! Create command allocator for each back buffer to prevent blocking
            ThrowIfFailed(m_device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&m_commandAllocators[n])));
        }
    }
}

void ImageViewer::CreatePipeline()
{
    // Create the root signature.
    {
        D3D12_FEATURE_DATA_ROOT_SIGNATURE featureData = {};

        // This is the highest version the sample supports. If CheckFeatureSupport succeeds, the HighestVersion returned will not be greater than this.
        featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_1;

        if (FAILED(m_device->CheckFeatureSupport(D3D12_FEATURE_ROOT_SIGNATURE, &featureData, sizeof(featureData))))
        {
            featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_0;
        }

        CD3DX12_DESCRIPTOR_RANGE1 ranges[1];
        ranges[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0, D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC);

        CD3DX12_ROOT_PARAMETER1 rootParameters[1];
        rootParameters[0].InitAsDescriptorTable(1, &ranges[0], D3D12_SHADER_VISIBILITY_PIXEL);

        D3D12_STATIC_SAMPLER_DESC sampler = {};
        sampler.Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
        sampler.AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
        sampler.AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
        sampler.AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
        sampler.MipLODBias = 0;
        sampler.MaxAnisotropy = 0;
        sampler.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
        sampler.BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
        sampler.MinLOD = 0.0f;
        sampler.MaxLOD = D3D12_FLOAT32_MAX;
        sampler.ShaderRegister = 0;
        sampler.RegisterSpace = 0;
        sampler.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

        CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc;
        rootSignatureDesc.Init_1_1(_countof(rootParameters), rootParameters, 1, &sampler, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

        ComPtr<ID3DBlob> signature;
        ComPtr<ID3DBlob> error;
        ThrowIfFailed(D3DX12SerializeVersionedRootSignature(&rootSignatureDesc, featureData.HighestVersion, &signature, &error));
        ThrowIfFailed(m_device->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&m_rootSignature)));
    }

    // Create the pipeline state, which includes compiling and loading shaders.
    {
        ComPtr<ID3DBlob> vertexShader;
        ComPtr<ID3DBlob> pixelShader;

        //! [TODO] Replace this with precompiled header
        //static constexpr wchar_t shaderPath[] = L"C:\\Users\\KURO\\Desktop\\mine\\misc\\sphalerite\\tools\\image_viewer\\shaders.hlsl";
        static constexpr wchar_t shaderPath[] = L"C:\\Mine\\Project\\e\\sphalerite\\tools\\image_viewer\\shaders.hlsl";
        ThrowIfFailed(D3DCompileFromFile(shaderPath, nullptr, nullptr, "VSMain", "vs_5_0", ShaderCompileFlags, 0, &vertexShader, nullptr));
        ThrowIfFailed(D3DCompileFromFile(shaderPath, nullptr, nullptr, "PSMain", "ps_5_0", ShaderCompileFlags, 0, &pixelShader, nullptr));

        // Define the vertex input layout.
        D3D12_INPUT_ELEMENT_DESC inputElementDescs[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
            { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
        };

        // Describe and create the graphics pipeline state object (PSO).
        D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
        psoDesc.InputLayout = { inputElementDescs, _countof(inputElementDescs) };
        psoDesc.pRootSignature = m_rootSignature.Get();
        psoDesc.VS = CD3DX12_SHADER_BYTECODE(vertexShader.Get());
        psoDesc.PS = CD3DX12_SHADER_BYTECODE(pixelShader.Get());
        psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
        psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
        psoDesc.DepthStencilState.DepthEnable = FALSE;
        psoDesc.DepthStencilState.StencilEnable = FALSE;
        psoDesc.SampleMask = UINT_MAX;
        psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
        psoDesc.NumRenderTargets = 1;
        psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
        psoDesc.SampleDesc.Count = 1;
        ThrowIfFailed(m_device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pipelineState)));
    }

    //! Create pipeline state for bit depth remapping
    {
        CD3DX12_DESCRIPTOR_RANGE1 ranges[2];

        //! Input image buffer
        ranges[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0, D3D12_DESCRIPTOR_RANGE_FLAG_DATA_VOLATILE);

        //! Output texture
        ranges[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 0, 0, D3D12_DESCRIPTOR_RANGE_FLAG_DATA_VOLATILE);

        CD3DX12_ROOT_PARAMETER1 rootParameters[1];

        //! Compute shader uniforms must have their shader visibility set to all
        rootParameters[0].InitAsDescriptorTable(2, ranges, D3D12_SHADER_VISIBILITY_ALL);

        CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc;
        rootSignatureDesc.Init_1_1(_countof(rootParameters), rootParameters, 0, nullptr);

        ComPtr<ID3DBlob> signature;
        ThrowIfFailed(D3DX12SerializeVersionedRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1_1, &signature, nullptr));
        ThrowIfFailed(m_device->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&m_bdrRootSignature)));

        ComPtr<ID3DBlob> computeShader;
        ComPtr<ID3DBlob> error;
        //static constexpr wchar_t shaderPath[] = L"C:\\Users\\KURO\\Desktop\\mine\\misc\\sphalerite\\tools\\image_viewer\\compute.hlsl";
        static constexpr wchar_t shaderPath[] = L"C:\\Mine\\Project\\e\\sphalerite\\tools\\image_viewer\\compute.hlsl";
        ThrowIfFailed(
            D3DCompileFromFile(shaderPath, nullptr, nullptr, "BitDepthRemap", "cs_5_0", ShaderCompileFlags, 0, &computeShader, &error)
            , error ? static_cast<wchar_t*>(error->GetBufferPointer()) : nullptr);

        D3D12_COMPUTE_PIPELINE_STATE_DESC psoDesc = {};
        psoDesc.pRootSignature = m_bdrRootSignature.Get();
        psoDesc.CS = CD3DX12_SHADER_BYTECODE(computeShader.Get());

        ThrowIfFailed(m_device->CreateComputePipelineState(&psoDesc, IID_PPV_ARGS(&m_bdrPipelineState)));
    }

    //! Create pipeline state for full screen quad
    {
        //! No root parameter is required so we simply create an empty root signature
        CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc;
        rootSignatureDesc.Init_1_1(0, nullptr, 0, nullptr);

        ComPtr<ID3DBlob> signature;
        ComPtr<ID3DBlob> error;
        ThrowIfFailed(D3DX12SerializeVersionedRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1_1, &signature, &error));
        ThrowIfFailed(m_device->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&m_fsqRootSignature)));

        ComPtr<ID3DBlob> vertexShader;
        ComPtr<ID3DBlob> pixelShader;

        //static constexpr wchar_t shaderPath[] = L"C:\\Users\\KURO\\Desktop\\mine\\misc\\sphalerite\\tools\\image_viewer\\shaders.hlsl";
        static constexpr wchar_t shaderPath[] = L"C:\\Mine\\Project\\e\\sphalerite\\tools\\image_viewer\\shaders.hlsl";
        ThrowIfFailed(D3DCompileFromFile(shaderPath, nullptr, nullptr, "FullScreenVS", "vs_5_0", ShaderCompileFlags, 0, &vertexShader, nullptr));
        ThrowIfFailed(D3DCompileFromFile(shaderPath, nullptr, nullptr, "FullScreenPS", "ps_5_0", ShaderCompileFlags, 0, &pixelShader, nullptr));

        D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
        psoDesc.pRootSignature = m_fsqRootSignature.Get();
        psoDesc.VS = CD3DX12_SHADER_BYTECODE(vertexShader.Get());
        psoDesc.PS = CD3DX12_SHADER_BYTECODE(pixelShader.Get());
        psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
        psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
        psoDesc.DepthStencilState.DepthEnable = FALSE;
        psoDesc.DepthStencilState.StencilEnable = FALSE;
        psoDesc.SampleMask = UINT_MAX;
        psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
        psoDesc.NumRenderTargets = 1;
        psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
        psoDesc.SampleDesc.Count = 1;
        ThrowIfFailed(m_device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_fsqPipelineState)));
    }

    {
        //! Create command list for main window
        ThrowIfFailed(m_device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT,
            m_commandAllocators[m_frameIndex].Get(), nullptr, IID_PPV_ARGS(&m_commandList)));
        NAME_D3D12_OBJECT(m_commandList);

        //! Close command list to unblock reset in the first frame.
        m_commandList->Close();
    }

    {
        //! Create synchronization object (a.k.a. fence)
        ThrowIfFailed(m_device->CreateFence(m_fenceValues[m_frameIndex], D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_fence)));
        ++m_fenceValues[m_frameIndex];

        //! Create an event handle to use for frame synchronization.
        m_fenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
        if (m_fenceEvent == nullptr)
        {
            ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
        }
    }
}

void ImageViewer::CreateResources()
{
    //! Initialize texture gpu handle to heap bottom
    m_textureGpuDescHandle = m_csuHeap->GetGPUDescriptorHandleForHeapStart();

    LoadCanvasQuad();
}

void ImageViewer::CreateImGui()
{
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls

    //! ImGui will take ownership of added font's memory and be responsible for management so we 
    //! don't need to preserve the returned pointer.
    ImFontConfig config;
    //config.OversampleH = 2;
    ImFont* font = io.Fonts->AddFontFromMemoryCompressedTTF(
        (void*)Roboto_compressed_data, Roboto_compressed_size, 16.0f);
    io.Fonts->Build();

    //! Setup Dear ImGui style
    ImGui::StyleColorsDark();

    LoadImGuiTheme();

    //! Setup Platform/Renderer backends
    ImGui_ImplWin32_Init(Win32Application::GetHwnd());
    ImGui_ImplDX12_Init(m_device.Get(), FrameCount,
        DXGI_FORMAT_R8G8B8A8_UNORM, m_csuHeap.Get(), m_csuCpuDescHandle, m_csuGpuDescHandle);
    m_csuCpuDescHandle.Offset(1, m_csuDescriptorSize); m_csuGpuDescHandle.Offset(1, m_csuDescriptorSize);
}

void ImageViewer::LoadCanvasQuad()
{
    //! Skip if m_imageInfo is not initialized
    if (m_imageInfo.m_width > 0)
    {
        //! Compensate for the width side panel to re-center displayed image in main window.
        //! Immediate mode gui will join our rendering directly so we have to do offsetting manually.
        float originOffsetX = SidePanelWidth / WindowWidth;
        float originOffsetY = StatusBarHeight / WindowHeight;

        float imageAspectRatio = float(m_imageInfo.m_width) / float(m_imageInfo.m_height);

        m_canvasQuad.Reset();

        //! Transform quad to square in screen space
        m_canvasQuad.TransformY(m_aspectRatio, originOffsetY);

        //! Transform screen space square to input image's aspect ratio
        m_canvasQuad.TransformX(imageAspectRatio, originOffsetX);
    }
    
    //const UINT vertexBufferSize = sizeof(triangleVertices);
    const UINT vertexBufferSize = sizeof(m_canvasQuad.m_vertices);

    const CD3DX12_HEAP_PROPERTIES heapProperties{ D3D12_HEAP_TYPE_UPLOAD };
    const CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Buffer(vertexBufferSize);

    // Note: using upload heaps to transfer static data like vert buffers is not 
    // recommended. Every time the GPU needs it, the upload heap will be marshalled 
    // over. Please read up on Default Heap usage. An upload heap is used here for 
    // code simplicity and because there are very few verts to actually transfer.
    ThrowIfFailed(m_device->CreateCommittedResource(
        &heapProperties,
        D3D12_HEAP_FLAG_NONE,
        &desc,
        D3D12_RESOURCE_STATE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(&m_vertexBuffer)));

    // Copy the triangle data to the vertex buffer.
    UINT8* pVertexDataBegin;
    CD3DX12_RANGE readRange(0, 0);        // We do not intend to read from this resource on the CPU.
    ThrowIfFailed(m_vertexBuffer->Map(0, &readRange, reinterpret_cast<void**>(&pVertexDataBegin)));
    //memcpy(pVertexDataBegin, triangleVertices, sizeof(triangleVertices));
    memcpy(pVertexDataBegin, m_canvasQuad.m_vertices, sizeof(m_canvasQuad.m_vertices));
    m_vertexBuffer->Unmap(0, nullptr);

    // Initialize the vertex buffer view.
    m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
    m_vertexBufferView.StrideInBytes = sizeof(Vertex);
    m_vertexBufferView.SizeInBytes = vertexBufferSize;
}

// Generate a simple black and white checkerboard texture.
void ImageViewer::LoadTexture(const std::filesystem::path& path)
{
    uint8_t* pMem = nullptr;
    uint32_t pitch = 0;

    //! Clear previous references
    m_image.reset();

    std::string error;
    m_image = Zinc::Image::STBReader::LoadImageFromFile(path.string(), &error);

    if (m_image != nullptr)
    {
        auto& info = Zinc::Image::GetPixelFormatInfo(m_image->GetPixelFormat());
        m_imageInfo.m_name = "test.png";
        m_imageInfo.m_format = info.m_name;
        m_imageInfo.m_numChannels = info.m_channels;
        m_imageInfo.m_alignment = info.m_alignment;
        m_imageInfo.m_blockWidth = info.m_blockWidth;
        m_imageInfo.m_blockHeight = info.m_blockHeight;
        m_imageInfo.m_bitsPerBlock = info.m_bitsPerBlock;

        m_imageInfo.m_sizeInBytes = m_image->GetSizeInBytes();

        //! Cached a copy of mip0's dimension for ease of use.
        m_imageInfo.m_width = m_image->GetWidth(0);
        m_imageInfo.m_height = m_image->GetHeight(0);

        for (uint32_t i = 0; i < m_image->GetMipCount(); ++i)
        {
            m_imageInfo.m_mipInfos.emplace_back(
                m_image->GetWidth(i), m_image->GetHeight(i), m_image->GetMipSize(i));
        }

        m_image->Get(0, pMem, pitch);
    }
    else
    {
        std::println("Failed to load texture, error message: \n{0}", error.c_str());
        return;
    }

    DXGI_FORMAT textureFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
    D3D12_RESOURCE_DIMENSION textureDimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;

    //! Create texture
    {
        //! Describe and create a Texture2D.
        D3D12_RESOURCE_DESC textureDesc = {};
        textureDesc.MipLevels = 1;
        textureDesc.Format = textureFormat;
        textureDesc.Width = m_imageInfo.m_width;
        textureDesc.Height = m_imageInfo.m_height;
        textureDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;
        textureDesc.DepthOrArraySize = 1;
        textureDesc.SampleDesc.Count = 1;
        textureDesc.SampleDesc.Quality = 0;
        textureDesc.Dimension = textureDimension;

        CD3DX12_HEAP_PROPERTIES heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);

        ThrowIfFailed(m_device->CreateCommittedResource(
            &heapProperties,
            D3D12_HEAP_FLAG_NONE,
            &textureDesc,
            D3D12_RESOURCE_STATE_COPY_DEST,
            nullptr,
            IID_PPV_ARGS(&m_texture)));

        //! Describe and create a SRV for the texture.
        D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
        srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
        srvDesc.Format = textureDesc.Format;
        srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
        srvDesc.Texture2D.MipLevels = 1;
        m_device->CreateShaderResourceView(m_texture.Get(), &srvDesc, m_csuCpuDescHandle);
        m_textureGpuDescHandle = m_csuGpuDescHandle;
        m_csuCpuDescHandle.Offset(1, m_csuDescriptorSize); m_csuGpuDescHandle.Offset(1, m_csuDescriptorSize);
        NAME_D3D12_OBJECT(m_texture);
    }
    
    //! Note: ComPtr's are CPU objects but this resource needs to stay in scope until
    //! the command list that references it has finished executing on the GPU.
    //! We will flush the GPU at the end of this method to ensure the resource is not
    //! prematurely destroyed.
    ComPtr<ID3D12Resource> stagingBuffer;

    ThrowIfFailed(m_commandAllocators[m_frameIndex]->Reset());
    ThrowIfFailed(m_commandList->Reset(m_commandAllocators[m_frameIndex].Get(), nullptr));

    if (m_image->GetPixelFormat() == Zinc::Image::R8G8B8)
    {
        //! Initiate a preprocessing procedure to remap 24 bit depth image to 32 bit
        auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);

        static constexpr UINT bufferElementSize = 12;
        UINT bufferElementCount = (m_imageInfo.m_width * m_imageInfo.m_height + 3) / 4;
        size_t bufferSize = bufferElementCount * bufferElementSize; //! 4 x 3-byte pixels per element

        //! Buffer for uploading
        auto bufferDesc = CD3DX12_RESOURCE_DESC::Buffer(bufferSize);
        ThrowIfFailed(m_device->CreateCommittedResource(
            &heapProperties,
            D3D12_HEAP_FLAG_NONE,
            &bufferDesc,
            D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS(&stagingBuffer)));

        //! Copy content to staging buffer
        {
            uint8_t* mapped = nullptr;

            //! Set read range to null to indicate we will read the entire subresource
            ThrowIfFailed(stagingBuffer->Map(0, nullptr, reinterpret_cast<void**>(&mapped)));
            memcpy(mapped, pMem, m_imageInfo.m_sizeInBytes);
            stagingBuffer->Unmap(0, nullptr);
        }

        //! Create uavs for texture and staging buffer
        D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
        srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
        srvDesc.Format = DXGI_FORMAT_UNKNOWN;
        srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
        srvDesc.Buffer.FirstElement = 0;
        srvDesc.Buffer.NumElements = bufferElementCount;
        srvDesc.Buffer.StructureByteStride = bufferElementSize;

        D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
        uavDesc.Format = textureFormat;
        uavDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2D;
        uavDesc.Texture2D.MipSlice = 0;
        uavDesc.Texture2D.PlaneSlice = 0;

        if (m_bdrResourceInitialized)
        {
            //! Reuse heap address
            m_device->CreateShaderResourceView(stagingBuffer.Get(), &srvDesc, m_bdrStagingBufferCpuUavHandle);
            m_device->CreateUnorderedAccessView(m_texture.Get(), nullptr, &uavDesc, m_bdrTextureCpuUavHandle);
        }
        else
        {
            //! Allocate new descriptor on heap
            m_device->CreateShaderResourceView(stagingBuffer.Get(), &srvDesc, m_csuCpuDescHandle);
            m_bdrStagingBufferCpuUavHandle = m_csuCpuDescHandle;
            m_bdrStagingBufferGpuUavHandle = m_csuGpuDescHandle;
            m_csuCpuDescHandle.Offset(1, m_csuDescriptorSize); m_csuGpuDescHandle.Offset(1, m_csuDescriptorSize);

            m_device->CreateUnorderedAccessView(m_texture.Get(), nullptr, &uavDesc, m_csuCpuDescHandle);
            m_bdrTextureCpuUavHandle = m_csuCpuDescHandle;
            m_csuCpuDescHandle.Offset(1, m_csuDescriptorSize); m_csuGpuDescHandle.Offset(1, m_csuDescriptorSize);

            m_bdrResourceInitialized = true;
        }

        CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(m_texture.Get(), 
            D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
        m_commandList->ResourceBarrier(1, &barrier);

        //! Already set when resetting m_commandList
        m_commandList->SetPipelineState(m_bdrPipelineState.Get());
        m_commandList->SetComputeRootSignature(m_bdrRootSignature.Get());

        ID3D12DescriptorHeap* ppHeaps[] = { m_csuHeap.Get() };
        m_commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

        //! Set descriptor table to consists 1 srv + 1 uav we just allocated on top of m_csuHeap
        m_commandList->SetComputeRootDescriptorTable(0, m_bdrStagingBufferGpuUavHandle);

        UINT threadGroupX = (bufferElementCount + 63) / 64; //! Divide by 64 (thread group size) and round up
        m_commandList->Dispatch(threadGroupX, 1, 1);

        barrier = CD3DX12_RESOURCE_BARRIER::Transition(m_texture.Get(), 
            D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
        m_commandList->ResourceBarrier(1, &barrier);
    }
    else
    {
        //! Upload image data
        
        const UINT64 stagingBufferSize = GetRequiredIntermediateSize(m_texture.Get(), 0, 1);

        auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
        const CD3DX12_RESOURCE_DESC stagingBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(stagingBufferSize);

        // Create the GPU upload buffer.
        ThrowIfFailed(m_device->CreateCommittedResource(
            &heapProperties,
            D3D12_HEAP_FLAG_NONE,
            &stagingBufferDesc,
            D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS(&stagingBuffer)));

        D3D12_SUBRESOURCE_DATA textureData = {};
        textureData.pData = pMem;
        textureData.RowPitch = pitch;
        textureData.SlicePitch = textureData.RowPitch * m_imageInfo.m_height;

        UpdateSubresources(m_commandList.Get(), m_texture.Get(), stagingBuffer.Get(), 0, 0, 1, &textureData);

        CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(m_texture.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
        m_commandList->ResourceBarrier(1, &barrier);
    }

    //! Close the command list to begin execution.
    ThrowIfFailed(m_commandList->Close());
    ID3D12CommandList* ppCommandLists[] = { m_commandList.Get() };
    m_commandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

    //! Wait for the command list to execute; we are reusing the same command 
    //! list in our main loop but for now, we just want to wait for setup to 
    //! complete before continuing.
    WaitForGPU();
}

void ImageViewer::OnUpdate()
{
}

void ImageViewer::OnRender()
{
    DrawImGui();

    //! Record all the commands we need to render the scene into the command list.
    PopulateCommandList();

    //! Execute the command list.
    ID3D12CommandList* ppCommandLists[] = { m_commandList.Get() };
    m_commandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

    //! Present the frame.
    ThrowIfFailed(m_swapChain->Present(1, 0));

    MoveToNextFrame();
}

void ImageViewer::OnDestroy()
{
    // Ensure that the GPU is no longer referencing resources that are about to be
    // cleaned up by the destructor.
    WaitForGPU();

    ShutdownImGui();

    CloseHandle(m_fenceEvent);
}

void ImageViewer::PopulateCommandList()
{
    // Command list allocators can only be reset when the associated 
    // command lists have finished execution on the GPU; apps should use 
    // fences to determine GPU execution progress.
    ThrowIfFailed(m_commandAllocators[m_frameIndex]->Reset());

    // However, when ExecuteCommandList() is called on a particular command 
    // list, that command list can then be reset at any time and must be before 
    // re-recording.
    ThrowIfFailed(m_commandList->Reset(m_commandAllocators[m_frameIndex].Get(), m_fsqPipelineState.Get()));

    m_commandList->RSSetViewports(1, &m_viewport);
    m_commandList->RSSetScissorRects(1, &m_scissorRect);

    CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(m_renderTargets[m_frameIndex].Get(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);

    // Indicate that the back buffer will be used as a render target.
    m_commandList->ResourceBarrier(1, &barrier);

    //! Get current frame buffer
    CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(m_rtvHeap->GetCPUDescriptorHandleForHeapStart(), m_frameIndex, m_rtvDescriptorSize);
    m_commandList->OMSetRenderTargets(1, &rtvHandle, FALSE, nullptr);

    //! Draw background full screen quad
    {
        m_commandList->SetGraphicsRootSignature(m_fsqRootSignature.Get());
        m_commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
        m_commandList->DrawInstanced(3, 1, 0, 0);
    }
    
    //! Draw canvas
    {
        //! Switch pso for canvas
        m_commandList->SetPipelineState(m_pipelineState.Get());

        // Set root signature
        m_commandList->SetGraphicsRootSignature(m_rootSignature.Get());

        ID3D12DescriptorHeap* ppHeaps[] = { m_csuHeap.Get() };
        m_commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

        m_commandList->SetGraphicsRootDescriptorTable(0, m_textureGpuDescHandle);

        //! Record commands for drawing the quad
        //const float clearColor[] = { 0.0f, 0.2f, 0.4f, 1.0f };
        //m_commandList->ClearRenderTargetView(rtvHandle, clearColor, 0, nullptr);
        m_commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
        m_commandList->IASetVertexBuffers(0, 1, &m_vertexBufferView);
        m_commandList->DrawInstanced(4, 1, 0, 0);
    }
    
    //! Draw UI
    {
        ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), m_commandList.Get());
    }
    
    //! Transition back buffer to present
    barrier = CD3DX12_RESOURCE_BARRIER::Transition(m_renderTargets[m_frameIndex].Get(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);

    // Indicate that the back buffer will now be used to present.
    m_commandList->ResourceBarrier(1, &barrier);

    ThrowIfFailed(m_commandList->Close());
}

void ImageViewer::MoveToNextFrame()
{
    //! Add a Signal command in the queue to watch completion of previous commands
    const UINT64 currerntFenceValue = m_fenceValues[m_frameIndex];
    ThrowIfFailed(m_commandQueue->Signal(m_fence.Get(), currerntFenceValue));

    //! Update the frame index
    m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();

    //! If the next frame is not ready to be rendered yet, wait until it is ready
    if (m_fence->GetCompletedValue() < m_fenceValues[m_frameIndex])
    {
        ThrowIfFailed(m_fence->SetEventOnCompletion(m_fenceValues[m_frameIndex], m_fenceEvent));
        WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);
    }

    //! Pass fence value to new frame
    m_fenceValues[m_frameIndex] = currerntFenceValue + 1;
}

void ImageViewer::WaitForGPU()
{
    ThrowIfFailed(m_commandQueue->Signal(m_fence.Get(), m_fenceValues[m_frameIndex]));
    ThrowIfFailed(m_fence->SetEventOnCompletion(m_fenceValues[m_frameIndex], m_fenceEvent));
    WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);
    ++m_fenceValues[m_frameIndex];
}

void ImageViewer::ShutdownImGui()
{
    ImGui_ImplDX12_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();
}

void ImageViewer::DrawImGui()
{
    //! Start a new frame, it's possible because this function is synchronous 
    //! with the main render loop
    ImGui_ImplDX12_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();

    //! Status bar
    {
        static constexpr ImGuiWindowFlags StatusBarFlags =
            ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_MenuBar;

        if (ImGui::BeginViewportSideBar("##StatusBar", NULL, ImGuiDir_Down, StatusBarHeight, StatusBarFlags)) {
            if (ImGui::BeginMenuBar()) {

                float baseCursorX = ImGui::GetCursorPosX();

                ImGui::SetCursorPosX(baseCursorX + 320.0f);
                ImGui::Text("File: %s", m_imageInfo.m_name.c_str());

                //! Status message
                if (!m_statusMessage.empty())
                {
                    if (m_statusDisplayCounter > 0)
                    {
                        float msgWidth = ImGui::CalcTextSize(m_statusMessage.c_str()).x;
                        ImGui::SetCursorPosX(baseCursorX + max(800.0f - msgWidth * 0.5f, 360.0f));
                        ImGui::Text("%s", m_statusMessage.c_str());
                        --m_statusDisplayCounter;
                    }
                    else
                    {
                        m_statusMessage.clear();
                    }
                }

                //! Frame time
                {
                    ImGui::SetCursorPosX(baseCursorX + 1180.0f);
                    ImGui::Text(" [ %.3f", ImGui::GetIO().DeltaTime * 1000.0f);
                    ImGui::SetCursorPosX(baseCursorX + 1238.0f);
                    ImGui::Text("ms ]");
                }
                
                ImGui::EndMenuBar();
            }
        }
        ImGui::End();
    }

    //! Side bar
    {
        static constexpr ImGuiWindowFlags SidePanelFlags =
            ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar;

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(15.0f, 15.0f));

        ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
        ImGui::SetNextWindowSize(ImVec2(SidePanelWidth, SidePanelHeight));
        ImGui::Begin("##SidePanel", nullptr, SidePanelFlags);
        {
            float baseCursorPosY = ImGui::GetCursorPosY();

            ImGui::Text("Image Properties:");
            ImGui::Spacing();

            ImDrawList* drawList = ImGui::GetWindowDrawList();

            const ImU32 bgCol = ImColor(ImVec4(0.082f, 0.09f, 0.114f, 1.0f));
            drawList->AddRectFilled(ImGui::GetCursorPos(), ImVec2(305.0f, 400.0f), bgCol);

            float propCursorPosX = ImGui::GetCursorPosX() + 10.0f;
            float propCursorPosY = ImGui::GetCursorPosY() + 10.0f;
            ImGui::SetCursorPos(ImVec2(propCursorPosX, propCursorPosY));

            ImGui::Text("Width: %d px", m_imageInfo.m_width);

            ImGui::SetCursorPosX(propCursorPosX);
            ImGui::Text("Height: %d px", m_imageInfo.m_height);

            ImGui::SetCursorPosX(propCursorPosX);
            ImGui::Text("Format: %s", m_imageInfo.m_format.c_str());

            ImGui::SetCursorPosX(propCursorPosX);
            ImGui::Text("Color channels per pixel: %d", m_imageInfo.m_numChannels);

            ImGui::SetCursorPosX(propCursorPosX);
            ImGui::Text("Block Info: [ %dx%d | %d bits per block ]", 
                m_imageInfo.m_blockWidth, m_imageInfo.m_blockHeight, m_imageInfo.m_bitsPerBlock);

            ImGui::SetCursorPosX(propCursorPosX);
            ImGui::Text("Alignment: %d bytes per block", m_imageInfo.m_alignment);

            ImGui::SetCursorPosX(propCursorPosX);
            ImGui::Text("Special Type: %s", m_imageInfo.m_type.c_str());

            ImGui::SetCursorPosX(propCursorPosX);
            static constexpr ImGuiTableFlags MipInfoFlags = 
                ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg | 
                ImGuiTableFlags_SizingFixedFit | ImGuiTableFlags_ScrollY;
            ImGui::Text("Mip Info:");
            ImGui::SetCursorPosX(propCursorPosX);
            if (ImGui::BeginTable("##MipInfo:", 4, MipInfoFlags, ImVec2(270.0f, 120.0f)))
            {
                ImGui::TableSetupColumn("Mip", 0, 25.0f);
                ImGui::TableSetupColumn("Width");
                ImGui::TableSetupColumn("Height");
                ImGui::TableSetupColumn("Size (Raw KB)");
                ImGui::TableHeadersRow();

                uint32_t rowCount = max(uint32_t(m_imageInfo.m_mipInfos.size()), 5);
                for (uint32_t row = 0; row < rowCount; row++)
                {
                    ImGui::TableNextRow();

                    if (row < m_imageInfo.m_mipInfos.size())
                    {
                        const auto& [w, h, s] = m_imageInfo.m_mipInfos[row];

                        ImGui::TableNextColumn();
                        ImGui::Text("%d", 0);
                        ImGui::TableNextColumn();
                        ImGui::Text("%d", w);
                        ImGui::TableNextColumn();
                        ImGui::Text("%d", h);
                        ImGui::TableNextColumn();
                        ImGui::Text("%.3f", s * 1e-3f);
                    }
                    else
                    {
                        ImGui::TableNextColumn(); ImGui::Text(" ");
                        ImGui::TableNextColumn(); ImGui::Text(" ");
                        ImGui::TableNextColumn(); ImGui::Text(" ");
                        ImGui::TableNextColumn(); ImGui::Text(" ");
                    }
                }
                ImGui::EndTable();
            }

            ImVec2 mousePos = ImGui::GetMousePos();

            //! Normalize and remap to screen space position
            ImVec2 normMousePos = ImVec2((mousePos.x / WindowWidth) * 2.0f - 1.0f, 
                1.0f - 2.0f * (mousePos.y / WindowHeight));

            float r = 0.0f, g = 0.0f, b = 0.0f, a = 0.0f;
            if (ImGui::IsWindowFocused() && m_canvasQuad.InBound(normMousePos.x, normMousePos.y))
            {
                //! Get canvas size in pixel in main window, this value can be precalculated by canvas
                //! on transformation but I'll just leave it there for simplicity.
                float sx = (m_canvasQuad.m_vertices[3].position.x - 
                    m_canvasQuad.m_vertices[0].position.x) * (0.5f * WindowWidth);
                float sy = (m_canvasQuad.m_vertices[3].position.y - 
                    m_canvasQuad.m_vertices[0].position.y) * (0.5f * WindowHeight);

                //! Get canvas origin in (main window's) pixel 
                float px = (m_canvasQuad.m_vertices[1].position.x * 0.5f + 0.5f) * WindowWidth;
                float py = (0.5f - m_canvasQuad.m_vertices[1].position.y * 0.5f) * WindowHeight;

                m_pixelCoordX = static_cast<uint32_t>((mousePos.x - px) / sx * m_imageInfo.m_width);
                m_pixelCoordY = static_cast<uint32_t>((mousePos.y - py) / sy * m_imageInfo.m_height);

                //! Get image memory
                uint8_t* pMem = nullptr;
                uint32_t pitch = 0;
                m_image->Get(0, pMem, pitch);

                //! Only valid for uncompressed format
                uint8_t* pixel = pMem + pitch * m_pixelCoordY + (m_pixelCoordX * m_imageInfo.m_bitsPerBlock / 8);
                Zinc::Image::PixelOp::GetPixel(pixel, m_image->GetPixelFormat(), r, g, b, a);
                m_pixelColor = ImColor(ImVec4(r, g, b, a));
            }

            ImGui::Dummy(ImVec2(0.0f, 4.0f));
            ImVec2 t = ImGui::GetCursorPos();
            ImVec2 p = ImVec2(t.x + 10.0f, t.y);
            const ImU32 bdCol = ImColor(ImVec4(0.125f, 0.145f, 0.173f, 1.0f));
            drawList->AddRectFilled(p, ImVec2(p.x + 50.0f, p.y + 50.0f), bdCol);
            drawList->AddRectFilled(ImVec2(p.x + 4.0f, p.y + 4.0f), ImVec2(p.x + 46.0f, p.y + 46.0f), m_pixelColor);

            ImGui::SetCursorPos(ImVec2(p.x + 60.0f, p.y + 8.0f));
            ImGui::Text("Coords: [ %d, %d ]", m_pixelCoordX, m_pixelCoordY);

            ImGui::SetCursorPos(ImVec2(p.x + 60.0f, p.y + 28.0f));
            ImGui::Text("Color: #%08x (%d, %d, %d)", m_pixelColor, 
                uint32_t(r * 255.0f), uint32_t(g * 255.0f), uint32_t(b * 255.0f));

            ImGui::SetCursorPosY(baseCursorPosY + 620.0f);
            if (ImGui::Button("File", ImVec2(290.0f, 30.0f)))
            {
                m_showInputFileDialog = !m_showInputFileDialog;
            }
            ImGui::Spacing();
            if (ImGui::Button("Save", ImVec2(290.0f, 30.0f)))
            {
                m_showOutputFileDialog = !m_showOutputFileDialog;
            }
        }
        ImGui::End();
        ImGui::PopStyleVar();
    }
    
    static constexpr ImVec2 FileDialogSize = ImVec2(600.0f, 60.0f);

    if (m_showInputFileDialog)
    {
        ImGui::SetNextWindowSize(FileDialogSize);
        ImGui::SetNextWindowPos(
            ImVec2((WindowWidth - FileDialogSize.x) * 0.5f, (WindowHeight - FileDialogSize.y) * 0.5f));
        ImGui::Begin("Input File Path (Full Path)", &m_showInputFileDialog, 
            ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse);
        char buf[255] = { 0 };
        ImGui::PushItemWidth(550.0f);
        bool entered = ImGui::InputText("##InputFilePath", buf, IM_ARRAYSIZE(buf), ImGuiInputTextFlags_EnterReturnsTrue);
        ImGui::PopItemWidth();
        ImGui::SameLine();
        std::filesystem::path path{ std::string(buf) };
        if (!std::filesystem::exists(path))
        {
            ImGui::Text("Nah");
        }
        else
        {
            ImGui::Text("Yah");
            if (entered)
            {
                m_showInputFileDialog = false;
                LoadTexture(path);
                LoadCanvasQuad(); //! Reload canvas to adjust size and position
            }
        }
        ImGui::End();
    }

    if (m_showOutputFileDialog)
    {
        ImGui::SetNextWindowSize(FileDialogSize);
        ImGui::SetNextWindowPos(
            ImVec2((WindowWidth - FileDialogSize.x) * 0.5f, (WindowHeight - FileDialogSize.y) * 0.5f));

        ImGui::Begin("Output File Path (Full Path)", &m_showOutputFileDialog,
            ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse);
        char buf[255] = { 0 };
        ImGui::PushItemWidth(550.0f);
        bool entered = ImGui::InputText("##OutputFilePath", buf, IM_ARRAYSIZE(buf), ImGuiInputTextFlags_EnterReturnsTrue);
        ImGui::PopItemWidth();
        ImGui::SameLine();
        std::filesystem::path path{ std::string(buf) };
        std::filesystem::path directory = path;
        if (!std::filesystem::exists(directory.remove_filename()))
        {
            ImGui::Text("Nah");
        }
        else
        {
            ImGui::Text("Yah");
            if (entered)
            {
                m_showOutputFileDialog = false;
                SaveTexture(path);
            }
        }
        ImGui::End();
    }

    //! Finalize
    ImGui::Render();
}

void ImageViewer::SaveTexture(const std::filesystem::path& path)
{
    if (!m_image)
    {
        return;
    }

    std::string error;
    if (Zinc::Image::WriteImageToFile(m_image, path, &error))
    {
        m_statusDisplayCounter = StatusDisplayCycle;
        //! Note: std::format doesn't support wchar_t* input so path is converted to
        //! string beforehead, which means there might be some problem if input path do
        //! contain unicode characters
        m_statusMessage = std::format("Saved to {}", path.string().c_str());
    }
    else
    {
        m_statusDisplayCounter = StatusDisplayCycle;
        m_statusMessage = std::format("Save failed: {}", error.c_str());
    }
}

void ImageViewer::LoadImGuiTheme()
{
    ImGuiStyle& style = ImGui::GetStyle();

    ImVec4 bgColor{  32 / 255.0f,  37 / 255.0f,  44 / 255.0f, 1.0f };
    ImVec4 bdColor{  28 / 255.0f,  33 / 255.0f,  39 / 255.0f, 1.0f };
    ImVec4 txColor{ 216 / 255.0f, 222 / 255.0f, 233 / 255.0f, 1.0f };

    style.Colors[ImGuiCol_Text] = txColor;
    style.Colors[ImGuiCol_WindowBg] = bgColor;
    style.Colors[ImGuiCol_MenuBarBg] = bgColor;
    style.Colors[ImGuiCol_Border] = bdColor;
    style.Colors[ImGuiCol_BorderShadow] = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
}