//! It's possible to use byte address buffer too, which also asks for DOWRD aligned access
//! anyway so there is no too much difference.
StructuredBuffer<uint3> inputBuffer : register(t0);
RWTexture2D<float4> outputTexture : register(u0);

uint GetByte(in uint value, in uint index)
{
    switch (index)
    {
        case 0:
            return value & 0x000000FF;
        case 1:
            return (value & 0x0000FFFF) >> 8;
        case 2:
            return (value & 0x00FFFFFF) >> 16;
        case 3:
            return value >> 24;
    }
    return 0;
}

float4 ToUnorm4(in uint3 u)
{
    //! Although embarrassing this is probably the best we can do to manually convert 
    //! 8 bit integral RGB color to unorm without hardware support.
    return float4(float(u.x) / 255.0, float(u.y) / 255.0, float(u.z) / 255.0, 1.0);
}

uint2 GetTexIdx(in uint width, in uint height, in uint bufferIdx)
{
    uint y = bufferIdx / width;
    uint x = bufferIdx - y * width;
    
    return uint2(x, y);
}

[numthreads(64, 1, 1)]
void BitDepthRemap(
    uint3 dispatchThreadId : SV_DispatchThreadID)
{
    uint width, height;
    outputTexture.GetDimensions(width, height);
    
    //! Get 4 x 24bit pixel from buffer with following layout:
    //! 
    //!  R G B R G B R G B R G B
    //! |-------|-------|-------| 
    //!    t.x     t.y     t.z
    uint3 t = inputBuffer[dispatchThreadId.x].xyz;
    
    //! For images whose size in pixel is not 4 pixel aligned, there will be 
    //! several out of bound writes in the last 1x4 block, which will be guarded by 
    //! execution unit internally and converted to noops so we can just leave it there.
    uint b = dispatchThreadId.x * 4;
    
    //! Remapping
    //! I index the input 32 bit uint as follows:
    //! 
    //! Most significant bit         Least significant bit
    //!  00000000 00000000 00000000 00000000
    //! |--------|--------|--------|--------|
    //!     3         2        1        0
    //! 
    //! which, on machines with little endian, which is a standard amoung todays
    //! GPU vendors, is represented as follows:
    //! 
    //!   R   G   B   R   G   B   R   G   B   R   G   B
    //!  --- --- --- --- --- --- --- --- --- --- --- ---
    //!   B0  B1  B2  B3  B0  B1  B2  B3  B0  B1  B2  B3
    //! 
    //! which is what the following code does.
    uint3 
    p = uint3(GetByte(t.x, 0), GetByte(t.x, 1), GetByte(t.x, 2));
    outputTexture[GetTexIdx(width, height, b    )] = ToUnorm4(p);
    
    p = uint3(GetByte(t.x, 3), GetByte(t.y, 0), GetByte(t.y, 1));
    outputTexture[GetTexIdx(width, height, b + 1)] = ToUnorm4(p);
    
    p = uint3(GetByte(t.y, 2), GetByte(t.y, 3), GetByte(t.z, 0));
    outputTexture[GetTexIdx(width, height, b + 2)] = ToUnorm4(p);
    
    p = uint3(GetByte(t.z, 1), GetByte(t.z, 2), GetByte(t.z, 3));
    outputTexture[GetTexIdx(width, height, b + 3)] = ToUnorm4(p);
}