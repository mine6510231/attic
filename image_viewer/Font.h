#pragma once

//! Precompiled font binary for imgui
extern const unsigned int Roboto_compressed_size;
extern const unsigned int Roboto_compressed_data[];
