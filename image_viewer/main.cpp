//
// Copyright 2024-2025 Wei Xinda. All rights reserved.
// License: https://opensource.org/license/mit/
//

// Modified based on official DirectX samples (D3D12HelloTexture to be specific)
// https://github.com/microsoft/DirectX-Graphics-Samples

//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#include "stdafx.h"
#include "ImageViewer.h"

_Use_decl_annotations_
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int nCmdShow)
{
    ImageViewer app(1280, 720, L"Sphalerite Image Viewer");
    return Win32Application::Run(&app, hInstance, nCmdShow);
}
