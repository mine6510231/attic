#pragma once

#include "Application.h"
#include "Font.h"

#include "zinc/image/Image.h"

#include <d3dx12.h>

#include <filesystem>

using namespace DirectX;

// Note that while ComPtr is used to manage the lifetime of resources on the CPU,
// it has no understanding of the lifetime of resources on the GPU. Apps must account
// for the GPU lifetime of resources to avoid destroying objects that may still be
// referenced by the GPU.
// An example of this can be found in the class method: OnDestroy().
using Microsoft::WRL::ComPtr;

class ImageViewer : public Application
{
    using Base = Application;
public:
    ImageViewer(UINT width, UINT height, std::wstring name);

    virtual void OnInit();
    virtual void OnUpdate();
    virtual void OnRender();
    virtual void OnDestroy();

private:
    //! Means both the maximum number of frames that will be queued to the GPU at a time, 
    //! as well as the number of back buffers in the DXGI swap chain
    static const UINT FrameCount = 3;
    static const UINT TexturePixelSize = 4;    // The number of bytes used to represent a pixel in the texture.

#if defined(_DEBUG)
    // Enable better shader debugging with the graphics debugging tools.
    static const UINT ShaderCompileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
    static const UINT ShaderCompileFlags = 0;
#endif

    struct Vertex
    {
        XMFLOAT3 position;
        XMFLOAT2 uv;
    };

    struct CanvasQuad
    {
        bool InBound(float x, float y)
        {
            if (x > m_vertices[3].position.x || y > m_vertices[3].position.y ||
                x < m_vertices[0].position.x || y < m_vertices[0].position.y)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        void TransformX(float scale, float bias)
        {
            m_vertices[0].position.x = m_vertices[0].position.x * scale + bias;
            m_vertices[1].position.x = m_vertices[1].position.x * scale + bias;
            m_vertices[2].position.x = m_vertices[2].position.x * scale + bias;
            m_vertices[3].position.x = m_vertices[3].position.x * scale + bias;
            m_dirty = true;
        }

        void TransformY(float scale, float bias)
        {
            m_vertices[0].position.y = m_vertices[0].position.y * scale + bias;
            m_vertices[1].position.y = m_vertices[1].position.y * scale + bias;
            m_vertices[2].position.y = m_vertices[2].position.y * scale + bias;
            m_vertices[3].position.y = m_vertices[3].position.y * scale + bias;
            m_dirty = true;
        }

        void Reset()
        {
            //! Define the geometry for a quad.
            //! Old time trick to draw SINGLE quad with only 4 vertices (no index) with triangle strip.
            //! Vertex layout:
            //! 
            //! 1 --------- 3
            //! |           |
            //! |           |
            //! |           |
            //! 0 --------- 2
            m_vertices[0] = { { -0.5f, -0.5f, 0.0f}, { 0.0f, 1.0f } };
            m_vertices[1] = { { -0.5f,  0.5f, 0.0f}, { 0.0f, 0.0f } };
            m_vertices[2] = { {  0.5f, -0.5f, 0.0f}, { 1.0f, 1.0f } };
            m_vertices[3] = { {  0.5f,  0.5f, 0.0f}, { 1.0f, 0.0f } };
            m_dirty = false;
        }

        Vertex m_vertices[4] = {};

        bool m_dirty = false;
    };

    struct ImageInfo
    {
        //! Element: { width, height, size }
        std::vector<std::tuple<uint32_t, uint32_t, uint32_t>> m_mipInfos;
        uint32_t m_height = 0;
        uint32_t m_width = 0;
        uint32_t m_sizeInBytes = 0;
        uint32_t m_numChannels = 0;
        uint32_t m_alignment = 0;
        uint32_t m_blockWidth = 0;
        uint32_t m_blockHeight = 0;
        uint32_t m_bitsPerBlock = 0;
        std::string m_format = "None";
        std::string m_name = "None";
        std::string m_type = "None";
    };

    //! Pipeline objects.
    CD3DX12_VIEWPORT m_viewport;
    CD3DX12_RECT m_scissorRect;
    ComPtr<IDXGISwapChain3> m_swapChain;
    ComPtr<ID3D12Device> m_device;
    ComPtr<ID3D12Resource> m_renderTargets[FrameCount];
    ComPtr<ID3D12CommandAllocator> m_commandAllocators[FrameCount];
    ComPtr<ID3D12CommandQueue> m_commandQueue;

    ComPtr<ID3D12RootSignature> m_rootSignature;
    ComPtr<ID3D12DescriptorHeap> m_rtvHeap;
    ComPtr<ID3D12DescriptorHeap> m_csuHeap; //! for cbv + srv + uav
    ComPtr<ID3D12PipelineState> m_pipelineState;
    ComPtr<ID3D12GraphicsCommandList> m_commandList;

    //! Pipeline state for full screen quad
    ComPtr<ID3D12RootSignature> m_fsqRootSignature;
    ComPtr<ID3D12PipelineState> m_fsqPipelineState;

    //! Pipeline state for bit depth remapping
    ComPtr<ID3D12RootSignature> m_bdrRootSignature;
    ComPtr<ID3D12PipelineState> m_bdrPipelineState;

    CD3DX12_CPU_DESCRIPTOR_HANDLE m_bdrStagingBufferCpuUavHandle;
    CD3DX12_GPU_DESCRIPTOR_HANDLE m_bdrStagingBufferGpuUavHandle;
    CD3DX12_CPU_DESCRIPTOR_HANDLE m_bdrTextureCpuUavHandle;
    bool m_bdrResourceInitialized = false;

    UINT m_rtvDescriptorSize;
    UINT m_csuDescriptorSize;

    CD3DX12_CPU_DESCRIPTOR_HANDLE m_csuCpuDescHandle;
    CD3DX12_GPU_DESCRIPTOR_HANDLE m_csuGpuDescHandle;

    //! App resources.
    ComPtr<ID3D12Resource> m_vertexBuffer;
    D3D12_VERTEX_BUFFER_VIEW m_vertexBufferView;

    CanvasQuad m_canvasQuad;
    Zinc::Image::IImagePtr m_image = nullptr;
    ImageInfo m_imageInfo;

    //! Pixel picker variables
    uint32_t m_pixelCoordX = 0;
    uint32_t m_pixelCoordY = 0;
    uint32_t m_pixelColor = 0;

    CD3DX12_GPU_DESCRIPTOR_HANDLE m_textureGpuDescHandle;
    ComPtr<ID3D12Resource> m_texture;
    
    //! Synchronization objects.
    UINT m_frameIndex;
    HANDLE m_fenceEvent;
    ComPtr<ID3D12Fence> m_fence;
    UINT64 m_fenceValues[FrameCount];

    static constexpr uint32_t StatusDisplayCycle = 150;
    uint32_t m_statusDisplayCounter = 0;
    std::string m_statusMessage = "";

    bool m_showInputFileDialog = false;
    bool m_showOutputFileDialog = false;

    void CreateContext();
    void CreatePipeline();
    void CreateResources();
    void CreateImGui();

    void LoadCanvasQuad();
    void LoadTexture(const std::filesystem::path& path);
    void LoadImGuiTheme();
    void SaveTexture(const std::filesystem::path& path);

    void PopulateCommandList();
    void MoveToNextFrame();
    void WaitForGPU();
    void ShutdownImGui();
    void DrawImGui();
};
