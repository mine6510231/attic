#include <thread>
#include <assert.h>
#include <mutex>
#include <iostream>
#include <print>
#include <bit>

#include "FileName.h"

#define MAKE_FOUR(n) (uint16_t(n) << 12 | uint16_t(n) << 8 | uint16_t(n) << 4 | uint16_t(n))

struct ReferenceMatrix4A
{
private:
    //! In theory, we need a full set of RowUpdateMask + ColumnUpdateMask and RowResetMask + ColumnResetMask
    //! to perform line update and reset without redundant operation. But in practice, it turns out that most of 
    //! recent (within two decades) architectures are equipped with "and not" instruction (e.g. ANDN on x86)
    //! that allow us to negate second operand (first source operand, e.g. ANDN a b <- a is the second operand) 
    //! for free, which means we can recover ColumnUpdateMask in UpdateLine() and RowResetMask in ResetLine() on 
    //! the fly without any cost and therefore don't need inline constant array for those two anymore, which 
    //! also saves us some read only data space.

    static constexpr uint16_t RowUpdateMask[4] =
    {
        uint16_t(0xF) << 12, uint16_t(0xF) << 8, uint16_t(0xF) << 4, uint16_t(0xF)
    };

    static constexpr uint16_t ColumnResetMask[4] =
    {
        //! ~ColumnUpdateMask[0], ~ColumnUpdateMask[1], ~ColumnUpdateMask[2], ~ColumnUpdateMask[3] 
        uint16_t(~(MAKE_FOUR(0x7))), uint16_t(~(MAKE_FOUR(0xB))), uint16_t(~MAKE_FOUR(0xD)), uint16_t(~(MAKE_FOUR(0xE)))
    };

public:
    static constexpr size_t EntryCount = 4;

    constexpr void UpdateLine(uint8_t n)
    {
        //! Set row n to 1, then set column n to 0 (the order is important and can't be combined)
        //! Ideally it should be ~ColumnResetMask[n] & (m_lines | RowUpdateMask[n]) to hint andnot pattern 
        //! but the current order is clearer and modern compilers are smart enough to identify pattern of 
        //! commutative operation regardless of ordering (except O0, where compiler will faithfully issue
        //! all instructions to help debugging).
        m_lines = (m_lines | RowUpdateMask[n]) & ~ColumnResetMask[n];
    }

    constexpr uint8_t LeastUsedLine()
    {
        //! With if else chain the best case is row 0, which only takes 1 instruction (1 SHR / AND + JE on x86), 
        //! the worst case is row 3, which needs 8 instruction (1 SHR + 3 AND + 4 JEs on x86), if the input 
        //! doesn't imply any specific ordering and every line has the equal possibility to be accessed, the 
        //! average instruction cost is (2 + 4 + 6 + 8) / 4 = 5, which is faster than corresponding bit hacks.
        if (m_lines >> 12 == 0) return 0;
        else if ((m_lines & 0x0F00) == 0) return 1;
        else if ((m_lines & 0x00F0) == 0) return 2;
        else if ((m_lines & 0x000F) == 0) return 3;
        return UINT8_MAX; // Unexpected error value
    }

    constexpr void ResetLine(uint8_t n)
    {
        //! Set row n to 0 AND set column n to 1 to maintain asymmetry of the matrix
        m_lines = (m_lines & ~RowUpdateMask[n]) | ColumnResetMask[n];
    }

    //! Row major 4x4 matrix
    uint16_t m_lines;
};

#undef MAKE_FOUR

#define MAKE_EIGHT(n) (uint64_t(n) << 56 | uint64_t(n) << 48 | uint64_t(n) << 40 | uint64_t(n) << 32 | \
    uint64_t(n) << 24 | uint64_t(n) << 16 | uint64_t(n) << 8  | uint64_t(n))

struct ReferenceMatrix8A
{
private:
    static constexpr uint64_t RowUpdateMask[8] =
    {
        uint64_t(0xFF) << 56, uint64_t(0xFF) << 48, uint64_t(0xFF) << 40, uint64_t(0xFF) << 32,
        uint64_t(0xFF) << 24, uint64_t(0xFF) << 16, uint64_t(0xFF) << 8 , uint64_t(0xFF),
    };

    static constexpr uint64_t ColumnResetMask[8] =
    {
        uint64_t(~MAKE_EIGHT(0x7F)), uint64_t(~MAKE_EIGHT(0xBF)), uint64_t(~MAKE_EIGHT(0xDF)), uint64_t(~MAKE_EIGHT(0xEF)),
        uint64_t(~MAKE_EIGHT(0xF7)), uint64_t(~MAKE_EIGHT(0xFB)), uint64_t(~MAKE_EIGHT(0xFD)), uint64_t(~MAKE_EIGHT(0xFE))
    };

public:
    static constexpr size_t EntryCount = 8;

    constexpr void UpdateLine(uint8_t n)
    {
        m_lines = (m_lines | RowUpdateMask[n]) & ~ColumnResetMask[n];
    }

    constexpr uint8_t LeastUsedLine()
    {
        //! Find leftmost first 0 byte in the matrix
        uint64_t t = (m_lines & MAKE_EIGHT(0x7F)) + MAKE_EIGHT(0x7F);
        t = ~(t | m_lines | MAKE_EIGHT(0x7F));
        return std::countl_zero<uint64_t>(t) >> 3;
    }

    constexpr void ResetLine(uint8_t n)
    {
        m_lines = (m_lines & ~RowUpdateMask[n]) | ColumnResetMask[n];
    }

    uint64_t m_lines{ 0 };
};

#undef MAKE_EIGHT

#include <immintrin.h>

#define MAKE_FOUR(n) (uint64_t(n) << 48 | uint64_t(n) << 32 | uint64_t(n) << 16 | uint16_t(n))
#define MAKE_FOUR_BY_FOUR(n) MAKE_FOUR(n), MAKE_FOUR(n), MAKE_FOUR(n), MAKE_FOUR(n)

struct ReferenceMatrix16A
{
    struct alignas(__m256i) V256
    {
        union
        {
            uint64_t i[4];
            __m256i v;
        };

        inline operator __m256i() const noexcept { return v; }
    };

    static constexpr V256 RowUpdateMask[16] = 
    {
        {{{ uint64_t(0xFFFF) << 48, 0, 0, 0	}}},
        {{{ uint64_t(0xFFFF) << 32, 0, 0, 0	}}},
        {{{ uint64_t(0xFFFF) << 16, 0, 0, 0	}}},
        {{{ uint64_t(0xFFFF),       0, 0, 0	}}},
        {{{ 0, uint64_t(0xFFFF) << 48, 0, 0	}}},
        {{{ 0, uint64_t(0xFFFF) << 32, 0, 0	}}},
        {{{ 0, uint64_t(0xFFFF) << 16, 0, 0	}}},
        {{{ 0, uint64_t(0xFFFF),       0, 0	}}},
        {{{ 0, 0, uint64_t(0xFFFF) << 48, 0	}}},
        {{{ 0, 0, uint64_t(0xFFFF) << 32, 0	}}},
        {{{ 0, 0, uint64_t(0xFFFF) << 16, 0	}}},
        {{{ 0, 0, uint64_t(0xFFFF),		  0	}}},
        {{{ 0, 0, 0, uint64_t(0xFFFF) << 48 }}},
        {{{ 0, 0, 0, uint64_t(0xFFFF) << 32 }}},
        {{{ 0, 0, 0, uint64_t(0xFFFF) << 16 }}},
        {{{ 0, 0, 0, uint64_t(0xFFFF)       }}},
    };

    static constexpr V256 ColumnResetMask[16] =
    {
        {{{ MAKE_FOUR_BY_FOUR(0x8000) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x4000) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x2000) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x1000) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0800) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0400) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0200) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0100) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0080) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0040) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0020) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0010) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0008) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0004) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0002) }}},
        {{{ MAKE_FOUR_BY_FOUR(0x0001) }}},
    };

    static constexpr V256 ZeroRowSearchMask = {{{ MAKE_FOUR_BY_FOUR(0x7FFF) }}};

public:
    static constexpr size_t EntryCount = 16;

    //! Not constexpr anymore, floating point register needs to be loaded and evaluated at runtime
    void UpdateLine(uint8_t n)
    {
        m_lines = AndNot(ColumnResetMask[n], Or(m_lines, RowUpdateMask[n]));
    }

    uint8_t LeastUsedLine()
    {
        __m256i t = Or(And(m_lines, ZeroRowSearchMask), ZeroRowSearchMask);
        t = Not(Or(t, Or(m_lines, ZeroRowSearchMask)));

        //! Offload vector for bit counting
        uint64_t i[4];
        Store(reinterpret_cast<float*>(i), t);


    }

    void ResetLine(uint8_t n)
    {
        m_lines = Or(AndNot(RowUpdateMask[n], m_lines), ColumnResetMask[n]);
    }

private:
    //! All bitwise, no logical
    __m256i Or(__m256i a, __m256i b)
    {
        return _mm256_or_si256(a, b);
    }

    __m256i And(__m256i a, __m256i b)
    {
        return _mm256_and_si256(a, b);
    }

    __m256i AndNot(__m256i a, __m256i b)
    {
        return _mm256_andnot_si256(a, b);
    }

    __m256i Not(__m256i a)
    {
        return _mm256_xor_si256(a, _mm256_set1_epi32(-1) /* All one bit mask */);
    }

    //! 256 bit simd instruction set usually only do at most 64 bit packed addition, if the operation doesn't 
    //! result in any carry then we can safely pretend it as a full length add.
    __m256i AddNoCarry(__m256i a, __m256i b)
    {
        return _mm256_add_epi64(a, b);
    }

    void Store(float* addr, __m256i a)
    {
        _mm256_storeu_ps(addr, _mm256_castsi256_ps(a));
    }

    //! Compiler will issue suitable instructions to init register
    __m256i m_lines{ 0 };
};

#undef MAKE_FOUR

//! Example
#if 1
#include <print>
int main(int argc, char** argv)
{
    using RefMatStr = Debug::ToString<ReferenceMatrix4A>;

    ReferenceMatrix4A refMat4;
    std::println("Matrix (Initial): \n{}", RefMatStr::AsOriginalMatrix(refMat4));

    refMat4.UpdateLine(0);
    std::println("Matrix (Update 0): \n{}", RefMatStr::AsOriginalMatrix(refMat4));

    refMat4.UpdateLine(1);
    std::println("Matrix (Update 1): \n{}", RefMatStr::AsOriginalMatrix(refMat4));

    refMat4.UpdateLine(3);
    std::println("Matrix (Update 3): \n{}", RefMatStr::AsOriginalMatrix(refMat4));

    std::println("Final result:\n");

    std::println("Bit field: {}\n", RefMatStr::AsBitField(refMat4));
    std::println("Compact matrix: \n{}", RefMatStr::AsCompactMatrix(refMat4));
    std::println("Original matrix: \n{}", RefMatStr::AsOriginalMatrix(refMat4));

    std::println("Least recently used line: {}", refMat4.LeastUsedLine());

    return 0;
}
#endif