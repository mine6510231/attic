#pragma once

#include <bit>
#include <limits>
#include <memory>
#include <stdexcept>
#include <type_traits>
#include <vector>

struct DummyPayload
{
    size_t a[10];
};

using T = DummyPayload;
using Allocator = std::allocator<T>;

namespace stable_vector_details
{
    //! This unsafe function has a false positive that is critical for us: zero returns true as well, it allows 
    //! us to expand empty vector whose size is zero via unified routine with normal power of 2 boundary detection.
    template <typename T> static constexpr bool is_pow2_unsafe(T n) { return (n & (n - T(1))) == T(0); }
    template <typename T> static constexpr bool is_pow2(T n) { return n < 1 ? false : is_pow2_unsafe(n); }

    template<class Pointer, class SizeType, class AllocatorType>
    struct node
    {
    private:
        using pointer = Pointer;
        using size_type = SizeType;
        using allocator_type = AllocatorType;

    public:
        using this_type = node<pointer, size_type, allocator_type>;
        using node_pointer = this_type*;
        using const_node_pointer = const this_type*;
        using node_reference = this_type&;
        using const_node_reference = const this_type&;
        using node_allocator_type = std::allocator_traits<AllocatorType>::template rebind_alloc<node>;
        using node_allocator_traits = std::allocator_traits<AllocatorType>::template rebind_traits<node>;

        pointer m_base;
        size_type m_count;

        //! This is here because the stable vector's extension that guarantees the added range of a single reservation
        //! is contiguous, which means we need to deallocate these contiguous region at once as well and therefore need 
        //! to keep track of the number of allocated objects along with a node, for self-contained node it's just m_count.
        size_type m_allocation_count;

        node()
            : m_base(nullptr)
            , m_count(0)
        {
        }
        node(pointer ptr, size_type count)
            : m_base(ptr)
            , m_count(count)
            , m_allocation_count(count)
        {
        }
        node(pointer ptr, size_type count, size_type allocation_count)
            : m_base(ptr)
            , m_count(count)
            , m_allocation_count(allocation_count)
        {
        }

        pointer first() const
        { 
            return m_base;
        }

        pointer operator[](size_type n) const
        {
            return m_base[n];
        }

        pointer last() const
        { 
            return m_base + m_count - 1;
        }
    };

    template<class NodeType>
    struct node_vector
    {
        using node = NodeType;
        using node_pointer = typename NodeType::node_pointer;
        using const_node_pointer = typename NodeType::const_node_pointer;
        using node_reference = typename NodeType::node_reference;
        using const_node_reference = typename NodeType::const_node_reference;
        using node_size_type = short; //! Must be a signed type
        using node_allocator_type = typename NodeType::node_allocator_type;
        using node_allocator_traits = typename NodeType::node_allocator_traits;

        node_vector()
            : m_last(-1)
            , m_capacity(0)
            , m_nodes(nullptr)
        {
        }

        template<typename... Args>
        void emplace_back(node_allocator_type& allocator, Args&&... args)
        {
            if (m_capacity <= ++m_last)
            {
                reserve_unsafe(allocator, next_capacity());
            }
            std::construct_at(&m_nodes[m_last], std::forward<Args>(args)...);
        }

        void resize_uninitialized_unsafe(node_allocator_type& allocator, node_size_type size)
        {
            node_size_type new_last = size - 1;
            if (new_last < m_last)
            {
                std::destroy_n(m_nodes, m_last - new_last);
                m_last = new_last;
            }
            else if (m_last < new_last)
            {
                if (m_capacity < size)
                {
                    reserve_unsafe(allocator, next_capacity(size));
                }
                std::uninitialized_default_construct_n(m_nodes[this->size()], m_last - new_last);
                m_last = new_last;
            }
        }

        void shrink_to_fit(node_allocator_type& allocator)
        {
            node_size_type size = this->size();
            if (size < m_capacity)
            {
                if (!size)
                {
                    release(allocator);
                    m_capacity = 0;
                }
                else
                {
                    reserve_copy_unsafe(allocator, size);
                }
            }
        }

        void clear()
        {
            std::destroy_n(m_nodes, size());
            m_last = -1;
        }

        node_pointer front() const
        {
            return m_nodes;
        }

        node_pointer back_unsafe() const
        {
            return m_nodes[m_last]; //! Underflow if m_last < 0.
        }

        node_size_type size() const
        {
            return m_last + 1;
        }

        node_size_type last() const
        {
            return m_last;
        }

        node_size_type capacity() const
        {
            return m_capacity;
        }

        bool empty() const
        {
            return m_last < 0;
        }

        const_node_reference operator[](node_size_type pos) const
        {
            return m_nodes[pos];
        }

        node_reference operator[](node_size_type pos)
        {
            return m_nodes[pos];
        }

        void release(node_allocator_type& allocator)
        {
            class node_allocator_traits::deallocate(allocator, m_nodes, m_capacity);
        }

    private:
        template<node_size_type N> struct is_pow2_t
        { 
            static constexpr bool value = is_pow2(N);
        };
        
        static constexpr node_size_type growth_step = 8;

        static_assert(is_pow2_t<growth_step>::value && "Growth step must be power of 2");

        node_size_type next_capacity()
        {
            return m_capacity + growth_step;
        }

        node_size_type next_capacity(node_size_type size)
        {
            static constexpr node_size_type mask = growth_step - 1;
            return (size + mask) & ~mask;
        }

        void reserve_unsafe(node_allocator_type& allocator, node_size_type new_cap)
        {
            if (!m_capacity)
            {
                reserve_no_copy_unsafe(new_cap);
            }
            else
            {
                reserve_copy_unsafe(new_cap);
            }
        }

        void reserve_copy_unsafe(node_allocator_type& allocator, node_size_type new_cap)
        {
            node_pointer nodes = class node_allocator_traits::allocate(allocator, new_cap);
            std::uninitialized_copy_n(m_nodes, size(), nodes);
            class node_allocator_traits::deallocate(allocator, m_nodes, m_capacity);
            m_nodes = nodes;
            m_capacity = new_cap;
        }

        void reserve_no_copy_unsafe(node_allocator_type& allocator, node_size_type new_cap)
        {
            node_pointer nodes = class node_allocator_traits::allocate(allocator, new_cap);
            m_nodes = nodes;
            m_capacity = new_cap;
        }

        //! We store the index of last node instead of size of node array to trade slower slow path
        //! (which we rarely hit) for faster fast path. 
        node_size_type m_last;
        node_size_type m_capacity;
        node_pointer m_nodes;
    };

    template<class ContainerType>
    class iterator
    {
        using this_type = iterator<ContainerType>;
        using pointer = typename ContainerType::pointer;
        using const_pointer = typename ContainerType::const_pointer;
        using reference = typename ContainerType::reference;
        using const_rerference = typename ContainerType::const_reference;
        using container_pointer = ContainerType*;

    public:
        using iterator_category = std::random_access_iterator_tag;
        using value_type = typename ContainerType::value_type;
        using difference_type = typename ContainerType::size_type;
        using size_type = typename ContainerType::size_type;

        iterator()
            : m_current()
            , m_index()
            , m_container()
        {
        }

        iterator(const iterator& other)
            : m_current(other.m_current)
            , m_index(other.m_index)
            , m_container(other.m_container)
        {
        }

        iterator(pointer current, size_type index, container_pointer container)
            : m_current(current)
            , m_index(index)
            , m_container(container)
        {
        }

        reference operator*() const noexcept { *m_current; }
        pointer operator->() const noexcept { m_current; }

        iterator& operator++() noexcept
        {
            if (is_pow2_unsafe(++m_index)) m_current = switch_node(index);
            else m_current++;
            return *this;
        }

        iterator operator++(int) noexcept
        {
            iterator t(*this);
            ++*this;
            return t;
        }

        iterator& operator--() noexcept
        {
            if (is_pow2_unsafe(--m_index)) m_current = switch_node(index);
            else if (!m_index) m_current = m_container->front();
            else m_current--;
            return *this;
        }

        iterator operator--(int) noexcept
        {
            iterator t(*this);
            --*this;
            return t;
        }

        iterator& operator+=(difference_type n) noexcept
        {
            if (!n) return *this;

            size_type new_index = m_index + n;
            size_type nlz = std::countl_zero(new_index);
            if (std::countl_zero(m_index) == nlz)
            {
                m_current += n;
                m_index = new_index;
            }
            else
            {
                size_type new_node_index = std::numeric_limits<size_type>::digits - nlz;

            }

        }

        iterator operator+(difference_type n) noexcept
        {
            iterator t(*this);
            return t += n;
        }

        iterator& operator-=(difference_type n) noexcept
        {

        }

        iterator operator-(difference_type n) noexcept
        {
            iterator t(*this);
            return t -= n;
        }

        reference operator[](difference_type n) const
        {
            return *(*this + n);
        }

        bool operator==(const iterator& other)
        {
            return m_current = other.m_current;
        }

        bool operator!=(const iterator& other)
        {
            return m_current != other.m_current;
        }

        bool operator<(const iterator& other)
        {
            return (m_container == other.m_container) ? m_index < other.m_index : m_current < other.m_current;
        }

        bool operator>(const iterator& other)
        {
            return other < *this;
        }

        bool operator<=(const iterator& other)
        {
            return !(other < *this);
        }

        bool operator>=(const iterator& other)
        {
            return !(*this < other);
        }

        inline friend class iterator operator+(difference_type n, iterator x)
        {
            iterator t(*this);
            return t += n;
        }

    private:
        pointer switch_node(size_type index)
        {
            auto node_index = m_container->get_node_index(index);
            return m_container->get_node_front(node_index);
        }

        pointer m_current;
        size_type m_index;
        container_pointer m_container;
    };

    //! Type used to tag that the inserted values should be default initialized
    struct default_init_t {};

    //! Type used to tag that the inserted values should be value initialized
    struct value_init_t {};

    static constexpr default_init_t default_init = default_init_t{};
    static constexpr value_init_t value_init = value_init_t{};
} // namespace stable_vector_details

class stable_vector
{
public:
    using value_type                = T;
    using pointer                   = T*;
    using const_pointer             = const T*;
    using reference                 = T&;
    using const_reference           = const T&;
    using size_type                 = std::size_t;
    using difference_type           = std::make_signed_t<std::size_t>;
    using allocator_type            = Allocator;

private:
    using this_type                 = stable_vector;
    using allocator_traits          = std::allocator_traits<allocator_type>::rebind_traits<value_type>;

    using node                      = stable_vector_details::node<pointer, size_type, allocator_type>;
    using node_vector               = stable_vector_details::node_vector<node>;
    using node_reference            = typename node::node_reference;
    using const_node_reference      = typename node::const_node_reference;
    using node_pointer              = typename node::node_pointer;
    using const_node_pointer        = typename node::const_node_pointer;
    using node_size_type            = typename node_vector::node_size_type;
    using node_allocator_type       = typename node::node_allocator_type;
    using node_allocator_traits     = typename node::node_allocator_traits;
    
    using default_init_t            = stable_vector_details::default_init_t;
    using value_init_t              = stable_vector_details::value_init_t;

    template<typename T> friend class stable_vector_details::iterator;

    struct members_holder
        : public allocator_type
        , public node_allocator_type
    {
        node_vector m_node_vector;

        pointer m_first;
        pointer m_last;

        this_type::size_type m_size;
        this_type::size_type m_capacity;
    } m_members;

    allocator_type& get_allocator() { return m_members; }
    const allocator_type& get_allocator() const { return m_members; }

    node_allocator_type& get_node_allocator() { return m_members; }
    const node_allocator_type& get_node_allocator() const { return m_members; }

public:
    //! ===============================
    //! Copy/move construction
    //! ===============================
    
    constexpr stable_vector() noexcept;
    constexpr explicit stable_vector(size_type n);
    constexpr stable_vector(size_type n, const value_type& value);
    template <class InputIterator>
    constexpr stable_vector(InputIterator first, InputIterator last);
    constexpr stable_vector(const stable_vector& other)
        noexcept(is_nothrow_copy_constructible_v<value_type>);
    constexpr stable_vector(static_vector&& other)
        noexcept(is_nothrow_move_constructible_v<value_type>);
    constexpr stable_vector(initializer_list<value_type> il);

    //! ===============================
    //! Copy/move assignment
    //! ===============================
    
    constexpr stable_vector& operator=(const stable_vector& other)
        noexcept(is_nothrow_copy_assignable_v<value_type>);
    constexpr stable_vector& operator=(stable_vector&& other);
    noexcept(is_nothrow_move_assignable_v<value_type>);
    template <class InputIterator>
    constexpr void assign(InputIterator first, InputIterator last);
    constexpr void assign(size_type n, const value_type& u);
    constexpr void assign(initializer_list<value_type> il);

    //! ===============================
    //! Destruction
    //! ===============================

    ~stable_vector()
    {
        todo;
    }

    //! ===============================
    //! Iterators
    //! ===============================



    //! ===============================
    //! Size/capacity
    //! ===============================
    
    bool empty() const noexcept
    {
        return !m_members.m_size;
    }

    size_type size() const noexcept
    {
        return m_members.m_size;
    }

    size_type max_size() const noexcept
    {
        return allocator_traits::max_size(get_allocator());
    }

    size_type capacity() const noexcept
    {
        return m_members.m_capacity;
    }

    void reserve(size_type new_cap)
    {
        reserve_impl(new_cap);
    }

    void resize(size_type size)
    {
        throw_if_overflow(size);
        resize_impl(size, stable_vector_details::value_init);
    }

    void resize(size_type size, default_init_t)
    {
        throw_if_overflow(size);
        resize_impl(size, stable_vector_details::default_init);
    }

    void resize(size_type size, const value_type& value)
    {
        throw_if_overflow(size);
        resize_impl(size, value);
    }

    void shrink_to_fit()
    {
        shrink_to_fit_impl();
    }

    //! ===============================
    //! Element and data access
    //! ===============================
    
    reference operator[](size_type pos)
    {
        return *at_impl(pos);
    }

    const_reference operator[](size_type pos) const
    {
        return *at_impl(pos);
    }

    reference at(size_type pos)
    {
        throw_if_out_of_range(pos);
        return *at_impl(pos);
    }

    const_reference at(size_type pos) const
    {
        throw_if_out_of_range(pos);
        return *at_impl(pos);
    }

    reference front()
    {
        return *(m_members.m_first);
    }

    const_reference front() const
    {
        return *(m_members.m_first);
    }

    reference back()
    {
        return *(m_members.m_last);
    }

    const_reference back() const
    {
        return *(m_members.m_last);
    }

    //! ===============================
    //! Modifiers
    //! ===============================
    
    void clear() noexcept
    {
        if (m_members.m_size) clear_impl();
    }

    template<typename... Args>
    reference emplace_back(Args&&... args)
    {
        if (stable_vector_details::is_pow2_unsafe(m_members.m_size) && (m_members.m_size == capacity()))
        {
            m_members.m_last = push_node();
        }
        else
        {
            m_members.m_last = at_unsafe_impl(m_members.m_size);
        }
        std::construct_at(m_members.m_last, std::forward<Args>(args)...);
        m_size++;
    }

    void push_back(const T& t)
    {
        emplace_back(t);
    }

    void push_back(T&& t)
    {
        emplace_back(std::move(t));
    }

    void pop_back()
    {
        std::destroy_at(m_members.m_last);
        m_members.m_last = at_impl(--m_members.m_size);
    }

private:
    //! ===============================
    //! Utilities
    //! ===============================

    static constexpr node_size_type get_node_index(size_type pos)
    {
        return std::bit_width(pos);
    }

    static constexpr size_type get_node_offset_unsafe(size_type pos, size_type node_index)
    {
        return pos - (size_type(1) << (node_index - 1));
    }

    pointer push_node()
    {
        pointer ptr;
        if (!m_members.m_capacity) [[unlikely]]
        {
            ptr = allocator_traits::allocate(get_allocator(), 1);
            m_members.m_node_vector.emplace_back(get_node_allocator(), ptr, 1);
            m_members.m_capacity = 1;
            m_members.m_first = m_members.m_last;
        }
        else [[likely]]
        {
            ptr = allocator_traits::allocate(get_allocator(), m_members.m_capacity);
            m_members.m_node_vector.emplace_back(get_node_allocator(), ptr, m_members.m_capacity);
            m_members.m_capacity <<= 1;
        }
        return ptr;
    }

    pointer get_node_front(node_size_type index) const
    {
        return m_members.m_node_vector[index].first();
    }

    //! ===============================
    //! Implementations
    //! ===============================
    
    pointer at_impl(size_type pos) const
    {
        return !pos ? m_members.m_first : at_unsafe_impl(pos);
    }

    pointer at_unsafe_impl(size_type pos) const
    {
        node_size_type index = get_node_index(pos);
        return m_members.m_node_vector[index][get_node_offset_unsafe(pos, index)];
    }

    void reserve_impl(size_type new_cap)
    {
        size_type cap = capacity();
        if (cap < new_cap)
        {
            node_size_type base = m_members.m_node_vector.size();
            m_members.m_node_vector.resize_uninitialized_unsafe(get_node_allocator(), get_node_index(new_cap));
            for (node_size_type i = base; i < m_members.m_node_vector.size(); ++i)
            {
                node_size_type n = 1 << i;
                auto& node = m_members.m_node_vector[i];
                node.m_base = allocator_traits::allocate(get_allocator(), n);
                node.m_count = n;
            }
        }
    }

    template<class U>
    void resize_impl(size_type size, const U& u)
    {
        if (!size)
        {
            clear_impl();
        }
        else if (size < m_size)
        {
            size_type cur_index = m_members.m_node_vector.last();
            size_type new_index = get_node_index(size);

            //! Purge fully released nodes
            for (size_type i = new_index; i < cur_index; ++i)
            {
                std::destroy_n(m_members.m_node_vector[i].m_base, m_members.m_node_vector[i].m_count);
            }

            //! Purge surplus elements in current node.
            pointer new_last = m_members.m_node_vector[new_index][get_node_offset_unsafe(m_members.m_size, new_index)];
            size_type surplus = m_members.m_node_vector[new_index].last() - new_last;

            if (surplus)
            {
                std::destroy_n(new_last + 1, surplus);
            }

            m_members.m_size = size;
            m_members.m_last = new_last;
        }
        else if (m_size < size)
        {
            if (1 < size)
            {
                reserve_impl(size);

                size_type cur_index = m_members.m_node_vector.last();
                size_type new_index = get_node_index(size);

                pointer cur_pointer = m_members.m_node_vector[cur_index][get_node_offset_unsafe(m_members.m_size, cur_index)];
                size_type num_fill = m_members.m_node_vector[cur_index].last() - cur_pointer;

                //! Fill current node
                if (num_fill)
                {
                    resize_construct_n_impl(cur_pointer + 1, num_fill, u);
                }

                //! Fill newly added nodes
                for (size_type i = new_index; i > cur_index; --i)
                {
                    resize_construct_n_impl(m_nodes[i].m_base, m_nodes[i].m_count);
                }

                m_members.m_size = size;
                m_members.m_last = m_nodes[new_index][get_node_offset_unsafe(m_members.m_size, new_index)];
            }
            else
            {
                resize_construct_impl(m_members.m_first, u);
                m_members.m_size = 1;
                m_members.m_last = m_members.m_first;
            }
        }
    }

    template<class U>
    void resize_construct_impl(pointer address, const U& u)
    {
        if constexpr (std::is_same_v<U, default_init_t>)
        {
            std::uninitialized_default_construct(address);
        }
        else if constexpr (std::is_same_v<U, value_init_t>)
        {
            std::uninitialized_value_construct(address);
        }
        else
        {
            std::uninitialized_fill(address, u);
        }
    }

    template<class U>
    void resize_construct_n_impl(pointer address, size_type count, const U& u)
    {
        if constexpr (std::is_same_v<U, default_init_t>)
        {
            std::uninitialized_default_construct_n(address, count);
        }
        else if constexpr (std::is_same_v<U, value_init_t>)
        {
            std::uninitialized_value_construct_n(address, count);
        }
        else
        {
            std::uninitialized_fill_n(address, count, u);
        }
    }

    void shrink_to_fit_impl()
    {
        size_type capacity = this->capacity();
        if (m_members.m_size < capacity)
        {
            size_type current = m_members.m_node_vector.capacity();
            size_type target = m_members.m_node_vector.size();

            //! Only shrink to the last allocated node to maintain stability (otherwise need to relocate elements)
            for (size_type i = target; i < current; ++i)
            {
                allocator_traits::deallocate(get_allocator(), m_members.m_node_vector[i].m_base, m_members.m_node_vector[i].m_count);
            }

            m_members.m_node_vector.shrink_to_fit();
        }
    }

    void clear_impl()
    {
        if (auto node_size = m_members.m_node_vector.size(); node_size != 0)
        {
            for (node_size_type i = 0; i < node_size; ++i)
            {
                std::destroy_n(m_members.m_node_vector[i].m_base, m_members.m_node_vector[i].m_count);
            }

            m_members.m_node_vector.clear();
        }

        m_members.m_size = 0;
        m_members.m_last = m_members.m_first;
    }

    void throw_if_out_of_range(size_type pos) const
    {
        if (size() < pos) [[unlikely]]
        {
            throw std::out_of_range("stable_vector::at out of range");
        }
    }

    void throw_if_overflow(size_type size) const
    {
        if (max_size() < size) [[unlikely]]
        {
            throw std::length_error("stable_vector::resize exceed max size");
        }
    }
};
