#pragma once

#include <cassert>
#include <cstdint>
#include <cstring> //! memset

#include <functional>
#include <type_traits>
#include <immintrin.h>

#ifndef Z_ATTR_OVERLAPPABLE
    #if defined(__has_cpp_attribute) && __has_cpp_attribute(no_unique_address)
        #if defined(_MSC_VER)
            #define Z_ATTR_OVERLAPPABLE [[msvc::no_unique_address]]
        #else
            #define Z_ATTR_OVERLAPPABLE [[no_unique_address]]
        #endif // #if defined(_MSC_VER)
    #else
        #define Z_ATTR_OVERLAPPABLE 
    #endif // #if defined(__has_cpp_attribute) && __has_cpp_attribute(no_unique_address)
#endif

//! Simplified red black tree for freelist allocator
#include "rbtree.inl"

namespace Zinc::Allocator
{
    //! ===============================
    //! Utilities
    //! ===============================
    namespace Internal
    {
        template <typename T, std::enable_if_t<std::is_integral_v<T>, bool> = true>
        constexpr bool IsPOT(T n)
        {
            return (n > 0) && !(n & (n - T(1)));
        }

        //! Alignment must be power of 2, as required by C++ specification.
        template <typename T, std::enable_if_t<std::is_integral_v<T> && std::is_unsigned_v<T>, bool> = true>
        constexpr T AlignUpPOT(T size, T alignment)
        {
            T mask = std::max<T>(alignment, T(1)) - T(1);
            return std::max<T>(size + mask & (~mask), alignment);
        }

        constexpr size_t ConstLog2(size_t val) 
        { 
            return val ? 1 + ConstLog2(val >> 1) : -1; 
        }

        //! Note: Functions suffixed with 32 only work for 32 bit unsigned integer specifically.
        uint8_t Log2POT32(uint32_t x)
        {
#if defined(_MSC_VER)
            return x <= 1 ? 0 : (32 - _lzcnt_u32(x - 1));
#else
            //! Default routine for gcc based compilers, if the compiler has different intrinsics 
            //! please consider adding special case like msvc.
            return x <= 1 ? 0 : (32 - __builtin_clz(x - 1));
#endif
        }

        uint32_t NextPOT32(uint32_t x) 
        {
            return 1u << Log2POT32(x);
        }
    }

    //! ===============================
    //! Shared data structures
    //! ===============================

    struct DefaultArenaTraits
    {
        using PtrType = void*;
        using OffsetType = uintptr_t;
        using SizeType = size_t;
    };

    //! Fancy name of a chunk of memory
    template<typename Traits = DefaultArenaTraits>
    struct Arena
    {
        using PtrType = Traits::PtrType;
        using OffsetType = Traits::OffsetType;
        using SizeType = size_t;

        Arena() = default;
        Arena(PtrType base)
            : m_base(base)
        {
        }
        Arena(PtrType base, SizeType size)
            : m_base(base)
            , m_size(size)
        {
        }

        PtrType m_base{ nullptr };
        SizeType m_size{ 0 };
    };

    template<typename ArenaType>
    class IAllocator
    {
    protected:
        using PtrType = ArenaType::PtrType;
        using OffsetType = ArenaType::OffsetType;
        using SizeType = ArenaType::SizeType;

        constexpr IAllocator() = default;
        constexpr IAllocator(PtrType base, SizeType size) : m_arena(base, size) {}

        ArenaType m_arena;
    };


    //! ===============================
    //! Monolith allocators
    //! ===============================

    //! This group of sub allocators are fully bounded, that is, they don't allocate any external memory 
    //! outside of the assigned arena, on the down side, it requires the arena must be fully committed 
    //! (can't be pure virtual address for gpu memory for example) for allocators to allocate extra metadata.
    namespace Monolith
    {
        //! Monolith allocators require the managed arena to be fully committed to allocate metadata therefore
        //! don't support pointer generalization.
        struct MonolithArenaTraits
        {
            using PtrType = void*;
            using OffsetType = uintptr_t;
            using SizeType = size_t;
        };

        using IAllocator = Zinc::Allocator::IAllocator<Arena<MonolithArenaTraits>>;

        class LinearAllocator final : /* private */ IAllocator
        {
            using Base = IAllocator;

        public:
            //! Required alignment of input arena, 1 means unaligned
            static constexpr size_t Alignment = 1;

            LinearAllocator(void* base, size_t size)
                : Base(base, size)
            {
            }

            ~LinearAllocator() = default;

            [[nodiscard]] void* Allocate(size_t size, size_t alignment = 1)
            {
                size_t alignedSize = Internal::AlignUpPOT(size, alignment);

                if (m_offset + alignedSize > m_arena.m_size) [[unlikely]]
                {
                    //! Fail on full
                    return nullptr;
                }
                else [[likely]]
                {
                    auto* ptr = (void*)(uintptr_t(m_arena.m_base) + uintptr_t(m_offset));
                    m_offset += alignedSize;
                    return ptr;
                }
            }

            void Deallocate(void*)
            {
            }

            void Reset()
            {
                m_offset = 0;
            }

        private:
            size_t m_offset{ 0 };
        };

        class StackAllocator final : IAllocator
        {
            using Base = IAllocator;

        public:
            static constexpr size_t TagSize = sizeof(size_t);

            //! Needs size_t aligned size tag.
            static constexpr size_t Alignment = sizeof(size_t);

            StackAllocator(void* base, size_t size)
                : Base(base, size)
            {
            }

            ~StackAllocator() = default;

            [[nodiscard]] void* Allocate(const size_t size, const size_t alignment = 1)
            {
                size_t alignedSize = Internal::AlignUpPOT(size, alignment);

                size_t t = m_bottomUpCursor + alignedSize;

                if ((t + TagSize) > m_topDownCursor) [[unlikely]]
                {
                    return nullptr;
                }

                auto* ptr = (void*)(uintptr_t(m_arena.m_base) + uintptr_t(m_bottomUpCursor));
                m_bottomUpCursor = t;

                size_t* tag = reinterpret_cast<size_t*>(uintptr_t(m_arena.m_base) + uintptr_t(m_topDownCursor));
                *tag = alignedSize;

                m_topDownCursor -= TagSize;

                return ptr;
            }

            void Deallocate(void* ptr)
            {
                if (m_bottomUpCursor == 0) [[unlikely]]
                {
                    return;
                }

                size_t t = m_topDownCursor + TagSize;
                size_t* tag = reinterpret_cast<size_t*>(uintptr_t(m_arena.m_base) + uintptr_t(t));
                m_bottomUpCursor -= (*tag);
                m_topDownCursor = t;
            }

            void Reset()
            {
                m_bottomUpCursor = 0;
                m_topDownCursor = m_arena.m_size;
            }

        private:
            size_t m_bottomUpCursor{ 0 };
            size_t m_topDownCursor{ 0 };
        };

        class PoolAllocator final : IAllocator
        {
            using Base = IAllocator;

        public:
            static constexpr size_t TagSize = sizeof(size_t);
            static constexpr size_t Alignment = sizeof(size_t);

            PoolAllocator(void* base, size_t size, size_t chunkSize)
                : Base(base, size)
                , m_chunkSize(chunkSize)
            {
                assert(chunkSize <= size && "Chunk size cannot be greater than arena size");
                assert(chunkSize >= TagSize && "It's recommanded to have chunk size as least as large as "
                    "sizeof(Traits::SizeType) for space efficiency, for ChunkSize == sizeof(Traits::SizeType), memory "
                    "overhead per allocation is 100%, that is, every allocation consumes 2x requested memory from arena");

                InitFreeList();
            }

            ~PoolAllocator() = default;

            [[nodiscard]] void* Allocate(const size_t size, const size_t alignment = 1)
            {
                assert(size <= m_chunkSize && alignment <= m_chunkSize && "Can't allocate memory larger than one chunk");

                if (m_freeChunkCount == 0) [[unlikely]]
                {
                    //! Out of memory, return failure.
                    return nullptr;
                }

                return PopFreeChunk();
            }

            void Deallocate(void* ptr)
            {
                assert(uintptr_t(ptr) > uintptr_t(m_arena.m_base) && 
                    uintptr_t(ptr) < (uintptr_t(m_arena.m_base) + uintptr_t(m_arena.m_size)) && 
                    "Pointer out of range");

                auto offset = size_t(uintptr_t(ptr) - uintptr_t(m_arena.m_base));
                assert((offset % m_chunkSize == 0) && "Pointer misaligned");

                PushFreeChunk(offset);
            }

            void Reset()
            {
                //! This will invalidate all allocated chunks immediately.
                InitFreeList();
            }

            size_t GetChunkSize() const 
            { 
                return m_chunkSize; 
            }

        private:
            void InitFreeList()
            {
                size_t numChunks = m_arena.m_size / (m_chunkSize + TagSize);

                //! Prewarm free list
                m_freeListCursor = size_t(uintptr_t(m_arena.m_base) + uintptr_t(m_arena.m_size));
                for (size_t i = 0; i < numChunks; ++i)
                {
                    PushFreeChunk(i * m_chunkSize);
                }
            }

            void PushFreeChunk(size_t offset)
            {
                m_freeListCursor -= TagSize;
                *((size_t*)(uintptr_t(m_arena.m_base) + uintptr_t(m_freeListCursor))) = offset;
                m_freeChunkCount++;
            }

            void* PopFreeChunk()
            {
                size_t offset = *(size_t*)(uintptr_t(m_arena.m_base) + uintptr_t(m_freeListCursor));
                void* address = (void*)(uintptr_t(m_arena.m_base) + uintptr_t(offset));
                m_freeChunkCount--;
                return address;
            }

            size_t m_chunkSize{ 0 };

            size_t m_freeListCursor{ 0 };
            size_t m_freeChunkCount{ 0 };
        };

        //! Simple binary buddy allocator based on Julian Vernay's implementation:
        //! https://jvernay.fr/en/blog/buddy-allocator/implementation/
        class BuddyAllocator final : IAllocator
        {
            using Base = IAllocator;

        public:
            //! This allow us to have 28 bit block index and compress block header into one qword.
            static constexpr uint32_t BlockSizeMin = 16;

            //! Since I used 28 bit unsigned int for indexing, the maximum block count can be no larger than 
            //! 2 ^ 28, which means the maximum block size is 2 ^ 31 (at least two largest block, each consists
            //! of 2 ^ 27 smallest block, each 2 ^ 4 bytes) which is 4GB in total and could cover most of use 
            //! cases. On a typical 64 bit operating system a process's virtual memory space is usually much 
            //! smaller than 64 bit (e.g. 47 bit on Windows) because systems usually have to reserve few bits 
            //! for various tasks like page management.
            static constexpr uint32_t BlockSizeMax = 1ull << 31;

            //! 8 byte aligned block header.
            static constexpr size_t Alignment = sizeof(uint64_t);

            BuddyAllocator(void* base, size_t size, size_t metadataSize,
                uint32_t blockSizeMin = BlockSizeMin, uint32_t blockSizeMax = BlockSizeMax)
                : Base(base, size)
                , m_blockSizeMinLog2(static_cast<uint8_t>(Internal::Log2POT32(blockSizeMin)))
                , m_blockSizeMaxLog2(static_cast<uint8_t>(Internal::Log2POT32(blockSizeMax)))
            {
                size_t contentSize = size - metadataSize;

                m_poolCount = 1 + static_cast<uint8_t>(m_blockSizeMaxLog2 - m_blockSizeMinLog2);
                m_blockCount = static_cast<uint32_t>(contentSize >> m_blockSizeMinLog2);

                //! Equivalent to BlockList[BlockCount], used offset to support copy / move
                m_blockListCursor = contentSize;

                //! Equivalent to FreeLists[PoolCount]
                m_freeListsCursor = contentSize + sizeof(Block) * m_blockCount;

                ValidateParams(size, metadataSize, blockSizeMin, blockSizeMax);

                InitFreeList();
            }

            ~BuddyAllocator() = default;

            static size_t GetMetadataSizeRequirement(size_t arenaSize, uint32_t blockSizeMin, uint32_t blockSizeMax)
            {
                assert(Internal::IsPOT(blockSizeMin) && "blockSizeMin must be power of 2");
                assert(Internal::IsPOT(blockSizeMax) && "blockSizeMax must be power of 2");
                assert((arenaSize >> 1) >= blockSizeMax && "Arena must be at least twice as large as blockSizeMax");

                uint32_t poolCount = 1 + static_cast<uint8_t>(Internal::Log2POT32(blockSizeMax / blockSizeMin));
                uint32_t blockCount = static_cast<uint32_t>(arenaSize / blockSizeMin);
                return sizeof(FreeListEntry) * poolCount + sizeof(Block) * blockCount;
            }

            [[nodiscard]] void* Allocate(const size_t size, const size_t alignment = 1)
            {
                size_t alignedSize = Internal::AlignUpPOT(size, 
                    static_cast<size_t>(std::max(static_cast<uint32_t>(alignment), 1u << m_blockSizeMinLog2)));

                uint32_t offset = AllocateBlock(static_cast<uint32_t>(alignedSize));

                return (void*)(uintptr_t(m_arena.m_base) + uintptr_t(offset));
            }

            void Deallocate(void* ptr)
            {
                uint32_t offset = static_cast<uint32_t>(uintptr_t(ptr) - uintptr_t(m_arena.m_base));

                assert((offset < ((1u << m_blockSizeMinLog2) * m_blockCount)) && 
                    ((offset % (1u << m_blockSizeMinLog2)) == 0) && "Invalid pointer");

                DeallocateBlock(offset);
            }

            void Reset()
            {
                //! Reset free list
                InitFreeList();
            }

            size_t GetPoolCount() const { return m_poolCount; }
            size_t GetBlockCount() const { return m_blockCount; }

        private:
            //! Compressed block header
            struct Block
            {
                static constexpr uint64_t InFreeListShift = 62;
                static constexpr uint64_t PoolIndexShift = 56;
                static constexpr uint64_t PrevShift = 28;

                static constexpr uint64_t InFreeListMask    = 1ull            << InFreeListShift;
                static constexpr uint64_t PoolIndexMask     = 0x3Full         << PoolIndexShift;
                static constexpr uint64_t PrevMask          = 0xFFFFFFFull    << PrevShift;
                static constexpr uint64_t NextMask          = 0xFFFFFFFull;

                static constexpr uint64_t InitialValue = (PrevMask | NextMask);

                void SetInFreeList(bool inFreeList) 
                { 
                    m_v = (m_v & (~InFreeListMask)) | (uint64_t(inFreeList) << InFreeListShift); 
                }

                void SetPoolIndex(uint8_t poolIndex) 
                { 
                    assert((poolIndex & uint8_t(~0x3F)) == 0 && "Invalid pool index");
                    m_v = (m_v & (~PoolIndexMask)) | (uint64_t(poolIndex) << PoolIndexShift);
                }

                void SetPrev(uint32_t prev)
                {
                    assert((prev & uint32_t(~0xFFFFFFF)) == 0 && "Invalid prev index");
                    m_v = (m_v & (~PrevMask)) | (uint64_t(prev) << PrevShift);
                }

                void SetNext(uint32_t next)
                {
                    assert((next & uint32_t(~0xFFFFFFF)) == 0 && "Invalid next index");
                    m_v = (m_v & (~NextMask)) | uint64_t(next);
                }

                bool GetInFreeList() const { return m_v & InFreeListMask; }
                uint8_t GetPoolIndex() const { return (m_v & PoolIndexMask) >> PoolIndexShift; }
                uint32_t GetPrev() const { return static_cast<uint32_t>((m_v & PrevMask) >> PrevShift); }
                uint32_t GetNext() const { return static_cast<uint32_t>(m_v & NextMask); }

                //! Reset both m_prev and m_next to InvalidBlockIndex
                void ResetLinkage() { m_v |= (PrevMask | NextMask); }

            private:
                //! unused       : 1
                //! m_inFreeList : 1
                //! m_poolIndex  : 6
                //! m_prev       : 28
                //! m_next       : 28
                uint64_t m_v = 0;
            };

            struct FreeListEntry
            {
                uint32_t m_head;
                uint32_t m_tail;
            };

            static constexpr uint32_t InvalidBlockIndex = static_cast<uint32_t>(Block::NextMask);

            static void AddBlockToFreeList(FreeListEntry* freeLists, Block* blocks, uint32_t index)
            {
                Block& block = blocks[index];
                FreeListEntry& freeList = freeLists[block.GetPoolIndex()];

                //! Insert new block to head
                uint32_t oldHead = freeList.m_head;
                freeList.m_head = index;

                if (oldHead == InvalidBlockIndex)
                {
                    //! Empty list, init as tail
                    freeList.m_tail = index;

                    //! Reset block linkage
                    block.ResetLinkage();
                }
                else
                {
                    //! Link nodes
                    block.SetNext(oldHead);
                    blocks[oldHead].SetPrev(index);
                }
            }

            static void RemoveBlockFromFreeList(FreeListEntry* freeLists, Block* blocks, uint32_t index)
            {
                Block& block = blocks[index];
                FreeListEntry& freeList = freeLists[block.GetPoolIndex()];

                uint32_t prev = block.GetPrev();
                uint32_t next = block.GetNext();
                if (prev == InvalidBlockIndex)
                {
                    freeList.m_head = next;
                }
                else
                {
                    blocks[prev].SetNext(next);
                }

                if (next == InvalidBlockIndex)
                {
                    freeList.m_tail = prev;
                }
                else
                {
                    blocks[next].SetPrev(prev);
                }
            }

            void InitFreeList()
            {
                uint32_t stride = uint32_t(1u) << (m_blockSizeMaxLog2 - m_blockSizeMinLog2); //! m_blockSizeMax / m_blockSizeMin
                uint32_t indexMax = m_blockCount - stride;
                uint32_t pool = m_poolCount - 1;

                static constexpr uint64_t FreeListInitValue = 
                    (static_cast<uint64_t>(InvalidBlockIndex) << 32) | InvalidBlockIndex;

                //! Initialize free lists 
                auto* freeLists = GetFreeList();
                uint64_t* freeListInitializer = reinterpret_cast<uint64_t*>(freeLists);
                for (uint32_t i = 0; i < pool; ++i)
                {
                    //! Equivalent to:
                    //!     m_head = UINT32_MAX;
                    //!     m_tail = UINT32_MAX;
                    freeListInitializer[i] = FreeListInitValue;
                }

                freeLists[pool].m_head = 0;
                freeLists[pool].m_tail = indexMax;

                auto* blocks = GetBlockList();

                //! Init blocklist, this is required to get proper invalid value when querying unallocated 
                //! buddy on deallocation.
                memset(blocks, 0, m_blockCount * sizeof(Block));

                //! Initialize the free list of biggest slots
                auto* blocks = GetBlockList();
                for (uint32_t i = 0; i <= indexMax; i += stride)
                {
                    auto& block = blocks[i];
                    block.SetInFreeList(true);
                    block.SetPoolIndex(pool);
                    block.SetPrev(i == 0 ? InvalidBlockIndex : i - stride);
                    block.SetNext(i == indexMax ? InvalidBlockIndex : i + stride);
                }
            }

            uint32_t AllocateBlock(uint32_t size)
            {
                uint8_t targetPool = Internal::Log2POT32(Internal::NextPOT32(size)) - m_blockSizeMinLog2;

                //! 1. Search for a free block of at least requested size
                auto* freeLists = GetFreeList();

                uint8_t pool = targetPool;
                uint32_t block = InvalidBlockIndex;
                for (; pool < m_poolCount; ++pool)
                {
                    block = freeLists[pool].m_head;
                    if (block != InvalidBlockIndex)
                    {
                        break;
                    }
                }

                if (block == InvalidBlockIndex)
                {
                    //! No available block found, out of memory
                    return InvalidBlockIndex;
                }

                //! 2. Remove block from freelist
                auto* blocks = GetBlockList();
                {
                    Block& b = blocks[block];
                    b.SetInFreeList(false);
                    b.SetPoolIndex(pool);
                }
                RemoveBlockFromFreeList(freeLists, blocks, block);

                //! 3. Split slot if necessary, adding the buddies to appropriate freelists
                while (pool > targetPool)
                {
                    pool--;
                    uint32_t buddy = block ^ (uint32_t(1) << pool);
                    blocks[buddy].SetInFreeList(true);
                    blocks[buddy].SetPoolIndex(pool);
                    AddBlockToFreeList(freeLists, blocks, buddy);
                }

                return (block << m_blockSizeMinLog2);
            }

            void DeallocateBlock(uint32_t offset)
            {
                uint32_t block = offset >> m_blockSizeMinLog2;
                auto* blocks = GetBlockList();

                uint8_t pool = blocks[block].GetPoolIndex();
                uint8_t poolOld = pool;

                auto* freeLists = GetFreeList();

                for (; (pool < m_poolCount - 1); ++pool)
                {
                    //! See whether the buddy is free, in which case remove it from freelist
                    uint32_t buddy = block ^ (uint32_t(1) << pool);
                    if (!blocks[buddy].GetInFreeList() || blocks[buddy].GetPoolIndex() < pool)
                    {
                        //! Buddy isn't in freelist (either allocated or in a lower level freelist)
                        break;
                    }

                    //! Remove buddy from local free list to merge upward.
                    blocks[buddy].SetInFreeList(false);
                    RemoveBlockFromFreeList(freeLists, blocks, buddy);

                    //! Update block index to the fusion of the two blocks
                    block &= UINT32_MAX << (pool + 1);
                }

                //! Add the new block to corresponding freelist
                blocks[block].SetInFreeList(true);
                blocks[block].SetPoolIndex(pool);
                AddBlockToFreeList(freeLists, blocks, block);
            }

            Block* GetBlockList()
            {
                return (Block*)(uintptr_t(m_arena.m_base) + uintptr_t(m_blockListCursor));
            }

            FreeListEntry* GetFreeList()
            {
                return (FreeListEntry*)(uintptr_t(m_arena.m_base) + uintptr_t(m_freeListsCursor));
            }

            void ValidateParams(size_t size, size_t metadataSize, uint32_t blockSizeMin, uint32_t blockSizeMax)
            {
                assert(blockSizeMin >= BlockSizeMin && blockSizeMax <= BlockSizeMax && 
                    "Out of range blockSizeMin / blockSizeMax");
                assert(m_blockSizeMinLog2 <= m_blockSizeMaxLog2 && "blockSizeMin should be no larger than blockSizeMax");
                assert(Internal::IsPOT(blockSizeMin) && "blockSizeMin must be power of 2");
                assert(Internal::IsPOT(blockSizeMax) && "blockSizeMax must be power of 2");
                assert(((size - metadataSize) >> 1) >= blockSizeMax && 
                    "Arena size excludes metadata must be at least twice as large as blockSizeMax");
                assert(metadataSize == GetMetadataSizeRequirement(size - metadataSize, blockSizeMin, blockSizeMax) &&
                    "Invalid metadata size");
                assert(m_blockCount > 0 && m_poolCount > 0 && "Invalid pool or block count");
                assert((((size - metadataSize) / blockSizeMin) < UINT32_MAX) && "Block count overflow");
            }

            size_t m_blockListCursor{ 0 };
            size_t m_freeListsCursor{ 0 };

            uint32_t m_blockCount{ 0 };
            uint8_t m_poolCount{ 0 };

            //! Cached log2(blockSizeMin/Max), they are more useful than original values which can be 
            //! easily reconstructed as well (1 cycle bit shift).
            uint8_t m_blockSizeMinLog2{ 0 };
            uint8_t m_blockSizeMaxLog2{ 0 };
        };

        class FreeListAllocator final
        {
            
        };

        //! 
        class TLSFAllocator final
        {

        };
    }
}