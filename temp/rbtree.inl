#pragma once

#include <cassert>
#include <cstdint>
#include <functional>

//! Super simplified version of boost::intrusive::rbtree (1.85).
//! Used lower case camel to match with the original boost implementation (easier for comparison). 

struct rbtree_base
{
    enum color { red, black };
};

template <class T>
struct rbtree_node
{
    using ptr = T*;
    using void_ptr = void*;
    using const_ptr = const ptr;
    using color = rbtree_base::color;

    static constexpr color color_red = color::red;
    static constexpr color color_black = color::black;

    static constexpr uintptr_t color_mask = uintptr_t(1);
    static constexpr uintptr_t parent_mask = ~uintptr_t(color_mask);

    inline static ptr to_ptr(T& n) { return &n; }
    inline static const_ptr to_ptr(const T& n) { return &n; }
    
    color get_color() const { return color(uintptr_t(m_pc) & color_mask); }
    ptr get_parent() const { return (ptr)(uintptr_t(m_pc) & parent_mask); }

    void set_color(color color) { m_pc = (ptr)((uintptr_t(m_pc) & parent_mask) | uintptr_t(color)); }
    void set_parent(ptr p) { m_pc = (ptr)((uintptr_t(m_pc) & color_mask) | uintptr_t(p)); }

    //! Packed parent pointer and color. Color is encoded in the least significant bit that
    //! shadowed by alignment (at least 2).
    ptr m_pc = 0;
    ptr m_left{ nullptr };
    ptr m_right{ nullptr };
};

template<class Node>
struct rbtree_node_traits
{
    using node = Node;
    using node_ptr = node::ptr;
    using const_node_ptr = node::const_ptr;
    using color = node::color;

    inline static node_ptr to_node_ptr(Node& n) { return Node::to_ptr(n); }
    inline static const_node_ptr to_node_ptr(const Node& n) { return Node::to_ptr(n); }

    inline static node_ptr get_parent(const_node_ptr n) { return n->get_parent(); }
    inline static void set_parent(node_ptr n, node_ptr p) { n->set_parent(p); }

    inline static color get_color(const_node_ptr n) { return n->get_color(); }
    inline static void set_color(node_ptr n, color c) { n->set_color(c); }

    inline static node_ptr get_left(const_node_ptr n) { return n->m_left; }
    inline static void set_left(node_ptr n, node_ptr l) { n->m_left = l; }

    inline static node_ptr get_right(const_node_ptr n) { return n->m_right; }
    inline static void set_right(node_ptr n, node_ptr r) { n->m_right = r; }

    inline static color red() { return node::color_red; }
    inline static color black() { return node::color_black; }
};

template<class NodeTraits>
class rbtree_impl
{
public:
    using node_traits = NodeTraits;
    using node = NodeTraits::node;
    using node_ptr = NodeTraits::node_ptr;
    using const_node_ptr = NodeTraits::const_node_ptr;
    using color = NodeTraits::color;

protected:
    struct rebalance_info
    {
        node_ptr x;
        node_ptr x_parent;
        node_ptr y;
    };

    struct insert_commit_data
    {
        bool link_left = false;
        node_ptr node{};
    };

    template<class Disposer>
    struct dispose_subtree_disposer
    {
        inline dispose_subtree_disposer(Disposer& disp, node_ptr subtree)
            : disposer_(&disp), subtree_(subtree)
        {
        }

        inline void release()
        {
            disposer_ = 0;
        }

        inline ~dispose_subtree_disposer()
        {
            if (disposer_) 
            {
                dispose_subtree(subtree_, *disposer_);
            }
        }
        Disposer* disposer_;
        const node_ptr subtree_;
    };

    template<class NodeTraits, class F>
    struct node_cloner
    {
        typedef typename NodeTraits::node_ptr  node_ptr;

        explicit node_cloner(const F& f)
            : t_(f)
        {
        }

        node_ptr operator()(node_ptr p)
        {
            node_ptr n = t_(p);
            NodeTraits::set_color(n, NodeTraits::get_color(p));
            return n;
        }

    private:
        F t_;
    };

    static node_ptr uncast(const_node_ptr ptr)
    {
        return const_cast<node_ptr>(ptr);
    }

public:
    //! <b>Requires</b>: 'n' is a node of the tree or a node initialized
    //!   by init(...) or init_node.
    //!
    //! <b>Effects</b>: Returns true if the node is initialized by init() or init_node().
    //!
    //! <b>Complexity</b>: Constant time.
    //!
    //! <b>Throws</b>: Nothing.
    static bool unique(node_ptr n) noexcept
    {
        return !node_traits::get_parent(n);
    }

    //! <b>Requires</b>: 'header' is the header node of a tree.
    //!
    //! <b>Effects</b>: Returns the first node of the tree, the header if the tree is empty.
    //!
    //! <b>Complexity</b>: Constant time.
    //!
    //! <b>Throws</b>: Nothing.
    static node_ptr begin_node(const_node_ptr header) noexcept
    {
        return node_traits::get_left(header);
    }

    //! <b>Requires</b>: 'header' is the header node of a tree.
    //!
    //! <b>Effects</b>: Returns the header of the tree.
    //!
    //! <b>Complexity</b>: Constant time.
    //!
    //! <b>Throws</b>: Nothing.
    static node_ptr end_node(const_node_ptr header) noexcept
    {
        return uncast(header);
    }

    //! <b>Requires</b>: 'header' is the header node of a tree.
    //!
    //! <b>Effects</b>: Returns the root of the tree if any, header otherwise
    //!
    //! <b>Complexity</b>: Constant time.
    //!
    //! <b>Throws</b>: Nothing.
    static node_ptr root_node(const_node_ptr header) noexcept
    {
        node_ptr p = node_traits::get_parent(header);
        return p ? p : uncast(header);
    }

    //! <b>Requires</b>: 'n' is a node of a tree but not the header.
    //!
    //! <b>Effects</b>: Returns the minimum node of the subtree starting at p.
    //!
    //! <b>Complexity</b>: Logarithmic to the size of the subtree.
    //!
    //! <b>Throws</b>: Nothing.
    static node_ptr minimum(node_ptr n)
    {
        for (node_ptr p_left = node_traits::get_left(n)
            ; p_left
            ; p_left = node_traits::get_left(n)) {
            n = p_left;
        }
        return n;
    }

    //! <b>Requires</b>: 'n' is a node of a tree but not the header.
    //!
    //! <b>Effects</b>: Returns the maximum node of the subtree starting at p.
    //!
    //! <b>Complexity</b>: Logarithmic to the size of the subtree.
    //!
    //! <b>Throws</b>: Nothing.
    static node_ptr maximum(node_ptr n)
    {
        for (node_ptr p_right = node_traits::get_right(n)
            ; p_right
            ; p_right = node_traits::get_right(n)) {
            n = p_right;
        }
        return n;
    }

    //! <b>Requires</b>: p is a node of a tree.
    //!
    //! <b>Effects</b>: Returns true if p is the header of the tree.
    //!
    //! <b>Complexity</b>: Constant.
    //!
    //! <b>Throws</b>: Nothing.
    static bool is_header(node_ptr p)
    {
        node_ptr p_left(node_traits::get_left(p));
        node_ptr p_right(node_traits::get_right(p));
        if (!node_traits::get_parent(p) || //Header condition when empty tree
            (p_left && p_right &&         //Header always has leftmost and rightmost
                (p_left == p_right ||      //Header condition when only node
                    (node_traits::get_parent(p_left) != p ||
                        node_traits::get_parent(p_right) != p))
                //When tree size > 1 headers can't be leftmost's
                //and rightmost's parent
                )) {
            return true;
        }
        return false;
    }

    //! <b>Requires</b>: 'n' is a node from the tree except the header.
    //!
    //! <b>Effects</b>: Returns the next node of the tree.
    //!
    //! <b>Complexity</b>: Average constant time.
    //!
    //! <b>Throws</b>: Nothing.
    static node_ptr next_node(node_ptr n) noexcept
    {
        node_ptr const n_right(node_traits::get_right(n));
        if (n_right) {
            return minimum(n_right);
        }
        else {
            node_ptr p(node_traits::get_parent(n));
            while (n == node_traits::get_right(p)) {
                n = p;
                p = node_traits::get_parent(p);
            }
            return node_traits::get_right(n) != p ? p : n;
        }
    }

    //! <b>Requires</b>: 'n' is a node from the tree except the leftmost node.
    //!
    //! <b>Effects</b>: Returns the previous node of the tree.
    //!
    //! <b>Complexity</b>: Average constant time.
    //!
    //! <b>Throws</b>: Nothing.
    static node_ptr prev_node(node_ptr n) noexcept
    {
        if (is_header(n)) {
            return node_traits::get_right(n);
        }
        else if (node_traits::get_left(n)) {
            return maximum(node_traits::get_left(n));
        }
        else {
            node_ptr p(n);
            node_ptr x = node_traits::get_parent(p);
            while (p == node_traits::get_left(x)) {
                p = x;
                x = node_traits::get_parent(x);
            }
            return x;
        }
    }

    //! <b>Requires</b>: 'n' must not be part of any tree.
    //!
    //! <b>Effects</b>: After the function unique(node) == true.
    //!
    //! <b>Complexity</b>: Constant.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Nodes</b>: If node is inserted in a tree, this function corrupts the tree.
    static void init(node_ptr n) noexcept
    {
        node_traits::set_parent(n, node_ptr());
        node_traits::set_left(n, node_ptr());
        node_traits::set_right(n, node_ptr());
    }

    //! <b>Requires</b>: header must not be part of any tree.
    //!
    //! <b>Effects</b>: Initializes the header to represent an empty tree.
    //!   unique(header) == true.
    //!
    //! <b>Complexity</b>: Constant.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Nodes</b>: If header is inserted in a tree, this function corrupts the tree.
    static void init_header(node_ptr header) noexcept
    {
        node_traits::set_parent(header, node_ptr());
        node_traits::set_left(header, header);
        node_traits::set_right(header, header);
        node_traits::set_color(header, node_traits::red());
    }

    //! <b>Requires</b>: "disposer" must be an object function
    //!   taking a node_ptr parameter and shouldn't throw.
    //!
    //! <b>Effects</b>: Empties the target tree calling
    //!   <tt>void disposer::operator()(node_ptr)</tt> for every node of the tree
    //!    except the header.
    //!
    //! <b>Complexity</b>: Linear to the number of element of the source tree plus the.
    //!   number of elements of tree target tree when calling this function.
    //!
    //! <b>Throws</b>: Nothing.
    template<class Disposer>
    static void clear_and_dispose(node_ptr header, Disposer disposer) noexcept
    {
        node_ptr source_root = node_traits::get_parent(header);
        if (!source_root)
            return;
        dispose_subtree(source_root, disposer);
        init_header(header);
    }

    template<class Disposer>
    static void dispose_subtree(node_ptr x, Disposer disposer) noexcept
    {
        while (x) {
            node_ptr save(node_traits::get_left(x));
            if (save) {
                // Right rotation
                node_traits::set_left(x, node_traits::get_right(save));
                node_traits::set_right(save, x);
            }
            else {
                save = node_traits::get_right(x);
                init(x);
                disposer(x);
            }
            x = save;
        }
    }

    template <class Cloner, class Disposer>
    static node_ptr clone_subtree
    (const_node_ptr source_parent, node_ptr target_parent
        , Cloner cloner, Disposer disposer
        , node_ptr& leftmost_out, node_ptr& rightmost_out
    )
    {
        node_ptr target_sub_root = target_parent;
        node_ptr source_root = node_traits::get_parent(source_parent);
        if (!source_root) {
            leftmost_out = rightmost_out = source_root;
        }
        else {
            //We'll calculate leftmost and rightmost nodes while iterating
            node_ptr current = source_root;
            node_ptr insertion_point = target_sub_root = cloner(current);

            //We'll calculate leftmost and rightmost nodes while iterating
            node_ptr leftmost = target_sub_root;
            node_ptr rightmost = target_sub_root;

            //First set the subroot
            node_traits::set_left(target_sub_root, node_ptr());
            node_traits::set_right(target_sub_root, node_ptr());
            node_traits::set_parent(target_sub_root, target_parent);

            dispose_subtree_disposer<Disposer> rollback(disposer, target_sub_root);
            while (true) {
                //First clone left nodes
                if (node_traits::get_left(current) &&
                    !node_traits::get_left(insertion_point)) {
                    current = node_traits::get_left(current);
                    node_ptr temp = insertion_point;
                    //Clone and mark as leaf
                    insertion_point = cloner(current);
                    node_traits::set_left(insertion_point, node_ptr());
                    node_traits::set_right(insertion_point, node_ptr());
                    //Insert left
                    node_traits::set_parent(insertion_point, temp);
                    node_traits::set_left(temp, insertion_point);
                    //Update leftmost
                    if (rightmost == target_sub_root)
                        leftmost = insertion_point;
                }
                //Then clone right nodes
                else if (node_traits::get_right(current) &&
                    !node_traits::get_right(insertion_point)) {
                    current = node_traits::get_right(current);
                    node_ptr temp = insertion_point;
                    //Clone and mark as leaf
                    insertion_point = cloner(current);
                    node_traits::set_left(insertion_point, node_ptr());
                    node_traits::set_right(insertion_point, node_ptr());
                    //Insert right
                    node_traits::set_parent(insertion_point, temp);
                    node_traits::set_right(temp, insertion_point);
                    //Update rightmost
                    rightmost = insertion_point;
                }
                //If not, go up
                else if (current == source_root) {
                    break;
                }
                else {
                    //Branch completed, go up searching more nodes to clone
                    current = node_traits::get_parent(current);
                    insertion_point = node_traits::get_parent(insertion_point);
                }
            }
            rollback.release();
            leftmost_out = leftmost;
            rightmost_out = rightmost;
        }
        return target_sub_root;
    }

    //! <b>Requires</b>: "cloner" must be a function
    //!   object taking a node_ptr and returning a new cloned node of it. "disposer" must
    //!   take a node_ptr and shouldn't throw.
    //!
    //! <b>Effects</b>: First empties target tree calling
    //!   <tt>void disposer::operator()(node_ptr)</tt> for every node of the tree
    //!    except the header.
    //!
    //!   Then, duplicates the entire tree pointed by "source_header" cloning each
    //!   source node with <tt>node_ptr Cloner::operator()(node_ptr)</tt> to obtain
    //!   the nodes of the target tree. If "cloner" throws, the cloned target nodes
    //!   are disposed using <tt>void disposer(node_ptr )</tt>.
    //!
    //! <b>Complexity</b>: Linear to the number of element of the source tree plus the
    //!   number of elements of tree target tree when calling this function.
    //!
    //! <b>Throws</b>: If cloner functor throws. If this happens target nodes are disposed.
    template <class Cloner, class Disposer>
    static void clone(const_node_ptr source_header, node_ptr target_header, Cloner cloner, Disposer disposer)
    {
        node_cloner<node_traits, Cloner> node_cloner(cloner);

        if (!unique(target_header)) 
        {
            clear_and_dispose(target_header, disposer);
        }

        node_ptr leftmost, rightmost;
        node_ptr new_root = clone_subtree(source_header, target_header, node_cloner, disposer, leftmost, rightmost);

        //! Now update header node
        node_traits::set_parent(target_header, new_root);
        node_traits::set_left(target_header, leftmost);
        node_traits::set_right(target_header, rightmost);
    }

    template<class KeyType, class KeyNodePtrCompare>
    static node_ptr lower_bound_loop(node_ptr x, node_ptr y, const KeyType& key, KeyNodePtrCompare comp)
    {
        while (x) {
            if (comp(x, key)) {
                x = node_traits::get_right(x);
            }
            else {
                y = x;
                x = node_traits::get_left(x);
            }
        }
        return y;
    }

    template<class KeyType, class KeyNodePtrCompare>
    static node_ptr upper_bound_loop(node_ptr x, node_ptr y, const KeyType& key, KeyNodePtrCompare comp)
    {
        while (x) {
            if (comp(key, x)) {
                y = x;
                x = node_traits::get_left(x);
            }
            else {
                x = node_traits::get_right(x);
            }
        }
        return y;
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   KeyNodePtrCompare is a function object that induces a strict weak
    //!   ordering compatible with the strict weak ordering used to create the
    //!   the tree. KeyNodePtrCompare can compare KeyType with tree's node_ptrs.
    //!
    //! <b>Effects</b>: Returns a node_ptr to the first element that is
    //!   not less than "key" according to "comp" or "header" if that element does
    //!   not exist.
    //!
    //! <b>Complexity</b>: Logarithmic.
    //!
    //! <b>Throws</b>: If "comp" throws.
    template<class KeyType, class KeyNodePtrCompare>
    inline static node_ptr lower_bound
    (const_node_ptr header, const KeyType& key, KeyNodePtrCompare comp)
    {
        return lower_bound_loop(node_traits::get_parent(header), uncast(header), key, comp);
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   KeyNodePtrCompare is a function object that induces a strict weak
    //!   ordering compatible with the strict weak ordering used to create the
    //!   the tree. KeyNodePtrCompare can compare KeyType with tree's node_ptrs.
    //!
    //! <b>Effects</b>: Returns a node_ptr to the first element that is greater
    //!   than "key" according to "comp" or "header" if that element does not exist.
    //!
    //! <b>Complexity</b>: Logarithmic.
    //!
    //! <b>Throws</b>: If "comp" throws.
    template<class KeyType, class KeyNodePtrCompare>
    inline static node_ptr upper_bound
    (const_node_ptr header, const KeyType& key, KeyNodePtrCompare comp)
    {
        return upper_bound_loop(node_traits::get_parent(header), uncast(header), key, comp);
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   KeyNodePtrCompare is a function object that induces a strict weak
    //!   ordering compatible with the strict weak ordering used to create the
    //!   the tree. KeyNodePtrCompare can compare KeyType with tree's node_ptrs.
    //!   'lower_key' must not be greater than 'upper_key' according to 'comp'. If
    //!   'lower_key' == 'upper_key', ('left_closed' || 'right_closed') must be true.
    //!
    //! <b>Effects</b>: Returns an a pair with the following criteria:
    //!
    //!   first = lower_bound(lower_key) if left_closed, upper_bound(lower_key) otherwise
    //!
    //!   second = upper_bound(upper_key) if right_closed, lower_bound(upper_key) otherwise
    //!
    //! <b>Complexity</b>: Logarithmic.
    //!
    //! <b>Throws</b>: If "comp" throws.
    //!
    //! <b>Note</b>: This function can be more efficient than calling upper_bound
    //!   and lower_bound for lower_key and upper_key.
    //!
    //! <b>Note</b>: Experimental function, the interface might change.
    template< class KeyType, class KeyNodePtrCompare>
    static std::pair<node_ptr, node_ptr> bounded_range
    (const_node_ptr header, const KeyType& lower_key , const KeyType& upper_key, KeyNodePtrCompare comp
        , bool left_closed, bool right_closed)
    {
        node_ptr y = uncast(header);
        node_ptr x = node_traits::get_parent(header);

        while (x) {
            //If x is less than lower_key the target
            //range is on the right part
            if (comp(x, lower_key)) {
                //Check for invalid input range
                assert(comp(x, upper_key));
                x = node_traits::get_right(x);
            }
            //If the upper_key is less than x, the target
            //range is on the left part
            else if (comp(upper_key, x)) {
                y = x;
                x = node_traits::get_left(x);
            }
            else {
                //x is inside the bounded range(lower_key <= x <= upper_key),
                //so we must split lower and upper searches
                //
                //Sanity check: if lower_key and upper_key are equal, then both left_closed and right_closed can't be false
                assert(left_closed || right_closed || comp(lower_key, x) || comp(x, upper_key));
                return std::pair<node_ptr, node_ptr>(
                    left_closed
                    //If left_closed, then comp(x, lower_key) is already the lower_bound
                    //condition so we save one comparison and go to the next level
                    //following traditional lower_bound algo
                    ? lower_bound_loop(node_traits::get_left(x), x, lower_key, comp)
                    //If left-open, comp(x, lower_key) is not the upper_bound algo
                    //condition so we must recheck current 'x' node with upper_bound algo
                    : upper_bound_loop(x, y, lower_key, comp)
                    ,
                    right_closed
                    //If right_closed, then comp(upper_key, x) is already the upper_bound
                    //condition so we can save one comparison and go to the next level
                    //following lower_bound algo
                    ? upper_bound_loop(node_traits::get_right(x), y, upper_key, comp)
                    //If right-open, comp(upper_key, x) is not the lower_bound algo
                    //condition so we must recheck current 'x' node with lower_bound algo
                    : lower_bound_loop(x, y, upper_key, comp)
                );
            }
        }
        return std::pair<node_ptr, node_ptr>(y, y);
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   KeyNodePtrCompare is a function object that induces a strict weak
    //!   ordering compatible with the strict weak ordering used to create the
    //!   the tree. KeyNodePtrCompare can compare KeyType with tree's node_ptrs.
    //!
    //! <b>Effects</b>: Returns an a pair of node_ptr delimiting a range containing
    //!   all elements that are equivalent to "key" according to "comp" or an
    //!   empty range that indicates the position where those elements would be
    //!   if there are no equivalent elements.
    //!
    //! <b>Complexity</b>: Logarithmic.
    //!
    //! <b>Throws</b>: If "comp" throws.
    template<class KeyType, class KeyNodePtrCompare>
    inline static std::pair<node_ptr, node_ptr> equal_range
    (const_node_ptr header, const KeyType& key, KeyNodePtrCompare comp)
    {
        return bounded_range(header, key, key, comp, true, true);
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   KeyNodePtrCompare is a function object that induces a strict weak
    //!   ordering compatible with the strict weak ordering used to create the
    //!   the tree. KeyNodePtrCompare can compare KeyType with tree's node_ptrs.
    //!
    //! <b>Effects</b>: Returns an a pair of node_ptr delimiting a range containing
    //!   the first element that is equivalent to "key" according to "comp" or an
    //!   empty range that indicates the position where that element would be
    //!   if there are no equivalent elements.
    //!
    //! <b>Complexity</b>: Logarithmic.
    //!
    //! <b>Throws</b>: If "comp" throws.
    template<class KeyType, class KeyNodePtrCompare>
    static std::pair<node_ptr, node_ptr> lower_bound_range
    (const_node_ptr header, const KeyType& key, KeyNodePtrCompare comp)
    {
        node_ptr const lb(lower_bound(header, key, comp));
        std::pair<node_ptr, node_ptr> ret_ii(lb, lb);
        if (lb != header && !comp(key, lb)) {
            ret_ii.second = next_node(ret_ii.second);
        }
        return ret_ii;
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   KeyNodePtrCompare is a function object that induces a strict weak
    //!   ordering compatible with the strict weak ordering used to create the
    //!   the tree. KeyNodePtrCompare can compare KeyType with tree's node_ptrs.
    //!
    //! <b>Effects</b>: Returns the number of elements with a key equivalent to "key"
    //!   according to "comp".
    //!
    //! <b>Complexity</b>: Logarithmic.
    //!
    //! <b>Throws</b>: If "comp" throws.
    template<class KeyType, class KeyNodePtrCompare>
    static std::size_t count(const_node_ptr header, const KeyType& key, KeyNodePtrCompare comp)
    {
        std::pair<node_ptr, node_ptr> ret = equal_range(header, key, comp);
        std::size_t n = 0;
        while (ret.first != ret.second) {
            ++n;
            ret.first = next_node(ret.first);
        }
        return n;
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   KeyNodePtrCompare is a function object that induces a strict weak
    //!   ordering compatible with the strict weak ordering used to create the
    //!   the tree. KeyNodePtrCompare can compare KeyType with tree's node_ptrs.
    //!
    //! <b>Effects</b>: Returns a node_ptr to the first element that is equivalent to
    //!   "key" according to "comp" or "header" if that element does not exist.
    //!
    //! <b>Complexity</b>: Logarithmic.
    //!
    //! <b>Throws</b>: If "comp" throws.
    template<class KeyType, class KeyNodePtrCompare>
    static node_ptr find(const_node_ptr header, const KeyType& key, KeyNodePtrCompare comp)
    {
        node_ptr end = uncast(header);
        node_ptr y = lower_bound(header, key, comp);
        return (y == end || comp(key, y)) ? end : y;
    }

    template<class NodePtrCompare>
    static void insert_equal_upper_bound_check(
        node_ptr h, node_ptr new_node, NodePtrCompare comp, insert_commit_data& commit_data, std::size_t* pdepth = 0)
    {
        std::size_t depth = 0;
        node_ptr y(h);
        node_ptr x(node_traits::get_parent(y));

        while (x) {
            ++depth;
            y = x;
            x = comp(new_node, x) ?
                node_traits::get_left(x) : node_traits::get_right(x);
        }
        if (pdepth)  *pdepth = depth;
        commit_data.link_left = (y == h) || comp(new_node, y);
        commit_data.node = y;
    }

    template<class NodePtrCompare>
    static void insert_equal_lower_bound_check(
        node_ptr h, node_ptr new_node, NodePtrCompare comp, insert_commit_data& commit_data, std::size_t* pdepth = 0)
    {
        std::size_t depth = 0;
        node_ptr y(h);
        node_ptr x(node_traits::get_parent(y));

        while (x) {
            ++depth;
            y = x;
            x = !comp(x, new_node) ?
                node_traits::get_left(x) : node_traits::get_right(x);
        }
        if (pdepth)  *pdepth = depth;
        commit_data.link_left = (y == h) || !comp(y, new_node);
        commit_data.node = y;
    }

    template<class NodePtrCompare>
    static void insert_equal_check(node_ptr header, node_ptr hint,
        node_ptr new_node, NodePtrCompare comp, insert_commit_data& commit_data, std::size_t* pdepth = 0)
    {
        if (hint == header || !comp(hint, new_node)) {
            node_ptr prev(hint);
            if (hint == node_traits::get_left(header) ||
                !comp(new_node, (prev = prev_node(hint)))) {
                bool link_left = unique(header) || !node_traits::get_left(hint);
                commit_data.link_left = link_left;
                commit_data.node = link_left ? hint : prev;
                if (pdepth) {
                    *pdepth = commit_data.node == header ? 0 : depth(commit_data.node) + 1;
                }
            }
            else {
                insert_equal_upper_bound_check(header, new_node, comp, commit_data, pdepth);
            }
        }
        else {
            insert_equal_lower_bound_check(header, new_node, comp, commit_data, pdepth);
        }
    }

    static void insert_before_check(
        node_ptr header, node_ptr pos, insert_commit_data& commit_data, std::size_t* pdepth = 0)
    {
        node_ptr prev(pos);
        if (pos != node_traits::get_left(header))
            prev = prev_node(pos);
        bool link_left = unique(header) || !node_traits::get_left(pos);
        commit_data.link_left = link_left;
        commit_data.node = link_left ? pos : prev;
        if (pdepth) {
            *pdepth = commit_data.node == header ? 0 : depth(commit_data.node) + 1;
        }
    }

    static void push_back_check(node_ptr header, insert_commit_data& commit_data, std::size_t* pdepth = 0) noexcept
    {
        node_ptr prev(node_traits::get_right(header));
        if (pdepth) {
            *pdepth = prev == header ? 0 : depth(prev) + 1;
        }
        commit_data.link_left = false;
        commit_data.node = prev;
    }


    static void push_front_check(node_ptr header, insert_commit_data& commit_data, std::size_t* pdepth = 0) noexcept
    {
        node_ptr pos(node_traits::get_left(header));
        if (pdepth) {
            *pdepth = pos == header ? 0 : depth(pos) + 1;
        }
        commit_data.link_left = true;
        commit_data.node = pos;
    }

    static void insert_commit(node_ptr header, node_ptr new_node, const insert_commit_data& commit_data) noexcept
    {
        //! Check if commit_data has not been initialized by a insert_unique_check call.
        assert(commit_data.node != node_ptr());
        node_ptr parent_node(commit_data.node);
        if (parent_node == header) {
            node_traits::set_parent(header, new_node);
            node_traits::set_right(header, new_node);
            node_traits::set_left(header, new_node);
        }
        else if (commit_data.link_left) {
            node_traits::set_left(parent_node, new_node);
            if (parent_node == node_traits::get_left(header))
                node_traits::set_left(header, new_node);
        }
        else {
            node_traits::set_right(parent_node, new_node);
            if (parent_node == node_traits::get_right(header))
                node_traits::set_right(header, new_node);
        }
        node_traits::set_parent(new_node, parent_node);
        node_traits::set_right(new_node, node_ptr());
        node_traits::set_left(new_node, node_ptr());
    }

    template<class NodePtrCompare>
    static node_ptr insert_equal(node_ptr h, node_ptr hint, node_ptr new_node, NodePtrCompare comp, std::size_t* pdepth = 0)
    {
        insert_commit_data commit_data;
        insert_equal_check<NodePtrCompare>(h, hint, new_node, comp, commit_data, pdepth);
        insert_commit(h, new_node, commit_data);
        rebalance_after_insertion(h, new_node);
        return new_node;
    }

    //! <b>Requires</b>: "h" must be the header node of a tree.
    //!   NodePtrCompare is a function object that induces a strict weak
    //!   ordering compatible with the strict weak ordering used to create the
    //!   the tree. NodePtrCompare compares two node_ptrs.
    //!
    //! <b>Effects</b>: Inserts new_node into the tree before the upper bound
    //!   according to "comp".
    //!
    //! <b>Complexity</b>: Average complexity for insert element is at
    //!   most logarithmic.
    //!
    //! <b>Throws</b>: If "comp" throws.
    template<class NodePtrCompare>
    static node_ptr insert_equal_upper_bound(node_ptr h, node_ptr new_node, NodePtrCompare comp, std::size_t* pdepth = 0)
    {
        insert_commit_data commit_data;
        insert_equal_upper_bound_check<NodePtrCompare>(h, new_node, comp, commit_data, pdepth);
        insert_commit(h, new_node, commit_data);
        rebalance_after_insertion(h, new_node);
        return new_node;
    }

    //! <b>Requires</b>: "h" must be the header node of a tree.
    //!   NodePtrCompare is a function object that induces a strict weak
    //!   ordering compatible with the strict weak ordering used to create the
    //!   the tree. NodePtrCompare compares two node_ptrs.
    //!
    //! <b>Effects</b>: Inserts new_node into the tree before the lower bound
    //!   according to "comp".
    //!
    //! <b>Complexity</b>: Average complexity for insert element is at
    //!   most logarithmic.
    //!
    //! <b>Throws</b>: If "comp" throws.
    template<class NodePtrCompare>
    static node_ptr insert_equal_lower_bound(node_ptr h, node_ptr new_node, NodePtrCompare comp, std::size_t* pdepth = 0)
    {
        insert_commit_data commit_data;
        insert_equal_lower_bound_check<NodePtrCompare>(h, new_node, comp, commit_data, pdepth);
        insert_commit(h, new_node, commit_data);
        rebalance_after_insertion(h, new_node);
        return new_node;
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   "pos" must be a valid iterator or header (end) node.
    //!   "pos" must be an iterator pointing to the successor to "new_node"
    //!   once inserted according to the order of already inserted nodes. This function does not
    //!   check "pos" and this precondition must be guaranteed by the caller.
    //!
    //! <b>Effects</b>: Inserts new_node into the tree before "pos".
    //!
    //! <b>Complexity</b>: Constant-time.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Note</b>: If "pos" is not the successor of the newly inserted "new_node"
    //! tree invariants might be broken.
    static node_ptr insert_before(node_ptr header, node_ptr pos, node_ptr new_node) noexcept
    {
        insert_commit_data commit_data;
        insert_before_check(header, pos, commit_data);
        insert_commit(header, new_node, commit_data);
        rebalance_after_insertion(header, new_node);
        return new_node;
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   "new_node" must be, according to the used ordering no less than the
    //!   greatest inserted key.
    //!
    //! <b>Effects</b>: Inserts new_node into the tree before "pos".
    //!
    //! <b>Complexity</b>: Constant-time.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Note</b>: If "new_node" is less than the greatest inserted key
    //! tree invariants are broken. This function is slightly faster than
    //! using "insert_before".
    static void push_back(node_ptr header, node_ptr new_node, std::size_t* pdepth = 0) noexcept
    {
        insert_commit_data commit_data;
        push_back_check(header, commit_data, pdepth);
        insert_commit(header, new_node, commit_data);
        rebalance_after_insertion(header, new_node);
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   "new_node" must be, according to the used ordering, no greater than the
    //!   lowest inserted key.
    //!
    //! <b>Effects</b>: Inserts new_node into the tree before "pos".
    //!
    //! <b>Complexity</b>: Constant-time.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Note</b>: If "new_node" is greater than the lowest inserted key
    //! tree invariants are broken. This function is slightly faster than
    //! using "insert_before".
    static void push_front(node_ptr header, node_ptr new_node, std::size_t* pdepth = 0) noexcept
    {
        insert_commit_data commit_data;
        push_front_check(header, commit_data, pdepth);
        insert_commit(header, new_node, commit_data);
        rebalance_after_insertion(header, new_node);
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   "commit_data" must have been obtained from a previous call to
    //!   "insert_unique_check". No objects should have been inserted or erased
    //!   from the set between the "insert_unique_check" that filled "commit_data"
    //!   and the call to "insert_commit".
    //!
    //!
    //! <b>Effects</b>: Inserts new_node in the set using the information obtained
    //!   from the "commit_data" that a previous "insert_check" filled.
    //!
    //! <b>Complexity</b>: Constant time.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Notes</b>: This function has only sense if a "insert_unique_check" has been
    //!   previously executed to fill "commit_data". No value should be inserted or
    //!   erased between the "insert_check" and "insert_commit" calls.
    inline static void insert_unique_commit(
        node_ptr header, node_ptr new_value, const insert_commit_data& commit_data) noexcept
    {
        insert_commit(header, new_value, commit_data);
        rebalance_after_insertion(header, new_value);
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   KeyNodePtrCompare is a function object that induces a strict weak
    //!   ordering compatible with the strict weak ordering used to create the
    //!   the tree. NodePtrCompare compares KeyType with a node_ptr.
    //!
    //! <b>Effects</b>: Checks if there is an equivalent node to "key" in the
    //!   tree according to "comp" and obtains the needed information to realize
    //!   a constant-time node insertion if there is no equivalent node.
    //!
    //! <b>Returns</b>: If there is an equivalent value
    //!   returns a pair containing a node_ptr to the already present node
    //!   and false. If there is not equivalent key can be inserted returns true
    //!   in the returned pair's boolean and fills "commit_data" that is meant to
    //!   be used with the "insert_commit" function to achieve a constant-time
    //!   insertion function.
    //!
    //! <b>Complexity</b>: Average complexity is at most logarithmic.
    //!
    //! <b>Throws</b>: If "comp" throws.
    //!
    //! <b>Notes</b>: This function is used to improve performance when constructing
    //!   a node is expensive and the user does not want to have two equivalent nodes
    //!   in the tree: if there is an equivalent value
    //!   the constructed object must be discarded. Many times, the part of the
    //!   node that is used to impose the order is much cheaper to construct
    //!   than the node and this function offers the possibility to use that part
    //!   to check if the insertion will be successful.
    //!
    //!   If the check is successful, the user can construct the node and use
    //!   "insert_commit" to insert the node in constant-time. This gives a total
    //!   logarithmic complexity to the insertion: check(O(log(N)) + commit(O(1)).
    //!
    //!   "commit_data" remains valid for a subsequent "insert_unique_commit" only
    //!   if no more objects are inserted or erased from the set.
    template<class KeyType, class KeyNodePtrCompare>
    static std::pair<node_ptr, bool> insert_unique_check(const_node_ptr header, 
        const KeyType& key, KeyNodePtrCompare comp, insert_commit_data& commit_data, std::size_t* pdepth = 0)
    {
        std::size_t depth = 0;
        node_ptr h(uncast(header));
        node_ptr y(h);
        node_ptr x(node_traits::get_parent(y));
        node_ptr prev = node_ptr();

        //Find the upper bound, cache the previous value and if we should
        //store it in the left or right node
        bool left_child = true;
        while (x) {
            ++depth;
            y = x;
            left_child = comp(key, x);
            x = left_child ?
                node_traits::get_left(x) : (prev = y, node_traits::get_right(x));
        }

        if (pdepth)  *pdepth = depth;

        //Since we've found the upper bound there is no other value with the same key if:
        //    - There is no previous node
        //    - The previous node is less than the key
        const bool not_present = !prev || comp(prev, key);
        if (not_present) {
            commit_data.link_left = left_child;
            commit_data.node = y;
        }
        return std::pair<node_ptr, bool>(prev, not_present);
    }

    //! <b>Requires</b>: "header" must be the header node of a tree.
    //!   KeyNodePtrCompare is a function object that induces a strict weak
    //!   ordering compatible with the strict weak ordering used to create the
    //!   the tree. NodePtrCompare compares KeyType with a node_ptr.
    //!   "hint" is node from the "header"'s tree.
    //!
    //! <b>Effects</b>: Checks if there is an equivalent node to "key" in the
    //!   tree according to "comp" using "hint" as a hint to where it should be
    //!   inserted and obtains the needed information to realize
    //!   a constant-time node insertion if there is no equivalent node.
    //!   If "hint" is the upper_bound the function has constant time
    //!   complexity (two comparisons in the worst case).
    //!
    //! <b>Returns</b>: If there is an equivalent value
    //!   returns a pair containing a node_ptr to the already present node
    //!   and false. If there is not equivalent key can be inserted returns true
    //!   in the returned pair's boolean and fills "commit_data" that is meant to
    //!   be used with the "insert_commit" function to achieve a constant-time
    //!   insertion function.
    //!
    //! <b>Complexity</b>: Average complexity is at most logarithmic, but it is
    //!   amortized constant time if new_node should be inserted immediately before "hint".
    //!
    //! <b>Throws</b>: If "comp" throws.
    //!
    //! <b>Notes</b>: This function is used to improve performance when constructing
    //!   a node is expensive and the user does not want to have two equivalent nodes
    //!   in the tree: if there is an equivalent value
    //!   the constructed object must be discarded. Many times, the part of the
    //!   node that is used to impose the order is much cheaper to construct
    //!   than the node and this function offers the possibility to use that part
    //!   to check if the insertion will be successful.
    //!
    //!   If the check is successful, the user can construct the node and use
    //!   "insert_commit" to insert the node in constant-time. This gives a total
    //!   logarithmic complexity to the insertion: check(O(log(N)) + commit(O(1)).
    //!
    //!   "commit_data" remains valid for a subsequent "insert_unique_commit" only
    //!   if no more objects are inserted or erased from the set.
    template<class KeyType, class KeyNodePtrCompare>
    static std::pair<node_ptr, bool> insert_unique_check(const_node_ptr header, node_ptr hint, 
        const KeyType& key, KeyNodePtrCompare comp, insert_commit_data& commit_data, std::size_t* pdepth = 0)
    {
        //hint must be bigger than the key
        if (hint == header || comp(key, hint)) {
            node_ptr prev(hint);
            //Previous value should be less than the key
            if (hint == begin_node(header) || comp((prev = prev_node(hint)), key)) {
                commit_data.link_left = unique(header) || !node_traits::get_left(hint);
                commit_data.node = commit_data.link_left ? hint : prev;
                if (pdepth) {
                    *pdepth = commit_data.node == header ? 0 : depth(commit_data.node) + 1;
                }
                return std::pair<node_ptr, bool>(node_ptr(), true);
            }
        }
        //Hint was wrong, use hintless insertion
        return insert_unique_check(header, key, comp, commit_data, pdepth);
    }

    static node_ptr erase(node_ptr header, node_ptr z) noexcept
    {
        node_ptr y(z);
        node_ptr x;
        const node_ptr z_left(node_traits::get_left(z));
        const node_ptr z_right(node_traits::get_right(z));

        if (!z_left) {
            x = z_right;    // x might be null.
        }
        else if (!z_right) { // z has exactly one non-null child. y == z.
            x = z_left;       // x is not null.
            assert(x);
        }
        else { //make y != z
            // y = find z's successor
            y = minimum(z_right);
            x = node_traits::get_right(y);     // x might be null.
        }

        node_ptr x_parent;
        const node_ptr z_parent(node_traits::get_parent(z));
        const bool z_is_leftchild(node_traits::get_left(z_parent) == z);

        if (y != z) { //has two children and y is the minimum of z
            //y is z's successor and it has a null left child.
            //x is the right child of y (it can be null)
            //Relink y in place of z and link x with y's old parent
            node_traits::set_parent(z_left, y);
            node_traits::set_left(y, z_left);
            if (y != z_right) {
                //Link y with the right tree of z
                node_traits::set_right(y, z_right);
                node_traits::set_parent(z_right, y);
                //Link x with y's old parent (y must be a left child)
                x_parent = node_traits::get_parent(y);
                assert(node_traits::get_left(x_parent) == y);
                if (x)
                    node_traits::set_parent(x, x_parent);
                //Since y was the successor and not the right child of z, it must be a left child
                node_traits::set_left(x_parent, x);
            }
            else { //y was the right child of y so no need to fix x's position
                x_parent = y;
            }
            node_traits::set_parent(y, z_parent);
            set_child(header, y, z_parent, z_is_leftchild);
        }
        else {  // z has zero or one child, x is one child (it can be null)
            //Just link x to z's parent
            x_parent = z_parent;
            if (x)
                NodeTraits::set_parent(x, z_parent);
            set_child(header, x, z_parent, z_is_leftchild);

            //Now update leftmost/rightmost in case z was one of them
            if (NodeTraits::get_left(header) == z) {
                //z_left must be null because z is the leftmost
                assert(!z_left);
                NodeTraits::set_left(header, !z_right ?
                    z_parent :  // makes leftmost == header if z == root
                    minimum(z_right));
            }
            if (NodeTraits::get_right(header) == z) {
                //z_right must be null because z is the rightmost
                assert(!z_right);
                NodeTraits::set_right(header, !z_left ?
                    z_parent :  // makes rightmost == header if z == root
                    maximum(z_left));
            }
        }

        rebalance_info info;

        //If z had 0/1 child, y == z and one of its children (and maybe null)
        //If z had 2 children, y is the successor of z and x is the right child of y
        info.x = x;
        info.y = y;
        //If z had 0/1 child, x_parent is the new parent of the old right child of y (z's successor)
        //If z had 2 children, x_parent is the new parent of y (z_parent)
        assert(!x || NodeTraits::get_parent(x) == x_parent);
        info.x_parent = x_parent;

        rebalance_after_erasure(header, z, info);
        return z;
    }

    //! Fix header and own's parent data when replacing x with own, providing own's old data with parent
    static void set_child(node_ptr header, node_ptr new_child, node_ptr new_parent, const bool link_left) noexcept
    {
        if (new_parent == header)
            node_traits::set_parent(header, new_child);
        else if (link_left)
            node_traits::set_left(new_parent, new_child);
        else
            node_traits::set_right(new_parent, new_child);
    }

    //! Rotate p to left (no header and p's parent fixup)
    static void rotate_left_no_parent_fix(node_ptr p, node_ptr p_right) noexcept
    {
        node_ptr p_right_left(node_traits::get_left(p_right));
        node_traits::set_right(p, p_right_left);
        if (p_right_left) {
            node_traits::set_parent(p_right_left, p);
        }
        node_traits::set_left(p_right, p);
        node_traits::set_parent(p, p_right);
    }

    //! Rotate p to left (with header and p's parent fixup)
    static void rotate_left(node_ptr p, node_ptr p_right, node_ptr p_parent, node_ptr header) noexcept
    {
        const bool p_was_left(node_traits::get_left(p_parent) == p);
        rotate_left_no_parent_fix(p, p_right);
        node_traits::set_parent(p_right, p_parent);
        set_child(header, p_right, p_parent, p_was_left);
    }

    //! Rotate p to right (no header and p's parent fixup)
    static void rotate_right_no_parent_fix(node_ptr p, node_ptr p_left) noexcept
    {
        node_ptr p_left_right(node_traits::get_right(p_left));
        node_traits::set_left(p, p_left_right);
        if (p_left_right) {
            node_traits::set_parent(p_left_right, p);
        }
        node_traits::set_right(p_left, p);
        node_traits::set_parent(p, p_left);
    }

    //! Rotate p to right (with header and p's parent fixup)
    static void rotate_right(node_ptr p, node_ptr p_left, node_ptr p_parent, node_ptr header) noexcept
    {
        const bool p_was_left(node_traits::get_left(p_parent) == p);
        rotate_right_no_parent_fix(p, p_left);
        node_traits::set_parent(p_left, p_parent);
        set_child(header, p_left, p_parent, p_was_left);
    }
    
    static void rebalance_after_erasure(node_ptr header, node_ptr z, const rebalance_info& info) noexcept
    {
        color new_z_color;
        if (info.y != z) {
            new_z_color = node_traits::get_color(info.y);
            node_traits::set_color(info.y, node_traits::get_color(z));
        }
        else {
            new_z_color = node_traits::get_color(z);
        }
        //Rebalance rbtree if needed
        if (new_z_color != node_traits::red()) {
            rebalance_after_erasure_restore_invariants(header, info.x, info.x_parent);
        }
    }

    static void rebalance_after_erasure_restore_invariants(node_ptr header, node_ptr x, node_ptr x_parent) noexcept
    {
        while (1) {
            if (x_parent == header || (x && node_traits::get_color(x) != node_traits::black())) {
                break;
            }
            //Don't cache x_is_leftchild or similar because x can be null and
            //equal to both x_parent_left and x_parent_right
            const node_ptr x_parent_left(node_traits::get_left(x_parent));
            if (x == x_parent_left) { //x is left child
                node_ptr w = node_traits::get_right(x_parent);
                assert(w);
                if (node_traits::get_color(w) == node_traits::red()) {
                    node_traits::set_color(w, node_traits::black());
                    node_traits::set_color(x_parent, node_traits::red());
                    rotate_left(x_parent, w, node_traits::get_parent(x_parent), header);
                    w = node_traits::get_right(x_parent);
                    assert(w);
                }
                node_ptr const w_left(node_traits::get_left(w));
                node_ptr const w_right(node_traits::get_right(w));
                if ((!w_left || node_traits::get_color(w_left) == node_traits::black()) &&
                    (!w_right || node_traits::get_color(w_right) == node_traits::black())) {
                    node_traits::set_color(w, node_traits::red());
                    x = x_parent;
                    x_parent = node_traits::get_parent(x_parent);
                }
                else {
                    if (!w_right || node_traits::get_color(w_right) == node_traits::black()) {
                        node_traits::set_color(w_left, node_traits::black());
                        node_traits::set_color(w, node_traits::red());
                        rotate_right(w, w_left, node_traits::get_parent(w), header);
                        w = node_traits::get_right(x_parent);
                        assert(w);
                    }
                    node_traits::set_color(w, node_traits::get_color(x_parent));
                    node_traits::set_color(x_parent, node_traits::black());
                    const node_ptr new_wright(node_traits::get_right(w));
                    if (new_wright)
                        node_traits::set_color(new_wright, node_traits::black());
                    rotate_left(x_parent, node_traits::get_right(x_parent), node_traits::get_parent(x_parent), header);
                    break;
                }
            }
            else {
                // same as above, with right_ <-> left_.
                node_ptr w = x_parent_left;
                if (node_traits::get_color(w) == node_traits::red()) {
                    node_traits::set_color(w, node_traits::black());
                    node_traits::set_color(x_parent, node_traits::red());
                    rotate_right(x_parent, w, node_traits::get_parent(x_parent), header);
                    w = node_traits::get_left(x_parent);
                    assert(w);
                }
                node_ptr const w_left(node_traits::get_left(w));
                node_ptr const w_right(node_traits::get_right(w));
                if ((!w_right || node_traits::get_color(w_right) == node_traits::black()) &&
                    (!w_left || node_traits::get_color(w_left) == node_traits::black())) {
                    node_traits::set_color(w, node_traits::red());
                    x = x_parent;
                    x_parent = node_traits::get_parent(x_parent);
                }
                else {
                    if (!w_left || node_traits::get_color(w_left) == node_traits::black()) {
                        node_traits::set_color(w_right, node_traits::black());
                        node_traits::set_color(w, node_traits::red());
                        rotate_left(w, w_right, node_traits::get_parent(w), header);
                        w = node_traits::get_left(x_parent);
                        assert(w);
                    }
                    node_traits::set_color(w, node_traits::get_color(x_parent));
                    node_traits::set_color(x_parent, node_traits::black());
                    const node_ptr new_wleft(node_traits::get_left(w));
                    if (new_wleft)
                        node_traits::set_color(new_wleft, NodeTraits::black());
                    rotate_right(x_parent, node_traits::get_left(x_parent), node_traits::get_parent(x_parent), header);
                    break;
                }
            }
        }
        if (x)
            node_traits::set_color(x, node_traits::black());
    }

    static void rebalance_after_insertion(node_ptr header, node_ptr p) noexcept
    {
        node_traits::set_color(p, node_traits::red());
        while (1) {
            node_ptr p_parent(node_traits::get_parent(p));
            const node_ptr p_grandparent(node_traits::get_parent(p_parent));
            if (p_parent == header || node_traits::get_color(p_parent) == node_traits::black() || p_grandparent == header) {
                break;
            }

            node_traits::set_color(p_grandparent, node_traits::red());
            node_ptr const p_grandparent_left(node_traits::get_left(p_grandparent));
            bool const p_parent_is_left_child = p_parent == p_grandparent_left;
            node_ptr const x(p_parent_is_left_child ? node_traits::get_right(p_grandparent) : p_grandparent_left);

            if (x && node_traits::get_color(x) == node_traits::red()) {
                node_traits::set_color(x, node_traits::black());
                node_traits::set_color(p_parent, node_traits::black());
                p = p_grandparent;
            }
            else { //Final step
                const bool p_is_left_child(node_traits::get_left(p_parent) == p);
                if (p_parent_is_left_child) { //p_parent is left child
                    if (!p_is_left_child) { //p is right child
                        rotate_left_no_parent_fix(p_parent, p);
                        //No need to link p and p_grandparent:
                        //    [node_traits::set_parent(p, p_grandparent) + node_traits::set_left(p_grandparent, p)]
                        //as p_grandparent is not the header, another rotation is coming and p_parent
                        //will be the left child of p_grandparent
                        p_parent = p;
                    }
                    rotate_right(p_grandparent, p_parent, node_traits::get_parent(p_grandparent), header);
                }
                else {  //p_parent is right child
                    if (p_is_left_child) { //p is left child
                        rotate_right_no_parent_fix(p_parent, p);
                        //No need to link p and p_grandparent:
                        //    [node_traits::set_parent(p, p_grandparent) + node_traits::set_right(p_grandparent, p)]
                        //as p_grandparent is not the header, another rotation is coming and p_parent
                        //will be the right child of p_grandparent
                        p_parent = p;
                    }
                    rotate_left(p_grandparent, p_parent, node_traits::get_parent(p_grandparent), header);
                }
                node_traits::set_color(p_parent, node_traits::black());
                break;
            }
        }
        node_traits::set_color(node_traits::get_parent(header), node_traits::black());
    }
};

//static_assert(std::convertible_to<node_ptr, void*>
//    && "node_ptr must be constructible from and convertible to void* for color bit packing");
//static_assert(alignof(node) >= 2 && "Alignment of Node must be greater than or equal to 2");

template <class T, class Compare = std::less<T>, class NodeTraits = rbtree_node_traits<T>>
class rbtree
    : public rbtree_impl<NodeTraits>
{
    using tree = rbtree<T, Compare, NodeTraits>;
    using base = rbtree_impl<NodeTraits>;
    using node_traits = NodeTraits;

    using node = node_traits::node;

    using node_ref = node&;
    using const_node_ref = const node&;

    using node_ptr = node_traits::node_ptr;
    using const_node_ptr = const node_ptr;

public:
    class const_iterator
    {
    public:
        const_iterator() = default;
        const_iterator(node_ptr node) : m_node(node) {}
        const_iterator(const const_iterator& other) : m_node(other.m_node) {}

        const_node_ref operator *() const { return *m_node; }
        const_node_ptr operator->() const { return m_node; }

        bool operator==(const const_iterator& rhs) const { return m_node == rhs.m_node; }
        bool operator!=(const const_iterator& rhs) const { return m_node != rhs.m_node; }

        bool operator==(std::nullptr_t) const { return m_node == nullptr; }
        bool operator!=(std::nullptr_t) const { return m_node != nullptr; }

        const_iterator& operator++()
        {
            m_node = (node_ptr)tree::base::next_node(m_node);
            return *this;
        }

        const_iterator& operator--()
        {
            m_node = (node_ptr)tree::base::prev_node(m_node);
            return *this;
        }

    protected:
        node_ptr m_node{ nullptr };
    };

    class iterator
        : public const_iterator
    {
        using base = const_iterator;

    public:
        iterator() = default;
        iterator(node_ptr node) : base(node) {}
        iterator(const iterator& other) : base(other.m_node) {}

        node_ref operator *() { return *base::m_node; }
        node_ptr operator->() { return base::m_node; }

        iterator& operator++() { base::operator++(); return *this; }
        iterator& operator--() { base::operator--(); return *this; }
    };

    template<typename NodePtrCompare>
    struct key_node_compare
    {
        //! NodePtrCompare
        bool operator()(node_ptr lhs, node_ptr rhs) const
        {
            return m_comp(lhs, rhs);
        }

        //! KeyNodePtrCompare
        bool operator()(const_node_ref lhs, node_ptr rhs) const
        {
            return m_comp(base::uncast(node_traits::to_node_ptr(lhs)), rhs);
        }

        bool operator()(node_ptr lhs, const_node_ref rhs) const
        {
            return m_comp(lhs, base::uncast(node_traits::to_node_ptr(rhs)));
        }

        NodePtrCompare m_comp;
    };

    node_ptr head_pointer() { return &m_head; }
    const_node_ptr head_pointer() const { return &m_head; }

    rbtree() { base::init_header(head_pointer()); }
    ~rbtree() { clear(); }

    iterator begin() noexcept { return { base::begin_node(head_pointer()) }; }
    const_iterator begin() const noexcept { return { base::begin_node(head_pointer()) }; }

    iterator end() noexcept { return { base::end_node(head_pointer()) }; }
    const_iterator end() const noexcept { return { base::end_node(head_pointer()); } }

    iterator root() noexcept { return { base::root_node(head_pointer()) }; }
    const_iterator root() noexcept { return { base::root_node(head_pointer()) }; }

    bool empty() const noexcept { return !m_size; }
    size_t size() const noexcept { return m_size; }

    //! <b>Requires</b>: value must be an lvalue
    //!
    //! <b>Effects</b>: Inserts value into the container before the upper bound.
    //!
    //! <b>Complexity</b>: Average complexity for insert element is at
    //!   most logarithmic.
    //!
    //! <b>Throws</b>: If the internal key_compare ordering function throws. Strong guarantee.
    //!
    //! <b>Note</b>: Does not affect the validity of iterators and references.
    //!   No copy-constructors are called.
    iterator insert_equal(node_ref value)
    {
        node_ptr to_insert(&value);
        iterator ret(base::insert_equal_upper_bound(head_pointer(), to_insert, m_comp));
        m_size++;
        return ret;
    }

    //! <b>Requires</b>: value must be an lvalue, and "hint" must be
    //!   a valid iterator.
    //!
    //! <b>Effects</b>: Inserts x into the container, using "hint" as a hint to
    //!   where it will be inserted. If "hint" is the upper_bound
    //!   the insertion takes constant time (two comparisons in the worst case)
    //!
    //! <b>Complexity</b>: Logarithmic in general, but it is amortized
    //!   constant time if t is inserted immediately before hint.
    //!
    //! <b>Throws</b>: If the internal key_compare ordering function throws. Strong guarantee.
    //!
    //! <b>Note</b>: Does not affect the validity of iterators and references.
    //!   No copy-constructors are called.
    iterator insert_equal(const_iterator hint, node_ref value)
    {
        node_ptr to_insert(&value);
        iterator ret(base::insert_equal(head_pointer(), hint.m_node, to_insert, m_comp);
        m_size++;
        return ret;
    }

    //! <b>Requires</b>: value must be an lvalue
    //!
    //! <b>Effects</b>: Inserts value into the container if the value
    //!   is not already present.
    //!
    //! <b>Complexity</b>: Average complexity for insert element is at
    //!   most logarithmic.
    //!
    //! <b>Throws</b>: If the comparison functor call throws.
    //!
    //! <b>Note</b>: Does not affect the validity of iterators and references.
    //!   No copy-constructors are called.
    std::pair<iterator, bool> insert_unique(node_ref value)
    {
        base::insert_commit_data commit_data;
        std::pair<node_ptr, bool> ret = base::insert_unique_check(head_pointer(), value, m_comp, commit_data);
        return std::pair<iterator, bool>
            (ret.second ? this->insert_unique_commit(value, commit_data): iterator(ret.first), ret.second);
    }

    //! <b>Requires</b>: value must be an lvalue, and "hint" must be
    //!   a valid iterator
    //!
    //! <b>Effects</b>: Tries to insert x into the container, using "hint" as a hint
    //!   to where it will be inserted.
    //!
    //! <b>Complexity</b>: Logarithmic in general, but it is amortized
    //!   constant time (two comparisons in the worst case)
    //!   if t is inserted immediately before hint.
    //!
    //! <b>Throws</b>: If the comparison functor call throws.
    //!
    //! <b>Note</b>: Does not affect the validity of iterators and references.
    //!   No copy-constructors are called.
    iterator insert_unique(const_iterator hint, node_ref value)
    {
        base::insert_commit_data commit_data;
        std::pair<node_ptr, bool> ret = base::insert_unique_check(head_pointer(), hint.m_node, value, m_comp, commit_data);
        return ret.second ? this->insert_unique_commit(value, commit_data) : iterator(ret.first);
    }

    //! <b>Requires</b>: value must be an lvalue, "pos" must be
    //!   a valid iterator (or end) and must be the succesor of value
    //!   once inserted according to the predicate
    //!
    //! <b>Effects</b>: Inserts x into the container before "pos".
    //!
    //! <b>Complexity</b>: Constant time.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Note</b>: This function does not check preconditions so if "pos" is not
    //! the successor of "value" container ordering invariant will be broken.
    //! This is a low-level function to be used only for performance reasons
    //! by advanced users.
    iterator insert_before(const_iterator pos, node_ref value) noexcept
    {
        node_ptr to_insert(node_traits::to_node_ptr(value));
        m_size++;
        return iterator(base::insert_before(head_pointer(), pos.m_node, to_insert));
    }

    //! <b>Requires</b>: value must be an lvalue, and it must be no less
    //!   than the greatest inserted key
    //!
    //! <b>Effects</b>: Inserts x into the container in the last position.
    //!
    //! <b>Complexity</b>: Constant time.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Note</b>: This function does not check preconditions so if value is
    //!   less than the greatest inserted key container ordering invariant will be broken.
    //!   This function is slightly more efficient than using "insert_before".
    //!   This is a low-level function to be used only for performance reasons
    //!   by advanced users.
    void push_back(node_ref value) noexcept
    {
        node_ptr to_insert(node_traits::to_node_ptr(value));
        m_size++;
        base::push_back(head_pointer(), to_insert);
    }

    //! <b>Requires</b>: value must be an lvalue, and it must be no greater
    //!   than the minimum inserted key
    //!
    //! <b>Effects</b>: Inserts x into the container in the first position.
    //!
    //! <b>Complexity</b>: Constant time.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Note</b>: This function does not check preconditions so if value is
    //!   greater than the minimum inserted key container ordering invariant will be broken.
    //!   This function is slightly more efficient than using "insert_before".
    //!   This is a low-level function to be used only for performance reasons
    //!   by advanced users.
    void push_front(node_ref value) noexcept
    {
        node_ptr to_insert(node_traits::to_node_ptr(value));
        m_size++;
        base::push_front(head_pointer(), to_insert);
    }

    //! <b>Effects</b>: Erases the element pointed to by i.
    //!
    //! <b>Complexity</b>: Average complexity for erase element is constant time.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Note</b>: Invalidates the iterators (but not the references)
    //!    to the erased elements. No destructors are called.
    iterator erase(const_iterator i) noexcept
    {
        iterator ret(i.m_node);
        ++ret;
        node_ptr to_erase(i.m_node);
        base::erase(head_pointer(), to_erase);
        m_size--;
        return ret;
    }

    //! <b>Effects</b>: Erases the range pointed to by b end e.
    //!
    //! <b>Complexity</b>: Average complexity for erase range is at most
    //!   O(log(size() + N)), where N is the number of elements in the range.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Note</b>: Invalidates the iterators (but not the references)
    //!    to the erased elements. No destructors are called.
    iterator erase(const_iterator b, const_iterator e) noexcept
    {
        size_t n;   
        return this->private_erase(b, e, n);
    }

    //! <b>Effects</b>: Erases all the elements with the given value.
    //!
    //! <b>Returns</b>: The number of erased elements.
    //!
    //! <b>Complexity</b>: O(log(size() + N).
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Note</b>: Invalidates the iterators (but not the references)
    //!    to the erased elements. No destructors are called.
    size_t erase(const_node_ref key) noexcept
    {
        return this->erase(key, m_comp);
    }

    //! <b>Requires</b>: key is a value such that `*this` is partitioned with respect to
    //!   comp(nk, key) and !comp(key, nk), with comp(nk, key) implying !comp(key, nk),
    //!   with nk the key_type of a value_type inserted into `*this`.
    //!
    //! <b>Effects</b>: Erases all the elements with the given key.
    //!   according to the comparison functor "comp".
    //!
    //! <b>Returns</b>: The number of erased elements.
    //!
    //! <b>Complexity</b>: O(log(size() + N).
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Note</b>: Invalidates the iterators (but not the references)
    //!    to the erased elements. No destructors are called.
    template <class KeyNodePtrCompare>
    size_t erase(const_node_ref key, KeyNodePtrCompare comp)
    {
        std::pair<iterator, iterator> p = equal_range(key, comp);
        size_t n;
        private_erase(p.first, p.second, n);
        return n;
    }

    iterator find(const_node_ref key)
    {
        return find(key, m_comp);
    }

    const_iterator find(const_node_ref key) const
    {
        return find(key, m_comp);
    }

    template<class KeyNodePtrCompare>
    iterator find(const_node_ref key, KeyNodePtrCompare comp)
    {
        return iterator(base::find(head_pointer(), key, comp));
    }

    template<class KeyNodePtrCompare>
    const_iterator find(const_node_ref key, KeyNodePtrCompare comp) const
    {
        return const_iterator(base::find(head_pointer(), key, comp));
    }

    //! <b>Effects</b>: Returns the number of contained elements with the given value
    //!
    //! <b>Complexity</b>: Logarithmic to the number of elements contained plus lineal
    //!   to number of objects with the given value.
    //!
    //! <b>Throws</b>: If `key_compare` throws.
    size_t count(const_node_ref key) const
    {
        return count(key, m_comp);
    }

    template<class KeyNodePtrCompare>
    size_t count(const_node_ref key, KeyNodePtrCompare comp) const
    {
        std::pair<const_iterator, const_iterator> ret = equal_range(key, comp);
        size_t n = 0;
        for (; ret.first != ret.second; ++ret.first) { ++n; }
        return n;
    }

    std::pair<iterator, iterator> equal_range(const_node_ref key) const
    {
        return equal_range(key, m_comp);
    }

    template<class KeyNodePtrCompare>
    std::pair<iterator, iterator> equal_range(const_node_ref key, KeyNodePtrCompare comp) const
    {
        std::pair<node_ptr, node_ptr> ret = base::equal_range(head_pointer(), key, comp);
        return std::pair<iterator, iterator>(iterator(ret.first), iterator(ret.second));
    }

    std::pair<iterator, iterator> lower_bound_range(const_node_ref key) const
    {
        return lower_bound_range(key, m_comp);
    }

    template<class KeyNodePtrCompare>
    std::pair<iterator, iterator> lower_bound_range(const_node_ref key, KeyNodePtrCompare comp)
    {
        std::pair<node_ptr, node_ptr> ret = base::lower_bound_range(head_pointer(), key, comp);
        return std::pair<iterator, iterator>(iterator(ret.first), iterator(ret.second));
    }

    std::pair<iterator, iterator> bounded_range(const_node_ref lower_key, const_node_ref upper_key,
        bool left_closed, bool right_closed)
    {
        return bounded_range(lower_key, upper_key, m_comp, left_closed, right_closed);
    }

    template<class KeyNodePtrCompare>
    std::pair<iterator, iterator> bounded_range(const_node_ref lower_key, const_node_ref upper_key,
        KeyNodePtrCompare comp, bool left_closed, bool right_closed)
    {
        std::pair<node_ptr, node_ptr> ret = base::bounded_range(head_pointer(), lower_key, upper_key,
            comp, left_closed, right_closed);
        return std::pair<iterator, iterator>(iterator(ret.first), iterator(ret.second));
    }

    void clear() noexcept
    {
        base::init_header(head_pointer());
        m_size = 0;
    }

    //! <b>Effects</b>: Erases all of the elements calling disposer(p) for
    //!   each node to be erased.
    //! <b>Complexity</b>: Average complexity for is at most O(log(size() + N)),
    //!   where N is the number of elements in the container.
    //!
    //! <b>Throws</b>: Nothing.
    //!
    //! <b>Note</b>: Invalidates the iterators (but not the references)
    //!    to the erased elements. Calls N times to disposer functor.
    template<class Disposer>
    void clear_and_dispose(Disposer disposer) noexcept
    {
        base::clear_and_dispose(head_pointer(), disposer);
        base::init_header(this->header_ptr());
        m_size = 0;
    }

    //! <b>Requires</b>: Disposer::operator()(pointer) shouldn't throw.
    //!   Cloner should yield to nodes equivalent to the original nodes.
    //!
    //! <b>Effects</b>: Erases all the elements from *this
    //!   calling Disposer::operator()(pointer), clones all the
    //!   elements from src calling Cloner::operator()(const_reference )
    //!   and inserts them on *this. Copies the predicate from the source container.
    //!
    //!   If cloner throws, all cloned elements are unlinked and disposed
    //!   calling Disposer::operator()(pointer).
    //!
    //! <b>Complexity</b>: Linear to erased plus inserted elements.
    //!
    //! <b>Throws</b>: If cloner throws or predicate copy assignment throws. Basic guarantee.
    template <class Cloner, class Disposer>
    inline void clone_from(const tree& src, Cloner cloner, Disposer disposer)
    {
        this->clear_and_dispose(disposer);
        if (!src.empty())
        {
            base::clone(src.head_pointer(), this->head_pointer(), cloner, disposer);
            m_comp = src.m_comp;
            m_size = src.m_size;
        }
    }

private:
    iterator insert_unique_commit(node_ref value, const insert_commit_data& commit_data) noexcept
    {
        node_ptr to_insert(node_traits::to_node_ptr(value));
        base::insert_unique_commit(head_pointer(), to_insert, commit_data);
        m_size++;
        return iterator{ to_insert };
    }

    iterator private_erase(const_iterator b, const_iterator e, size_t& n)
    {
        for (n = 0; b != e; ++n)
            this->erase(b++);
        return iterator{ b.m_node };
    }

    key_node_compare m_comp;
    node m_head;
    size_t m_size = 0;
};
