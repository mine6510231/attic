#pragma once

#include "pch.h"

#include "Tor.h"

class Application
{
public:
    Application(uint32_t width, uint32_t height, const wchar_t* name);
    virtual ~Application();

    virtual void Init() {};
    virtual void Tick() {};
    virtual void Simulate() {};
    virtual void Render() {};
    virtual void Shutdown() {};

    uint32_t GetWidth() const { return m_width; }
    uint32_t GetHeight() const { return m_height; }
    const WCHAR* GetTitle() const { return m_title.c_str(); }

    virtual void ParseCommandLineArgs(_In_reads_(argc) WCHAR* argv[], int argc) {};

protected:
    void SetCustomWindowText(LPCWSTR text);

    uint32_t m_width;
    uint32_t m_height;
    std::wstring m_title;
};

class Win32Application
{
public:
    static int Run(Application* app, HINSTANCE hInstance, int nCmdShow);
    static HWND GetHwnd() { return m_hwnd; }

protected:
    static LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
    static HWND m_hwnd;
};
