#include "pch.h"
#include "Application.h"

#include "imgui_impl_win32.h"

using namespace Microsoft::WRL;

Application::Application(uint32_t width, uint32_t height, const wchar_t* name) 
    : m_width(width)
    , m_height(height)
    , m_title(name)
{
}

Application::~Application()
{
}

void Application::SetCustomWindowText(LPCWSTR text)
{
    std::wstring windowText = m_title + L": " + text;
    SetWindowText(Win32Application::GetHwnd(), windowText.c_str());
}

//! Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

HWND Win32Application::m_hwnd = nullptr;

int Win32Application::Run(Application* app, HINSTANCE hInstance, int nCmdShow)
{
    //! Parse the command line parameters
    int argc;
    LPWSTR* argv = CommandLineToArgvW(GetCommandLineW(), &argc);
    app->ParseCommandLineArgs(argv, argc);
    LocalFree(argv);

    //! Initialize the window class.
    WNDCLASSEX windowClass = { 0 };
    windowClass.cbSize = sizeof(WNDCLASSEX);
    windowClass.style = CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = WindowProc;
    windowClass.hInstance = hInstance;
    windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    windowClass.lpszClassName = L"SPRTToolkit";
    RegisterClassEx(&windowClass);

    RECT windowRect = { 0, 0, static_cast<LONG>(app->GetWidth()), static_cast<LONG>(app->GetHeight()) };
    AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE);

    //! Create the window and store a handle to it.
    m_hwnd = CreateWindow(
        windowClass.lpszClassName,
        app->GetTitle(),
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        windowRect.right - windowRect.left,
        windowRect.bottom - windowRect.top,
        nullptr,        // We have no parent window.
        nullptr,        // We aren't using menus.
        hInstance,
        app);

    //! Initialize the sample. OnInit is defined in each child-implementation of DXSample.
    app->Init();

    ShowWindow(m_hwnd, nCmdShow);

    MSG msg = {};

    //! Main loop.
    while (msg.message != WM_QUIT)
    {
        app->Tick();

        //! Process any messages in the queue.
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    app->Shutdown();

    //! Return this part of the WM_QUIT message to Windows.
    return static_cast<char>(msg.wParam);
}

//! Main message handler for the sample.
LRESULT CALLBACK Win32Application::WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    //! Tunnel window messages to imgui for input handling.
    ImGui_ImplWin32_WndProcHandler(hWnd, message, wParam, lParam);

    Application* app = reinterpret_cast<Application*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));

    switch (message)
    {
    case WM_CREATE:
    {
        //! Save the Application* passed in to CreateWindow.
        LPCREATESTRUCT pCreateStruct = reinterpret_cast<LPCREATESTRUCT>(lParam);
        SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pCreateStruct->lpCreateParams));
    }
    return 0;
    case WM_KEYDOWN:
        if (wParam == VK_ESCAPE)
        {
            PostQuitMessage(0);
        }
        return 0;
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    }

    //! Handle any messages the switch statement didn't.
    return DefWindowProc(hWnd, message, wParam, lParam);
}
