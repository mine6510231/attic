#pragma once

//! Simplest wrappers to improve quality of life

#include "pch.h"

#ifdef _DEBUG

void DebugPrintAndBreak(HRESULT h, const wchar_t* format, ...);
void DebugPrintWarningMessage(const wchar_t* window, const wchar_t* format, ...);

//! Wrap a macro for painless removal in release mode
#define HENSURE(x, f, ...) \
    if (!x) \
    { \
        DebugPrintAndBreak(x, f, __VA_ARGS__); \
    }

#define HCHECK(x) \
    assert(x == S_OK)

#define WARNING(x, f, ...)\
    DebugPrintWarningMessage(x, f, __VA_ARGS__);

#else
#define WARNING(x, f, ...)
#define HENSURE(x, f, ...)
#define HCHECK(x) x
#endif

#define USE_DXGI_VERSION_1(x) \
    using x##X = x; static constexpr uint32_t x##Version = 0;

#define USE_DXGI_VERSION_2(x, y) \
    using x##X = x##y; static constexpr uint32_t x##Version = y;

#define EXPAND(x) x
#define SPECIALIZE(_1, _2, Name, ...) Name
#define USE_DXGI_VERSION(...) \
    EXPAND(SPECIALIZE(__VA_ARGS__, USE_DXGI_VERSION_2, USE_DXGI_VERSION_1) (__VA_ARGS__))

#ifndef DXGI_VERSION_ADAPTER
#define DXGI_VERSION_ADAPTER 3
#endif

#ifndef DXGI_VERSION_DEVICE
#define DXGI_VERSION_DEVICE 7
#endif

#ifndef DXGI_VERSION_FACTORY
#define DXGI_VERSION_FACTORY 7
#endif

#ifndef DXGI_VERSION_SWAP_CHAIN
#define DXGI_VERSION_SWAP_CHAIN 3
#endif

#ifndef DXGI_VERSION_FENCE
#define DXGI_VERSION_FENCE 1
#endif

#ifndef DXGI_VERSION_GRAPHICS_COMMAND_LIST
#define DXGI_VERSION_GRAPHICS_COMMAND_LIST 7
#endif

USE_DXGI_VERSION(IDXGIAdapter, DXGI_VERSION_ADAPTER); //! Minimum: >= 1
USE_DXGI_VERSION(ID3D12Device, DXGI_VERSION_DEVICE);
USE_DXGI_VERSION(IDXGIFactory, DXGI_VERSION_FACTORY); //! Minimum: >= 3
USE_DXGI_VERSION(IDXGISwapChain, DXGI_VERSION_SWAP_CHAIN);
USE_DXGI_VERSION(ID3D12Fence, DXGI_VERSION_FENCE);
USE_DXGI_VERSION(ID3D12GraphicsCommandList, DXGI_VERSION_GRAPHICS_COMMAND_LIST);

//! Use a namespace to separate from D3D12 and D3DX12 built-ins
namespace Tor
{
    namespace Utils
    {
        void WaitForGPU(ID3D12FenceX* fence, HANDLE fenceEvent, size_t fenceValue);
    }

    struct DescriptorHeap
    {
    public:
        static std::shared_ptr<DescriptorHeap> Create(
            ID3D12Device* device, const D3D12_DESCRIPTOR_HEAP_DESC& desc, const wchar_t* name);

        ~DescriptorHeap();

        //! The returned GPU descriptor handle is valid only if the heap is set
        //! with flag D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE, otherwise it's
        //! undefined.
        std::tuple<D3D12_CPU_DESCRIPTOR_HANDLE, D3D12_GPU_DESCRIPTOR_HANDLE>
            PushBack(uint32_t count = 1);

        std::tuple<D3D12_CPU_DESCRIPTOR_HANDLE, D3D12_GPU_DESCRIPTOR_HANDLE>
            PopBack(uint32_t count = 1);

        std::tuple<D3D12_CPU_DESCRIPTOR_HANDLE, D3D12_GPU_DESCRIPTOR_HANDLE>
            At(uint32_t index);

        ID3D12DescriptorHeap* Get() const { return m_heap; };
        uint32_t GetSize() const { return m_size; }
        uint32_t GetCapacity() const { return m_capacity; }
        const wchar_t* GetName() const { return m_name.c_str(); }
        
        //! ID3D12DescriptorHeap member functions forwarding
        inline D3D12_CPU_DESCRIPTOR_HANDLE GetCPUDescriptorHandleForHeapStart()
        {
            return m_heap->GetCPUDescriptorHandleForHeapStart();
        }
        inline D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandleForHeapStart()
        {
            return m_heap->GetGPUDescriptorHandleForHeapStart();
        }
        inline D3D12_DESCRIPTOR_HEAP_DESC GetDesc()
        {
            return m_heap->GetDesc();
        }

        //! Member are exposed for flexibility, but generally not recommanded, 
        //! I personally never do this except for tiny test programs I worked on along.
        ID3D12DescriptorHeap* m_heap = nullptr;
        std::wstring m_name;
        uint32_t m_size = 0;
        uint32_t m_capacity = 0;
        uint32_t m_stride = 0; //! Descriptor size

        //! Although this simple descriptor heap wrapper forces synchronous cpu & gpu descriptor
        //! allocation, since the start address of cpu and gpu descriptor in heap could be different
        //! , they are still needed to be computed and stored separately
        D3D12_CPU_DESCRIPTOR_HANDLE m_cpuAddress;
        D3D12_GPU_DESCRIPTOR_HANDLE m_gpuAddress;

    protected:
        DescriptorHeap(ID3D12Device* device, const D3D12_DESCRIPTOR_HEAP_DESC& desc, const wchar_t* name);
    };

    struct Device
    {
    public:
        struct Desc
        {
            uint32_t m_factoryFlags = 0;
            DXGI_GPU_PREFERENCE m_preference = DXGI_GPU_PREFERENCE_HIGH_PERFORMANCE;
            D3D_FEATURE_LEVEL m_featureLevel = D3D_FEATURE_LEVEL_12_2;
            bool m_enableDebugLayer = true;
        };

        static std::shared_ptr<Device> Create(const Desc& desc, const wchar_t* name);

        ~Device();

        ID3D12DeviceX* Get() const { return m_device; }
        const Desc& GetDesc() const { return m_desc; }
        IDXGIAdapterX* GetAdapter() const { return m_adapter; }
        IDXGIFactoryX* GetFactory() const { return m_factory; }
        const wchar_t* GetName() const { return m_name.c_str(); }

        //! Normally you would prefer to separate them up for best flexibility in real product.
        //! I just grouped them together here for simplicity.
        ID3D12DeviceX* m_device = nullptr;
        IDXGIAdapterX* m_adapter = nullptr;
        IDXGIFactoryX* m_factory = nullptr;

        Desc m_desc;
        std::wstring m_name;

    protected:
        Device(const Desc& desc, const wchar_t* name);
    };

    struct SwapChain
    {
    public:
        struct Desc
        {
            uint32_t m_bufferCount = 0;
            uint32_t m_width = 0;
            uint32_t m_height = 0;
            DXGI_FORMAT m_format = DXGI_FORMAT_UNKNOWN;
            DXGI_SWAP_EFFECT m_swapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;

            DXGI_SWAP_CHAIN_FULLSCREEN_DESC* m_fullScreenDesc = nullptr;
            IDXGIOutput* m_restrictedOutput = nullptr;
        };

        static std::shared_ptr<SwapChain> Create(const Desc& desc, ID3D12DeviceX* device, 
            ID3D12CommandQueue* queue, IDXGIFactoryX* factory, HWND hwnd, const wchar_t* name);

        ~SwapChain();

        IDXGISwapChainX* Get() const { return m_swapChain; }
        const Desc& GetDesc() const { return m_desc; }
        ID3D12Resource* GetBuffer(uint32_t index) const { return m_frameBuffers[index]; }
        std::span<ID3D12Resource*> GetBuffers() { return m_frameBuffers; }
        ID3D12DescriptorHeap* GetHeap() const { return m_frameBufferHeap; }
        D3D12_CPU_DESCRIPTOR_HANDLE GetCpuBufferAddress(uint32_t index);
        D3D12_GPU_DESCRIPTOR_HANDLE GetGpuBufferAddress(uint32_t index);

        //! IDXGISwapChain member functions forwarding
        inline void Present(
            uint32_t syncInterval = 1, uint32_t flags = 0, DXGI_PRESENT_PARAMETERS* params = nullptr) const
        {
            if (params)
            {
                m_swapChain->Present1(syncInterval, flags, params);
            }
            else
            {
                m_swapChain->Present(syncInterval, flags);
            }
        }
        inline uint32_t GetCurrentBackBufferIndex() const
        {
            return m_swapChain->GetCurrentBackBufferIndex(); 
        }

        //! I allocate separate descriptor heap for frame buffers to enclose it into SwapChain class, 
        //! it's solely for convenience and not recommended in any other case. Generally you may want your
        //! process's memory as continuous as possible, i.e. closer objects in logic are also closer in memory, 
        //! to reduce overhead of memory searching / address translation and improve cache performance.
        ID3D12DescriptorHeap* m_frameBufferHeap = nullptr;
        IDXGISwapChainX* m_swapChain = nullptr;
        std::vector<ID3D12Resource*> m_frameBuffers;
        uint32_t m_heapStride = 0;

        Desc m_desc;
        std::wstring m_name;

    protected:
        SwapChain(const Desc& desc, ID3D12DeviceX* device, 
            ID3D12CommandQueue* queue, IDXGIFactoryX* factory, HWND hwnd, const wchar_t* name);
    };

    struct RootSignatureBuilder
    {
        ~RootSignatureBuilder();

        static constexpr D3D_ROOT_SIGNATURE_VERSION Version = D3D_ROOT_SIGNATURE_VERSION_1_1;

        RootSignatureBuilder* Init(ID3D12DeviceX* device);
        RootSignatureBuilder* SetFlags(D3D12_ROOT_SIGNATURE_FLAGS flags);
        RootSignatureBuilder* AddShaderInputConstants(uint32_t num32BitValues,
            uint32_t registerId, uint32_t registerSpace = 0, D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);
        RootSignatureBuilder* AddShaderInputDescriptor(uint32_t registerId, uint32_t registerSpace = 0,
            D3D12_ROOT_DESCRIPTOR_FLAGS flags = D3D12_ROOT_DESCRIPTOR_FLAG_NONE,
            D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);
        RootSignatureBuilder* AddShaderInputDescriptorTable(D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);
        RootSignatureBuilder* AddDescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE type,
            uint32_t numDescriptors, uint32_t baseRegister, 
            uint32_t registerSpace = 0, uint32_t offset = 0,
            D3D12_DESCRIPTOR_RANGE_FLAGS flags = D3D12_DESCRIPTOR_RANGE_FLAG_NONE);
        RootSignatureBuilder* FinalizeDescriptorTable();
        RootSignatureBuilder* AddStaticSampler(D3D12_FILTER filter, 
            D3D12_TEXTURE_ADDRESS_MODE addressU, D3D12_TEXTURE_ADDRESS_MODE addressV, D3D12_TEXTURE_ADDRESS_MODE addressW,
            uint32_t registerId, uint32_t registerSpace = 0, 
            D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL,
            D3D12_COMPARISON_FUNC comparisonFuncs = D3D12_COMPARISON_FUNC_NEVER,
            D3D12_STATIC_BORDER_COLOR borderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK,
            float mipLodBias = 0.0f, uint32_t maxAnisotropy = 0, 
            float minLod = 0.0, float maxLod = D3D12_FLOAT32_MAX);
        Microsoft::WRL::ComPtr<ID3DBlob> Finalize();
        void Reset();

        std::vector<D3D12_ROOT_PARAMETER1> m_rootParameters;
        D3D12_ROOT_SIGNATURE_FLAGS m_flags = D3D12_ROOT_SIGNATURE_FLAG_NONE;
        const wchar_t* m_name = nullptr;

        std::vector<std::pair<D3D12_SHADER_VISIBILITY, std::vector<D3D12_DESCRIPTOR_RANGE1>>> m_descriptorTables;
        std::vector<D3D12_STATIC_SAMPLER_DESC> m_samplers;
        uint32_t m_workingDescriptorTableIndex = 0;
        bool m_workingDescriptorTableScope = false;
    };

    struct Fence
    {
    public:
        struct Event
        {
            Event(const wchar_t* name) : m_name(name) 
            {
                m_event = CreateEvent(nullptr, false, false, m_name);
                if (m_event == nullptr)
                {
                    HCHECK(HRESULT_FROM_WIN32(GetLastError()));
                }
            }
            ~Event() { CloseHandle(m_event); }

            HANDLE m_event;
            const wchar_t* m_name = nullptr;
        };

        enum class State
        {
            Standby,
            Signaled
        };

        static std::shared_ptr<Fence> Create(ID3D12DeviceX* device, 
            State initialState = State::Signaled, D3D12_FENCE_FLAGS flags = D3D12_FENCE_FLAG_NONE);

        ~Fence();

        void Signal(); //! Signal on cpu 
        void Signal(ID3D12CommandQueue* queue); //! Signal on gpu

        size_t Increment() { return ++m_pendingValue; };
        
        void Wait();
        void Wait(const Event& event);
        void WaitFor(const Event& event, size_t value);
        using SignalCallback = std::function<void()>;
        void WaitCpuAsync(const SignalCallback& callback);

        void Reset();

        ID3D12FenceX* Get() const { return m_fence; }
        State GetState() const;
        size_t GetPendingValue() const { return m_pendingValue; }
        size_t GetCompletedValue() const { return m_fence->GetCompletedValue(); }

        ID3D12FenceX* m_fence = nullptr;
        size_t m_pendingValue = 1;

    protected:
        Fence(ID3D12DeviceX* device, State initialState, D3D12_FENCE_FLAGS flags);
        std::thread m_waitThread;
    };

    //! Constant buffer is a quite special resource in modern graphics API as it can be both written by CPU 
    //! and read by GPU simultaneously across the program's lifetime, which, not a surprise to anyone, 
    //! introduces certain synchronization problems. Therefore we will keep multiple copies of constant data
    //! under the hood to avoid race condition, which is exactly what old DX11 driver internally did for you.

    namespace Internal
    {
        //! A simplest object pool specifically for allocating command list and 
        //! command list allocator, where both could have many instances across the rendering process.
        template <typename ObjectType>
        struct ObjectPool
        {
            template <typename T> using Ptr = std::shared_ptr<T>;

            static constexpr uint32_t CollectLatency = 3;

            static std::shared_ptr<ObjectPool> Create()
            {
                ObjectPool* pool = new ObjectPool();
                return std::shared_ptr<ObjectPool>(pool);
            }

            ObjectPool() {}
            ~ObjectPool() { Reset(); }

            void Reset()
            {
                std::lock_guard<std::mutex> lock1(m_allocMutex);
                {
                    std::lock_guard<std::mutex> lock2(m_freeMutex);
                    m_items.clear();
                    while (!m_freeList.empty())
                    {
                        m_freeList.pop();
                    }
                    m_pending.clear();
                    m_collectCycle = 0;
                }
            }

            template <typename ...Args>
            ObjectType* Allocate(Args&&... args)
            {
                ObjectType* recycledItem = nullptr;
                {
                    std::lock_guard<std::mutex> lock(m_allocMutex);
                    if (!m_freeList.empty())
                    {
                        recycledItem = m_freeList.front();
                        m_freeList.pop();
                    }
                    else
                    {
                        Ptr<ObjectType> newItem = ObjectType::Create(std::forward<Args>(args)...);
                        if (newItem)
                        {
                            m_items.insert(newItem);
                        }
                        return newItem.get();
                    }
                }

                if (recycledItem)
                {
                    recycledItem->Reset(std::forward<Args>(args)...);
                }
                return recycledItem;
            }

            //! Add the input object to the pending list for recycling. Unlike adding released
            //! object directly into free list, in this way we can avoid having to lock free list for
            //! both allocating and freeing, which could result in serious blocking if there are multiple 
            //! threads obtain and release objects simultaneously.
            void Free(ObjectType* a)
            {
                std::lock_guard<std::mutex> lock(m_freeMutex);
                m_pending.push_back(a);
            }

            void Collect()
            {
                if (m_collectCycle < CollectLatency)
                {
                    ++m_collectCycle;
                }
                else
                {
                    std::lock_guard<std::mutex> lock1(m_allocMutex);
                    {
                        std::lock_guard<std::mutex> lock2(m_freeMutex);
                        for (const auto item : m_pending)
                        {
                            m_freeList.push(item);
                        }
                        m_pending.clear();
                        m_collectCycle = 0;
                    }
                }
            }

            //! Release all recycled objects in m_freelist immediately. This could be useful to deal with
            //! peak of heavily multithreaded cases, where a large number of threads (e.g. 1000) allocate 
            //! lots of objects at once before garbage collection, for example, for initialization, while only 
            //! a few of them (e.g. 10) would be used later on, which leads to severe memory redundancy 
            //! (99% of objects are never reused but never released).
            void Prune()
            {
                if (!m_freeList.empty())
                {
                    std::lock_guard<std::mutex> lock(m_allocMutex);
                    while (!m_freeList.empty())
                    {
                        auto* item = m_freeList.front();
                        m_items.erase(Ptr<ObjectType>(item));
                        m_freeList.pop();
                    }
                }
            }

        private:
            std::unordered_set<Ptr<ObjectType>> m_items;
            std::queue<ObjectType*> m_freeList;
            std::vector<ObjectType*> m_pending;
            std::mutex m_allocMutex;
            std::mutex m_freeMutex;
            uint32_t m_collectCycle = 0;
        };
    } // Internal

    struct CommandList;
    struct CommandAllocator;

    using CommandListPool = Internal::ObjectPool<CommandList>;
    using CommandAllocatorPool = Internal::ObjectPool<CommandAllocator>;

    //! A thin wrapper around command list to support pooling
    struct CommandList
    {
        friend struct CommandListAllocator;
    public:
        static std::shared_ptr<CommandList> Create(uint32_t nodeMask, ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type, 
            ID3D12CommandAllocator* allocator, const wchar_t* name, D3D12_COMMAND_LIST_FLAGS flags = D3D12_COMMAND_LIST_FLAG_NONE);

        ~CommandList();

        void Reset(uint32_t nodeMask, ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type,
            ID3D12CommandAllocator* allocator, const wchar_t* name, D3D12_COMMAND_LIST_FLAGS flags = D3D12_COMMAND_LIST_FLAG_NONE);

        ID3D12GraphicsCommandListX* Get() const { return m_commandList; }

        //! Release the object return it back to the pool
        void Free();

        ID3D12GraphicsCommandListX* m_commandList = nullptr;

        //! Parent pool will be set by parent CommandListAllocator directly on allocation.
        CommandListPool* m_parentPool = nullptr;

    protected:
        CommandList(uint32_t nodeMask, ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type, 
            ID3D12CommandAllocator* allocator, const wchar_t* name, D3D12_COMMAND_LIST_FLAGS flags);
    };

    struct CommandAllocator
    {
        friend struct CommandListAllocator;
    public:
        static std::shared_ptr<CommandAllocator> Create(ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type);

        ~CommandAllocator();

        void Reset(ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type);

        ID3D12CommandAllocator* Get() const { return m_commandAllocator; }

        void Free();

        ID3D12CommandAllocator* m_commandAllocator = nullptr;
        CommandAllocatorPool* m_parentPool = nullptr;

    protected:
        CommandAllocator(ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type);
    };

    struct CommandListAllocator
    {
    public:
        enum class Queue : uint32_t
        {
            Graphics = 0,
            Compute = 1,
            Copy = 2,

            Count
        };
        static constexpr uint32_t QueueCount = static_cast<uint32_t>(Queue::Count);

        static std::shared_ptr<CommandListAllocator> Create(ID3D12DeviceX* device, 
            const wchar_t* name, uint32_t nodeMask = 0, D3D12_COMMAND_LIST_FLAGS flags = D3D12_COMMAND_LIST_FLAG_NONE);

        ~CommandListAllocator();

        //! We return a weak reference to caller as the allocated command list is internally
        //! managed by object pool for reuse.
        CommandList* Allocate(Queue queue, const wchar_t* name);

        //! Similar to the idea of driver's context roll, this function opens new CommandAllocator
        //! from the pool as new front for creating command list and return the previous allocator 
        //! without releasing it to let caller to decide when to destroy it, this gives user program
        //! some time to either wait previously recorded command list to finish or reuse the allocator
        //! for various purpose.
        //! Note: First invocation will return null pointer because command allocators are lazily initialized
        CommandAllocator* Roll(Queue queue);

        //! This version free current front allocator immediately and allocate new one as front.
        void FRoll(Queue queue);

        CommandAllocator* Front(Queue queue) const { return m_frontAllocators[static_cast<uint32_t>(queue)]; }

        //! Garbage collect
        void Collect();

        std::array<std::shared_ptr<CommandAllocatorPool>, QueueCount> m_commandAllocatorPools;
        std::array<std::shared_ptr<CommandListPool>, QueueCount> m_commandListPools;
        std::array<CommandAllocator*, QueueCount> m_frontAllocators = { 0 };
        std::wstring m_name;

        //! Cached weak reference of device context for resource allocation
        ID3D12DeviceX* m_device = nullptr;

        //! GPU index, this is initialzed on construction and inherited by all command lists the allocator
        //! allocates.
        uint32_t m_nodeMask = 0;
        D3D12_COMMAND_LIST_FLAGS m_flags = D3D12_COMMAND_LIST_FLAG_NONE;

    protected:
        CommandListAllocator(ID3D12DeviceX* device, 
            const wchar_t* name, uint32_t nodeMask, D3D12_COMMAND_LIST_FLAGS flags);
    };

    struct AsyncUploadQueue
    {
    public:
        using CompleteCallback = std::function<void()>;
        using FenceValue = uint64_t;

        enum RequestType : uint32_t
        {
            Buffer = 0,
            Texture = 1
        };

        struct Desc
        {
            size_t m_stagingAreaSizeInBytes = 4ull * 1024 * 1024;
            uint32_t m_contextCount = 1;
        };

        //! This struct should be exactly 56 bytes to let the final request variant
        //! elegantly 64 bytes aligned.
        struct TextureUploadRequest
        {
            ID3D12Resource* m_resource = nullptr;

            //! Subresources to upload placed in order within range 
            //! [arrayStart, arrayEnd][mipStart, mipEnd], same as DX12 convention mip level is indexed first: 
            //! element0{ mip0, mip1, mip2 ... }, element1{ mip0, mip1, mip2 ... } and so on so forth.
            //! Each face of cubemap is treated as a single subresource and for cubemap array it's:
            //! cubemap0: face0: mip0, mip1, ..., face1: mip0, mip1, ..., ...
            //! cubemap1: face0: mip0, mip1, ..., face1: mip0, mip1, ..., ...
            //! ... until 6 * cubemap array size
            //! All depth slices of a volume texture's mipmap is represented by a single subresource entry.
            std::span<D3D12_SUBRESOURCE_DATA> m_subresources;

            //! Left open subresources range: [mipStart, mipEnd)
            uint16_t m_mipStart = 0;
            uint16_t m_mipEnd = UINT16_MAX;
            uint16_t m_arrayStart = 0;
            uint16_t m_arrayEnd = UINT16_MAX;
            uint16_t m_blockHeight = 1;

            bool m_waitForUpload = true;
        };

        struct Request
        {
            std::variant<TextureUploadRequest> m_request;
            FenceValue m_fenceValue;
        };

        //! Force heap allocation and reference counted.
        static std::shared_ptr<AsyncUploadQueue> Create(ID3D12DeviceX* device, const Desc& desc, int priority = 0);

        ~AsyncUploadQueue();

        FenceValue QueueUpload(const TextureUploadRequest& request);

        bool IsRequestFinished(FenceValue value);
        void WaitFor(FenceValue value);

        struct Context
        {
            ~Context();

            ID3D12Resource* m_stagingBuffer = nullptr;
            ID3D12CommandAllocator* m_ca = nullptr;
            ID3D12GraphicsCommandListX* m_cl = nullptr;

            ID3D12FenceX* m_fence = nullptr;
            uint64_t m_fenceValue = 0;
            HANDLE m_fenceEvent;

            uint8_t* m_stagingBufferData = nullptr;
            uint32_t m_dataOffset = 0;
        };

        Context& BeginContext();
        void EndContext();

        bool m_recording = false;

        ID3D12CommandQueue* m_uploadQueue = nullptr;
        ID3D12DeviceX* m_device = nullptr;

        std::vector<Context> m_contexts;
        uint32_t m_contextFrontIndex = 0;

        Desc m_desc;

        ID3D12FenceX* m_uploadFence = nullptr;

        //! Upload fence value could be increased by multiple threads simultaneously so it's set as atomic.
        //! Contexts are implicitly processed by m_taskExecutor in order so their fence value are just plain integers.
        std::atomic_uint64_t m_uploadFenceValue = 0;
        HANDLE m_uploadFenceEvent;

        std::queue<Request> m_requests;
        std::mutex m_requestsMutex;
        std::thread m_handler;
        std::condition_variable m_handlerCondition;
        std::atomic_bool m_exiting = false;

    protected:
        AsyncUploadQueue(ID3D12DeviceX* device, const Desc& decs, int priority);

        void Queue(const Request& request);
        void Process();
        void UploadTexture(const TextureUploadRequest& request, FenceValue fenceValue);
        void UploadBuffer();
    };

} // namespace Tor
