#pragma once

//! Exclude rarely-used stuff from Windows headers.
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

//! Windows platform
#include <windows.h>
#include <wrl.h>
#include <comdef.h>
#include <intrin.h>
#include <pix.h>
#include <shellapi.h>

//! DX12
#include <d3d12.h>
#include <dxgi1_6.h>
#include <D3Dcompiler.h>
#include <DirectXMath.h>

//! The helper library d3dx12.h is not part of Windows SDK (only a legacy version included in Visual Studio
//! for DirectX project template) but separately maintained on a git repository and copied into my project 
//! in "torbernite\ext\DirectX-Headers", which needs to be added to include search path (either manually 
//! or via build system) to work properly.
//! Git repo (MIT licensed): https://github.com/microsoft/DirectX-Headers
#include <d3dx12.h>

//! C++
//! Drop winrt's min/max macros
#undef min
#undef max
#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <functional>
#include <format>
#include <memory>
#include <mutex>
#include <print>
#include <string>
#include <type_traits>
#include <vector>
#include <span>
#include <unordered_set>
#include <queue>
#include <variant>