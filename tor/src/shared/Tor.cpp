#include "pch.h"

#include "Tor.h"

namespace Tor
{
    //! =======================================================
    //! Common utilities
    //! =======================================================

    inline void Utils::WaitForGPU(ID3D12FenceX* fence, HANDLE fenceEvent, uint64_t fenceValue)
    {
        //! There are still previously placed fence value not signaled yet, wait until all works before
        //! the fence has finished and the desired fence value is reached
        if (fenceValue > fence->GetCompletedValue())
        {
            fence->SetEventOnCompletion(fenceValue, fenceEvent);
            WaitForSingleObjectEx(fenceEvent, INFINITE, FALSE);
        }
    }

    //! =======================================================
    //! Descriptor heap
    //! =======================================================

    std::shared_ptr<DescriptorHeap> DescriptorHeap::Create(
        ID3D12Device* device, const D3D12_DESCRIPTOR_HEAP_DESC& desc, const wchar_t* name)
    {
        DescriptorHeap* heap = new DescriptorHeap(device, desc, name);
        return std::shared_ptr<DescriptorHeap>(heap);
    }

    DescriptorHeap::DescriptorHeap(
        ID3D12Device* device, const D3D12_DESCRIPTOR_HEAP_DESC& desc, const wchar_t* name)
    {
        HRESULT ret = device->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&m_heap));
        HENSURE(ret == S_OK, L"Descriptor heap [%ls] creation failed", name);
        m_stride = device->GetDescriptorHandleIncrementSize(desc.Type);
        m_cpuAddress = m_heap->GetCPUDescriptorHandleForHeapStart();
        if ((desc.Flags & D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE) != 0)
        {
            m_gpuAddress = m_heap->GetGPUDescriptorHandleForHeapStart();
        }
        else
        {
            //! Set gpu address to invalid for cpu only heaps (like render target view (RTV))
            //! Technically 0 is not a totally invalid virtual address for descriptor heap but 
            //! very likely.
            m_gpuAddress.ptr = 0;
        }
        m_capacity = desc.NumDescriptors;
        m_name.assign(name);
        m_heap->SetName(m_name.c_str());
    }

    std::tuple<D3D12_CPU_DESCRIPTOR_HANDLE, D3D12_GPU_DESCRIPTOR_HANDLE>
    DescriptorHeap::PushBack(uint32_t count)
    {
        assert(m_size + count <= m_capacity);

        auto cpuAddr = m_cpuAddress;
        auto gpuAddr = m_gpuAddress;

        SIZE_T offset = count * m_stride;

        //! Move cursor
        m_cpuAddress.ptr += offset;

        if (m_gpuAddress.ptr)
        {
            m_gpuAddress.ptr += offset;
        }

        m_size += count;

        return { cpuAddr, gpuAddr };
    }

    std::tuple<D3D12_CPU_DESCRIPTOR_HANDLE, D3D12_GPU_DESCRIPTOR_HANDLE>
    DescriptorHeap::PopBack(uint32_t count)
    {
        assert(m_size >= count);

        SIZE_T offset = count * m_stride;
        m_cpuAddress.ptr -= offset;

        if (m_gpuAddress.ptr)
        {
            m_gpuAddress.ptr -= offset;
        }

        m_size -= count;

        return { m_cpuAddress, m_gpuAddress };
    }

    std::tuple<D3D12_CPU_DESCRIPTOR_HANDLE, D3D12_GPU_DESCRIPTOR_HANDLE>
    DescriptorHeap::At(uint32_t index)
    {
        assert(index < m_size);

        SIZE_T offset = (m_size - index) * m_stride;

        //! Note the cache m_cpuAddress and m_gpuAddress are point to heap bottom, i.e. the 
        //! next available empty slot / the end of the heap it manages so we deduce cursor to
        //! move backwards to find the indexed element instead of move forwards from the top, 
        //! which is also feasible via GetCPU/GPUDescriptorHandleForHeapStart() which could involve
        //! unnecessary copy of values.
        D3D12_CPU_DESCRIPTOR_HANDLE cpuAddr{ m_cpuAddress.ptr - offset };
        D3D12_GPU_DESCRIPTOR_HANDLE gpuAddr{ m_gpuAddress.ptr ? (m_gpuAddress.ptr - offset) : 0 };
        return { cpuAddr, gpuAddr };
    }

    DescriptorHeap::~DescriptorHeap()
    {
        if (m_heap != nullptr)
        {
            m_heap->Release();
        }
    }

    //! =======================================================
    //! Device
    //! =======================================================

    std::shared_ptr<Device> Device::Create(const Desc& desc, const wchar_t* name)
    {
        Device* device = new Device(desc, name);
        return std::shared_ptr<Device>(device);
    }

    Device::Device(const Desc& desc, const wchar_t* name)
    {
        uint32_t factoryFlags = desc.m_factoryFlags;

        // !Enable the debug layer (requires the Graphics Tools "optional feature").
        //! NOTE: Enabling the debug layer after device creation will invalidate the active device.
        if (desc.m_enableDebugLayer)
        {
            Microsoft::WRL::ComPtr<ID3D12Debug> debugController;
            if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
            {
                debugController->EnableDebugLayer();
                factoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
            }
        }

        HRESULT ret = CreateDXGIFactory2(factoryFlags, IID_PPV_ARGS(&m_factory));
        HENSURE(ret == S_OK, L"Failed to create factory");

        DXGI_ADAPTER_DESC1 adapterDesc;

        //! Enumerate adapaters
        for (uint32_t index = 0; SUCCEEDED(m_factory->EnumAdapterByGpuPreference(
            index, desc.m_preference, IID_PPV_ARGS(&m_adapter))); ++index)
        {
            m_adapter->GetDesc1(&adapterDesc);

            if (adapterDesc.Flags & DXGI_ADAPTER_FLAG3_SOFTWARE)
            {
                //! Skip software renderer
                continue;
            }

            //! Check whether the adapter supports requested feature level
            if (SUCCEEDED(
                D3D12CreateDevice(m_adapter, desc.m_featureLevel, _uuidof(ID3D12DeviceX), nullptr)))
            {
                break;
            }
        }

        ret = D3D12CreateDevice(m_adapter, desc.m_featureLevel, IID_PPV_ARGS(&m_device));
        HENSURE(ret == S_OK, L"Failed to create device from adapter [%ls]", adapterDesc.Description);

        wchar_t buf[256];
        swprintf(buf, _countof(buf), L"[%ls]_%ls", adapterDesc.Description, name);
        m_name.assign(buf);
        m_device->SetName(m_name.c_str());

        m_desc = desc;
    }

    Device::~Device()
    {
        if (m_device != nullptr) m_device->Release();
        if (m_adapter != nullptr) m_adapter->Release();
        if (m_factory != nullptr) m_factory->Release();
    }

    //! =======================================================
    //! Swap chain
    //! =======================================================

    std::shared_ptr<SwapChain> SwapChain::Create(const Desc& desc, ID3D12DeviceX* device, 
        ID3D12CommandQueue* queue, IDXGIFactoryX* factory, HWND hwnd, const wchar_t* name)
    {
        SwapChain* swapChain = new SwapChain(desc, device, queue, factory, hwnd, name);
        return std::shared_ptr<SwapChain>(swapChain);
    }

    SwapChain::SwapChain(const Desc& desc, ID3D12DeviceX* device, 
        ID3D12CommandQueue* queue, IDXGIFactoryX* factory, HWND hwnd, const wchar_t* name)
    {
        //! Create swap chain
        {
            DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
            swapChainDesc.BufferCount = desc.m_bufferCount;
            swapChainDesc.Width = desc.m_width;
            swapChainDesc.Height = desc.m_height;
            swapChainDesc.Format = desc.m_format;
            swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
            swapChainDesc.SwapEffect = desc.m_swapEffect;
            swapChainDesc.SampleDesc.Count = 1;

            //! Exploit old swap chain creation interface to bind window handle
            IDXGISwapChain1* swapChain = nullptr;
            HRESULT ret = factory->CreateSwapChainForHwnd(
                queue, hwnd, &swapChainDesc,
                desc.m_fullScreenDesc ? desc.m_fullScreenDesc : nullptr,
                desc.m_restrictedOutput ? desc.m_restrictedOutput : nullptr,
                &swapChain);
            HENSURE(ret == S_OK, L"Failed to create swap chain [%ls]", name);

            //! Lift to desired version of swap chain interface.
            swapChain->QueryInterface(_uuidof(IDXGISwapChainX), (void**)&m_swapChain);

            ret = factory->MakeWindowAssociation(hwnd, DXGI_MWA_NO_ALT_ENTER);
            HENSURE(ret == S_OK, L"Failed to associate with application window");

            m_name.assign(name);
        }
        
        //! Create frame buffers
        {
            D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
            rtvHeapDesc.NumDescriptors = desc.m_bufferCount;
            rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
            rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE; //! RTV heap must have none.
            HRESULT ret = device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&m_frameBufferHeap));
            HENSURE(ret == S_OK, L"Failed to create descriptor heap for frame buffers");

            wchar_t buf[128];
            swprintf(buf, 128 * sizeof(wchar_t), L"%ls_FrameBufferHeap", m_name.c_str());
            m_frameBufferHeap->SetName(buf);

            m_heapStride = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

            m_frameBuffers.resize(desc.m_bufferCount);

            for (uint32_t i = 0; i < desc.m_bufferCount; ++i)
            {
                m_swapChain->GetBuffer(i, __uuidof(ID3D12Resource), (void**)&m_frameBuffers[i]);

                D3D12_CPU_DESCRIPTOR_HANDLE handle{
                    m_frameBufferHeap->GetCPUDescriptorHandleForHeapStart().ptr + m_heapStride * i};
                device->CreateRenderTargetView(m_frameBuffers[i], nullptr, handle);

                swprintf(buf, 128 * sizeof(wchar_t), L"%ls_Frame_%d", m_name.c_str(), i);
                m_frameBuffers[i]->SetName(buf);
            }
        }

        m_desc = desc;
    }

    SwapChain::~SwapChain()
    {
        if (m_frameBufferHeap != nullptr) m_frameBufferHeap->Release();
        if (m_swapChain != nullptr) m_swapChain->Release();
        m_frameBuffers.clear();
    }

    D3D12_CPU_DESCRIPTOR_HANDLE SwapChain::GetCpuBufferAddress(uint32_t index)
    {
        assert(index < m_desc.m_bufferCount);
        return { m_frameBufferHeap->GetCPUDescriptorHandleForHeapStart().ptr + m_heapStride * index };
    }

    D3D12_GPU_DESCRIPTOR_HANDLE SwapChain::GetGpuBufferAddress(uint32_t index)
    {
        assert(index < m_desc.m_bufferCount);
        return { m_frameBufferHeap->GetGPUDescriptorHandleForHeapStart().ptr + m_heapStride * index };
    }

    //! =======================================================
    //! Root signature builder
    //! =======================================================

    RootSignatureBuilder::~RootSignatureBuilder()
    {
        Reset();
    }

    RootSignatureBuilder* RootSignatureBuilder::Init(ID3D12DeviceX* device)
    {
        D3D12_FEATURE_DATA_ROOT_SIGNATURE featureData;
        featureData.HighestVersion = Version;
        if (FAILED(device->CheckFeatureSupport(D3D12_FEATURE_ROOT_SIGNATURE, &featureData, sizeof(featureData))))
        {
            featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_0;
        }

        if (featureData.HighestVersion < Version)
        {
            assert(false && "Root signature version not supported");
            return nullptr;
        }

        return this;
    }

    RootSignatureBuilder* RootSignatureBuilder::SetFlags(D3D12_ROOT_SIGNATURE_FLAGS flags)
    {
        m_flags = flags;
        return this;
    }

    RootSignatureBuilder* RootSignatureBuilder::AddShaderInputConstants(uint32_t num32BitValues, 
        uint32_t registerId, uint32_t registerSpace, D3D12_SHADER_VISIBILITY visibility)
    {    
        auto& parameter = m_rootParameters.emplace_back();
        parameter.ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
        parameter.Constants.Num32BitValues = num32BitValues;
        parameter.Constants.ShaderRegister = registerId;
        parameter.Constants.RegisterSpace = registerSpace;
        parameter.ShaderVisibility = visibility;
        return this;
    }

    RootSignatureBuilder* RootSignatureBuilder::AddShaderInputDescriptor(uint32_t registerId, uint32_t registerSpace,
        D3D12_ROOT_DESCRIPTOR_FLAGS flags, D3D12_SHADER_VISIBILITY visibility)
    {
        auto& parameter = m_rootParameters.emplace_back();
        parameter.ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
        parameter.Descriptor.ShaderRegister = registerId;
        parameter.Descriptor.RegisterSpace = registerSpace;
        parameter.Descriptor.Flags = flags;
        parameter.ShaderVisibility = visibility;
        return this;
    }

    RootSignatureBuilder* RootSignatureBuilder::AddShaderInputDescriptorTable(D3D12_SHADER_VISIBILITY visibility)
    {
        m_workingDescriptorTableScope = true;
        m_workingDescriptorTableIndex = static_cast<uint32_t>(m_descriptorTables.size());
        auto& table = m_descriptorTables.emplace_back();
        table.first = visibility;
        return this;
    }

    RootSignatureBuilder* RootSignatureBuilder::AddDescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE type,
        uint32_t numDescriptors, uint32_t baseRegister, uint32_t registerSpace, uint32_t offset,
        D3D12_DESCRIPTOR_RANGE_FLAGS flags)
    {
        assert(m_workingDescriptorTableScope);
        auto& range = m_descriptorTables[m_workingDescriptorTableIndex].second.emplace_back();
        range.RangeType = type;
        range.NumDescriptors = numDescriptors;
        range.BaseShaderRegister = baseRegister;
        range.RegisterSpace = registerSpace;
        range.Flags = flags;
        range.OffsetInDescriptorsFromTableStart = offset;
        return this;
    }

    RootSignatureBuilder* RootSignatureBuilder::FinalizeDescriptorTable()
    {
        auto& table = m_descriptorTables[m_workingDescriptorTableIndex];
        auto& parameter = m_rootParameters.emplace_back();
        parameter.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
        parameter.DescriptorTable.NumDescriptorRanges = static_cast<uint32_t>(table.second.size());
        parameter.DescriptorTable.pDescriptorRanges = table.second.data();
        parameter.ShaderVisibility = table.first;
        m_workingDescriptorTableScope = false;
        return this;
    }

    RootSignatureBuilder* RootSignatureBuilder::AddStaticSampler(D3D12_FILTER filter,
        D3D12_TEXTURE_ADDRESS_MODE addressU, D3D12_TEXTURE_ADDRESS_MODE addressV, D3D12_TEXTURE_ADDRESS_MODE addressW,
        uint32_t registerId, uint32_t registerSpace, D3D12_SHADER_VISIBILITY visibility,
        D3D12_COMPARISON_FUNC comparisonFuncs, D3D12_STATIC_BORDER_COLOR borderColor,
        float mipLodBias, uint32_t maxAnisotropy, float minLod, float maxLod)
    {
        auto& sampler = m_samplers.emplace_back();
        sampler.Filter = filter;
        sampler.AddressU = addressU;
        sampler.AddressV = addressV;
        sampler.AddressW = addressW;
        sampler.MipLODBias = mipLodBias;
        sampler.MaxAnisotropy = maxAnisotropy;
        sampler.ComparisonFunc = comparisonFuncs;
        sampler.BorderColor = borderColor;
        sampler.MinLOD = minLod;
        sampler.MaxLOD = maxLod;
        sampler.ShaderRegister = registerId;
        sampler.RegisterSpace = registerSpace;
        sampler.ShaderVisibility = visibility;
        return this;
    }

    Microsoft::WRL::ComPtr<ID3DBlob> RootSignatureBuilder::Finalize()
    {
        //! For now I'll stick built version to 1.1
        D3D12_VERSIONED_ROOT_SIGNATURE_DESC desc{};
        desc.Version = Version;
        desc.Desc_1_1.NumParameters = static_cast<uint32_t>(m_rootParameters.size());
        desc.Desc_1_1.pParameters = m_rootParameters.data();
        desc.Desc_1_1.NumStaticSamplers = static_cast<uint32_t>(m_samplers.size());
        desc.Desc_1_1.pStaticSamplers = m_samplers.size() > 0 ? m_samplers.data() : nullptr;
        desc.Desc_1_1.Flags = m_flags;

        Microsoft::WRL::ComPtr<ID3DBlob> signature;
        Microsoft::WRL::ComPtr<ID3DBlob> error;
        HRESULT ret = D3DX12SerializeVersionedRootSignature(&desc, Version, &signature, &error);
        if (ret != S_OK)
        {
            wprintf(L"Root signature serialization failed: %ls\n", (wchar_t*)error->GetBufferPointer());
            return nullptr;
        }
        return signature;
    }

    void RootSignatureBuilder::Reset()
    {
        if (!m_descriptorTables.empty())
        {
            for (auto& tables : m_descriptorTables)
            {
                tables.second.clear();
            }
        }
        m_descriptorTables.clear();
        m_samplers.clear();
        m_rootParameters.clear();
        m_flags = D3D12_ROOT_SIGNATURE_FLAG_NONE;
        m_workingDescriptorTableIndex = 0;
        m_workingDescriptorTableScope = false;
    }

    //! =======================================================
    //! Fence
    //! =======================================================

    std::shared_ptr<Fence> Fence::Create(
        ID3D12DeviceX* device, State initialState, D3D12_FENCE_FLAGS flags)
    {
        Fence* fence = new Fence(device, initialState, flags);
        return std::shared_ptr<Fence>(fence);
    }

    Fence::Fence(ID3D12DeviceX* device, State initialState, D3D12_FENCE_FLAGS flags)
    {
        HRESULT ret = device->CreateFence(0, flags, _uuidof(ID3D12FenceX), (void**)&m_fence);
        HENSURE(ret == S_OK, L"Failed to create fence");
        m_pendingValue = (initialState == State::Signaled) ? 0 : 1;
    }

    Fence::~Fence()
    {
        if (m_fence != nullptr) m_fence->Release();
    }

    void Fence::Signal()
    {
        m_fence->Signal(m_pendingValue);
    }

    void Fence::Signal(ID3D12CommandQueue* queue)
    {
        queue->Signal(m_fence, m_pendingValue);
    }

    void Fence::Wait()
    {
        Wait({ L"WaitOnCpu" });
    }

    void Fence::Wait(const Event& event)
    {
        WaitFor(event, m_pendingValue);
    }

    void Fence::WaitFor(const Event& event, size_t value)
    {
        if (value > m_fence->GetCompletedValue())
        {
            m_fence->SetEventOnCompletion(value, event.m_event);
            WaitForSingleObjectEx(event.m_event, INFINITE, false);
        }
    }

    void Fence::WaitCpuAsync(const SignalCallback& callback)
    {
        if (m_waitThread.joinable())
        {
            m_waitThread.join();
        }
        m_waitThread = std::thread([this, callback]() {
            Wait();
            callback();
        });
    }

    void Fence::Reset()
    {
        Increment();
    }

    Fence::State Fence::GetState() const
    {
        return (m_pendingValue <= m_fence->GetCompletedValue()) ? State::Standby : State::Signaled;
    }

    //! =======================================================
    //! Command list allocator
    //! =======================================================

    std::shared_ptr<CommandList> CommandList::Create(uint32_t nodeMask, ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type,
        ID3D12CommandAllocator* allocator, const wchar_t* name, D3D12_COMMAND_LIST_FLAGS flags)
    {
        CommandList* commandList = new CommandList(nodeMask, device, type, allocator, name, flags);
        return std::shared_ptr<CommandList>(commandList);
    }

    CommandList::CommandList(uint32_t nodeMask, ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type,
        ID3D12CommandAllocator* allocator, const wchar_t* name, D3D12_COMMAND_LIST_FLAGS flags)
    {
        HRESULT ret = device->CreateCommandList1(
            nodeMask, type, flags, __uuidof(ID3D12GraphicsCommandListX), (void**)&m_commandList);
        HENSURE(ret == S_OK, L"Failed to create command list [%ls]", name);
        m_commandList->SetName(name);
        m_commandList->Reset(allocator, nullptr);
    }

    CommandList::~CommandList()
    {
        if (m_commandList != nullptr) m_commandList->Release();
    }

    void CommandList::Reset(uint32_t nodeMask, ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type,
        ID3D12CommandAllocator* allocator, const wchar_t* name, D3D12_COMMAND_LIST_FLAGS flags)
    {
        //! These fields are consistent throughout the life time of parent CommandListAllocator thus
        //! can be safely ignored on reuse. They are only defined as parameter to match with Create()'s
        //! signature, which is required by the object pool.
        (void)nodeMask;
        (void)device;
        (void)type;
        (void)flags;
        m_commandList->SetName(name);
        //! Note: As a linear allocator ID3D12CommandAllocator will not release command list's memory
        //! when it is reset, instead, a new chunk of memory will be allocated and the old memory
        //! will not be freed until the command allocator itself is reset. It means the command allocator
        //! needs to be periodically reset throughout the program to avoid consuming too much memory.
        m_commandList->Reset(allocator, nullptr);
    }

    void CommandList::Free()
    {
        if (m_parentPool)
        {
            m_parentPool->Free(this);
        }
    }

    std::shared_ptr<CommandAllocator> CommandAllocator::Create(ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type)
    {
        CommandAllocator* commandAllocator = new CommandAllocator(device, type);
        return std::shared_ptr<CommandAllocator>(commandAllocator);
    }

    CommandAllocator::CommandAllocator(ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type)
    {
        HCHECK(device->CreateCommandAllocator(type, IID_PPV_ARGS(&m_commandAllocator)));
    }

    CommandAllocator::~CommandAllocator()
    {
        if (m_commandAllocator != nullptr) m_commandAllocator->Release();
    }

    void CommandAllocator::Reset(ID3D12DeviceX* device, D3D12_COMMAND_LIST_TYPE type)
    {
        //! Command list allocator can only be reset when all associated command lists have finished execution 
        //! on the GPU. It's user's burden to track if GPU execution is in progress via, for example, fences or something else.
        m_commandAllocator->Reset();
    }

    void CommandAllocator::Free()
    {
        if (m_parentPool != nullptr)
        {
            m_parentPool->Free(this);
        }
    }

    std::shared_ptr<CommandListAllocator> CommandListAllocator::Create(ID3D12DeviceX* device, 
        const wchar_t* name, uint32_t nodeMask, D3D12_COMMAND_LIST_FLAGS flags)
    {
        CommandListAllocator* allocator = new CommandListAllocator(device, name, nodeMask, flags);
        return std::shared_ptr<CommandListAllocator>(allocator);
    }

    CommandListAllocator::CommandListAllocator(ID3D12DeviceX* device, 
        const wchar_t* name, uint32_t nodeMask, D3D12_COMMAND_LIST_FLAGS flags)
    {
        m_device = device;
        m_nodeMask = nodeMask;
        m_flags = flags;
        for (auto& pool : m_commandAllocatorPools)
        {
            pool = CommandAllocatorPool::Create();
        }
        for (auto& pool : m_commandListPools)
        {
            pool = CommandListPool::Create();
        }
        m_name.assign(name);
    }

    CommandListAllocator::~CommandListAllocator()
    {
        for (auto& pool : m_commandAllocatorPools)
        {
            pool.reset();
        }
        for (auto& pool : m_commandListPools)
        {
            pool.reset();
        }
    }

    CommandList* CommandListAllocator::Allocate(Queue queue, const wchar_t* name)
    {
        D3D12_COMMAND_LIST_TYPE type = D3D12_COMMAND_LIST_TYPE_NONE;
        switch (queue)
        {
        case Queue::Graphics: type = D3D12_COMMAND_LIST_TYPE_DIRECT; break;
        case Queue::Compute: type = D3D12_COMMAND_LIST_TYPE_COMPUTE; break;
        case Queue::Copy: type = D3D12_COMMAND_LIST_TYPE_COPY; break;
        default: break;
        }

        if (type != D3D12_COMMAND_LIST_TYPE_NONE)
        {
            uint32_t index = static_cast<uint32_t>(queue);
            //! For now [20240325] we just omit the flag parameter as current D3D12 hasn't 
            //! provided any meaningful flag for command list yet.
            auto* commandList = m_commandListPools[index]->Allocate(m_nodeMask, m_device, 
                type, m_frontAllocators[index]->Get(), name, m_flags);
            commandList->m_parentPool = m_commandListPools[index].get();
            return commandList;
        }
        else
        {
            //! Unimplemented command queue type, just return null.
            return nullptr;
        }
    }

    CommandAllocator* CommandListAllocator::Roll(Queue queue)
    {
        uint32_t index = static_cast<uint32_t>(queue);
        CommandAllocator* allocator = m_frontAllocators[index];
        switch (queue)
        {
        case Queue::Graphics:
            m_frontAllocators[index] = m_commandAllocatorPools[index]->Allocate(m_device, D3D12_COMMAND_LIST_TYPE_DIRECT);
            break;
        case Queue::Compute:
            m_frontAllocators[index] = m_commandAllocatorPools[index]->Allocate(m_device, D3D12_COMMAND_LIST_TYPE_COMPUTE);
            break;
        case Queue::Copy:
            m_frontAllocators[index] = m_commandAllocatorPools[index]->Allocate(m_device, D3D12_COMMAND_LIST_TYPE_COPY);
            break;
        }
        m_frontAllocators[index]->m_parentPool = m_commandAllocatorPools[index].get();
        return allocator;
    }

    void CommandListAllocator::FRoll(Queue queue)
    {
        auto* allocator = Roll(queue);
        if (allocator)
        {
            allocator->Free();
        }
    }

    void CommandListAllocator::Collect()
    {
        for (auto& p : m_commandAllocatorPools)
        {
            p->Collect();
        }
        for (auto& p : m_commandListPools)
        {
            p->Collect();
        }
    }

    //! =======================================================
    //! Async upload queue
    //! =======================================================

    AsyncUploadQueue::Context::~Context()
    {
        m_stagingBuffer->Release();
        m_ca->Release();
        m_cl->Release();
        m_fence->Release();
    }

    std::shared_ptr<AsyncUploadQueue> AsyncUploadQueue::Create(ID3D12DeviceX* device, const Desc& desc, int priority)
    {
        AsyncUploadQueue* uploadQueue = new AsyncUploadQueue(device, desc, priority);
        return std::shared_ptr<AsyncUploadQueue>(uploadQueue);
    }

    AsyncUploadQueue::AsyncUploadQueue(ID3D12DeviceX* device, const Desc& desc, int priority)
    {
        m_desc = desc;
        m_device = device;

        //! Create copy queue
        {
            D3D12_COMMAND_QUEUE_DESC desc = {};
            desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
            desc.Type = D3D12_COMMAND_LIST_TYPE_COPY;
            desc.Priority = priority;
            device->CreateCommandQueue(&desc, __uuidof(ID3D12CommandQueue), (void**)&m_uploadQueue);
            m_uploadQueue->SetName(L"AsyncUploadQueue_CopyQueue");
        }

        //! Create fence
        {
            m_device->CreateFence(0, D3D12_FENCE_FLAG_NONE, __uuidof(ID3D12FenceX), (void**)&m_uploadFence);
            m_uploadFenceEvent = CreateEvent(nullptr, FALSE, FALSE, L"[AsyncUploadQueue]: Wait for upload done");
        }

        //! Create resources for each frame packet
        {
            m_contexts.reserve(m_desc.m_contextCount * sizeof(Context));
            for (uint32_t i = 0; i < m_desc.m_contextCount; ++i)
            {
                m_contexts.emplace_back();
                Context& context = m_contexts.back();
                device->CreateFence(0, D3D12_FENCE_FLAG_NONE, __uuidof(ID3D12FenceX), (void**)&context.m_fence);

                context.m_fenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);

                CD3DX12_HEAP_PROPERTIES heapProperties(D3D12_HEAP_TYPE_UPLOAD);
                CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Buffer(m_desc.m_stagingAreaSizeInBytes);

                device->CreateCommittedResource(
                    &heapProperties,
                    D3D12_HEAP_FLAG_NONE,
                    &desc,
                    D3D12_RESOURCE_STATE_GENERIC_READ,
                    nullptr,
                    __uuidof(ID3D12Resource),
                    (void**)&context.m_stagingBuffer);

                //! This indicates we never read from mapped buffer (i.e. write only)
                CD3DX12_RANGE readRange(0, 0);

                //! Keep staging buffer mapped across its lifetime, which is a legal usage defined by 
                //! specification not hack.
                context.m_stagingBuffer->Map(0, &readRange, reinterpret_cast<void**>(&context.m_stagingBufferData));

                //! Create command allocator
                device->CreateCommandAllocator(
                    D3D12_COMMAND_LIST_TYPE_COPY, __uuidof(ID3D12CommandAllocator), (void**)&context.m_ca);

                //! Create command list
                device->CreateCommandList(
                    0, D3D12_COMMAND_LIST_TYPE_COPY,
                    context.m_ca,
                    nullptr,
                    __uuidof(ID3D12GraphicsCommandListX),
                    (void**)&context.m_cl);
                context.m_cl->Close();
            }
        }

        //! Initiate background request processing thread
        {
            m_handler = std::thread(&AsyncUploadQueue::Process, this);
        }
    }

    AsyncUploadQueue::~AsyncUploadQueue()
    {
        m_exiting = true;
        //! Notify task executing thread to quite immediately
        m_handlerCondition.notify_all();

        m_uploadQueue->Release();
        m_contexts.clear();
        m_uploadFence->Release();
        if (m_handler.joinable())
        {
            m_handler.join();
        }
        while (!m_requests.empty())
        {
            m_requests.pop();
        }
    }

    uint64_t AsyncUploadQueue::QueueUpload(const TextureUploadRequest& request)
    {
        uint64_t fenceValue = ++m_uploadFenceValue;
        Queue({ request, fenceValue });
        return fenceValue;
    }

    bool AsyncUploadQueue::IsRequestFinished(FenceValue value)
    {
        return m_uploadFence->GetCompletedValue() >= value;
    }

    void AsyncUploadQueue::WaitFor(FenceValue value)
    {
        Utils::WaitForGPU(m_uploadFence, m_uploadFenceEvent, value);
    }

    AsyncUploadQueue::Context& AsyncUploadQueue::BeginContext()
    {
        assert(!m_recording);
        Context& context = m_contexts[m_contextFrontIndex];

        //! Wait remaining works in frame packet to finish before recycling.
        Utils::WaitForGPU(context.m_fence, context.m_fenceEvent, context.m_fenceValue);
        ++context.m_fenceValue;

        context.m_dataOffset = 0;
        context.m_ca->Reset();
        context.m_cl->Reset(context.m_ca, nullptr);

        m_recording = true;
        return context;
    }

    void AsyncUploadQueue::EndContext()
    {
        assert(m_recording);
        Context& context = m_contexts[m_contextFrontIndex];

        //! Submit commands
        context.m_cl->Close();
        ID3D12CommandList* commandLists[] = { context.m_cl };
        m_uploadQueue->ExecuteCommandLists(1, commandLists);
        m_uploadQueue->Signal(context.m_fence, context.m_fenceValue);

        //! Roll context
        m_contextFrontIndex = (m_contextFrontIndex + 1) % m_desc.m_contextCount;
        m_recording = false;
    }

    inline void AsyncUploadQueue::Queue(const Request& request)
    {
        std::lock_guard<std::mutex> lock(m_requestsMutex);
        m_requests.emplace(request);
        m_handlerCondition.notify_all();
    }

    void AsyncUploadQueue::Process()
    {
        //! An implementation of upload queue mainly focuses on latency (delay of processing new request) 
        //! over throughput (because it's simpler)

        //! Runs forever in a background thread
        for (;;)
        {
            std::unique_lock<std::mutex> lock(m_requestsMutex);
            m_handlerCondition.wait(lock, [this]() { return !m_requests.empty() || m_exiting; });
            if (m_exiting)
            {
                return;
            }
            Request request = std::move(m_requests.front());
            m_requests.pop();
            lock.unlock();

            if (std::holds_alternative<TextureUploadRequest>(request.m_request))
            {
                UploadTexture(std::get<TextureUploadRequest>(request.m_request), request.m_fenceValue);
            }
        }
    }

    void AsyncUploadQueue::UploadTexture(const TextureUploadRequest& request, FenceValue fenceValue)
    {
        auto AlignUpPOT = [](uint32_t v, uint32_t a)->uint32_t { return (v + (a - 1)) & ~(a - 1); };
        auto AlignDownPOT = [](uint32_t v, uint32_t a)->uint32_t { return v & ~(a - 1); };

        auto& context = BeginContext();

        auto desc = request.m_resource->GetDesc();
        bool isVolume = desc.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE3D;

        //! Clamp mip and array upper bound to mip level and array size respectively
        uint16_t mipEnd = std::min(request.m_mipEnd, desc.MipLevels);
        uint16_t arrayEnd = std::min(request.m_arrayEnd, desc.DepthOrArraySize);

        for (uint16_t mip = request.m_mipStart; mip < mipEnd; ++mip)
        {
            auto subresource = request.m_subresources[mip - request.m_mipStart];

            uint32_t mipWidth = static_cast<uint32_t>(desc.Width >> mip);
            uint32_t mipHeight = static_cast<uint32_t>(desc.Height >> mip);
            uint32_t mipDepth = isVolume ? std::max(desc.DepthOrArraySize, static_cast<uint16_t>(1u)) >> mip : 1;

            assert(mipWidth > 0 && mipHeight > 0);

            uint32_t rowPitchA = AlignUpPOT(static_cast<uint32_t>(subresource.RowPitch), D3D12_TEXTURE_DATA_PITCH_ALIGNMENT);
            uint32_t slicePitchA = AlignUpPOT(static_cast<uint32_t>(subresource.SlicePitch), D3D12_TEXTURE_DATA_PLACEMENT_ALIGNMENT);

            uint32_t rows = static_cast<uint32_t>(subresource.SlicePitch / subresource.RowPitch);

            //! This defines subparition of a single subresource in case of staging buffer is too small to 
            //! hold the entire subresource (e.g. mip 0 of 4k texture), or the subresource contains multiple 
            //! slices that could be merge in one go (e.g. volume texture). 
            //! By default uploader will try to copy a single slice of subresource as a whole without splitting.
            uint32_t rowsPerPartition = rows;
            uint32_t partitionSize = slicePitchA;
            uint16_t partitionCount = 1;

            bool split = false;

            //! If the current slice is too large to fit into a single context then preparing to split them by rows.
            if (slicePitchA > m_desc.m_stagingAreaSizeInBytes)
            {
                uint32_t minSize = AlignUpPOT(rowPitchA, D3D12_TEXTURE_DATA_PLACEMENT_ALIGNMENT);
                if (minSize > m_desc.m_stagingAreaSizeInBytes)
                {
                    //! No enough space for even a single row, assert and notify user the upload procedure 
                    //! may fail due to memory overflow
                    assert(false &&
                        "No enough space to upload current subresource, please consider increasing staging buffer size");
                }
                split = true;
                rowsPerPartition = static_cast<uint32_t>(m_desc.m_stagingAreaSizeInBytes / rowPitchA);
                partitionCount = (rows + (rowsPerPartition - 1)) / rowsPerPartition;
                partitionSize = AlignUpPOT(rowsPerPartition * rowPitchA, D3D12_TEXTURE_DATA_PLACEMENT_ALIGNMENT);
            }
            else if (isVolume)
            {
                //! Try merging multiple slices 
                uint16_t slicesPerPartition = static_cast<uint16_t>(m_desc.m_stagingAreaSizeInBytes / slicePitchA);
                partitionCount = std::max((mipDepth + (slicesPerPartition - 1)) / slicesPerPartition, 1u);
                rowsPerPartition *= slicesPerPartition;
                partitionSize = AlignUpPOT(rowsPerPartition * rowPitchA, D3D12_TEXTURE_DATA_PLACEMENT_ALIGNMENT);
            }
            assert(partitionSize <= m_desc.m_stagingAreaSizeInBytes && "Final staging area size out of bound due to alignment");

            for (uint16_t array = request.m_arrayStart; array < arrayEnd; ++array)
            {
                //! Plane slice is ignored because it's less commonly used. So do array size (last parameter)
                //! because it's solely used to derive offset of plane slice.
                const uint32_t subresourceIndex = D3D12CalcSubresource(mip, array, 0, desc.MipLevels, 0);
                CD3DX12_TEXTURE_COPY_LOCATION dst(request.m_resource, subresourceIndex);

                for (uint16_t partition = 0; partition < partitionCount; ++partition)
                {
                    if (partitionSize > m_desc.m_stagingAreaSizeInBytes - context.m_dataOffset)
                    {
                        EndContext();
                        context = BeginContext();
                    }

                    if (desc.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE3D && !split && rowsPerPartition > rows)
                    {
                        //! Merge path
                        uint32_t rowStart = rowsPerPartition * partition;
                        uint32_t rowEnd = std::min(rowStart + rowsPerPartition, mipDepth * rows);
                        for (uint32_t row = rowStart; row < rowEnd; ++row)
                        {
                            memcpy(context.m_stagingBufferData + context.m_dataOffset + (row - rowStart) * rowPitchA,
                                static_cast<const uint8_t*>(subresource.pData) + row * subresource.RowPitch, subresource.RowPitch);
                        }

                        uint32_t sliceToCopy = (rowEnd - rowStart) / rows;
                        uint32_t sliceStart = partition * sliceToCopy;
                        if (sliceStart + sliceToCopy > mipDepth)
                        {
                            sliceToCopy = mipDepth - sliceStart;
                        }

                        D3D12_PLACED_SUBRESOURCE_FOOTPRINT fp;
                        fp.Footprint.Width = mipWidth;
                        fp.Footprint.Height = mipHeight;
                        fp.Footprint.Depth = sliceToCopy;
                        fp.Footprint.Format = desc.Format;
                        fp.Footprint.RowPitch = rowPitchA;
                        fp.Offset = context.m_dataOffset;

                        CD3DX12_TEXTURE_COPY_LOCATION src(context.m_stagingBuffer, fp);
                        context.m_cl->CopyTextureRegion(&dst, 0, 0, sliceStart, &src, nullptr);
                    }
                    else if (!split)
                    {
                        uint32_t rowStart = rowsPerPartition * partition;
                        for (uint32_t row = rowStart; row < rowStart + rowsPerPartition; ++row)
                        {
                            memcpy(context.m_stagingBufferData + context.m_dataOffset + (row - rowStart) * rowPitchA,
                                static_cast<const uint8_t*>(subresource.pData) + row * subresource.RowPitch, subresource.RowPitch);
                        }

                        D3D12_PLACED_SUBRESOURCE_FOOTPRINT fp;
                        fp.Footprint.Width = mipWidth;
                        fp.Footprint.Height = mipHeight;
                        fp.Footprint.Depth = 1;
                        fp.Footprint.Format = desc.Format;
                        fp.Footprint.RowPitch = rowPitchA;
                        fp.Offset = context.m_dataOffset;

                        CD3DX12_TEXTURE_COPY_LOCATION src(context.m_stagingBuffer, fp);
                        context.m_cl->CopyTextureRegion(&dst, 0, 0, partition, &src, nullptr);
                    }
                    else
                    {
                        uint32_t rowStart = rowsPerPartition * partition;
                        uint32_t rowEnd = std::min(rowStart + rowsPerPartition, rows);
                        for (uint32_t row = rowStart; row < rowEnd; ++row)
                        {
                            memcpy(context.m_stagingBufferData + context.m_dataOffset + (row - rowStart) * rowPitchA,
                                static_cast<const uint8_t*>(subresource.pData) + row * subresource.RowPitch, subresource.RowPitch);
                        }

                        //! Block compressed images are stored as linear memory, while have to be copied with regular texture
                        //! dimenstion as required by CopyTextureRegion.
                        uint32_t heightToCopy = (rowEnd - rowStart) * request.m_blockHeight;
                        uint32_t heightStart = rowStart * request.m_blockHeight;

                        if (heightStart + heightToCopy > mipHeight)
                        {
                            heightToCopy = mipHeight - heightStart;
                        }

                        D3D12_PLACED_SUBRESOURCE_FOOTPRINT fp;
                        fp.Footprint.Width = mipWidth;
                        fp.Footprint.Height = heightToCopy;
                        fp.Footprint.Depth = 1;
                        fp.Footprint.Format = desc.Format;
                        fp.Footprint.RowPitch = rowPitchA;
                        fp.Offset = context.m_dataOffset;

                        CD3DX12_TEXTURE_COPY_LOCATION src(context.m_stagingBuffer, fp);
                        context.m_cl->CopyTextureRegion(&dst, 0, heightStart, 0, &src, nullptr);
                    }
                    context.m_dataOffset += partitionSize;
                }
            }

            EndContext();

            m_uploadQueue->Signal(m_uploadFence, fenceValue);

            if (request.m_waitForUpload)
            {
                WaitFor(fenceValue);
            }
        }
    }

    void AsyncUploadQueue::UploadBuffer()
    {
        assert(false && "Unimplemented");
    }

} // namespace Tor

#ifdef _DEBUG

void DebugPrintAndBreak(HRESULT h, const wchar_t* format, ...)
{
    //! If you think about it the implementation of this function is a total disaster, which includes
    //! but not limited to: pure C code in C++ code file, duplicate/redundant formatting & printing,
    //! platform specific functions, no polymorphism on character type and so on. Because I simply used the
    //! most trivial way I can come up with to implement it in the shortest time to focus on major part of this
    //! project, but in general poorly written code like this should never appear in real production.

    static wchar_t buf1[256];
    static wchar_t buf2[320];

    if (format)
    {
        va_list args;
        va_start(args, format);
        //! [WARNING] "_vsnwprintf_s" is Windows specific
        _vsnwprintf_s(buf1, _countof(buf1), _TRUNCATE, format, args);
        va_end(args);

        swprintf(buf2, _countof(buf2), L"HRESULT Error [0x%08x]: %ls >> %ls\n", h, _com_error(h).ErrorMessage(), buf1);
    }
    else
    {
        swprintf(buf2, _countof(buf2), L"HRESULT Error [0x%08x]: %ls\n", h, _com_error(h).ErrorMessage());
    }

    //! Print to console 
    fwprintf(stderr, L"%ls", buf2);

    //! Print to Visual Studio's output window
    OutputDebugString(buf2);

    //! Break program
    __debugbreak();
}

void DebugPrintWarningMessage(const wchar_t* window, const wchar_t* format, ...)
{
    static wchar_t buf1[256];
    static wchar_t buf2[272];
    
    va_list args;
    va_start(args, format);
    _vsnwprintf_s(buf1, _countof(buf1), _TRUNCATE, format, args);
    va_end(args);

    swprintf(buf2, _countof(buf2), L"[%ls]: %ls", window, buf1);
    wprintf(L"%ls", buf2);
    OutputDebugString(buf2);
}

#endif // ifdef _DEBUG