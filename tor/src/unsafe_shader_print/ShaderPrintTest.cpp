#include "pch.h"

#define DXGI_VERSION_DEVICE 10

#include "ShaderPrintTest.h"
#include "Auxiliary.h"
#include "EmbeddedModel_Spot.h"

//! Note: To avoid having to keep a copy of dxil.dll in the project the shader is unsigned hence can only be 
//! used in Windows's developer mode (dxc requires dxil.dll that ship with Windows SDK to 'sign' shader binaries 
//! after compilation to mark it as eligible to be used in release environment, there are workarounds to circumvent 
//! this but for simplicity I'll just leave it there) 
#include "draw_vs.hlsl.h"
#include "draw_ps.hlsl.h"

namespace
{
    constexpr D3D_SHADER_MODEL ShaderModel = D3D_SHADER_MODEL_6_6;
    constexpr DXGI_FORMAT SwapChainFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
}

ShaderPrintTest::ShaderPrintTest(uint32_t width, uint32_t height, const wchar_t* name)
    : Base(width, height, name)
{    
}

ShaderPrintTest::~ShaderPrintTest()
{
}

void ShaderPrintTest::Init()
{
    InitContext();
}

void ShaderPrintTest::InitContext()
{
    //! Device
    {
        Tor::Device::Desc desc;
        m_device = Tor::Device::Create(desc, L"SPTDevice");
    }

    auto* device = m_device->Get();

    //! Feature check
    {
        D3D12_FEATURE_DATA_SHADER_MODEL sm = { ShaderModel };
        HCHECK(device->CheckFeatureSupport((D3D12_FEATURE)D3D12_FEATURE_SHADER_MODEL, 
            &sm, sizeof(sm)));
    }
    
    {
        D3D12_COMMAND_QUEUE_DESC desc = {};
        desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
        desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

        HCHECK(device->CreateCommandQueue(&desc, IID_PPV_ARGS(&m_graphicsQueue)));
        m_graphicsQueue->SetName(L"SPTGraphicsQueue");
    }
    
    {
        Tor::SwapChain::Desc desc;
        desc.m_bufferCount = FrameCount;
        desc.m_width = m_width;
        desc.m_height = m_height;
        desc.m_format = SwapChainFormat;
        m_swapChain = Tor::SwapChain::Create(desc, device, 
            m_graphicsQueue.Get(), m_device->GetFactory(), Win32Application::GetHwnd(), L"SPTSwapChain");

        m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();
    }

    {
        D3D12_DESCRIPTOR_HEAP_DESC desc = {};
        desc.NumDescriptors = FrameCount + 1;
        desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
        desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        m_rtvHeap = Tor::DescriptorHeap::Create(device, desc, L"SPTRTVHeap");

        desc.NumDescriptors = 1;
        desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
        desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
        m_csuHeap = Tor::DescriptorHeap::Create(device, desc, L"SPTCSUHeap");

    }

    {
        //! For this simple example two small staging buffers are quite enough.
        Tor::AsyncUploadQueue::Desc desc;
        desc.m_stagingAreaSizeInBytes = 1ull * 1024 * 1024;
        desc.m_contextCount = 2;
        m_uploadQueue = Tor::AsyncUploadQueue::Create(device, desc);
    }

    m_commandListAllocator = Tor::CommandListAllocator::Create(device, L"SPTCommandListAllocator");
}

void ShaderPrintTest::InitPipeline()
{
    m_geomtryPass.Init(m_device->Get());
}

void ShaderPrintTest::InitTestModel()
{
    {
        //! A merged buffer is used to upload all mesh data (vertex attributes + indices)
        //! at once to improve locality and reduce unnecessary CPU-GPU communication,
        //! which is expensive (because PCIe cable is slow, even for PCIe 5.0).
        const CD3DX12_HEAP_PROPERTIES properties{ D3D12_HEAP_TYPE_UPLOAD };

        //! Size in bytes
        size_t bufferSize = EMSpotVerticesSize + EMSpotTrianglesSize;
        const CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Buffer(bufferSize);

        m_device->Get()->CreateCommittedResource(&properties, D3D12_HEAP_FLAG_NONE, &desc,
            D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&m_spotBuffer));
        m_spotBuffer->SetName(L"EMSpotMeshBuffer");

        //! Map mesh data
        uint8_t* pMem = nullptr;
        CD3DX12_RANGE readRange(0, 0);
        //! Null read range indicates the entire subresource could be read from CPU, for write only 
        //! we need to pass a range whose begin == end, like the one defined above
        m_spotBuffer->Map(0, &readRange, reinterpret_cast<void**>(&pMem));

        //! Vertex attributes
        memcpy(pMem, (void*)EMSpotVertices, EMSpotVerticesSize);

        //! Indices
        pMem += EMSpotTrianglesSize;
        memcpy(pMem, (void*)EMSpotTriangles, EMSpotTrianglesSize);

        //! Set pWrittenRange to nullptr to indicate the enture subresource has been written
        m_spotBuffer->Unmap(0, nullptr);

        m_spotVbv.BufferLocation = m_spotBuffer->GetGPUVirtualAddress();
        m_spotVbv.StrideInBytes = sizeof(EMSpotVertex);
        m_spotVbv.SizeInBytes = EMSpotVerticesSize;

        m_spotIbv.BufferLocation = m_spotVbv.BufferLocation + EMSpotVerticesSize;
        m_spotIbv.Format = DXGI_FORMAT_R16_UINT; //! EmbeddedModel_Spot uses 16 bit index internally
        m_spotIbv.SizeInBytes = EMSpotTrianglesSize;
    }

    {
        //! Create and upload diffuse texture

        static constexpr uint32_t SpotDiffuseSize = 512;

        D3D12_RESOURCE_DESC textureDesc = {};
        textureDesc.MipLevels = 1;
        textureDesc.Format = DXGI_FORMAT_BC7_UNORM;
        textureDesc.Width = SpotDiffuseSize;
        textureDesc.Height = SpotDiffuseSize;
        textureDesc.Flags = D3D12_RESOURCE_FLAG_NONE;
        textureDesc.DepthOrArraySize = 1;
        textureDesc.SampleDesc.Count = 1;
        textureDesc.SampleDesc.Quality = 0;
        textureDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;

        CD3DX12_HEAP_PROPERTIES heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);

        //! For this sample I'll just create texture as committed resource for simplicity.
        HCHECK(m_device->Get()->CreateCommittedResource(
            &heapProperties,
            D3D12_HEAP_FLAG_NONE,
            &textureDesc,
            D3D12_RESOURCE_STATE_COPY_DEST,
            nullptr,
            IID_PPV_ARGS(&m_spotAlbedo)));

        D3D12_SUBRESOURCE_DATA textureData = {};
        textureData.pData = EMSpotTextureData;

        //! Block compressed textures store the entire 4x4 block in the first row, therefore it's 
        //! texture width x block width x block height x pixel size (which is 1 byte here for BC7), 
        //! for uncompressed texture it's just texture width x pixel size
        textureData.RowPitch = SpotDiffuseSize * 16;
        textureData.SlicePitch = SpotDiffuseSize * SpotDiffuseSize;

        D3D12_SUBRESOURCE_DATA subresources[] = { textureData };

        Tor::AsyncUploadQueue::TextureUploadRequest request;
        request.m_resource = m_spotAlbedo.Get();
        request.m_subresources = { subresources };
        auto fenceValue = m_uploadQueue->QueueUpload(request);
        m_uploadQueue->WaitFor(fenceValue);

        //! Describe and create a SRV for the texture.
        D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
        srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
        srvDesc.Format = textureDesc.Format;
        srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
        srvDesc.Texture2D.MipLevels = 1;

        const auto [srvHandle, _] = m_csuHeap->PushBack();
        m_device->Get()->CreateShaderResourceView(m_spotAlbedo.Get(), &srvDesc, srvHandle);
    }
}

void ShaderPrintTest::WaitForGPU()
{
    static Tor::Fence::Event GpuSyncEvent{ L"WaitForGpu" };

    //! Push a sync symbol to the command queue
    m_fence->Signal(m_graphicsQueue.Get());

    //! Wait on CPU for the symbol to be signaled on GPU (i.e. all command lists before it in 
    //! the queue has finished execution)
    m_fence->Wait(GpuSyncEvent);

    //! Advance fence value
    m_fence->Increment();
}

void ShaderPrintTest::MoveToNextFrame()
{
    size_t currentFenceValue = m_fence->GetPendingValue();
    m_fenceValues[m_frameIndex] = currentFenceValue;
    m_fence->Signal(m_graphicsQueue.Get());
    m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();
    if (m_fence->GetCompletedValue() < m_fenceValues[m_frameIndex])
    {
        //! Wait if the next frame is not ready to be rendered yet.
        m_fence->Wait();
    }
    m_fenceValues[m_frameIndex] = m_fence->Increment();
}

void ShaderPrintTest::GeometryPass::Init(ID3D12DeviceX* device)
{
    //! Root signature
    {
        Tor::RootSignatureBuilder builder;
        builder.Init(device);

        static constexpr D3D12_ROOT_SIGNATURE_FLAGS flags = 
            D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
            D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
            D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;
        builder.SetFlags(flags);

        ComPtr<ID3DBlob> signature = builder.
            AddShaderInputDescriptorTable(D3D12_SHADER_VISIBILITY_ALL)
                ->AddDescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0, 0)
                ->AddDescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0) //! Spot's Albedo
            ->FinalizeDescriptorTable()
            ->Finalize();

        device->CreateRootSignature(0,
            signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&m_rootSignature));
        m_rootSignature->SetName(L"GeometryPassRootSignature");


    }
    

    
    
}

