#include "pch.h"

#include "WaveTest.h"

_Use_decl_annotations_
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int nCmdShow)
{
    WaveTest app(1600, 900, L"Wave intrinsic test");
    return Win32Application::Run(&app, hInstance, nCmdShow);
}
