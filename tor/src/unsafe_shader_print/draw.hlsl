#include "cb.h"

struct VSIn
{
    float3 Position : POSITION;
    float2 UV : TEXCOORD0;
};

struct VSOut
{
    float4 Position : SV_POSITION;
    float2 UV : TEXCOORD0;
};

struct PSOut
{
    float4 Color : SV_Target0;
};

Texture2D<float4> DiffuseTexture : register(t0);
SamplerState DiffuseTextureSampler : register(s0);

float4x4 GetWorldMatrix()
{
    return float4x4(
        WorldMatrix[0],
        WorldMatrix[1],
        WorldMatrix[2],
        float4(0.0, 0.0, 0.0, 1.0)
    );
}

VSOut VSMain(VSIn IN)
{
    VSOut OUT;

    OUT.Position = mul(ViewProjectionMatrix, mul(GetWorldMatrix(), float4(IN.Position.xyz, 1.0)));
    OUT.UV = IN.UV;

    return OUT;
}

PSOut PSMain(VSOut IN)
{
    PSOut OUT;
    
    float4 color = DiffuseTexture.SampleLevel(DiffuseTextureSampler, IN.UV, 0.0);
    OUT.Color = color;
    
    return OUT;
}