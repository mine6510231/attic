﻿//! A lovely cow model obtained from https://www.cs.cmu.edu/~kmcrane/Projects/ModelRepository

#pragma once

struct EMSpotVertex {
    float m_pos[3];
    float m_uv[2];
};

struct EMSpotTriangle {
	short m_index[3];
};

//! Index type: 16 bit unsigned int
//! Texture format: DXGI_FORMAT_BC7_UNORM
//! Texture size: 512x512

extern EMSpotVertex const EMSpotVertices[];
extern EMSpotTriangle const EMSpotTriangles[];
extern unsigned long long const EMSpotTextureData[];

extern size_t const EMSpotVerticesSize;
extern size_t const EMSpotTrianglesSize;
extern size_t const EMSpotTextureDataSize;

extern size_t const EMSpotVertexCount;
extern size_t const EMSpotTriangleCount;

//! Assert for some base type size assumption, which is required for the 
//! embedded vertex and texture data to be parsed correctly.
static_assert(sizeof(short) == 2 && "EmbeddedModel assumes 16 bit short");
static_assert(sizeof(unsigned long long) == 8 && "EmbeddedModel assumes 64 bit unsigned long long");