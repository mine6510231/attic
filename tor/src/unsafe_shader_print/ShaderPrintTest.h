#pragma once

#include "Application.h"

#include "cb.h"

using namespace DirectX;
using Microsoft::WRL::ComPtr;

class ShaderPrintTest : public Application
{
    using Base = Application;
public:
    ShaderPrintTest(uint32_t width, uint32_t height, const wchar_t* name);
    ~ShaderPrintTest();

    static const uint32_t FrameCount = 2;

    void Init() override;
    void Tick() override;
    void Simulate() override;
    void Render() override;
    void Shutdown() override;

private:
    void InitContext();
    void InitPipeline();
    void InitTestModel();

    void WaitForGPU();
    void MoveToNextFrame();

    struct GeometryPass
    {
        void Init(ID3D12DeviceX* device);
        void Render(ID3D12GraphicsCommandListX* commandlist);

        ComPtr<ID3D12RootSignature> m_rootSignature;
        ComPtr<ID3D12PipelineState> m_pipelineState;

        ComPtr<ID3D12Resource> m_constantBuffer;
        GeometryPassConstants m_constantBufferData;
        //uint8_t* m_constantBufferBegin;

        ComPtr<ID3D12Resource> m_output;
    };

    Camera m_camera;

    GeometryPass m_geomtryPass;

    //! Cow model (a.k.a. Spot)
    ComPtr<ID3D12Resource> m_spotBuffer;
    ComPtr<ID3D12Resource> m_spotAlbedo;
    D3D12_VERTEX_BUFFER_VIEW m_spotVbv;
    D3D12_INDEX_BUFFER_VIEW m_spotIbv;

    CD3DX12_VIEWPORT m_viewport;
    CD3DX12_RECT m_scissorRect;

    std::shared_ptr<Tor::Device> m_device = nullptr;
    std::shared_ptr<Tor::SwapChain> m_swapChain = nullptr;

    std::shared_ptr<Tor::DescriptorHeap> m_rtvHeap = nullptr;
    std::shared_ptr<Tor::DescriptorHeap> m_csuHeap = nullptr;
    
    std::shared_ptr<Tor::AsyncUploadQueue> m_uploadQueue = nullptr;

    ComPtr<ID3D12CommandQueue> m_graphicsQueue;
    ComPtr<ID3D12CommandQueue> m_computeQueue;

    std::shared_ptr<Tor::CommandListAllocator> m_commandListAllocator = nullptr;

    uint32_t m_frameIndex = 0;

    std::shared_ptr<Tor::Fence> m_fence = nullptr;
    size_t m_fenceValues[FrameCount] = { 0 };
};