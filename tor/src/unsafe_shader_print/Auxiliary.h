#pragma once

#include "pch.h"

using namespace DirectX;

struct Camera
{
    void Init(XMFLOAT3& position, float zoom);
    void Update(float delta);
    void SetSpeed(float speed);
    void Reset();

    XMFLOAT3 m_initialPosition;
    XMFLOAT3 m_position;

    XMFLOAT4X4 m_viewMatrix;
    XMFLOAT4X4 m_projectionMatrix;

    float m_moveSpeed;            // Speed at which the camera moves, in units per second.
    float m_turnSpeed;            // Speed at which the camera turns, in radians per second.
};
