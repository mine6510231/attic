#pragma once

#ifdef __cplusplus
#include <cstdint>
#include <DirectXMath.h>
using namespace DirectX;

struct GeometryPassConstants
{
    XMFLOAT3X4 m_worldMatrix;
    XMFLOAT4X4 m_viewProjectionMatrix;
    XMFLOAT2   m_mousePosition;
    XMFLOAT2   m_resolution;  //! Render target resolution
    float      m_time;
    uint32_t   m_padding[31]; //! Aligned to 256 byte cache line boundary
};

#else

cbuffer GeometryPassConstants : register(b0)
{
    float3x4 WorldMatrix;
    float4x4 ViewProjectionMatrix;
    float2   MousePosition;
    float2   Resolution;
    float    Time;
    uint    _Padding[31];
}

#endif