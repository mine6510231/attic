#pragma once

#include "Application.h"

#include "cb.h"

//! A simple program to test Tor framework (in Utils.h) and wave intrinsics

using namespace DirectX;
using Microsoft::WRL::ComPtr;

class WaveTest : public Application
{
    using Base = Application;
public:
    WaveTest(uint32_t width, uint32_t height, const wchar_t* name);
    ~WaveTest();

    void Init() override;
    void Tick() override;
    void Simulate() override;
    void Render() override;
    void Shutdown() override;

private:
    static const UINT FrameCount = 2;

    struct Vertex
    {
        XMFLOAT3 position;
        XMFLOAT2 uv;
    };

    // Pipeline objects.
    CD3DX12_VIEWPORT m_viewport;
    CD3DX12_RECT m_scissorRect;

    std::shared_ptr<Tor::Device> m_device = nullptr;
    std::shared_ptr<Tor::SwapChain> m_swapChain = nullptr;

    std::shared_ptr<Tor::DescriptorHeap> m_rtvHeap = nullptr;
    std::shared_ptr<Tor::DescriptorHeap> m_csuHeap = nullptr;

    std::shared_ptr<Tor::CommandListAllocator> m_commandListAllocator = nullptr;

    ComPtr<ID3D12CommandQueue> m_commandQueue;

    ComPtr<ID3D12RootSignature> m_fullScreenPassRootSignature;
    ComPtr<ID3D12PipelineState> m_fullScreenPassPSO;

    ComPtr<ID3D12RootSignature> m_magnifyPassRootSignature;
    ComPtr<ID3D12PipelineState> m_magnifyPassPSO;

    ComPtr<ID3D12Resource> m_fullScreenPassRenderTarget;

    ComPtr<ID3D12Resource> m_constantBuffer;
    SimpleConstants m_constantBufferData;
    UINT8* m_pCbSrvDataBegin;

    UINT m_frameIndex;

    std::shared_ptr<Tor::Fence> m_fence = nullptr;
    size_t m_fenceValues[FrameCount] = {0};

    //! Shader Model 6 feature support result
    D3D12_FEATURE_DATA_D3D12_OPTIONS1 m_waveIntrinsicsSupport;
    UINT m_mode = 0;
    bool m_setFirstLaneInRed = false;

    void CreateContext();
    void CreatePipeline();
    void CreateImGui();

    void LoadSizeDependentResources();
    void MoveToNextFrame();
    void WaitForGpu();
    void DrawScene();
    void DrawImGui();
};
