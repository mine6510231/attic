#include "cb.h"

struct PSInput
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD;
};

Texture2D g_texture : register(t0);
SamplerState g_sampler : register(s0);

float4 GetVertexPositionAndTexCoords(uint vertexID)
{
    float u = float((vertexID << 1) & 2);
    float v = float(vertexID & 2);

    float x = u * 2.0 - 1.0;
    float y = v * -2.0 + 1.0;

    return float4(x, y, u, v);
}

PSInput VSMain(uint vertexId : SV_VertexID)
{
    PSInput result;

    float4 posTex = GetVertexPositionAndTexCoords(vertexId);
    
    result.position = float4(posTex.x, posTex.y, 0.0, 1.0);
    result.uv = float2(posTex.z, posTex.w);
    
    return result;
}

float4 PSMain(PSInput input) : SV_TARGET
{
    float aspectRatio = Resolution.x / Resolution.y;
    float magnifiedFactor = 6.0;
    float magnifiedAreaSize = 0.05;
    float magnifiedAreaBorder = 0.005;

    // check the distance between this pixel and mouse location in UV space. 
    float2 normalizedPixelPos = input.uv;                           
    float2 normalizedMousePos = MousePosition / Resolution;         // convert mouse position to uv space.
    float2 diff = abs(normalizedPixelPos - normalizedMousePos);     // distance from this pixel to mouse.

    float4 color = g_texture.Sample(g_sampler, normalizedPixelPos);

    if (IsMagnify == 0)
    {
        return color;
    }
    else
    {
        // if the distance from this pixel to mouse is touching the border of the magnified area, color it as cyan.
        if (diff.x < (magnifiedAreaSize + magnifiedAreaBorder) && diff.y < (magnifiedAreaSize + magnifiedAreaBorder) * aspectRatio)
        {
            color = float4(0.0, 1.0, 1.0, 1.0);
        }
        
        // if the distance from this pixel to mouse is inside the magnified area, enable the magnify effect.
        if (diff.x < magnifiedAreaSize && diff.y < magnifiedAreaSize * aspectRatio)
        {
            color = g_texture.Sample(g_sampler,
            normalizedMousePos + (normalizedPixelPos - normalizedMousePos) * rcp(magnifiedFactor));
        }
        
        return color;
    }
}
