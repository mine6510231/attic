/*
*  Note that SM6 is not supported in the integrated shader compiling functionality in Viusal Studio 2017.
*  In the VS project file, we specify the shaders will be built via a custom build script, CompileShader_SM6.bat.
*  Please refer to CompileShader_SM6.bat to see compiling commands. 
*  
*  You may need to modify the SDK paths in CompileShader_SM6.bat to match the installed SDK. By default the path is pointing to 15063 SDK.
*/

#include "cb.h"

struct PSInput
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD;
};

float4 GetVertexPositionAndTexCoords(uint vertexID)
{
    float u = float((vertexID << 1) & 2);
    float v = float(vertexID & 2);

    float x = u * 2.0 - 1.0;
    float y = v * -2.0 + 1.0;

    return float4(x, y, u, v);
}

// use this to generate grid-like texture
float TexPattern(float2 position)
{
    float scale = 0.13;
    float t = sin(position.x * scale) + cos(position.y * scale);
    float c = smoothstep(0.0, 0.2, t * t);
    
    return c;
}

PSInput VSMain(uint vertexId : SV_VertexID)
{
    PSInput result;

    float4 posTex = GetVertexPositionAndTexCoords(vertexId);
    
    result.position = float4(posTex.x, posTex.y, 0.0, 1.0);
    result.uv = float2(posTex.z, posTex.w);
    
    return result;
}

float4 PSMain(PSInput input) : SV_TARGET
{
    float4 outputColor;

    // Add grid-like texture pattern on top of the color
    float texP = TexPattern(input.position.xy);
    outputColor = texP * float4(input.uv.x, input.uv.y, 0.0, 1.0);

    switch (RenderMode)
    {
        case 0:
        {
            //! Just pass through the color we generate before
            break;
        }
        case 1:
        {
            //! Example of query intrinsics: WaveGetLaneIndex
            //! Gradiently color the wave block by their lane id. Black for the smallest lane id and White for the largest lane id.
            outputColor = WaveGetLaneIndex() / float(LaneSize);
            break;
        }
        case 2:
        {
            //! Example of query intrinsics: WaveIsFirstLane
            if (WaveIsFirstLane())
            {
                outputColor = float4(1.0, 0.0, 0.0, 1.0);
            }
            else
            {
                float t = float(WaveGetLaneIndex()) / float(LaneSize);
                outputColor = float4(t, t, t, 1.0);
            }    
            break;
        }
        case 3:
        {
            //! Example of query intrinsics: WaveActiveMin/Max
            //! Mark first lane to red, lower half of lanes in wave to minimum color, upper half to maximum.
            uint t = WaveGetLaneIndex();
            if (t < (LaneSize >> 1))
            {
                if (FirstLaneInRed && t == 0)
                {
                    outputColor = float4(1.0, 0.0, 0.0, 1.0);
                }
                else
                {
                    outputColor = WaveActiveMin(outputColor);
                }    
            }
            else
            {
                outputColor = WaveActiveMax(outputColor);
            }
            break;
        }
        case 4:
        {
            if (FirstLaneInRed && WaveIsFirstLane())
            {
                outputColor = float4(1.0, 0.0, 0.0, 1.0);
            }
            else
            {
                //! Example of vote intrinsics: WaveActiveBallot
                //! Percentage of black pixel (# of lanes in black / # of total lanes).
                bool isBlack = (outputColor.x + outputColor.y + outputColor.z) < 0.0005;
                uint4 activeLaneMask = WaveActiveBallot(isBlack);
                uint numBlackLanes = countbits(activeLaneMask.x) + countbits(activeLaneMask.y) + countbits(activeLaneMask.z) + countbits(activeLaneMask.w);
                //! The above procedure can be more efficiently implemented as:
                //! uint numBlackLanes = WaveActiveCountBits(hasColor);
                
                //! WaveActiveBallot returns a fixed uint4 bit mask which represents 128 active lanes 
                //! (i.e. Full WGP/SM cooperation mode in recent AMD/NVidia GPUs (RX6000/7000 series, 
                //! RTX30/40 series), where each processor consists 4 CUs/Wraps, each CU/Wrap contains 
                //! 32 lanes, so 128 in total) maximally, so we divide <numBlackLanes> by a constant 128.
                float blackRatio = float(numBlackLanes) / 128.0f;
                outputColor = float4(blackRatio, blackRatio, blackRatio, 1.0);
            }
            break;
        }
        case 5:
        {
            //! Example of wave broadcast intrinsics: WaveReadLaneFirst
            //! Broadcast the color in first lan to the wave.
            if (FirstLaneInRed && WaveIsFirstLane())
            {
                outputColor = float4(1.0, 0.0, 0.0, 1.0);
            }
            else
            {
                outputColor = WaveReadLaneFirst(outputColor);
            }
            break;
        }
        case 6:
        {
            //! Example of wave reduction intrinsics: WaveActiveSum
            //! Paint the wave with the averaged color inside the wave.
            if (FirstLaneInRed && WaveIsFirstLane())
            {
                outputColor = float4(1.0, 0.0, 0.0, 1.0);
            }
            else
            {
                uint4 activeLaneMask = WaveActiveBallot(true);
                uint numActiveLanes = countbits(activeLaneMask.x) + countbits(activeLaneMask.y) + countbits(activeLaneMask.z) + countbits(activeLaneMask.w);
                float4 avgColor = WaveActiveSum(outputColor) / float(numActiveLanes);
                outputColor = avgColor;
            }
            break;
        }
        case 7:
        {
            //! Example of wave scan intrinsics: WavePrefixSum
            //! First, compute the prefix sum of distance each lane to first lane.
            //! Then, use the prefix sum value to color each pixel.
            if (FirstLaneInRed && WaveIsFirstLane())
            {
                outputColor = float4(1.0, 0.0, 0.0, 1.0);
            }
            else
            {
                float4 basePos = WaveReadLaneFirst(input.position);
                float4 prefixSumPos = WavePrefixSum(input.position - basePos);
        
                // Get the number of total active lanes.
                uint4 activeLaneMask = WaveActiveBallot(true);
                uint numActiveLanes = countbits(activeLaneMask.x) + countbits(activeLaneMask.y) + countbits(activeLaneMask.z) + countbits(activeLaneMask.w);
            
                outputColor = prefixSumPos / numActiveLanes;
            }   
            break;
        }
        case 8:
        {
            //! Example of Quad-Wide shuffle intrinsics: QuadReadAcrossX and QuadReadAcrossY
            //! Color pixels based on their quad id:
            //!  q0 -> orange
            //!  q1 -> green
            //!  q2 -> blue
            //!  q3 -> white
            //!
            //!   -------------> x
            //!  |   [0] [1]
            //!  |   [2] [3]
            //!  V
            //!  Y
            //!
            float dx = QuadReadAcrossX(input.position.x)-input.position.x;
            float dy = QuadReadAcrossY(input.position.y)-input.position.y;

            //! q0 (dx/y = q1(<-get from opposite part along X in the quad) - q0)
            if (dx > 0 && dy > 0)
                outputColor = float4(1, 0, 0, 1);
            //! q1 (dx/y = q0 - q1)
            else if (dx <0 && dy > 0)
                outputColor = float4(0, 1, 0, 1);
            //! q2 (dx/y = q3 - q2)
            else if (dx > 0 && dy < 0)
                outputColor = float4(0, 0, 1, 1);
            //! q3 (dx/y = q2 - q3)
            else if (dx < 0 && dy < 0)
                outputColor = float4(1, 1, 1, 1);
            else
            //! set out of boundary access to purple (e.g. last row/column if 
            //! render target resolution is odd number, where q1 and q3 do not exist)
                outputColor = float4(0, 1, 1, 1);
            break;
        }
        default: break;
    }
    
    return outputColor;
}
