#include "pch.h"

#include "WaveTest.h"

//! Precompiled shaders
#include "wave_vs.hlsl.h"
#include "wave_ps.hlsl.h"
#include "magnify_vs.hlsl.h"
#include "magnify_ps.hlsl.h"

#include "imgui.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx12.h"

namespace
{
    constexpr wchar_t WaveTestWindowName[] = L"WaveTest";
}

WaveTest::WaveTest(uint32_t width, uint32_t height, const wchar_t* name)
    : Base(width, height, name)
{
}

WaveTest::~WaveTest()
{
}

void WaveTest::Init()
{
    CreateContext();
    CreatePipeline();
    CreateImGui();
}

void WaveTest::CreateContext()
{
    //! Create device
    Tor::Device::Desc deviceDesc;
    m_device = Tor::Device::Create(deviceDesc, L"DefaultDevice");
    auto* device = m_device->Get();

    //! Check for required feature
    {
        D3D12_FEATURE_DATA_SHADER_MODEL shaderModelSupport = { D3D_SHADER_MODEL_6_0 };
        HCHECK(device->CheckFeatureSupport((D3D12_FEATURE)D3D12_FEATURE_SHADER_MODEL, 
            &shaderModelSupport, sizeof(shaderModelSupport)));

        HCHECK(device->CheckFeatureSupport((D3D12_FEATURE)D3D12_FEATURE_D3D12_OPTIONS1, 
            &m_waveIntrinsicsSupport, sizeof(m_waveIntrinsicsSupport)));
    }
    
    //! Describe and create the command queue.
    D3D12_COMMAND_QUEUE_DESC queueDesc = {};
    queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
    queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

    HCHECK(device->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_commandQueue)));
    m_commandQueue->SetName(L"DefaultCommandQueue");

    Tor::SwapChain::Desc swapChainDesc;
    swapChainDesc.m_bufferCount = FrameCount;
    swapChainDesc.m_width = m_width;
    swapChainDesc.m_height = m_height;
    swapChainDesc.m_format = DXGI_FORMAT_R8G8B8A8_UNORM;
    m_swapChain = Tor::SwapChain::Create(swapChainDesc, 
        m_device->Get(), m_commandQueue.Get(), m_device->GetFactory(), 
        Win32Application::GetHwnd(), L"DefaultSwapChain");

    m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();

    //! Create descriptor heaps.
    {
        D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
        rtvHeapDesc.NumDescriptors = FrameCount + 1;    //! swap chain back buffers + 1 intermediate scene buffer
        rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
        rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE; //! No effect on rtv heap so just none.
        m_rtvHeap = Tor::DescriptorHeap::Create(m_device->Get(), rtvHeapDesc, L"DefaultRTVHeap");

        D3D12_DESCRIPTOR_HEAP_DESC csuHeapDesc = {};
        csuHeapDesc.NumDescriptors = (1 * FrameCount) + 2;  // 1 constant buffer for each frame and then 2 SRV. 
        csuHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
        csuHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
        m_csuHeap = Tor::DescriptorHeap::Create(m_device->Get(), csuHeapDesc, L"DefaultCSUHeap");
    }

    //! Create command list allocator.
    m_commandListAllocator = Tor::CommandListAllocator::Create(m_device->Get(), L"DefaultCommandListAllocator");
}

void WaveTest::CreatePipeline()
{
    //! Get raw DXGI device
    auto* device = m_device->Get();

    //! Create root signatures
    {
        Tor::RootSignatureBuilder builder;
        if (!builder.Init(device))
        {
            WARNING(WaveTestWindowName, L"Failed to initialize root signature builder");
        }

        static constexpr D3D12_ROOT_SIGNATURE_FLAGS flags =
            D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
            D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
            D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;
        builder.SetFlags(flags);

        ComPtr<ID3DBlob> signature = builder.
            AddShaderInputDescriptorTable(D3D12_SHADER_VISIBILITY_ALL)
                ->AddDescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0, 0)
            ->FinalizeDescriptorTable()
            ->Finalize();
        device->CreateRootSignature(0, 
            signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&m_fullScreenPassRootSignature));
        m_fullScreenPassRootSignature->SetName(L"FullScreenPassRootSignature");

        builder.Reset();

        signature = builder.SetFlags(flags)
            ->AddShaderInputDescriptorTable(D3D12_SHADER_VISIBILITY_ALL)
                ->AddDescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0, 0)
            ->FinalizeDescriptorTable()
            ->AddShaderInputDescriptorTable(D3D12_SHADER_VISIBILITY_PIXEL)
                ->AddDescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0)
            ->FinalizeDescriptorTable()
            ->AddStaticSampler(
                D3D12_FILTER_MIN_MAG_MIP_POINT,
                D3D12_TEXTURE_ADDRESS_MODE_WRAP,
                D3D12_TEXTURE_ADDRESS_MODE_WRAP,
                D3D12_TEXTURE_ADDRESS_MODE_WRAP,
                0, 0, 
                D3D12_SHADER_VISIBILITY_PIXEL
            )
            ->Finalize();
        device->CreateRootSignature(0,
            signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&m_magnifyPassRootSignature));
        m_magnifyPassRootSignature->SetName(L"MagnifyPassRootSignature");
    }

    //! Describe and create the graphics pipeline state object (PSO).
    {
        D3D12_GRAPHICS_PIPELINE_STATE_DESC desc = {};
        desc.pRootSignature = m_fullScreenPassRootSignature.Get();
        desc.VS = { g_Wave_VS, sizeof(g_Wave_VS) };
        desc.PS = { g_Wave_PS, sizeof(g_Wave_PS) };
        desc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
        desc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
        desc.DepthStencilState.DepthEnable = FALSE;
        desc.DepthStencilState.StencilEnable = FALSE;
        desc.SampleMask = UINT_MAX;
        desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
        desc.NumRenderTargets = 1;
        desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
        desc.SampleDesc.Count = 1;
        HCHECK(device->CreateGraphicsPipelineState(&desc, IID_PPV_ARGS(&m_fullScreenPassPSO)));
        m_fullScreenPassPSO->SetName(L"FullScreenPassPSO");

        desc.pRootSignature = m_magnifyPassRootSignature.Get();
        desc.VS = { g_Magnify_VS, sizeof(g_Magnify_VS) };
        desc.PS = { g_Magnify_PS, sizeof(g_Magnify_PS) };
        desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
        HCHECK(device->CreateGraphicsPipelineState(&desc, IID_PPV_ARGS(&m_magnifyPassPSO)));
        m_magnifyPassPSO->SetName(L"MagnifyPassPSO");
    }

    //! Create a constant buffer.
    {
        const UINT constantBufferSize = SimpleConstants_Size;

        CD3DX12_HEAP_PROPERTIES cbPropertes{ D3D12_HEAP_TYPE_UPLOAD };
        CD3DX12_RESOURCE_DESC cbDesc = CD3DX12_RESOURCE_DESC::Buffer(constantBufferSize * FrameCount);

        HCHECK(device->CreateCommittedResource(
            &cbPropertes,
            D3D12_HEAP_FLAG_NONE,
            &cbDesc,
            D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS(&m_constantBuffer)));
        
        // Describe and create a constant buffer view.
        D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc = {};
        cbvDesc.BufferLocation = m_constantBuffer->GetGPUVirtualAddress();
        cbvDesc.SizeInBytes = constantBufferSize;

        for (UINT n = 0; n < FrameCount; n++)
        {
            const auto [cbvHandle,_] = m_csuHeap->PushBack();
            device->CreateConstantBufferView(&cbvDesc, cbvHandle);
            cbvDesc.BufferLocation += constantBufferSize;
        }

        //! Map and initialize the constant buffer. We don't unmap this until the
        //! app closes. Keeping things mapped for the lifetime of the resource is okay.
        CD3DX12_RANGE readRange(0, 0);        // We do not intend to read from this resource on the CPU.
        m_constantBuffer->Map(0, &readRange, reinterpret_cast<void**>(&m_pCbSrvDataBegin));
        memcpy(m_pCbSrvDataBegin, &m_constantBufferData, sizeof(m_constantBufferData));
    }

    LoadSizeDependentResources();

    //! Create Fence
    m_fence = Tor::Fence::Create(device);
}

void WaveTest::CreateImGui()
{
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;

    //! Disable layout config file.
    io.IniFilename = nullptr;

    ImGui::StyleColorsDark();

    ImGui_ImplWin32_Init(Win32Application::GetHwnd());
    const auto [fontAtlasCpuHandle, fontAtlasGpuHandle] = m_csuHeap->PushBack();
    ImGui_ImplDX12_Init(m_device->Get(), FrameCount, DXGI_FORMAT_R8G8B8A8_UNORM,
        m_csuHeap->Get(), fontAtlasCpuHandle, fontAtlasGpuHandle);
}

void WaveTest::LoadSizeDependentResources()
{
    auto* device = m_device->Get();

    //! Create frame resources.
    {
        //! Create the texture.
        {
            D3D12_RESOURCE_DESC textureDesc = {};
            textureDesc.MipLevels = 1;
            textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
            textureDesc.Width = m_width;
            textureDesc.Height = m_height;
            textureDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;
            textureDesc.DepthOrArraySize = 1;
            textureDesc.SampleDesc.Count = 1;
            textureDesc.SampleDesc.Quality = 0;
            textureDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;

            const float ccolor[4] = { 0, 0, 0, 0 };
            CD3DX12_CLEAR_VALUE clearValue(DXGI_FORMAT_R8G8B8A8_UNORM, ccolor);

            CD3DX12_HEAP_PROPERTIES properties{ D3D12_HEAP_TYPE_DEFAULT };

            device->CreateCommittedResource(
                &properties,
                D3D12_HEAP_FLAG_NONE,
                &textureDesc,
                D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
                &clearValue,
                IID_PPV_ARGS(&m_fullScreenPassRenderTarget));
            m_fullScreenPassRenderTarget->SetName(L"FullScreenPassRenderTarget");

            //! Create resource views
            const auto [rtvCpuHandle, _1] = m_rtvHeap->PushBack();
            device->CreateRenderTargetView(m_fullScreenPassRenderTarget.Get(), nullptr, rtvCpuHandle);

            const auto [srvCpuHandle, _2] = m_csuHeap->PushBack();
            device->CreateShaderResourceView(m_fullScreenPassRenderTarget.Get(), nullptr, srvCpuHandle);
        }

        m_viewport.Width = static_cast<float>(m_width);
        m_viewport.Height = static_cast<float>(m_height);

        m_scissorRect.left = 0;
        m_scissorRect.top = 0;
        m_scissorRect.right = static_cast<LONG>(m_width);
        m_scissorRect.bottom = static_cast<LONG>(m_height);
    }
}

void WaveTest::Tick()
{
    Simulate();
    Render();
}

void WaveTest::Simulate()
{
    static float time = 0;
    const float aspectRatio = float(m_width) / float(m_height);
    m_constantBufferData.m_orthProjMatrix = XMMatrixTranspose(XMMatrixOrthographicLH(2.0f * aspectRatio, 2.0f, 0.0f, 1.0f));  // Transpose from row-major to col-major, which by default is used in HLSL.
    m_constantBufferData.m_renderingMode = m_mode;
    m_constantBufferData.m_laneSize = m_waveIntrinsicsSupport.WaveLaneCountMin;
    m_constantBufferData.m_time = time;

    ImVec2 mousePos = ImGui::GetMousePos();
    m_constantBufferData.m_mousePosition.x = mousePos.x;
    m_constantBufferData.m_mousePosition.y = mousePos.y;
    m_constantBufferData.m_resolution.x = static_cast<float>(m_width);
    m_constantBufferData.m_resolution.y = static_cast<float>(m_height);
    m_constantBufferData.m_magnify = static_cast<uint32_t>(ImGui::IsMouseDown(ImGuiMouseButton_Right));
    m_constantBufferData.m_firstLaneInRed = static_cast<uint32_t>(m_setFirstLaneInRed);

    memcpy(m_pCbSrvDataBegin + (SimpleConstants_Size * m_frameIndex), &m_constantBufferData, sizeof(m_constantBufferData)); // Copy to the constant buffer for the current frame.
}

void WaveTest::Render()
{
    DrawImGui();
    DrawScene();

    m_swapChain->Present();

    MoveToNextFrame();

    m_commandListAllocator->Collect();
}

void WaveTest::WaitForGpu()
{
    static Tor::Fence::Event GpuSyncEvent { L"WaitForGpu" };
    
    //! Push a sync symbol to the command queue
    m_fence->Signal(m_commandQueue.Get());
    
    //! Wait on CPU for the symbol to be signaled on GPU (i.e. all command lists before it in 
    //! the queue has finished execution)
    m_fence->Wait(GpuSyncEvent);
    
    //! Advance fence value
    m_fence->Increment();
}

void WaveTest::MoveToNextFrame()
{
    size_t currentFenceValue = m_fence->GetPendingValue();
    m_fenceValues[m_frameIndex] = currentFenceValue;
    m_fence->Signal(m_commandQueue.Get());
    m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();
    if (m_fence->GetCompletedValue() < m_fenceValues[m_frameIndex])
    {
        //! Wait if the next frame is not ready to be rendered yet.
        m_fence->Wait();
    }
    m_fenceValues[m_frameIndex] = m_fence->Increment();
}

void WaveTest::Shutdown()
{
    //! Flush GPU pipeline before destruction
    WaitForGpu();
}

void WaveTest::DrawScene()
{
    static constexpr Tor::CommandListAllocator::Queue queue = Tor::CommandListAllocator::Queue::Graphics;

    //! Free last command allocator and roll a new one for this frame.
    m_commandListAllocator->FRoll(queue);
    Tor::CommandList* commandList = m_commandListAllocator->Allocate(queue, L"DefaultCommandList");

    auto* cl = commandList->Get();

    //! Create barrier desc
    D3D12_RESOURCE_BARRIER barrier;
    barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;

    //! Global setup
    {
        cl->RSSetViewports(1, &m_viewport);
        cl->RSSetScissorRects(1, &m_scissorRect);
    }
    
    //! Render Pass 1: Draw a full screen triangle to write color to the intermediate texture.
    {
        static constexpr wchar_t RenderPass1Name[] = L"Full Screen Pass";

        //! Use 1 as first parameter for ASCII input
        cl->BeginEvent(0, RenderPass1Name, sizeof(RenderPass1Name));

        cl->SetPipelineState(m_fullScreenPassPSO.Get());
        cl->SetGraphicsRootSignature(m_fullScreenPassRootSignature.Get());
        ID3D12DescriptorHeap* ppHeaps[] = { m_csuHeap->Get() };
        cl->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);
        cl->SetGraphicsRootDescriptorTable(0, m_csuHeap->GetGPUDescriptorHandleForHeapStart());

        //! [TODO] try enhanced barrier
        barrier.Transition.pResource = m_fullScreenPassRenderTarget.Get();
        barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
        barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
        barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
        cl->ResourceBarrier(1, &barrier);

        const auto [rtv, _] = m_rtvHeap->At(0);
        cl->OMSetRenderTargets(1, &rtv, FALSE, nullptr);

        cl->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
        cl->DrawInstanced(3, 1, 0, 0);

        barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
        barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
        cl->ResourceBarrier(1, &barrier);

        cl->EndEvent();
    }
    
    //! Render Pass 2: Draw full screen triangle to draw input texture and magnified region to back buffer
    {
        static constexpr wchar_t RenderPass2Name[] = L"Magnification Pass";
        cl->BeginEvent(0, RenderPass2Name, sizeof(RenderPass2Name));

        cl->SetPipelineState(m_magnifyPassPSO.Get());
        cl->SetGraphicsRootSignature(m_magnifyPassRootSignature.Get());
        const auto [cpuCbvHandle, gpuCbvHandle] = m_csuHeap->At(m_frameIndex);
        const auto [cpuSrvHandle, gpuSrvHandle] = m_csuHeap->At(FrameCount);
        cl->SetGraphicsRootDescriptorTable(0, gpuCbvHandle);
        cl->SetGraphicsRootDescriptorTable(1, gpuSrvHandle);
        cl->RSSetViewports(1, &m_viewport);
        cl->RSSetScissorRects(1, &m_scissorRect);

        barrier.Transition.pResource = m_swapChain->GetBuffer(m_frameIndex);
        barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
        barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
        cl->ResourceBarrier(1, &barrier);
        D3D12_CPU_DESCRIPTOR_HANDLE cpuRtvHandle = m_swapChain->GetCpuBufferAddress(m_frameIndex);
        cl->OMSetRenderTargets(1, &cpuRtvHandle, FALSE, nullptr);

        const float clearColor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
        cl->ClearRenderTargetView(cpuRtvHandle, clearColor, 0, nullptr);
        cl->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
        cl->DrawInstanced(3, 1, 0, 0);

        cl->EndEvent();
    }
    
    //! Draw UI
    {
        static constexpr wchar_t UIPassName[] = L"ImGui Pass";
        cl->BeginEvent(0, UIPassName, sizeof(UIPassName));

        ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), cl);

        barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
        barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
        cl->ResourceBarrier(1, &barrier);

        cl->EndEvent();
    }

    cl->Close();
    ID3D12CommandList* ppCommandLists[] = { cl };
    m_commandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

    commandList->Free();
}

void WaveTest::DrawImGui()
{
    //! "Flip" imgui frame buffer 
    ImGui_ImplDX12_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();

    ImGui::SetNextWindowSize(ImVec2(250.0f, 310.0f));
    ImGui::Begin("Control");
    {
        ImGui::Text("Frame: %d", ImGui::GetFrameCount());
        ImGui::Text("FPS:   %.1f", ImGui::GetIO().Framerate);

        static const char* ModeNames[] = { 
            "Default", 
            "WaveGetLaneIndex", 
            "WaveIsFirstLane", 
            "WaveActiveMin/Max", 
            "WaveActiveBallot", 
            "WaveReadLaneFirst", 
            "WaveActiveSum", 
            "WavePrefixSum", 
            "QuadReadAcrossX/Y"
        };
        if (ImGui::BeginListBox("Mode", ImVec2(150.0f, 160.0f)))
        {
            for (int n = 0; n < IM_ARRAYSIZE(ModeNames); n++)
            {
                const bool selected = (m_mode == n);
                if (ImGui::Selectable(ModeNames[n], selected))
                {
                    m_mode = n;
                }
                if (selected)
                {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndListBox();
        }

        ImGui::Text("Wave lane count min: %d", m_waveIntrinsicsSupport.WaveLaneCountMin);
        ImGui::Text("Wave lane count max: %d", m_waveIntrinsicsSupport.WaveLaneCountMax);
        ImGui::Text("Total lane count:    %d", m_waveIntrinsicsSupport.TotalLaneCount);

        //! Set first lane of each wave to red to help identifying wave pattern in some modes.
        ImGui::Checkbox("First lane in red", &m_setFirstLaneInRed);
    }
    ImGui::End();

    //! Finalize styling and start rendering
    ImGui::Render();
}