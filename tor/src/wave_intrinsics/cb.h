#pragma once

#ifdef __cplusplus
#include <cstdint>
#include <DirectXMath.h>
using namespace DirectX;
//! Layout of this struct should match with corresponding constant buffer
//! defined below with same name as a replacement of reflection.
struct SimpleConstants
{
    XMMATRIX m_orthProjMatrix;
    XMFLOAT2 m_mousePosition;
    XMFLOAT2 m_resolution;
    float m_time;
    uint32_t m_renderingMode;
    uint32_t m_laneSize;
    uint32_t m_magnify;
    uint32_t m_firstLaneInRed;
    uint32_t m_padding[38]; // Padding so the struct is 256-byte aligned.
};

//! Note due to the legacy byte packing rule of HLSL's constant buffer the size of C++ proxy is not 
//! always same as corresponding constant buffer so please be careful. For more info please refer to:
//! https://github.com/microsoft/DirectXShaderCompiler/wiki/Buffer-Packing
constexpr size_t SimpleConstants_Size = sizeof(SimpleConstants);

#else

cbuffer SimpleConstants : register(b0)
{
    float4x4 OrthProjMatrix;
    float2 MousePosition;
    float2 Resolution;
    float Time;
    uint RenderMode;
    uint LaneSize;
    uint IsMagnify;
    uint FirstLaneInRed;
    uint Padding[38];
}

#endif
